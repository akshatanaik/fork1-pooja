$(document).ready(function(){

		//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		$( ".tabs" ).tabs();
		//bind the validation engine & ajax submit 

		$.validate({
			form : '#pricing',
			onSuccess : function() {
				//alert('hi');
     			pricing();
      			return false; // Will stop the submission of the form
    		},
		});

		
		
});

var pricingCalendar={

	init:function(mode){
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		var url = window.location.href;
		var res = url.split("=");
		var type =  res[res.length-1];
		//alert(type);

		if(mode == 'year'){

			for(i = 0; i <= 11; i++){
				$('#booking_calendar'+i).fullCalendar({
    				contentHeight: 20,
					header: {
						left: '',
						center: 'title',
						right: ''

					},
					weekMode:'liquid',
					selectable: true,
					selectHelper: true,
					height: 600,
					firstDay:1,

					select: function( startDate, endDate, allDay, jsEvent, view ) {
					 	var check = $.fullCalendar.formatDate(startDate,'yyyy-MM-dd');
    					var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
					   
					   
					    //console.log(type);
					    if(check < today)
					    {
					       $.pnotify({title: 'Error', text: "You can not select past dates", opacity: .8, type: 'error'});
							return;
					    }
					    else
					    {
					    	if(type == 'undefined')
					    	{
					       		$.pnotify({title: 'Error', text: "Please choose pricing or shortbreaks.", opacity: .8, type: 'error'});
								return;
					    	}
					    	else if(type == 'shortbreaks')  pricingCalendar.addShortBreaks(startDate,endDate,jsEvent);
					    	else pricingCalendar.addPricing(startDate,endDate,jsEvent);
					        //pricingCalendar.addPricing(startDate,endDate,jsEvent);
					       
					    }
						
					},
					 editable: true,
					 disableDragging:true,
					 events: function(start, end, callback) {

						pricingCalendar.displayPricing(start, end, callback);
						
					},

					//when the user clicks on the event we will show details of the event 
					eventClick: function(calEvent, jsEvent, view) {
						//console.log(calEvent);
						//pricingCalendar.edit_booking(calEvent, jsEvent, view);
					},
					eventMouseover: function(calEvent, jsEvent, view) {
						//need to get popover working here!!

					},
					eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
						//pricingCalendar.update_dates(event,dayDelta,minuteDelta);
					},

					eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
						//here we always deal with start date
						//pricingCalendar.update_dates(event,dayDelta,minuteDelta);
					},

					eventRender: function (event, element, view) {
            			/*if (event.start.getMonth() != view.start.getMonth())
                			return false;*/
        			}
				});

			}

		}
	},

	//next function
		displayPricing : function(start, end, callback){
			//here get the  property id if any
					var sPageURL = window.location.search.substring(1);
					var property_id='';
					var sURLVariables = sPageURL.split('&');
					for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == 'property_id') 
				        {
				            property_id= sParameterName[1];
				        }
				    }


					//get the base url
					var baseurl = $('body').data('baseurl');
				
					$.ajax({
							url: baseurl+'index.php/pricing/get_updated_pricing?property_id='+property_id,
							dataType: 'json',
							data: {
								start: Math.round(start.getTime() / 1000),
								end: Math.round(end.getTime() / 1000),
								response:'json',
															},
							success: function(response) {
								if(response.status=='error'){
									$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
									return;
								}

								var events = [];
								var d = [];

								$(response.response.records).each(function(index,list) {
									
									var price = list.price;
									list.arival_time='12:00:00';
									list.departure_time='12:00:00';
									
									var string1=list.start_date.split("-");
									var string2=list.arival_time.split(":");
									var startdate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);
									
									
									var string1=list.end_date.split("-");
									string2=list.departure_time.split(":");
									var enddate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);

									var weekday = new Array(7);
									weekday[0]=  "Sunday";
									weekday[1] = "Monday";
									weekday[2] = "Tuesday";
									weekday[3] = "Wednesday";
									weekday[4] = "Thursday";
									weekday[5] = "Friday";
									weekday[6] = "Saturday";

									var n = weekday[startdate.getDay()];
									
									if(list.price != 0){
										events.push({
											id:list.id,
											
											title: '$'+list.price,
											start: startdate,
											end: enddate,
											property_id:list.property_id,
											allDay: true
										});
									}
									
								
									if(response.response.shortbreak_price == null){
									}else{	
										if(list.isShortbreak == 1){
												
											var midweek_min_price=Array();
											var weekend_min_price=Array();
											
											if(n == "Monday"){
												
												var ed = startdate.getDate() + 3; 												
												var end_date=new Date(string1[0],string1[1]-1, ed,string2[0], string2[1]);											
												
												$(response.response.shortbreak_price).each(function(index,list_sb_price) {
												 
													midweek_min_price = (price * list_sb_price.midweek_percentage)/100 ;
													weekend_min_price = (price * list_sb_price.weekend_percentage)/100 ;
												 });
												if(response.response.shortbreak_price == ''){
												 	events.push({
														id:list.id,
														title: 'Shortbreak',
														start: startdate,
														end: end_date,
														property_id:list.property_id,
														allDay: true,
														color:'#39B7CD',
													});

													var sd = ed + 1;
													var edd = sd +2;
										
													var st_date=new Date(string1[0],string1[1]-1, sd,string2[0], string2[1]);
													var ed_date=new Date(string1[0],string1[1]-1, edd,string2[0], string2[1]);
													events.push({
														id:list.id,
														title: 'Shortbreak',
														start: st_date,
														end: ed_date,
														property_id:list.property_id,
														allDay: true,
														color:'#39B7CD',
													});
												}else{
													events.push({
														id:list.id,
														title: 'Shortbreak'+' ' +'$'+midweek_min_price,
														start: startdate,
														end: end_date,
														property_id:list.property_id,
														allDay: true,
														color:'#39B7CD',
													});
												
													var sd = ed + 1;															
													var edd = sd +2;
										
													var st_date=new Date(string1[0],string1[1]-1, sd,string2[0], string2[1]);
													var ed_date=new Date(string1[0],string1[1]-1, edd,string2[0], string2[1]);
													events.push({
														id:list.id,
														title: 'Shortbreak'+' ' +'$'+weekend_min_price,
														start: st_date,
														end: ed_date,
														property_id:list.property_id,
														allDay: true,
														color:'#39B7CD',
													});
												}										
											}
										}
										
									}


								});
								
								//var d = [];
								$(response.response.pricing_settings).each(function(index,list_pricing_settings) {
									d= list_pricing_settings;
									var res = d.split("=");
								   	
								   	var month_calendar0=$('#booking_calendar0 #title_month').text();
									var mt0 = month_calendar0.split(" ");
									if(res[0] == 'July' && mt0[0] =='July'){
										
										$("#booking_calendar0 .img").attr("id",mt0[0]);
										if(res[1] == 'Sunday' &&$("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-sun #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' &&$("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-mon #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-tue #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-wed #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-thu #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-fri #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar0 .img").attr('id')==mt0[0]){
											$("#booking_calendar0 .fc-sat #"+mt0[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

								
									var month_calendar1=$('#booking_calendar1 #title_month').text();
									var mt1 = month_calendar1.split(" ");
									if(res[0] == 'August' && mt1[0] =='August'){
										$("#booking_calendar1 .img").attr("id",mt1[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											//alert('hi');
											$("#booking_calendar1 .fc-sun #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-mon #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-tue #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-wed #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-thu #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-fri #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar1 .img").attr('id')==mt1[0]){
											$("#booking_calendar1 .fc-sat #"+mt1[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar2=$('#booking_calendar2 #title_month').text();
									var mt2 = month_calendar2.split(" ");
									if(res[0] == 'September' && mt2[0] =='September'){
										$("#booking_calendar2 .img").attr("id",mt2[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-sun #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-mon #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-tue #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-wed #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-thu #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-fri #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar2 .img").attr('id')==mt2[0]){
											$("#booking_calendar2 .fc-sat #"+mt2[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar3=$('#booking_calendar3 #title_month').text();
									var mt3 = month_calendar3.split(" ");
									if(res[0] == 'October' && mt3[0] =='October'){
										$("#booking_calendar3 .img").attr("id",mt3[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-sun #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-mon #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-tue #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-wed #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-thu #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-fri #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar3 .img").attr('id')==mt3[0]){
											$("#booking_calendar3 .fc-sat #"+mt3[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar4=$('#booking_calendar4 #title_month').text();
									var mt4 = month_calendar4.split(" ");
									if(res[0] == 'November' && mt4[0] =='November'){
										$("#booking_calendar4 .img").attr("id",mt4[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-sun #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-mon #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-tue #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-wed #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-thu #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-fri #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar4 .img").attr('id')==mt4[0]){
											$("#booking_calendar4 .fc-sat #"+mt4[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar5=$('#booking_calendar5 #title_month').text();
									var mt5 = month_calendar5.split(" ");
									if(res[0] == 'December' && mt5[0] =='December'){
										$("#booking_calendar5 .img").attr("id",mt5[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-sun #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-mon #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-tue #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-wed #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-thu #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-fri #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar5 .img").attr('id')==mt5[0]){
											$("#booking_calendar5 .fc-sat #"+mt5[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar2=$('#booking_calendar6 #title_month').text();
									var mt6 = month_calendar2.split(" ");
									if(res[0] == 'January' && mt6[0] =='January'){
										$("#booking_calendar6 .img").attr("id",mt6[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-sun #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-mon #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-tue #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-wed #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-thu #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-fri #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar6 .img").attr('id')==mt6[0]){
											$("#booking_calendar6 .fc-sat #"+mt6[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar3=$('#booking_calendar7 #title_month').text();
									var mt7 = month_calendar3.split(" ");
									if(res[0] == 'February' && mt7[0] =='February'){
										$("#booking_calendar7 .img").attr("id",mt7[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-sun #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-mon #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-tue #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-wed #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-thu #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-fri #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar7 .img").attr('id')==mt7[0]){
											$("#booking_calendar7 .fc-sat #"+mt7[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

										var month_calendar8=$('#booking_calendar8 #title_month').text();
									var mt8 = month_calendar8.split(" ");
									if(res[0] == 'March' && mt8[0] =='March'){
										$("#booking_calendar8 .img").attr("id",mt8[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-sun #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-mon #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-tue #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-wed #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-thu #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-fri #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar8 .img").attr('id')==mt8[0]){
											$("#booking_calendar8 .fc-sat #"+mt8[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

								
									var month_calendar9=$('#booking_calendar9 #title_month').text();
									var mt9 = month_calendar9.split(" ");
									if(res[0] == 'April' && mt9[0] =='April'){
										$("#booking_calendar9 .img").attr("id",mt9[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-sun #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-mon #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-tue #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-wed #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-thu #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-fri #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar9 .img").attr('id')==mt9[0]){
											$("#booking_calendar9 .fc-sat #"+mt9[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar10=$('#booking_calendar10 #title_month').text();
									var mt10 = month_calendar10.split(" ");
									if(res[0] == 'May' && mt10[0] =='May'){
										$("#booking_calendar10 .img").attr("id",mt10[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-sun #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-mon #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-tue #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-wed #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-thu #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-fri #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar10 .img").attr('id')==mt10[0]){
											$("#booking_calendar10 .fc-sat #"+mt10[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}

									var month_calendar11=$('#booking_calendar11 #title_month').text();
									var mt11 = month_calendar11.split(" ");
									if(res[0] == 'June' && mt11[0] =='June'){
										$("#booking_calendar11 .img").attr("id",mt11[0]);
										if(res[1] == 'Sunday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-sun #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Monday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-mon #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Tuesday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-tue #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Wednesday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-wed #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Thursday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-thu #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Friday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-fri #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										if(res[1] == 'Saturday' && $("#booking_calendar11 .img").attr('id')==mt11[0]){
											$("#booking_calendar11 .fc-sat #"+mt11[0]).css('background-image', 'url(' + '../public/app/img/star.png '+ ')');
											return;
										}
										
									}
										


								});
								callback(events);
							}
						});
		},

		addPricing:function(start, end,jsEvent){
				//display the form
				//dunmp the dates:-
				pricingCalendar.resetdata();
			
				$('#start_date').val($.fullCalendar.formatDate( start, 'dd-MM-yyyy' ));
				
				$('#save').show();
				$('#edit').hide();
				$('#fModal1').modal();
				
				
		},

		resetdata:function(){

			$('#start_date').val('');
			$('#end_date').val('');
			$('#price').val('');
			
		},

		save:function(){
			//execute the save action
			var options = {
			        target:'#output2',
			        url:$('#pricing_form').attr('href'),
			        dataType:  'json',
			        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//reload calendar:-
		        			$('#pricing_calendar').fullCalendar('refetchEvents');
		        			$('#fModal1').modal('hide');
		        			window.location.href = window.location.href;
		        		}
			        }// post-submit callback
			   };

			$('#pricing_form').ajaxSubmit(options);
			return false;
		},

		
		update:function(){
			//execute the save action
			var options = {
			        target:'#output2',
			        url:$('#pricing_form').attr('href'),
			        dataType:  'json',
			        success:function(responseText, statusText, xhr, $form) {

			        		var baseurl = $('body').data('baseurl');
			        		if(responseText.status=='error'){
			        				//if error notify that to user

			        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
			        		}else{
			        			//reload calendar:-
			        			$('#pricing_calendar').fullCalendar('refetchEvents');
			        			$('#fModal1').modal('hide');
			        		}
			        }// post-submit callback
			   };

			$('#pricing_form').ajaxSubmit(options);
			return false;
		},

		display_pricing_or_shortbreaks : function(type,property_id){		
				window.location.href= $('body').data('baseurl') +'index.php/pricing?property_id='+property_id+'&type='+type;	
		},


		//=====METHODS FOR SHORTBREAk=========
		displayShortBreaks : function(start, end, callback){
			//here get the  property id if any
					var sPageURL = window.location.search.substring(1);
					var property_id='';
					var sURLVariables = sPageURL.split('&');
					for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == 'property_id') 
				        {
				            property_id= sParameterName[1];
				        }
				    }


					//get the base url
					var baseurl = $('body').data('baseurl');
					//alert(property_id);
					$.ajax({
							url: baseurl+'index.php/pricing/get_shortbreaks?property_id='+property_id,
							dataType: 'json',
							data: {
								start: Math.round(start.getTime() / 1000),
								end: Math.round(end.getTime() / 1000),
								response:'json',
															},
							success: function(response) {
								if(response.status=='error'){
									$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
									return;
								}

								var events = [];

								$(response.response.records).each(function(index,list) {
									list.arival_time='12:00:00';
									list.departure_time='12:00:00';
									
									var string1=list.start_date.split("-");
									var string2=list.arival_time.split(":");
									var startdate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);
									
									
									var string1=list.end_date.split("-");
									string2=list.departure_time.split(":");
									var enddate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);
									
									
									events.push({
										color: 'black',
										id:list.id,
										title: 'Shortbreaks',
										start: startdate,
										end: enddate,
										property_id:list.property_id,
										allDay: true
									});

								});
								callback(events);

							}
						});
		},

		addShortBreaks:function(start, end,jsEvent){
				//display the form
				//dunmp the dates:-
				$('#end_date_sb').val('');
				$('#start_date_sb').val($.fullCalendar.formatDate( start, 'dd-MM-yyyy' ));
				
				$('#save').show();
				$('#edit').hide();
				$('#fModal_sb').modal();
				
				
		},

		saveShortBreaks:function(){
			//execute the save action
			var options = {
			        target:'#output2',
			        url:$('#shortbreak_form').attr('href'),
			        dataType:  'json',
			        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//reload calendar:-
		        			$('#pricing_calendar').fullCalendar('refetchEvents');
		        			$('#fModal_sb').modal('hide');
		        			window.location.href = window.location.href;
		        		}
			        }// post-submit callback
			   };

			$('#shortbreak_form').ajaxSubmit(options);
			return false;
		},

};

//rerender back to the form
function pricing(form){
	
	var options = { 
		target:'#output2',
	    url:$('#pricing').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){
				 				
			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
				$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
				
				window.location.href =baseurl+ 'index.php/pricing?property_id='+response.property_id+'&type=undefined';

		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
				
		},
			
		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}

