$(document).ready(function(){

		//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		//bind the validation engine & ajax submit 

		$.validate({
			form : '#login_form',
			onSuccess : function() {
     			login.authenticate();
      			return false; // Will stop the submission of the form
    		},
		});

		//bind validation for forgot password
		$.validate({
			form : '#forgot_password_form',
			onSuccess : function() {
     			login.forgot_password();
      			return false; // Will stop the submission of the form
    		},
		});
});

var login={
		authenticate:function(){
			var options = {
		        target:'#output2',
		        url:$('#login_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {
		        		
		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user
		        				//console.log('response'+responseText.response);
		        				//document.getElementById('error-div').innerHTML ='nvjdfgj';
		        				document.getElementById('err-div').innerHTML =responseText.response;
		        				//$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			/*var page = $('#returl').val();
		        			window.location.href =baseurl+ "index.php/"+page;*/
		        			//console.log(baseurl+"index.php/dashboard");
		        			window.location.href = baseurl+"index.php/dashboard";
		        		}
		        }  // post-submit callback
		    };
		    
		    $('#login_form').ajaxSubmit(options);
		    return false;
		},

		forgot_password:function(){
			
			var options = {
		        target:'#output2',
		        url:$('#forgot_password_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {
		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user
		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			$.pnotify({title: 'Success', text: responseText.response, opacity: .8, type: 'success'});
		        			//close the  modal
		        			//alert('baseurl');alert(baseurl);
		        			window.location.href =baseurl+ 'index.php/login';

		        		}

		        }  // post-submit callback

		    };

		    $('#forgot_password_form').ajaxSubmit(options);
		    return false;
		},

		resetdata:function(){
			$('#inputEmail').val('');  //
			$('#inputPassword').val('');
		}


}

function calLoginModal(){
	login.resetdata();

	$('#loginModal').modal();
}

