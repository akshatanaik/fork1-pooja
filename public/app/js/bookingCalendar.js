/**
 *this js files contaims all utils needed  for booking calendar
 * 
 */
  var events1 = [];
var bookingCalendar={
		init:function(mode){
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();

			if(mode == 'month'){
				$('#booking_calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,agendaDay'
					},
					 selectable: true,
					 selectHelper: true,
					 height: 600,
					 select: function( startDate, endDate, allDay, jsEvent, view ) {
					 	var check = $.fullCalendar.formatDate(startDate,'yyyy-MM-dd');
    					var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
    					
    					/* It assigns selected end date to Departure Date - on Select*/
    					$("#inputDeparture_date").empty();
    					document.getElementById("inputDeparture_date").disabled = false;
    					var combo1 = document.getElementById("inputDeparture_date");
		     	 		var option1 = document.createElement("option");
	    		 		option1.text = $.fullCalendar.formatDate(endDate,'dd-MM-yyyy');
	    		 		option1.value = $.fullCalendar.formatDate(endDate,'dd-MM-yyyy');
	    		 		combo1.add(option1);
                         /* ----- */
					  
					    if(check < today)
					    {
					       $.pnotify({title: 'Error', text: "You can not book for past dates", opacity: .8, type: 'error'});
							return;
					    }
					    else
					    {
					        bookingCalendar.addBooking(startDate,endDate,jsEvent);
					    }
						
					},
					 editable: true,
					 disableDragging:true,
					 events: function(start, end, callback) {

						bookingCalendar.displayBookings(start, end, callback);


					},

					//when the user clicks on the event we will show details of the event 
					eventClick: function(calEvent, jsEvent, view) {
						//console.log(calEvent);
						//bookingCalendar.edit_booking(calEvent, jsEvent, view);
						var myNode = document.getElementById("property_name");
							while (myNode.firstChild) {
							    myNode.removeChild(myNode.firstChild);
							}
							/*end*/

							var start_date=calEvent.start;
							var end_date=calEvent.end;
							
							//get the base url
							var baseurl = $('body').data('baseurl');
							$.ajax({
									url: baseurl+'index.php/calendar/get_bookings',
									dataType: 'json',
									data: {
										start: Math.round(start_date.getTime() / 1000),
										end: Math.round(end_date.getTime() / 1000),
										response:'json',
									},
									
									success: function(response) {
										if(response.status=='error'){
											$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
											return;
										}

										//var property = Array();
										$(response.response.records).each(function(index,list) {
											var length=list.length;
												//alert(length);
												for(var i=0;i<length;i++){
													arrival_date=list[i][0].arrival_date;
													departure_date=list[i][0].departure_date;
													property_id=list[i][0].property_id;
													property= list[i][0].property_name;
													var iDiv = document.createElement('div');
													iDiv.id = 'block';
													iDiv.className = 'span3';
													iDiv.innerHTML = "Property Name";  
													document.getElementById('property_name').appendChild(iDiv);

													var iDiv1 = document.createElement('div');
													iDiv1.id = 'block1';
													iDiv1.className = 'span8';
													iDiv1.innerHTML = '<a href="javascript:void(0);" onclick="bookingCalendar.eventClick(arrival_date,departure_date,this.innerHTML);">'+property+'</a>';

													var input = document.createElement('input');
													input.id = property;
													input.type = 'hidden';
													input.value = property_id;
													document.getElementById('property_name').appendChild(input);
													document.getElementById('property_name').appendChild(iDiv1);
													//index++;
												}
																		
										});							
										
										$('#bModal').modal();
									}
								});
					},
					

					eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
						bookingCalendar.update_dates(event,dayDelta,minuteDelta);
					},

					eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
						//here we always deal with start date
						bookingCalendar.update_dates(event,dayDelta,minuteDelta);
					},
				});

			}else if(mode == 'year'){

				for(i = 0; i <= 11; i++){
					$('#booking_calendar'+i).fullCalendar({
	    				contentHeight: 20,
						header: {
							left: 'prev,next today',
							center: 'title',
							right: 'month,agendaWeek,agendaDay'
						},
						 selectable: true,
						 selectHelper: true,
						 height: 600,
						 select: function( startDate, endDate, allDay, jsEvent, view ) {
						 	var check = $.fullCalendar.formatDate(startDate,'yyyy-MM-dd');
	    					var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
						    
						    if(check < today)
						    {
						       $.pnotify({title: 'Error', text: "You can not book for past dates", opacity: .8, type: 'error'});
								return;
						    }
						    else
						    {
						        bookingCalendar.addBooking(startDate,endDate,jsEvent);
						    }
							
						},
						 editable: true,
						 disableDragging:true,
						 events: function(start, end, callback) {

							bookingCalendar.displayBookings(start, end, callback);
							
						},
						
						//when the user clicks on the event we will show details of the event 
						eventClick: function(calEvent, jsEvent, view) {
							//console.log(calEvent);
							//alert('calEvent--before');
							//bookingCalendar.edit_booking(calEvent, jsEvent, view);
							var myNode = document.getElementById("property_name");
							while (myNode.firstChild) {
							    myNode.removeChild(myNode.firstChild);
							}
							/*end*/

							var start_date=calEvent.start;
							var end_date=calEvent.end;

							
							//get the base url
							var baseurl = $('body').data('baseurl');
							$.ajax({
									url: baseurl+'index.php/calendar/get_bookings',
									dataType: 'json',
									data: {
										start: Math.round(start_date.getTime() / 1000),
										end: Math.round(end_date.getTime() / 1000),
										response:'json',
									},
									
									success: function(response) {
										if(response.status=='error'){
											$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
											return;
										}

										//var i=0;
										//var property = Array();
										$(response.response.records).each(function(index,list) {												
												var length=list.length;
												//alert(length);
												for(var i=0;i<length;i++){
													arrival_date=list[i][0].arrival_date;
													departure_date=list[i][0].departure_date;
													property_id=list[i][0].property_id;
													property= list[i][0].property_name;
													var iDiv = document.createElement('div');
													iDiv.id = 'block';
													iDiv.className = 'span3';
													iDiv.innerHTML = "Property Name";  
													document.getElementById('property_name').appendChild(iDiv);

													var iDiv1 = document.createElement('div');
													iDiv1.id = 'block1';
													iDiv1.className = 'span8';
													iDiv1.innerHTML = '<a href="javascript:void(0);" onclick="bookingCalendar.eventClick(arrival_date,departure_date,this.innerHTML);">'+property+'</a>';

													var input = document.createElement('input');
													input.id = property;
													input.type = 'hidden';
													input.value = property_id;
													document.getElementById('property_name').appendChild(input);
													document.getElementById('property_name').appendChild(iDiv1);
													//index++;
												}
																					
																		
										});																
										
										$('#bModal').modal();
									}
								});
						},							
						
					
						eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
							bookingCalendar.update_dates(event,dayDelta,minuteDelta);
						},

						eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
							//here we always deal with start date
							bookingCalendar.update_dates(event,dayDelta,minuteDelta);
						},
					});

				}

			}else{

				$('#booking_calendar_week').fullCalendar({
					header: {
					     left   : 'prev,next',
					     center : 'title',
					     right  : 'agendaDay',
					    },
					defaultView: 'basicWeek',
					selectable: true,
					selectHelper: true,
					height: 1000,
					disableDragging:true,
					select: function( startDate, endDate, allDay, jsEvent, view ) {
					 	if($('#role').val() == "housekeeper"){
							$('#fModal').hide();
						}else{
							var check = $.fullCalendar.formatDate(startDate,'yyyy-MM-dd');
	    					var today = $.fullCalendar.formatDate(new Date(),'yyyy-MM-dd');
						    
						    if(check < today)
						    {
						       $.pnotify({title: 'Error', text: "You can not book for past dates", opacity: .8, type: 'error'});
								return;
						    }
						    else
						    {
						        bookingCalendar.addBooking(startDate,endDate,jsEvent);
						    }
						}
					},
					 editable: true,
					 events: function(start, end, callback) {

						bookingCalendar.displayBookingsWeek(start, end, callback);


					},

					//when the user clicks on the event we will show details of the event 
					eventClick: function(calEvent, jsEvent, view) {
						//console.log(calEvent);
						//bookingCalendar.edit_booking(calEvent, jsEvent, view);
						var myNode = document.getElementById("property_name");
							while (myNode.firstChild) {
							    myNode.removeChild(myNode.firstChild);
							}
							/*end*/

							var start_date=calEvent.start;
							var end_date=calEvent.end;
							
							//get the base url
							var baseurl = $('body').data('baseurl');
							$.ajax({
									url: baseurl+'index.php/calendar/get_bookings',
									dataType: 'json',
									data: {
										start: Math.round(start_date.getTime() / 1000),
										end: Math.round(end_date.getTime() / 1000),
										response:'json',
									},
									
									success: function(response) {
										if(response.status=='error'){
											$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
											return;
										}

										//var property = Array();
										$(response.response.records).each(function(index,list) {
											var length=list.length;
												//alert(response.response.records);
												for(var i=0;i<length;i++){

													arrival_date=list[i][0].arrival_date;
													departure_date=list[i][0].departure_date;
													property_id=list[i][0].property_id;
													property= list[i][0].property_name;
													var iDiv = document.createElement('div');
													iDiv.id = 'block';
													iDiv.className = 'span3';
													iDiv.style.height="150px";
													iDiv.innerHTML = "Property Name";  
													document.getElementById('property_name').appendChild(iDiv);

													var iDiv1 = document.createElement('div');
													iDiv1.id = 'block1';
													iDiv1.className = 'span8';
													iDiv1.style.height="150px";
													iDiv1.innerHTML = '<a href="javascript:void(0);" onclick="bookingCalendar.eventClick(arrival_date,departure_date,this.innerHTML);">'+property+'</a>';

													var input = document.createElement('input');
													input.id = property;
													input.type = 'hidden';
													input.style.height="150px";
													input.value = property_id;
													document.getElementById('property_name').appendChild(input);
													document.getElementById('property_name').appendChild(iDiv1);
													//index++;
												}
																		
										});							
										
										$('#bModal').modal();
									}
								});
					},
					
					
					eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
						bookingCalendar.update_dates(event,dayDelta,minuteDelta);
					},

					eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
						//here we always deal with start date
						bookingCalendar.update_dates(event,dayDelta,minuteDelta);
					},		

				});
			}
		},

		//next function
		displayBookings : function(start, end, callback){


			//here get the  property id if any
					var sPageURL = window.location.search.substring(1);
					var property_id='';
					var sURLVariables = sPageURL.split('&');
					for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == 'property_id') 
				        {
				            property_id= sParameterName[1];
				        }
				    }


					//get the base url
					var baseurl = $('body').data('baseurl');
					$.ajax({
							url: baseurl+'index.php/calendar/get_bookings?property_id='+property_id,
							dataType: 'json',
							data: {
								start: Math.round(start.getTime() / 1000),
								end: Math.round(end.getTime() / 1000),
								response:'json',
															},
							success: function(response) {

								if(response.status=='error'){
									$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
									return;
								}

								var events = [];
							
								$(response.response.records).each(function(index,list) {
                                       
									var string1=list.arrival_date.split("-");
									var string2=list.arival_time.split(":");
									var startdate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);

									string1=list.departure_date.split("-");
									string2=list.departure_time.split(":");
									var enddate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);

									if(index!=0 || index==0){
										events.push({
											id:list.id,
											title:list.property_name,
											start: startdate,
											end: enddate,											
											starttime:list.arival_time,
											endtime:list.departure_time,
											nights: list.nights,
											status: list.status,
											cost: list.cost,
											adults: list.adults,
											childrens: list.childrens,
											infants: list.infants,
											customer:list.customer_name,
											property_id:list.property_id,											
											allDay: true
											
										});
									}
								});
                              
								callback(events);
								/* To display events in particular row*/
                                var str1,str2,str3;
                                var map_Properties_top = [];
                                var j=0;

								$('div.head').each(function () {
									/* This will fetch all properties and their css-top(div) values */
									str1 = $(this).text().trim();
									str2 = $(this).offset().top;
									map_Properties_top.push({id:str1,top:str2});
								});
								  
								
									var all_events = $('div.fc-event span.fc-event-title');
									$(all_events).each(function () 
									{
										/* This will take all events and assigns css-top(div-event) = css-top(div)*/
										str3 = $(this).text().trim();
										for(i=0;i<map_Properties_top.length;i++){

								  		if(map_Properties_top[i].id.trim() ==  str3)
								  		{
								  			var top_px = (parseInt(map_Properties_top[i].top)-180)+"px";
								  			$('div.fc-event')[j].style.top = top_px;
								  			//console.log($('div.fc-event')[j].style.top + k);
								  		}
								  				
								  	}	
												 
									 j++;	
								});
									 
									
							}
						});
		},		
		displayBookingsWeek : function(start, end, callback){


			//here get the  property id if any
					var sPageURL = window.location.search.substring(1);
					var property_id='';
					var sURLVariables = sPageURL.split('&');
					for (var i = 0; i < sURLVariables.length; i++) 
				    {
				        
				        var sParameterName = sURLVariables[i].split('=');
				        if (sParameterName[0] == 'property_id') 
				        {
				            property_id= sParameterName[1];
				        }
				    }


					//get the base url
					var baseurl = $('body').data('baseurl');
					
					$.ajax({
							url: baseurl+'index.php/calendar/get_bookings?property_id='+property_id,
							dataType: 'json',
							data: {
								start: Math.round(start.getTime() / 1000),
								end: Math.round(end.getTime() / 1000),
								response:'json',
															},
							success: function(response) {

								if(response.status=='error'){
									$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
									return;
								}

								//var events = [];

								$(response.response.records).each(function(index,list) {
                                      
									var string1=list.arrival_date.split("-");
									var string2=list.arival_time.split(":");
									var startdate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);
									
									
									string1=list.departure_date.split("-");
									string2=list.departure_time.split(":");
									//var a = parseInt(string2[0]) + 1;
									//string2[0] = a;
								   var enddate=new Date(string1[0],string1[1]-1, string1[2],string2[0], string2[1]);
								//	var d1 = new Date(enddate);
								//	enddate = new Date(d1.getTime() + 86400000);
								//	enddate = new Date(d1.getTime() + 86400000);
								//	while(startdate.valueOf() != enddate.valueOf()){
										//alert(startdate);
										if(index!=0 || index==0){
											events1.push({
												id:list.id,
												title:list.property_name,
												start: startdate,
												end: enddate,											
												starttime:list.arival_time,
												endtime:list.departure_time,
												nights: list.nights,
												status: list.status,
												cost: list.cost,
												adults: list.adults,
												childrens: list.childrens,
												infants: list.infants,
												customer:list.customer_name,
												property_id:list.property_id,											
												allDay: true
												
											});
										} 
								//	var d = new Date(startdate);
								//	startdate = new Date(d.getTime() + 86400000);
									
							//	} 
								});
                             	
								callback(events1);
								/* To display events in particular row*/
                                var str1,str2,str3;
                                var map_Properties_top = [];
                                var j=0;

								$('div.head').each(function () {
									/* This will fetch all properties and their css-top(div) values */
									str1 = $(this).text().trim();
									str2 = $(this).offset().top;
									map_Properties_top.push({id:str1,top:str2});
								});
								  
								
									var all_events = $('div.fc-event span.fc-event-title');
									$(all_events).each(function () 
									{
										/* This will take all events and assigns css-top(div-event) = css-top(div)*/
										str3 = $(this).text().trim();
										for(i=0;i<map_Properties_top.length;i++){

								  		if(map_Properties_top[i].id.trim() ==  str3)
								  		{
								  			var top_px = (parseInt(map_Properties_top[i].top)-180)+"px";
								  			$('div.fc-event')[j].style.top = top_px;
								  			//console.log($('div.fc-event')[j].style.top + k);
								  		}
								  				
								  	}	
												 
									 j++;	
								});
									 
									
							}
						});
		},		

		addBooking:function(start, end,jsEvent){
				//display the form
				//dunmp the dates:-
				bookingCalendar.resetdata();
			
				$('#inputArival_date').val($.fullCalendar.formatDate( start, 'dd-MM-yyyy' ));
				$('#inputDeparture_date').val($.fullCalendar.formatDate( end, 'dd-MM-yyyy' ));
				var len=booking.daysOfStay(start,end);
				
				//if no of days is less than 3 ,throw error

				var weekday = new Array(7);
				weekday[0]=  "Sunday";
				weekday[1] = "Monday";
				weekday[2] = "Tuesday";
				weekday[3] = "Wednesday";
				weekday[4] = "Thursday";
				weekday[5] = "Friday";
				weekday[6] = "Saturday";

				var n = weekday[start.getDay()];
				
				if(n == "Monday"){
					if(len == 7){
						$('#inputNights').val(len);
						$('#save').show();
						$('#edit').hide();
						$('#fModal').modal();
					}else{									
						
						$.pnotify({title: 'Error', text: 'Please select minimum 7 days', opacity: .8, type: 'error'});
					}
				}else if(n == "Friday"){
					
					if(len == 7){
									
						$('#inputNights').val(len);
						$('#save').show();
						$('#edit').hide();
						$('#fModal').modal();

					}else{						

						$.pnotify({title: 'Error', text: 'Please select minimum 7 days', opacity: .8, type: 'error'});
					}
				}else{
					$.pnotify({title: 'Error', text: 'Please book the cottage either from Monday or from Friday', opacity: .8, type: 'error'});
				} 
		},

		availability_property_wise : function(property_id){
			if(property_id!='' && property_id!=undefined){
				window.location.href= $('body').data('baseurl') +'index.php/calendar?type=month&property_id='+property_id;
			}
		},
		
		pricing:function(property_id){
			if(property_id!='' && property_id!=undefined){
				var baseurl = $('body').data('baseurl');
					var start_date=document.getElementById('inputArival_date').value;
					var startDate=start_date.split("-").reverse().join("-");
					var end_date=document.getElementById('inputDeparture_date').value;
					var endDate=end_date.split("-").reverse().join("-");
					//get the price for given start date and end date
					$.ajax({
						url: baseurl+'index.php/calendar/get_pricing?property_id='+property_id+'&&start_date='+startDate+'&&end_date='+endDate,
						dataType: 'json',
						data: {
							response:'json',
						},
						success: function(response) {
							$(response.response.records).each(function(index,list) {
								var price =list.price;	

								if(price==0){
									$.pnotify({title: 'Error', text: 'Price is not set for this property. Please contact the administrator', opacity: .8, type: 'error'});
									$('#fModal').hide();
								}else{
									if(list.isShortbreak == 1){
										$(response.response.shortbreak).each(function(index,list) {
										
											if(price < list.min_price){
												$.pnotify({title: 'Error', text: 'No booking can ever be taken below this amount.', opacity: .8, type: 'error'});
												$('#fModal').hide();
											}else{
												var date = new Date(startDate);
												
												var weekday = new Array(7);
												weekday[0]=  "Sunday";
												weekday[1] = "Monday";
												weekday[2] = "Tuesday";
												weekday[3] = "Wednesday";
												weekday[4] = "Thursday";
												weekday[5] = "Friday";
												weekday[6] = "Saturday";

												var n = weekday[date.getDay()];

												if(n == "Monday"){
													var midweek_percentage=list.midweek_percentage;	
													var result = (midweek_percentage / 100) * price;
													$('#inputCost').val(result);	
												}else if(n == "Friday"){
													var weekend_percentage=list.weekend_percentage;
													var result = (weekend_percentage / 100) * price;
													$('#inputCost').val(result);		
												}
											}												
										});
									}else{
										$('#inputCost').val(price);	
									}
								}			
						
							});
						}
					});
			}
		},

		resetdata:function(){

			$('#inputArival_date').val('');
			$('#inputDeparture_date').val('');
			$('#inputNights').val('');
			$('#inputAdults').val('');
			$('#children').val('');
			$('#inputInfants').val('');
			$('#inputCustomer_name').val('');
			$('#inputProperty_name').val('');
			$('#inputCost').val('');
			$('#id').val('');
			$('#inputStatus').val('');
			$('#inputArival_time').val('');
			$('#inputDeparture_time').val('');
		},

		edit_booking:function(calEvent, jsEvent, view){
				//display the form
				//dunmp the dates:-
				//alert('hiii');
				$('#inputArival_date').val($.fullCalendar.formatDate( calEvent.start, 'dd-MM-yyyy' ));
				$('#inputDeparture_date').val($.fullCalendar.formatDate( calEvent.end, 'dd-MM-yyyy' ));
				$('#inputNights').val(calEvent.nights);
				$('#inputAdults').val(calEvent.adults);
				$('#inputChildrens').val(calEvent.childrens);
				$('#inputCustomer_name').val(calEvent.customer);
				$('#inputProperty_name').val(calEvent.property_id);
				$('#id').val(calEvent.id);
				//hide the save
				$('#save').hide();
				$('#edit').show();
				$('#bModal').modal('hide');
				$('#fModal').modal();
				
		},

		save:function(){
			//execute the save action
			var options = {
			        target:'#output2',
			        url:$('#booking_form').attr('href'),
			        dataType:  'json',
			        success:function(responseText, statusText, xhr, $form) {

			        		var baseurl = $('body').data('baseurl');
			        		if(responseText.status=='error'){
			        				//if error notify that to user

			        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
			        		}else{
			        			//reload calendar:-
			        			$('#booking_calendar').fullCalendar('refetchEvents');
			        			$('#fModal').modal('hide');
			        			window.location.href=window.location.href;
			        		}
			        }// post-submit callback
			   };

			$('#booking_form').ajaxSubmit(options);
			return false;
		},

		
		update:function(){
			var id=document.getElementById('inputProperty_name').value;
			//alert(id);
			document.getElementById('id').value=id;
			//execute the save action
			var options = {
			        target:'#output2',
			        url:$('#booking_form').attr('href'),
			        dataType:  'json',
			        success:function(responseText, statusText, xhr, $form) {

			        		var baseurl = $('body').data('baseurl');
			        		if(responseText.status=='error'){
			        				//if error notify that to user

			        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
			        		}else{
			        			//reload calendar:-
			        			$('#booking_calendar').fullCalendar('refetchEvents');
			        			$('#fModal').modal('hide');
			        		}
			        }// post-submit callback
			   };

			$('#booking_form').ajaxSubmit(options);
			return false;
		},

		update_dates:function(event,daydelta,minuteDelta){
			//here update the event & fetch it again:-
			$.ajax({
				url: $('body').data('baseurl')+'index.php/bookings/update_dates',
				dataType: 'json',
				type: "POST",
				data: {
					inputArival_date:$.fullCalendar.formatDate( event.start, 'dd-MM-yyyy' ),
					inputDeparture_date: $.fullCalendar.formatDate( event.end, 'dd-MM-yyyy' ),
					inputProperty_name:event.property_id,
					id:event.id,
				},
				success: function(responseText) {
					if(responseText.status=='error'){
			        	//if error notify that to user
			        	$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
			        	$('#booking_calendar').fullCalendar('refetchEvents');
			        }else{
			        			//reload calendar:-
			        	$('#booking_calendar').fullCalendar('refetchEvents');
			        }
				}
			});

		},

		showBookingDetails:function(calevent, jsEvent, view){
				//build a tooltip to show the info

		},
		eventClick: function(arrival_date,departure_date,property) {						
			$('#save').hide();
			$('#edit').show();

			var property_id=document.getElementById(property).value;			

			/*reset the booked property list*/
			var myNode = document.getElementById("property_name");
			while (myNode.firstChild) {
			    myNode.removeChild(myNode.firstChild);
			}
			/*end*/			

			$('#bModal').modal('hide');	

			$.ajax({
				url: $('body').data('baseurl')+'index.php/calendar/get_booked_property_bookingdetails?property_id='+property_id+'&&start='+arrival_date+'&&end='+departure_date,
				dataType: 'json',
				type: "POST",
				data: {
					response:'json',
				},
				success: function(response) {
					if(response.status=='error'){
			        	//if error notify that to user
			        	$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
			        	
			        }else{
			        	$(response.response.records).each(function(index,list) {
			        		//var arrival_date=list.arrival_date;
							
							//Formatting date 
							var d = new Date(list.arrival_date); //var curr_month = '\'d.getMonth()'.'; //Months are zero based
							
							var curr_date = d.getDate();
								var fd = curr_date.toString().length;
								if(fd == 1) var date = '0'+ curr_date;
								else  var date = curr_date;
							
							var curr_month = d.getMonth() + 1; //Months are zero based
								var f = curr_month.toString().length;
								if(f == 1) var month = '0'+ curr_month;
								else  var month = curr_month;
							
							var curr_year = d.getFullYear();
							var arrival_date = date + "-" + month + "-" + curr_year;
							
							var d2 = new Date(list.departure_date);
							
							var curr_dat = d2.getDate();
								var fd2 = curr_dat.toString().length;
								if(fd2 == 1) var date2 = '0'+ curr_dat;
								else  var date2 = curr_dat;
							
							var curr_mont = d2.getMonth() + 1; //Months are zero based
								var f2 = curr_mont.toString().length;
								if(f2 == 1) var month2 = '0'+ curr_mont;
								else  var month2 = curr_mont;
							
							var curr_yea = d2.getFullYear();
							var departure_date = date2 + "-" + month2 + "-" + curr_yea;
							  
				        	$('#inputArival_date').val(arrival_date);
							$('#inputDeparture_date').val(departure_date);
							$('#inputNights').val(list.nights);
							$('#inputAdults').val(list.adults);
							$('#inputChildrens').val(list.children);
							$('#inputInfants').val(list.infants);
							$('#inputCustomer_name').val(list.customer_name);
							$('#inputProperty_name').val(list.property_id);	
							$('#inputArival_time').val(list.arival_time);
							$('#inputDeparture_time').val(list.departure_time);	
							$('#inputStatus').val(list.status);
							$('#inputCost').val(list.cost);	
				        	
				        	$('#fModal').modal();
				        });
				       
			        }
				}
			});	
		},

		
};

