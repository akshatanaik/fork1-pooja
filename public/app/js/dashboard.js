
var  dashboard={
		init_admin:function(){
			//init the calendar
			dashboard.init_calendar();

			//load the stats & other information async
			dashboard.render_bookings_per_property_chart();
			dashboard.staff_web_booking_stats();
		},

		init_housekeeper:function(){
			//init the calendar
			dashboard.init_calendar();

			//load the stats & other information async
			dashboard.render_bookings_per_property_chart();
			dashboard.staff_web_booking_stats();
			
		},

		init_calendar:function(){
			//init the calendar
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();

			$('#booking_calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek'
				},
				 selectable: true,
				 selectHelper: true,
				 height: 400,
				 events: function(start, end, callback) {
					bookingCalendar.displayBookings(start, end, callback);
				},

			});
		},

		render_bookings_per_property_chart:function(){
			var baseurl = $('body').data('baseurl');
			$.get(baseurl+'index.php/dashboard/get_propertywise_booking_stats', function(data) { 
					//console.log(JSON.parse(data)); 
					var stats = [];

					$.each( JSON.parse(data), function( key, value ) {
					 	stats[key] = { label: value.property_name, data: parseInt(value.total_bookings) };
					});

					$.plot($("#chart-3"), stats, 
					{
							series: {
								pie: { show: true }
							},
							legend: { show: false }
					});

				}
			);
		},

		staff_web_booking_stats:function(){
			var baseurl = $('body').data('baseurl');
			$.get(baseurl+'index.php/dashboard/get_staff_web_bookings_stats', function(data) { 
				
					console.log(JSON.parse(data)); 
					var d1 = [];var d2 = [];
					var i=0;
					var j=0;
					var stack = 0, bars = true, lines = false, steps = false;
					$.each( JSON.parse(data), function( key, value ) {
					 	if(value.isWebBooking==0){
					 		d1.push([value.day, parseInt(value.total_bookings)]);
					 		i++;
					 	}else{
					 		d2.push([value.day, parseInt(value.total_bookings)]);
					 		j++;
					 	}
					});
					console.log(d1);
					$.plot($("#chart-2"), [ { data: d1, label: "Staff Bookings" }, { data: d2, label: "Web Bookings" } ], {
			            series: {
			                stack: stack,
			                lines: { show: lines, fill: true, steps: steps },
			                bars: { show: bars, barWidth: 0.9 }
			            }
       				});

				}
			);
		},

};