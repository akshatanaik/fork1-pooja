 $(document).ready(function(){

            report.init();

        });

var report={
	filter:function(){
		//execute the save action
		var options = {
		        target:'#output2',
		        url:$('#report_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/reports/pricing_report';
		        		}
		        }  // post-submit callback
		   };

		$('#report_form').ajaxSubmit(options);
		return false;
	},

	init:function(){
		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();
		
		//bind the validation engine & ajax submit 
		$( "#start_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
		$( "#end_date" ).datepicker({ dateFormat: 'dd-mm-yy' });		
		
		/*$.validate({
			form : '#report_form',			
		});*/


	},	

	commission:function(property_id){
		console.log('property id='+property_id);
		if(property_id!='' && property_id!=undefined){
			var baseurl = $('body')	.data('baseurl');
			$.ajax({
				url:baseurl+'index.php/commissions?propertyid='+property_id,
				dataType:'json',
				data:{response:'json'},
				success:function(response){

				}

			});
		}
	}
};

