 $(document).ready(function(){

            booking.init();

        });

var booking={
	save:function(){
		//execute the save action
		var options = {
		        target:'#output2',
		        url:$('#booking_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/bookings';
		        		}
		        }  // post-submit callback
		   };

		$('#booking_form').ajaxSubmit(options);
		return false;
	},

	init:function(){
		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();
		
		//bind the validation engine & ajax submit 
		$( "#inputArival_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
		$( "#inputDeparture_date" ).datepicker({ dateFormat: 'dd-mm-yy' });
		$('#inputArival_time').timepicker();
		$('#inputDeparture_time').timepicker();
		$.validate({
			form : '#booking_form',
			onSuccess : function() {
     			booking.save();
      			return false; // Will stop the submission of the form
    		},
		});
	},
	/**
	 * [daysOfStay compues the length of the stay]
	 * @return {integer
	 */
	daysOfStay:function(start,end){
		var date1 = new Date(start);
		var date2 = new Date(end);
		//Get 1 day in milliseconds
		var one_day=1000*60*60*24;
		
		  // Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();

		  // Calculate the difference in milliseconds
		var difference_ms = date2_ms - date1_ms;
		    
		 // Convert back to days and return
		 return Math.round(difference_ms/one_day); 
	},

	delete :function(id){
		$( "#dialog-confirm" ).dialog({
			  resizable: false,
			  height:160,
			  modal: true,
			  buttons: {
				"Delete": function() {
					var baseurl=$('body').data('baseurl');
					$.get(baseurl+'index.php/bookings/delete/',{'id':id},function(response){
					  	if(response.status=='success'){	
							window.location.href =baseurl+ 'index.php/bookings/';	
						}
					},'json');
					$( this ).dialog( "close" );
				},
				Cancel: function() {
				  $( this ).dialog( "close" );
				}
			  }
		});
	},

	// Added to get the next 7 days from arrival date in drop down list - while new booking.
	getNextday:function(){
		 console.log('being called');
		 $("#inputDeparture_date").empty();
	     var arrival_date = $('#inputArival_date').val();

	     arrival_date_new = arrival_date.split("-");
	     var d = new Date(arrival_date_new[2], arrival_date_new[1] - 1, arrival_date_new[0]);

		var temp = d.toString("MMM").split(' ');
		var display_Msg = '1' + " Nights " + temp[0] + " " + d.getDate() + " " +  temp[1] + " " + d.getFullYear();
			
		d.setTime((d.getTime() + 86400 * 1000*1)); 
		var date = (d.getDate() < 10 ? '0' : '') + d.getDate();
	    var month = parseInt(d.getMonth())+1;
	    var newDate = date+"-"+(month < 10 ? '0' : '') + month+"-"+d.getFullYear();
				    
	    arrival_date = newDate;
			// alert(arrival_date);
	    for(i=1;i<=6;i++) 
		{
	     	arrival_date_new = arrival_date.split("-");
	     	d = new Date(arrival_date_new[2], arrival_date_new[1] - 1, arrival_date_new[0]);
			
			temp = d.toString("MMM").split(' ');
			
			display_Msg = (i) + " Nights " + temp[0] + " " + temp[2] + " " +  temp[1] + " " + d.getFullYear();
			//alert(display_Msg);
			d.setTime((d.getTime() + 86400 * 1000*1)); 
			date = (d.getDate() < 10 ? '0' : '') + d.getDate();
	     	month = parseInt(d.getMonth())+1;
	      
			 newDate = date+"-"+(month < 10 ? '0' : '') + month+"-"+d.getFullYear();
				    
	     	 
	     	 combo = document.getElementById("inputDeparture_date");
	     	 option = document.createElement("option");
    		 option.text = display_Msg;
    		 option.value = arrival_date;
    		 combo.add(option);

    		 arrival_date = newDate;

			//alert(arrival_date);
    		 document.getElementById("inputDeparture_date").disabled = false;
    	}
	},

};

