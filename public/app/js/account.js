// JavaScript Documentsave:function(){
	
	$(document).ready(function(){
         
			
			//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		$( ".tabs" ).tabs();
		//bind the validation engine & ajax submit 
	
});

		//execute the save action
var account={
	save:function(){
		var options = {
		        target:'#output2',
		        url:$('#account_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/bookings';
		        		}
		        }  // post-submit callback
		   };

	$('#account_form').ajaxSubmit(options);
		return false;
	},
	update:function(){
		
		//execute the save action
		var options = {
		        target:'#output2',
		        url:$('#usr_registration').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
							$.pnotify({title: 'Success', text: responseText.response, opacity: .8, type: 'success'});
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/dashboard';
		        		}
		        }  // post-submit callback
		   };

		$('#usr_registration').ajaxSubmit(options);
		return false;
	}
};

