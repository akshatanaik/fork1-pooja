$(document).ready(function(){
            register.init();

});

var register={

	save:function(){
		
		var baseurl = $('body').data('baseurl');
		//execute the save action
		var options = {
		        target:'#output2',
		        url:baseurl+'index.php/registration/save',
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {
		        		if(responseText.status=='error')
						{
							document.getElementById('error-div').innerHTML =responseText.response;
		        			
		        			//if error notify that to user
		        			//$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/dashboard';
		        		}
		        }  // post-submit callback
		   };

		$('#user_registration').ajaxSubmit(options);
		return false;
	},

	init:function(){
		var $messages = $('#error-message-wrapper');
		$.validate({
			modules : 'security',
			errorMessagePosition : $messages,
			form : '#user_registration',
			onModulesLoaded : function() {
		      $('input[name="password_confirmation"]').displayPasswordStrength();
		    },
			onSuccess : function() {
				$messages.hide();
     			register.save();
      			return false; // Will stop the submission of the form
    		},
    		onError:function(){
    			$messages.show();
    		}
		});
		if($("#user_registration").length > 0){
	        $('#user_registration').stepy({
	            titleClick:true,
	           
	        });
	    }
	},
	
	resetdata:function(){
		$('#firstname').val('');
		$('#lastname').val('');
		$('#email').val('');
		$('#password_confirmation').val('');
		$('#password').val('');
	},
	
	dismiss:function(){
		//alert('hi');
		window.location.href = window.location.href;
	}

};

function calModal(){
	register.resetdata();
	$('#loginModal').hide();

	$('#regModal').modal();
	
	/*$('#save').hide();
	$('#edit').hide();
	$('.btn btn-error').hide();*/
}


