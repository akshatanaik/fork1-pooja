
$(document).ready(function(){

		//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		$( ".tabs" ).tabs();
		//bind the validation engine & ajax submit 

		$.validate({
			form : '#essentials',
			onSuccess : function() {
     			propertydetails();
      			return false; // Will stop the submission of the form
    		},
		});

		$.validate({
			form : '#facilities',
			onSuccess : function() {
     			propertyfeatures();
      			return false; // Will stop the submission of the form
    		},
		});

		$.validate({
			form : '#suitability',
			onSuccess : function() {
     			propertysuitablity();
      			return false; // Will stop the submission of the form
    		},
		});

		//bind validation for forgot password
		$.validate({
			form : '#location',
			onSuccess : function() {
     			locationdetails();
      			return false; // Will stop the submission of the form
    		},
		});
});


//rerender back to the form
function propertydetails(form){
	
	var options = { 
		target:'#output2',
	    url:$('#essentials').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){
				 				
			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
				$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
				
				window.location.href =baseurl+ 'index.php/property/propertyList';

		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
				
		},
			
		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}


//rerender back to the form
function propertyfeatures(form){
	
	var options = { 
		target:'#output2',
	    url:$('#facilities').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){

			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
			$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
			window.location.href =baseurl+ "index.php/"+"/property/propertyList";
		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
		},
		dataType:  'json',       
		clearForm : false,
	};
	$(form).ajaxSubmit(options);
	return false; 
}

//rerender back to the form
function propertysuitablity(form){
	var options = { 
		target:'#output2',
	    url:$('#suitability').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){

			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});

				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
		
			$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
			window.location.href =baseurl+ "index.php/"+"/property/propertyList";

		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
		},
		dataType:  'json',       
		clearForm : false,
	};
	$(form).ajaxSubmit(options);
	return false; 
}

//validates location details form
function locationdetails(form){
	
	var options = { 
		target:'#output2',
	    url:$('#location').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){
			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
			$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
			window.location.href =baseurl+ "index.php/property/propertyList";
		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
				
		},
			
		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}


//validates location details form
function bookingdetails(form){
	
	var options = { 
		beforeSubmit: bfrsubmit_fun,
		success: function(response){
			if(response.status=='error'){
				$('#errormsg').css('visibility','visible');
				var response ='<div class="message error" style="text-align:center;"><p><h4>Error!</h4>'+response.response+'</p> </div>';
				document.getElementById('save_details').style.disabled="false;"
				$('#errormsg').html(response);
				return;
			}

			if(response.status=='success'){
				$('#errormsg').css('visibility','hidden');
				$('#successmsg').css('visibility','visible');
				var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
				$('#successmsg').html(response_msg);
			}

		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
		},

		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}

//delete property from listview.
function deleteProperty(id){
	$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Delete": function() {
				var baseurl=$('body').data('baseurl');
			  $.get(baseurl+'index.php/property/delete/',{'id':id},function(response)
			  {
			  	if(response.status=='success'){	
					window.location.href =baseurl+ 'index.php/property/propertyList';	
				}
			  },'json');	
			  $( this ).dialog( "close" );
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}










