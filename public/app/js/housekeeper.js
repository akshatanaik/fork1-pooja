$(document).ready(function(){

		//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		$( ".tabs" ).tabs();
		//bind the validation engine & ajax submit 

		
		$.validate({
			form : '#personal_details',
			onSuccess : function() {
     			housekeeper();
      			return false; // Will stop the submission of the form
    		},
		});
		
});


//delete housekeeper from listview.
/*function deleteRecord(id){
	//alert('hi');
	$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Delete": function() {
			  var baseurl=$('body').data('baseurl');
			  $.get(baseurl+'index.php/housekeeper/delete/',{'id':id},function(response)
			  { 	
			  	if(response.status=='success'){	
					window.location.href =baseurl+ 'index.php/housekeeper/';	
				}									   
			  },'json');	
			  $( this ).dialog( "close" );
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}*/

//rerender back to the form
function housekeeper(form){
	
	var options = { 
		target:'#output2',
	    url:$('#personal_details').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){
				 				
			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
		
			$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
			window.location.href =baseurl+ 'index.php/housekeeper';										
						
		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
				
		},
			
		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}

function deactivate(id){
	$( "#deacitive-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Yes": function() {
				var baseurl=$('body').data('baseurl');
				$.get(baseurl+'index.php/housekeeper/deactivate_housekeeper/',{'id':id},function(response){
				  	if(response.status=='success'){	
						window.location.href =baseurl+ 'index.php/housekeeper';	
					}
				},'json');
				$( this ).dialog( "close" );
			},
			No: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}


function activate(id){
	$( "#acitive-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Yes": function() {
				var baseurl=$('body').data('baseurl');
				$.get(baseurl+'index.php/housekeeper/activate_housekeeper/',{'id':id},function(response){
				  	if(response.status=='success'){	
						window.location.href =baseurl+ 'index.php/housekeeper';	
					}
				},'json');
				$( this ).dialog( "close" );
			},
			No: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}

function reset_password(id,email){
	$( "#reset-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Yes": function() {
				var baseurl=$('body').data('baseurl');
				$.get(baseurl+'index.php/housekeeper/reset_password',{'id':id,'email':email},function(response){
				  	if(response.status=='success'){	
				  		var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
		
						$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
						window.location.href =baseurl+ 'index.php/housekeeper';	
					}
				},'json');
				$( this ).dialog( "close" );
			},
			No: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}