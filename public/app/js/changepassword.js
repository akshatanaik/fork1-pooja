$(document).ready(function(){

		//initialize the tooltips needed
		$(".tip").tooltip({placement: 'top', trigger: 'hover'});

		//initialize  the uniform library for the form
		$(".row-form,.row-fluid,.dialog,.loginBox,.block,.block-fluid").find("input:checkbox, input:radio, input:file").not(".skip, input.ibtn").uniform();

		$( ".tabs" ).tabs();
		//bind the validation engine & ajax submit 

		
		$.validate({
			form : '#changePassword_form',
			onSuccess : function() {
     			changepassword();
      			return false; // Will stop the submission of the form
    		},
		});
		
});


//rerender back to the form
function changepassword(form){
	
	var options = { 
		target:'#output2',
	    url:$('#changePassword_form').attr('href'),
		dataType:  'json',
		success: function(response,statusText, xhr, $form){
				 				
			if(response.status=='error'){
				$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
				
				return;
			}
			var baseurl = $('body').data('baseurl');
			var response_msg ='<div class="message success" style="text-align:center;"><p><h4>Success!</h4>'+response.response+'</p> </div>';
		
			$.pnotify({title: 'Success', text: response.response, opacity: .8, type: 'success'});
			window.location.href =baseurl+'index.php/login/' ;					
						
		},  // post-submit callback 
		error : function (XMLHttpRequest, textStatus, errorThrown){
			alert(XMLHttpRequest.responseText);
				
		},
			
		dataType:  'json',       
		clearForm : false,
	};
	
	$(form).ajaxSubmit(options);
	return false; 
}