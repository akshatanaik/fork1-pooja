	function set_commision(property_id){
	//set_commision:function(property_id){
		console.log('property_id is'+property_id);
		var baseurl = $('body').data('baseurl');
		/*$.get(baseurl+'index.php/commissions/getBookingsOfProperty/',{'property_id':property_id},function(response){

		},'json');*/

		$.ajax({
			url: baseurl+'index.php/commissions/getBookingsOfProperty?property_id='+property_id,
			dataType: 'json',
			data: {
				response:'json',
			},
			success: function(response) {
				if(response.status=='error'){
					$.pnotify({title: 'Error', text: response.response, opacity: .8, type: 'error'});
					return;
				}
				
				if(response.response.netBookingValue == ''){
					alert('No response');
				}

				document.getElementById('bookingVal').value = response.response.netBookingValue;
				
			}
		});
	}

	function save(){
		console.log('saving..');
			//execute the save action
		var options = {
		        target:'#output2',
		        url:$('#commissions_form').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/commissions';
		        		}
		        }  // post-submit callback
		   };

		$('#commissions_form').ajaxSubmit(options);
		return false;
	}