$(document).ready(function(){
            users.init();

});

var users={
	save:function(){
		alert('in');
		//execute the save action
		var options = {
		        target:'#output2',
		        url:$('#user_registration').attr('href'),
		        dataType:  'json',
		        success:function(responseText, statusText, xhr, $form) {

		        		var baseurl = $('body').data('baseurl');
		        		if(responseText.status=='error'){
		        				//if error notify that to user

		        				$.pnotify({title: 'Error', text: responseText.response, opacity: .8, type: 'error'});
		        		}else{
		        			//go back to list page:-
		        			window.location.href=$('body').data('baseurl')+'index.php/registration/success';
		        		}
		        }  // post-submit callback
		   };

		$('#user_registration').ajaxSubmit(options);
		return false;
	},

	init:function(){
		var $messages = $('#error-message-wrapper');
		$.validate({
			modules : 'security',
			errorMessagePosition : $messages,
			form : '#user_registration',
			onModulesLoaded : function() {
		      $('input[name="password_confirmation"]').displayPasswordStrength();
		    },
			onSuccess : function() {
				$messages.hide();
     			users.save();
      			return false; // Will stop the submission of the form
    		},
    		onError:function(){
    			$messages.show();
    		}
		});
		if($("#user_registration").length > 0){
	        $('#user_registration').stepy({
	            titleClick:true,
	            back: function(index) {

	            },
	            next: function(index) {

	            },
	            finish: function(index) {

	            }
	        });
	    }
	},

};

 function deactivate(id){
	$( "#deacitive-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Yes": function() {
				var baseurl=$('body').data('baseurl');
				$.get(baseurl+'index.php/users/deactivate_user/',{'id':id},function(response){
				  	if(response.status=='success'){	
						window.location.href =baseurl+ 'index.php/users';	
					}
				},'json');
				$( this ).dialog( "close" );
			},
			No: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}


function reset_password(id,email){
	$( "#reset-confirm" ).dialog({
		  resizable: false,
		  height:160,
		  modal: true,
		  buttons: {
			"Yes": function() {
				var baseurl=$('body').data('baseurl');
				$.get(baseurl+'index.php/users/reset_password',{'id':id,'email':email},function(response){
				  	if(response.status=='success'){	
						window.location.href =baseurl+ 'index.php/users';	
					}
				},'json');
				$( this ).dialog( "close" );
			},
			No: function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}


