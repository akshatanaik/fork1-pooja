<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/

/**
* Permissions class dealing with all the permissions  for users
*
*/
class permissions {

	var $CI;

	function permissions()
	{
		$CI =& get_instance();
	}

	public function has_permissions($permission_name=''){
		//if the logged in user is admin  he has permissions for everything
		if($CI->sessions->getsessiondata('role')=='admin') return true;
	}
}