<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class pricing_m extends CI_Model {
		//declare and assign variable.	
		private $module ='basic_pricing';
		private $module1 ='property_price_settings';
		private $module2 ='pricing_shortbreaks';
		
	
		public function get_pricing($id){
			$query = $this->db->select('*')->from('basic_pricing')->where('property_id',$id)->get();
			
    		return $query->result_array();
		}	

		public function save_pricing($start_date,$end_date,$price,$id){

		    $where = "property_id='".$id."'";
			$where.= " AND start_date >= '".$start_date."' AND end_date <= '".$end_date."'";
			$data['price']= $price;
		   	//print_r($where);	
		   	//print_r($data);		
		    $update_sql=$this->db->update_string($this->module,$data,$where);
		    //print_r($update_sql);
		  
		    if(!$result=$this->db->query($update_sql))
		    {
			  throw new Exception($this->db->_error_message());
			  return FALSE;
		    }
	
		    $id=$this->db->insert_id();
	
		    return $id;


			
		}

		public function list_pricing(){
			$query = $this->db->select('*')->from('basic_pricing')->get();
			
    		return $query->result_array();
		}	

		public function list_pricing_id($id){
			$query = $this->db->select('*')->from('basic_pricing')->where('property_id',$id)->get();
			
    		return $query->result_array();
		}	
	

		public function save_shortbreak_upd($start_date,$end_date,$id){

		   $where = "property_id='".$id."'";
			$where.= " AND start_date >= '".$start_date."' AND end_date <= '".$end_date."'";
			$data['isShortbreak']= 1;
		   	//print_r($where);	
		   	//print_r($data);		
		    $update_sql=$this->db->update_string($this->module,$data,$where);
		    //print_r($update_sql);
		  
		    if(!$result=$this->db->query($update_sql))
		    {
			  throw new Exception($this->db->_error_message());
			  return FALSE;
		    }
			//print_r($result);
		    $id=$this->db->insert_id();
	
		    return $id;


			
		}	

		public function filter_pricing($start,$end,$id){

			$query='Select * from '.$this->module.' WHERE start_date = "'.$start.'" AND property_id="'.$id.'"';
			
			if(!$result=$this->db->query($query))
			{
				throw new Exception($this->db->_error_message());
				return FALSE;
			}

			return $result->row_array();
		}

		public function filter_pricing_for_reports($start,$end,$id){
			if($id != '' || !empty($id)){
				$query=$this->db->query('Select * from '.$this->module.' WHERE start_date = "'.$start.'" AND end_date<= "'.$end.'" AND property_id="'.$id.'"');
			}else{
				$query=$this->db->query('Select * from '.$this->module.' WHERE start_date = "'.$start.'" AND end_date<= "'.$end.'"');
			}
			
			/*if(!$result=$this->db->query($query))
			{
				throw new Exception($this->db->_error_message());
				return FALSE;
			}

			return $result->row_array();*/
			return $query;
		}

		public function update($arrival_date,$departure_date){
			$this->db->where('start_date =', $arrival_date);
			//$this->db->where('end_date =', $departure_date); 

   			 return $this->db->update("basic_pricing", array('status' => 'Booked'));
			
		}	
		
		/*
		save the property details data into db.
	*/
	public function savePricingDetail($data=array())
	{
		if(empty($data)){
			//oops
		}		
		
		foreach ($data as $row)	{

			$result = array(
		        'start_date' =>$row['start_date'],
		        'end_date' =>$row['end_date'],
		        'price' =>$row['price'],
		        'status' =>$row['status'],
		        'property_id' =>$row['property_id']
		    );
		    
		    $this->db->insert('basic_pricing', $result);
		}
		
		$id=$this->db->insert_id();
		
		return $id;
	}

	public function save_price_settings($data=array())
	{
		if(empty($data)){
			//oops
		}		
		
	
		$this->db->insert('property_price_settings', $data);
		
		
		$id=$this->db->insert_id();
		
		return $id;
	}	

	public function list_of_pricing($id)
	{
		$query = $this->db->select('*')->from('property_price_settings')->where('property_id',$id)->get();
			
    	return $query->result_array();
	}

	/*public function get_pricing_settings($id,$month){
		$query = $this->db->select('*')->from('property_price_settings')->where('month',$month)->where('property_id',$id)->get();	
    	return $query->result_array();
	}*/

	public function list_of_shortbreak($id)
	{
		$query = $this->db->select('*')->from('pricing_shortbreaks')->where('property_id',$id)->get();
			
    	return $query->result_array();
	}

	public function save_shortbreaks($data=array())
	{
		if(empty($data)){
			//oops
		}		
		
	
		$this->db->insert('pricing_shortbreaks', $data);
		
		
		$id=$this->db->insert_id();
		
		return $id;
	}	

	public function update_shortbreaks($data=array(),$where)
	{
		$update_sql=$this->db->update_string($this->module2,$data,$where);
		 
		 if(!$result=$this->db->query($update_sql))
		 {
			throw new Exception($this->db->_error_message());
			return FALSE;
		 }
	
		 $id=$this->db->insert_id();
	
		 return $id;
	}

	public function update_pricing_settings($data=array(),$where)
	{
		$update_sql=$this->db->update_string($this->module1,$data,$where);
		 
		 if(!$result=$this->db->query($update_sql))
		 {
			throw new Exception($this->db->_error_message());
			return FALSE;
		 }
	
		 $id=$this->db->insert_id();
	
		 return $id;
	}

	public function delete($id='')
	{
		try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$delete_sql="DELETE FROM ".$this->module1." WHERE property_id='".$id."'";
			
			if(!$result=$this->db->query($delete_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
			
	 }


	/**
	 *  get list of pricing between custom dates
	*/
	public function filter_pricing_by_date_property($date1,$date2,$property_id){
		

		$where =' start_date >= "'.$date1.'" AND start_date <="'.$date2.'"';

		if($property_id!=null && $property_id!=''){
			$where.=' AND basic_pricing.property_id='.$property_id.'';
			//$where.=' AND basic_pricing.price!=0';
		}

		try{

			$query = 'Select * from basic_pricing WHERE '.$where.'';
			//print_r($query);
			$result = $this->db->query($query);

			return $result->result_array();
		}catch(Exception $e){
			throw $e;
		}
	}

	/**
	 *  get list of get_shortbreaks between custom dates
	*/
	public function get_shortbreaks_price($property_id){


		if($property_id!=null && $property_id!=''){
			$where = 'pricing_shortbreaks.property_id='.$property_id.'';
		}

		try{

			$query = 'Select * from pricing_shortbreaks WHERE '.$where.'';
			//print_r($query);
			$result = $this->db->query($query);
			//print_r($result->result_array());
			return $result->result_array();
		}catch(Exception $e){
			throw $e;
		}
	}

	/*public function get_cleaning_days($property_id){


		if($property_id!=null && $property_id!=''){
			$where = 'property_price_settings.property_id='.$property_id.'';
		}

		try{

			$query = 'Select * from property_price_settings WHERE '.$where.'';
			//print_r($query);
			$result = $this->db->query($query);
			//print_r($result->result_array());
			return $result->result_array();
		}catch(Exception $e){
			throw $e;
		}
	}*/

	public function get_pricing_settings($id){
		$month = array('January','February','March','April','May','June','July','August','September','October','November','December');
		$cleaning_days = array();
		for($i = 0; $i < sizeof($month); $i++){
			//print_r($month[$i]);
			$query = $this->db->select('*')->from('property_price_settings')->where('month',$month[$i])->where('property_id',$id)->get();
			$dys = $query->result_array();
			if(!(empty($dys))){
				//print_r($dys[0]['month']);
				if($dys[0]['month'] == 'January'){
					
					if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
					


				}
				if($dys[0]['month'] == 'February'){
					
						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 


				}
				if($dys[0]['month'] == 'March'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
					

				}
				if($dys[0]['month'] == 'April'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 

				}
				if($dys[0]['month'] == 'May'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
					

				}
				if($dys[0]['month'] == 'June'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
				}
				if($dys[0]['month'] == 'July'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
					
				}
				if($dys[0]['month'] == 'August'){

					if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
					

				}
				if($dys[0]['month'] == 'September'){

					if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
				}
				if($dys[0]['month'] == 'October'){

						if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
				}
				if($dys[0]['month'] == 'November'){

					if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 

				}
				if($dys[0]['month'] == 'December'){

					if($dys[0]['sun'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Sunday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['mon'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Monday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['tues'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Tuesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['wed'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Wednesday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['thur'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Thursday";					
						//print_r($cleaning_days);
					} 
					if($dys[0]['fri'] == 1){										
						$cleaning_days[] = $dys[0]['month'] . "=Friday";
						//print_r($cleaning_days);
					} 
					if($dys[0]['sat'] == 1){
						$cleaning_days[] = $dys[0]['month'] . "=Saturday";
						//print_r($cleaning_days);
					} 
				}
			
			}
		
		}
		return $cleaning_days;
		
    	
	}



	

		
}
	