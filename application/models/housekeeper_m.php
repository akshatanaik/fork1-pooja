<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class housekeeper_m extends CI_Model {
	private $module ='users';
	private $module1 ='user_permissions';
	private $module2 ='housekeeper_relation';


	/*
		save the housekepeers info into the database.
	*/
	public function save_housekeeper_details($data=array())
	{
		if(empty($data)){
			//oops
		}
		
		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
		//now insert
		//build the insert sql			
		$insert_sql=$this->db->insert_string($this->module,$data);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		$id=$this->db->insert_id();
		
		return $id;
	}

	/*
		save the housekepeers permissions info into the database.
	*/
	public function save_permissions($data=array())
	{
		if(empty($data)){
			//oops
		}
		
		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
		//now insert
		//build the insert sql			
		$insert_sql=$this->db->insert_string($this->module1,$data);
		//var_dump($insert_sql);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		$id=$this->db->insert_id();
		
		return $id;
	}

	/*
		Update the housekeeper account.
	*/	
	public function update_permissions($data=array())
	{
		$delete_sql="DELETE FROM ".$this->module1." WHERE user_id='".$data['user_id']."'";
		$result=$this->db->query($delete_sql);
	}

	/*
		Update the housekeeper account.
	*/	
	public function update($data=array(),$where='')
	{
		  
		if($where=='') 
			$where = "id='".$data['id']."'"; 
    
		
		$update_sql=$this->db->update_string($this->module,$data,$where);
		  
		if(!$result=$this->db->query($update_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
	
		$id=$this->db->insert_id();

	
		return $id;
		
	}


	/*
		User where role is housekeeoer
	*/
	function  list_of_housekeepers(){
		$query="SELECT * FROM ".$this->module." where role='housekeeper'";
		$result=$this->db->query($query);
		return $result->result_array();
	}

	public function getHousekeeper($id=''){
		if($id==''){
			//error
		}

		$query='Select * from '.$this->module.' WHERE Id=\''.$id.'\' LIMIT 1';
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		return $result->row_array();
	}


	public function getHousekeeperProperties($id=''){
		if($id==''){
			//error
		}

		$query="Select * from housekeeper_relation WHERE user_id =".$id;
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		return $result->result_array();
	}
	
	public function get_user_permissions($id=''){
		try{
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$query='Select * from '.$this->module1.' WHERE user_id=\''.$id.'\' ';
			$result=$this->db->query($query);

			return $result->result_array();
		}catch(Exception $e){
			throw $e;
			return false;
		}
	}

	/*public function delete($id='')
	{
		try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$delete_sql="DELETE FROM ".$this->module." WHERE id='".$id."'";
			
			if(!$result=$this->db->query($delete_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
			
	 }*/

	 public function deactivate_housekeeper($id='')
	{
		try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$update_sql="Update ".$this->module." set isactive = 0 WHERE id='".$id."'";
			
			if(!$result=$this->db->query($update_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
			
	 }

	  public function activate_housekeeper($id='')
	{
		try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$update_sql="Update ".$this->module." set isactive = 1 WHERE id='".$id."'";
			
			if(!$result=$this->db->query($update_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
			
	 }

	 public function reset_password($id='',$password)
	{
	 	try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$update_sql="Update ".$this->module." set password ='".$password."'  WHERE id='".$id."'";
			
			if(!$result=$this->db->query($update_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}

			//to get firstname and password
			$select_sql='SELECT * FROM '.$this->module .' where id='.$id.'';
			$res=$this->db->query($select_sql);
			
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
		
		return $res->result_array();
	 }

	 function  list_of_inactive_housekeepers(){
		$query="SELECT * FROM ".$this->module." where role='housekeeper' and isactive=0 AND owner_id=".$this->sessions->getsessiondata('user_id')."";
		$result=$this->db->query($query);
		return $result->result_array();
	}

	function  list_of_active_housekeepers(){
		$query="SELECT * FROM ".$this->module." where role='housekeeper' and isactive=1 AND owner_id=".$this->sessions->getsessiondata('user_id')."";
		$result=$this->db->query($query);
		return $result->result_array();
	}
	
	public function save_housekeeper_relation($data=array())

	{
	
		if(empty($data)){
		//oops
		}

		
		$insert_sql=$this->db->insert_string($this->module2,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}
		
		$id=$this->db->insert_id();
		return $id;

	}

	public function delete_housekeeper_relation($id=array())

	{
	
		if(empty($id)){
		//oops
		}
	
	
		$delete_sql="DELETE FROM ".$this->module2." WHERE user_id='".$id."'";

		if(!$result1=$this->db->query($delete_sql)){

				throw new Exception($this->db->_error_message());

				return FALSE;
			} 
		
	

	}


	
}