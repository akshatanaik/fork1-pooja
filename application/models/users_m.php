<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class users_m extends CI_Model {
	private $module ='users';

	public function get_user_by_username($username){
		if($username=='')return null;

		$query='SELECT * FROM '.$this->module.' WHERE username="'.$username.'" ';

		$result=$this->db->query($query);

		if(!$result)
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		//now check :-
		if($result->num_rows() == 0)
		{
			return null;
		}

		$user = $result->row_array();
		return $user;
	}

	public function verify_login($username,$password){
		//this method check to see for a valid login
		if(trim($username)=='' || trim($password)=='')
		{
			//error both are required fields
			throw new Exception('Username/Password is missing');
			return false;
		}

		$query='SELECT * FROM '.$this->module.' WHERE username="'.$username.'"  AND isactive=1 ';
		$result=$this->db->query($query);

		if(!$result)
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		//now check :-
		if($result->num_rows() == 0)
		{
			//invalid username :- we need to throw a system exception instead of database error
			throw new Exception('Invalid username provided');
			return false;
		}


		$user = $result->row_array();

		//create a login history:-
		$this->load->helper('network');
		
		$login_history['user_id']=$user['id'];
		$login_history['ipaddress']=ip_address();
		$login_history['user_agent']=user_agent();
		$login_history['comment']=' Successfull login';

		if($user['role'] == 'housekeeper') $passowrd = $password;
		else $password = md5($password);
		//compare password
		if($user['password'] != $password)
		{
			//invalid password 
			$login_history['comment']=' Invalid password';

			//save the login history 
			$this->db->query($this->db->insert_string('login_history', $login_history));

			//return from this point-we need to throw a system exception instead of database error
			throw new Exception('Invalid password provided');
			return false;
		}

		//set the required data in sessions so that they can be used later
		$this->sessions->setsessiondata(array('logged_in'=>TRUE,'isadmin'=>$user['isadmin'],'role'=>$user['role'],'user_id'=>$user['id']));

		$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider']));

		//later set this permissions:
		if($user['role']=='housekeeper'){
			$permissions = $this->get_user_permissions();
			$this->sessions->setsessiondata(array('permissions'=>$permissions));
		}

		//success full login return true;
		//create login i=history record
		$this->db->query($this->db->insert_string('login_history', $login_history));
		return true;
	}

	/*
	get user records  based on facebook id

	*/
	function verify_login_facebook($id=''){
		//this method check to see for a valid login
		if(trim($id)=='')
		{
			//error both are required fields
			throw new Exception('Facebook id is missing');
			return false;
		}
		$this->load->helper('network');
		$query='SELECT * FROM '.$this->module.' WHERE facebook_user_id="'.$id.'"  AND isactive=1 ';
		$result=$this->db->query($query);

		if(!$result)
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		$login_history['ipaddress']=ip_address();
		$login_history['user_agent']=user_agent();
		$login_history['comment']='Successfull login';
		
		//now check :-
		if($result->num_rows() == 0)
		{
			//invalid username :- we need to throw a system exception instead of database error
			$login_history['comment']=' Invalid facebook login/user inactive..tried by facebook user with id '.$id;
			$this->db->query($this->db->insert_string('login_history', $login_history));
			throw new Exception('Invalid facebook login');
			return false;
		}
		$user = $result->row_array();

		//create a login history:-
		$this->load->helper('network');

		$login_history['user_id']=$user['id'];

		//set the required data in sessions so that they can be used later
		$this->sessions->setsessiondata(array('logged_in'=>TRUE,'isadmin'=>$user['isadmin'],'role'=>$user['role'],'user_id'=>$user['id']));

		$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider']));

		//later set this permissions:
		if($user['role']=='housekeeper'){
			$permissions = $this->get_user_permissions();
			$this->sessions->setsessiondata(array('permissions'=>$permissions));
		}

		//success full login return true;
		//create login i=history record
		$this->db->query($this->db->insert_string('login_history', $login_history));
		return true;
	}


	
	/*
	get user records  based on facebook id

	*/
	function verify_facebook_user($data = array()){
		
		$query='SELECT * FROM '.$this->module.' WHERE facebook_user_id="'.$data["facebook_user_id"].'"';
		$result=$this->db->query($query);

		/*echo'<pre>';
		print_r($result);
		echo'</pre>';*/

		if(!$result){
			//echo('not result==>');
		}
		
		//now check :-
		if($result->num_rows() == 0)
		{
			//echo('not num_rows==>');
			$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
			
			//now insert
			//build the insert sql
			$insert_sql=$this->db->insert_string($this->module,$data);
			if(!$result=$this->db->query($insert_sql))
			{
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
			$id=$this->db->insert_id();

			$query='SELECT * FROM '.$this->module.' WHERE facebook_user_id="'.$data["facebook_user_id"].'"';
		    $result=$this->db->query($query);
			
		}

		//create a login history:-
		$this->load->helper('network');
		$login_history['ipaddress']=ip_address();
		$login_history['user_agent']=user_agent();
		$login_history['comment']='Successfull login';
		

		$user = $result->row_array();

		//create a login history:-
		$this->load->helper('network');

		$login_history['user_id']=$user['id'];

		//set the required data in sessions so that they can be used later
		$this->sessions->setsessiondata(array('logged_in'=>TRUE,'isadmin'=>$user['isadmin'],'role'=>$user['role'],'user_id'=>$user['id']));

		$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider'],'image_url'=>$user['profile_image_url']));

		//later set this permissions:
		if($user['role']=='housekeeper'){
			$permissions = $this->get_user_permissions();
			$this->sessions->setsessiondata(array('permissions'=>$permissions));
		}

		//success full login return true;
		//create login i=history record
		$this->db->query($this->db->insert_string('login_history', $login_history));
		return true;
		

	}

	/*
	get user records  based on facebook id

	*/
	function verify_login_twitter($id=''){
		//this method check to see for a valid login
		if(trim($id)=='')
		{
			//error both are required fields
			throw new Exception('Facebook id is missing');
			return false;
		}
		$this->load->helper('network');
		$query='SELECT * FROM '.$this->module.' WHERE twitter_user_id="'.$id.'"  AND isactive=1 ';
		$result=$this->db->query($query);

		if(!$result)
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		$login_history['ipaddress']=ip_address();
		$login_history['user_agent']=user_agent();
		$login_history['comment']='Successfull login';
		
		//now check :-
		if($result->num_rows() == 0)
		{
			//invalid username :- we need to throw a system exception instead of database error
			$login_history['comment']=' Invalid facebook login/user inactive..tried by facebook user with id '.$id;
			$this->db->query($this->db->insert_string('login_history', $login_history));
			throw new Exception('Invalid facebook login');
			return false;
		}
		$user = $result->row_array();

		//create a login history:-
		$this->load->helper('network');

		$login_history['user_id']=$user['id'];

		//set the required data in sessions so that they can be used later
		$this->sessions->setsessiondata(array('logged_in'=>TRUE,'isadmin'=>$user['isadmin'],'role'=>$user['role'],'user_id'=>$user['id']));

		$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider']));

		//later set this permissions:
		if($user['role']=='housekeeper'){
			$permissions = $this->get_user_permissions();
			$this->sessions->setsessiondata(array('permissions'=>$permissions));
		}

		//success full login return true;
		//create login i=history record
		$this->db->query($this->db->insert_string('login_history', $login_history));
		return true;
	}

	
	/*
	get user records  based on google id

	*/
	function verify_login_google($id=''){
		try{
			//this method check to see for a valid login
			if(trim($id)=='')
			{
				//error both are required fields
				throw new Exception('Google id is missing');
				return false;
			}
			$this->load->helper('network');
			$query='SELECT * FROM '.$this->module.' WHERE google_user_id="'.$id.'"  AND isactive=1 ';
			$result=$this->db->query($query);

			if(!$result)
			{
				throw new Exception($this->db->_error_message());
				return FALSE;
			}

			$login_history['ipaddress']=ip_address();
			$login_history['user_agent']=user_agent();
			$login_history['comment']='Successfull login';
			
			//now check :-
			if($result->num_rows() == 0)
			{
				//invalid username :- we need to throw a system exception instead of database error
				$login_history['comment']=' Invalid google login/user inactive..tried by google user with id '.$id;
				$this->db->query($this->db->insert_string('login_history', $login_history));
				throw new Exception('Invalid google login');
				return false;
			}
			$user = $result->row_array();

			//create a login history:-
			$this->load->helper('network');

			$login_history['user_id']=$user['id'];

			//set the required data in sessions so that they can be used later
			$this->sessions->setsessiondata(array('logged_in'=>TRUE,'isadmin'=>$user['isadmin'],'role'=>$user['role'],'user_id'=>$user['id']));

			$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider']));

			//later set this permissions:
			if($user['role']=='housekeeper'){
				$permissions = $this->get_user_permissions();
				$this->sessions->setsessiondata(array('permissions'=>$permissions));
			}

			//success full login return true;
			//create login i=history record
			$this->db->query($this->db->insert_string('login_history', $login_history));
			return true;
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
	}

	/**
	 *  get the list of allowed permissions for the user
	 *  like manage property or allow booking  this applies only in case of house keepers
	 */
	function get_user_permissions(){
		return array();
	}

	function  list_of_users(){
		$query='SELECT * FROM '.$this->module;
		$result=$this->db->query($query);
		return $result->result_array();
	}

	public function create($data=array())
	{
		if(empty($data)){
			//oops
		}

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
		//now insert
		//build the insert sql
		$insert_sql=$this->db->insert_string($this->module,$data);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		$id=$this->db->insert_id();

		return $id;
	}

	public function update($data=array()){

		$insert_sql=$this->db->update_string($this->module,$data,'id='.$data['id']);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		return $data['id'];
	}

	function getUser($id=''){
		if($id==''){
			//error
		}

		/*$query = $this->db->get_where($this->module, array('id' => $id));

		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}*/

		$query ='SELECT * FROM '.$this->module.' WHERE id=\''.$id.'\'';
		$result=$this->db->query($query);
		//return $result->result_array();

		return $result->row_array();
	}

	public function  filter_user_list($filters){

		if(!$query = $this->db->get_where($this->module, $filters))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		return $query->result_array();
	}

	public function deactivate_user($id='')
	{
		try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$update_sql="Update ".$this->module." set isactive = 0 WHERE id='".$id."'";
			
			if(!$result=$this->db->query($update_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
			
	 }

	 public function reset_password($id='',$password)
	{
	 	try{
				
			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			
			$update_sql="Update ".$this->module." set password ='".$password."'  WHERE id='".$id."'";
			
			if(!$result=$this->db->query($update_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}

			//to get firstname and password
			$select_sql='SELECT * FROM '.$this->module .' where id='.$id.'';
			$res=$this->db->query($select_sql);
			
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}
		
		return $res->result_array();
	 }

	 function  list_of_inactive_users(){
		$query='SELECT * FROM '.$this->module .' where isactive=0';
		$result=$this->db->query($query);
		return $result->result_array();
	}

	function  list_of_active_users(){
		$query='SELECT * FROM '.$this->module .' where isactive=1';
		$result=$this->db->query($query);
		return $result->result_array();
	}

	function  change_password($current_password,$new_password){
		

		$user_id = $this->sessions->getsessiondata('user_id');
	
		$query ='SELECT * FROM '.$this->module .' where id=\''.$user_id.'\'';
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		if($result->num_rows() ==0){
			//invalid username :- we need to throw a system exception instead of database error
			throw new Exception('User is not present!');
			return false;
		}
		$user = $result->row_array();
		$current_password = md5($current_password);
		
		if($current_password !== $user['password'] ){
			throw new Exception('Invalid current password entered!');
			return ;
		}
		
		if($current_password == $user['password'] ){
			$user['password']=md5($new_password);
		}
		
		$this->db->update($this->module, $user, "id =".$user['id']);
		//now return back the user back 
		$user['password']=$new_password;
		return $user;
	}

	/**
	* Forgot password
	*
	*/
	public function forgot_password($username_or_email){
		
		if(trim($username_or_email)==''){
			//error both are required fields
			throw new Exception('username/email is missing');
			return false;
		}
		$query ='Select id,firstname,lastname,username,email,password from users where username=\''.$username_or_email.'\' OR email=\''.$username_or_email.'\'';
		
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		
		if($result->num_rows() ==0){
			//invalid username :- we need to throw a system exception instead of database error
			throw new Exception('Invalid username/email provided!');
			return false;
		}

		$user = $result->row_array(); 
		$user['last_modified_date'] = date( 'Y-m-d H:i:s');
		$password = get_random_password();
		$user['password'] = md5($password);
		
		$this->db->update($this->module, $user, "id =".$user['id']);
		//now return back the user back 
		$user['password']=$password;
		return $user;
	}

	public function user_logged_in_for_1st_time(){
		$user_id = $this->sessions->getsessiondata('user_id');
	
		$query ='SELECT id FROM login_history where user_id=\''.$user_id.'\'';
		$result=$this->db->query($query);
		return $result->result_array();
	}

}