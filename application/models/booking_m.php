<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
class booking_m extends CI_Model {
	//declare and assign variable.	
	private $module ='booking';

	public function create($data=array())
	{
		if(empty($data)){
			//oops
		}

		if(!$this->validate_booking($data['arrival_date'],$data['departure_date'],$data['property_id'])){
			throw new Exception('A booking already exist please book for other day');
			return false;
		}

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
		//now insert
		//build the insert sql
		$insert_sql=$this->db->insert_string($this->module,$data);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		$id=$this->db->insert_id();

		return $id;
	}

	public function update($data=array(),$where){

		/*$bookingArr = $this->getBooking($data['id']);
		
		$bookingArr['arrival_date'] ='0000-00-00';
		$bookingArr['departure_date'] ='0000-00-00';

		$insert_sql=$this->db->update_string($this->module,$bookingArr,'property_id='.$data['id']);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}*/

		/*if(!$this->validate_booking($data['arrival_date'],$data['departure_date'],$data['property_id'])){  
			throw new Exception('A booking already exist please book for other day');
			return false;
		}

		$insert_sql=$this->db->update_string($this->module,$data,'id='.$data['id']);
		if(!$result=$this->db->query($insert_sql))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}
		return $data['id'];*/

		$update_sql=$this->db->update_string($this->module,$data,$where);		 

		 if(!$result=$this->db->query($update_sql))

		 {

			throw new Exception($this->db->_error_message());

			return FALSE;

		 }	

		 $id=$this->db->insert_id();	

		 return $id;

	}

	/**
	* method that returns bookings list
	* @params : $where clause
	*/

	public function filter_shortbreak($date1,$date2)
	{
		
		
		try{
			$query="SELECT isShortbreak from basic_pricing where start_date>='".$date1."' AND end_date='".$date2."'";
			//$query="SELECT isShortbreak from basic_pricing where start_date IN (SELECT start_date from basic_pricing where isShortbreak=1)";
			
	    	//$query="SELECT isShortbreak from basic_pricing where start_date BETWEEN '2015-01-19' AND '2015-01-25'";

			}
			catch(Exception $e)
			{
			throw $e;
		   	}
		
		$result = $this->db->query($query);
		
		return $result->result_array();

	}


	public function list_of_bookings($where='',$limit_order=''){
		try{



			$query='Select '.$this->module.'.id, '.$this->module.'.property_id, customer_name, arrival_date, nights, status, cost, departure_date, departure_time,
			 '.$this->module.'.created_date, '.$this->module.'.last_modified_date, arival_time, adults, children, infants,property_name from '.$this->module.',property
			 WHERE '.$this->module.'.property_id=property.property_id';

			//adding where clause based on the logged in user
			$condn= array();
			if(!$this->sessions->getsessiondata('isadmin')){
				//$condn[]='property_id IN (Select id from property WHERE user_id=\''.$this->sessions->getsessiondata('user_id').'\')';
				if($this->sessions->getsessiondata('role') == 'housekeeper'){
					$owner_id = $this->fetching_owner_id();
					$condn[]='property.user_id =\''.$owner_id.'\'';
				}else{
					$condn[]='property.user_id =\''.$this->sessions->getsessiondata('user_id').'\'';
				}
			}
			if($where!=''){
				$condn[]=$where;
			}
			if(count($condn)>0){
				$query.= ' AND '.join(' AND ',$condn);
			}

			if($limit_order!='')	$query.= ' '.$limit_order;
 			else $query.= ' ORDER BY '.$this->module.'.id DESC ';
 			//print_r($query);
			$result = $this->db->query($query);
			//print_r($result->result_array());
			return $result->result_array();
		}catch(Exception $e){
			throw $e;
		}
	}

	/*
		get latest set of bookings
	*/
	function get_latest_bookings(){
		$limit=' ORDER BY '.$this->module.'.created_date DESC LIMIT 10 ';
		return $this->list_of_bookings('',$limit);
	}

	public function fetching_owner_id(){
		$query = 'Select * from users where id =\''.$this->sessions->getsessiondata('user_id').'\'';
		$result = $this->db->query($query);
		$housekeeper_data = $result->result_array();
		$owner_id = $housekeeper_data[0]['owner_id'];	
		return $owner_id;
	}

	function get_bookingcount_groupedby_property(){
		$query='Select COUNT('.$this->module.'.Id) as total_bookings,property_name from '.$this->module.',property  WHERE '.$this->module.'.property_id=property.property_id ';

		if(!$this->sessions->getsessiondata('isadmin')){

			if($this->sessions->getsessiondata('role') == 'housekeeper'){
				$owner_id = $this->fetching_owner_id();
				$query.=' AND property.user_id =\''.$owner_id.'\'';
			}else{
				$query.=' AND property.user_id =\''.$this->sessions->getsessiondata('user_id').'\'';
			}
				
		}
		$query.=' GROUP BY '.$this->module.'.property_id';
		$result = $this->db->query($query);

		return $result->result_array();
	}

	function get_bookingcount_groupedby_date(){
		$date = date('Y-m-d');
		//print_r($date);
		$query='Select COUNT('.$this->module.'.Id) as total_bookings,DAY('.$this->module.'.created_date) as day,isWebBooking from '.$this->module.',property  WHERE '.$this->module.'.property_id=property.property_id ';
		//$query.=' AND DATE('.$this->module.'.created_date) >=CURDATE()';
		$query.=' AND DATE('.$this->module.'.created_date) >= '.$date.'';
		if(!$this->sessions->getsessiondata('isadmin')){

			if($this->sessions->getsessiondata('role') == 'housekeeper'){
				$owner_id = $this->fetching_owner_id();
				$query.=' AND property.user_id =\''.$owner_id.'\'';
			}else{
				$query.=' AND property.user_id =\''.$this->sessions->getsessiondata('user_id').'\'';
			}
				
		}
		$query.=' GROUP BY DAY('.$this->module.'.created_date),isWebBooking';
		
		$result = $this->db->query($query);

		return $result->result_array();
	}

	/**
	 *  get list of bookings between custom dates
	*/
	public function filter_bookings_by_date_property($date1,$date2,$property_id){
		$where =' arrival_date >= "'.$date1.'" AND arrival_date <="'.$date2.'"';
		


		if($property_id!=null && $property_id!=''){

			$where.=' AND booking.property_id='.$property_id;
			//$where.=' AND property.property_id='.$property_id;
			
			

		}

		//$where.=' AND property.user_id =\''.$this->sessions->getsessiondata('user_id').'\'';


		//$id= $_SESSION['user_id'];

		//$result =$this->db->query('SELECT booking.arrival_date,booking.departure_date,property.property_id,property.property_name FROM booking INNER JOIN property ON booking.property_id=property.property_id WHERE booking.arrival_date >= '.$date1.' AND booking.arrival_date <='.$date2.' AND property.user_id='.$id.'');
		
		//return $result->result_array();
		return $this->list_of_bookings($where);
	}

	public function filter_bookings_by_date_property_user($date1,$date2,$property_id){
		
		$where='booking.property_id='.$property_id;
		
		return $this->list_of_bookings($where);
	}

	
	public function getBooking($id='',$arrival_date=''){
		if($id=='' && $arrival_date=''){
			//error
		}

		$query='Select * from '.$this->module.' WHERE property_id=\''.$id.'\'  AND arrival_date=\''.$arrival_date.'\' LIMIT 1';
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		return $result->row_array();
	}

	public function getBooking_for_edit($id='',$arrival_date=''){
		if($id==''){
			//error
		}

		$query='Select * from '.$this->module.' WHERE Id=\''.$id.'\' LIMIT 1';
		if(!$result=$this->db->query($query))
		{
			throw new Exception($this->db->_error_message());
			return FALSE;
		}

		return $result->row_array();
	}

	public function delete($id='')
	{
		try{

			if($id==''){
				//show exception that id is missing
				throw new exception('Id is missing');
				return FALSE;
			}
			$delete_sql="DELETE FROM ".$this->module." WHERE id='".$id."'";

			if(!$result=$this->db->query($delete_sql)){
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
		}catch(Exception $e)
		{
			throw $e;
			return false;
		}

	 }

	/* To validate if booking is done, if not allow booking */
	public function validate_booking($date,$date2,$property){

		$query = 'Select arrival_date,departure_date from '.$this->module.' WHERE property_id=\''.$property.'\'';

		$result = $this->db->query($query);
		$available_booking = $result->result_array();

		$dates=Array(); 
		foreach($available_booking as $booking){

			$dt=Array($booking['arrival_date'],$booking['departure_date']); 

			$i=0; 
			while (strtotime($dt[1])>=strtotime("+".$i." day",strtotime($dt[0]))) 
				$dates[]=date("Y-m-d",strtotime("+".$i++." day",strtotime($dt[0]))); 
		}

		if ($this->in_array_any(array($date,$date2), $dates )) return false;
		else return true;
	}

	//Added in_array_any 21/5/14 ,since booking was overlapping.
	function in_array_any($needles, $dates) {
    	return !!array_intersect($needles, $dates);
    }
	
	
	function list_booking($date1,$date2) {
    	try{
    		$where =' arrival_date >= "'.$date1.'" AND arrival_date <="'.$date2.'"';
			$query = $this->db->query('SELECT * FROM '.$this->module.' where '.$where.'');

			return $query->result();
				
		}catch(Exception $e){
			throw $e;
		}
		
    }

    function list_booking_user($start,$end) {
    	try{
    		$id= $_SESSION['user_id'];
			
			$start = str_replace('-', '/', $start);
			$start = date('Y-m-d',strtotime($start . "+1 days"));

			$end = str_replace('-', '/', $end);
			$end = date('Y-m-d',strtotime($end . "+1 days"));

			$query= "SELECT booking.property_id FROM property INNER JOIN booking ON booking.property_id=property.property_id WHERE booking.arrival_date='$start' AND booking.departure_date='$end' AND property.user_id ='$id'";
			
			$query=$this->db->query($query);

			return $query->result();
				
		}catch(Exception $e){
			throw $e;
		}
		
    }

    function search($data){
    	//$field = $data['field'];
    	$id1= $_SESSION['user_id'];

    	//var_dump($id1);
    	$find = $data['find'];
    	$find = strtoupper($find);  
        $find = strip_tags($find);  
        $find = trim ($find);   

    	try{
			if($find ==''){	
				// by poonam
				$query = "SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name, property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND users.id=$id1"; 

				//throw new exception('You forgot to enter a search term');
			   // return false;
			}
			else
			{
			//$where ="firstname LIKE'%$find%' OR lastname LIKE'%$find%' OR email LIKE'%$find%' OR contact_name LIKE'%$find%' OR booking_email LIKE'%$find%'";
			//$query = "SELECT * FROM users where ".$where." ";
			//$query = "SELECT * FROM booking where upper($field) LIKE'%$find%'";
			//$query = 'SELECT * FROM users where "'.$find.'"  IN(upper(firstname),upper(lastname),upper(email))';
			//$query="SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND users.lastname LIKE '%$find%' OR users.firstname LIKE '%$find%' OR users.email LIKE '%$find%' OR booking.customer_name LIKE '%$find%'";

		   $query = "(SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name,property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND users.lastname LIKE '%$find%') 
           UNION
           (SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name, property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND users.firstname LIKE '%$find%') 
           UNION
           (SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name, property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND booking.customer_name LIKE '%$find%')
           UNION
           (SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name, property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND booking.id LIKE '%$find%')
           UNION
           (SELECT DISTINCT users.firstname, users.lastname, booking.id, booking.arrival_date, booking.departure_date, booking.customer_name, property.property_name FROM users INNER JOIN booking INNER JOIN property WHERE users.id=property.user_id AND booking.property_id=property.property_id  AND property.property_name LIKE '%$find%')";
			
			

			}
			//var_dump($query);
			$query.=" LIMIT 10";
			//print_r($query);

			

			$result = $this->db->query($query);

			//var_dump($result);
			//var_dump($result);
			return $result;	
		
		}catch(Exception $e)
		{
		
		echo "enter proper value";
   		 // Do something
		return false;
		}
	}
}