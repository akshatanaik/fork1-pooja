<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	

class property_m extends CI_Model {

	//declare and assign variable.	

	private $module ='property';

	private $module1='property_facilities_and_features';

	private $module2='property_suitability';

	private $module3='property_location';

	private $module4='property_photo';

	private $module5='add_housekeeper';

	

	

	/*

		gets all the property for the current user.

	*/	

	public function property_list()

	{

		//$id=$this->session->userdata('id');



		$id=$this->sessions->getsessiondata('user_id');

		

		try{



			$query = $this->db->query('SELECT * FROM '.$this->module.' Where user_id='.$id.'');
			//var_dump($query);
			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	} 

	public function property_details($url_id)

	{

		//$id=$this->session->userdata('id');



		$id=$this->sessions->getsessiondata('user_id');

		

		try{



			$query = $this->db->query('SELECT * FROM '.$this->module.' Where user_id='.$id.' AND property_id='.$url_id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	} 




	/*

		check if facility record is present for a given id

	*/

	public function propertyFacilityData($id)

	{

		try{



			$query = $this->db->query('SELECT * FROM '.$this->module1.' Where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	}



	/*

		check if suitability record is present for a given id

	*/

	public function propertySuitabilityData($id)

	{

		try{



			$query = $this->db->query('SELECT * FROM '.$this->module2.' Where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	}



	/*

		check if facility record is present for a given id

	*/

	public function propertyLocationData($id)

	{

		try{



			$query = $this->db->query('SELECT * FROM '.$this->module3.' Where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	}



	/*

		delete property from db

	*/

	public function delete($id='')

	{

		try{

				

			if($id==''){

				//show exception that id is missing

				throw new exception('Id is missing');

				return FALSE;

			}

			

			$delete_sql="DELETE FROM ".$this->module." WHERE property_id='".$id."'";

			

			if(!$result=$this->db->query($delete_sql)){

				throw new Exception($this->db->_error_message());

				return FALSE;

			}

		}catch(Exception $e)

		{

			throw $e;

			return false;

		}

			

	 }



	/*

		gets the list of owners for a particular house keeper

	*/

	public function getListofOwners()

	{

	   		

		//$id=$this->session->userdata('id');



		$id=$this->sessions->getsessiondata('user_id');

		

		try{

			$query = $this->db->query('SELECT user_id FROM users Where id='.$id.'');

			

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

			

	}





	/*

		 list of properties for a  house keeper

	*/

	public function propertyListforHousekeeper($user_id)

	{

	   		

		try{

			$query = $this->db->query('SELECT * FROM '.$this->module.' Where user_id='.$user_id.'');

			

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

			

	}



	/*

		save the property details data into db.

	*/

	public function savePropertyDetail($data=array())

	{

		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}



	/*

		save the property features data into db.

	*/

	public function savePropertyFeatures($data=array())

	{

		

		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module1,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}





	/*

		save the property suitability data into db.

	*/

	public function savePropertySuitability($data=array())

	{

		

		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module2,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}



	/*

		save the property location details data into db.

	*/

	public function savePropertyLocationDetails($data=array())

	{



		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module3,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}



	/*

		Gets the property info data for a particular id.

	*/

	public function getPropertyDetails($id)

	{

		try{



			$query_str =$this->db->query( "SELECT property.*,property_suitability.*,property_facilities_and_features.*  FROM property ,property_suitability,property_facilities_and_features  where property_suitability.property_id =$id AND property_facilities_and_features.property_id =$id AND property.property_id =$id");

            return $query_str;

				

		}catch(Exception $e){

			throw $e;

		}

	}







	/*

		updates the property details data from an edit page into the database.

	*/

	public function updatePropertyDetails($data=array(),$where)

	{

		$update_sql=$this->db->update_string($this->module,$data,$where);

		 

		 if(!$result=$this->db->query($update_sql))

		 {

			throw new Exception($this->db->_error_message());

			return FALSE;

		 }

	

		 $id=$this->db->insert_id();

	

		 return $id;

	}



	/*

		updates the property features and facilities data from an edit page into the database.

	*/

	public function updatePropertyFeatures($data=array(),$where)

	{

		$update_sql=$this->db->update_string($this->module1,$data,$where);

		  

		if(!$result=$this->db->query($update_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

	

		$id=$this->db->insert_id();

	

		return $id;

	}



	/*

		updates the property suitability data from an edit page into the database.

	*/

	public function updatePropertySuitability($data=array(),$where)

	{

		$update_sql=$this->db->update_string($this->module2,$data,$where);

		 

		if(!$result=$this->db->query($update_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

	

		$id=$this->db->insert_id();

	

		return $id;

	}



	/*

		Gets the property location  data for a particular id.

	*/

	public function getLocationDetails($id)

	{

		try{

			

			$query=$this->db->query('SELECT * FROM '.$this->module3.' WHERE property_id="'.$id.'"');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	

	}





	/*

		updates the property location data from an edit page into the database.

	*/

	public function updateLocationDetails($data=array(),$where)

	{

		$update_sql=$this->db->update_string($this->module3,$data,$where);

		 

		if(!$result=$this->db->query($update_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

	

		$id=$this->db->insert_id();

	

		return $id;

	}







	/*

		Saves the property features data of a particular id into the database.

	*/

	public function savePropertyFeaturesforId($data=array(),$id)

	{

		$query = 'SELECT * FROM '.$this->module1.' Where property_id='.$id.'';

		

		if(!$result=$this->db->query($query)){

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		

		//now check :-

		if(!$result->num_rows() == 0){

			

			throw new Exception('Property details avaliable for this property.');

			return false;

		}else{

			

			if(empty($data)){

				//oops

			}

			

			$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

			//now insert

			//build the insert sql			

			$insert_sql=$this->db->insert_string($this->module1,$data);

			

			if(!$result=$this->db->query($insert_sql))

			{

				throw new Exception($this->db->_error_message());

				return FALSE;

			}

			

			$id=$this->db->insert_id();

			

			return $id;

		}

			

	}



	

	/*

		Saves the property suitability data of a particular id into the database.

	*/

	public function  savePropertySuitabilityforId($data=array(),$id)

	{

		$query = 'SELECT * FROM '.$this->module2.' Where property_id='.$id.'';

		

		if(!$result=$this->db->query($query)){

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		//now check :-

		if(!$result->num_rows() == 0){

			throw new Exception('Property details avaliable for this property.');

			return false;

		}else{

			

			if(empty($data)){

				//oops

			}

			

			$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

			//now insert

			//build the insert sql			

			$insert_sql=$this->db->insert_string($this->module2,$data);

			

			if(!$result=$this->db->query($insert_sql))

			{

				throw new Exception($this->db->_error_message());

				return FALSE;

			}

			

			$id=$this->db->insert_id();

			

			return $id;

		}

	}





	/*

		Saves the property location data of a particular id into the database.

	*/

	public function savePropertyLocationforId($data=array(),$id)

	{

		$query = 'SELECT * FROM '.$this->module3.' Where property_id='.$id.'';

		

		if(!$result=$this->db->query($query)){

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		//now check :-

		if(!$result->num_rows() == 0){

			throw new Exception('Location details avaliable for this property.');

			return false;

		}else{

			

			if(empty($data)){

				//oops

			}

			

			$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

			//now insert

			//build the insert sql			

			$insert_sql=$this->db->insert_string($this->module3,$data);

			

			if(!$result=$this->db->query($insert_sql))

			{

				throw new Exception($this->db->_error_message());

				return FALSE;

			}

			

			$id=$this->db->insert_id();

			

			return $id;

		}

	}



	

	/*

		save the housekepeers info into the database.

	*/

	public function saveHousekeeperDetails($data=array())

	{

		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module5,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}



	/*

		Update the housekeeper account.

	*/	

	public function update($data=array(),$where='')

	{

		  

		if($where=='') 

			$where = "id='".$data['id']."'"; 



		   			

		$update_sql=$this->db->update_string($this->module,$data,$where);

		  //print_r($update_sql);

		  

		if(!$result=$this->db->query($update_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

	

		$id=$this->db->insert_id();

	

		return $id;

		

	}



	/*

		get all the photos of a property id

	*/

	public function getPropertyPhotos($id)

	{

		try{

			//$query = $this->db->query('SELECT * FROM '.$this->module4.' where property_id="'.$id.'"');


			$this->db->select()->from($this->module4)->where('property_id',$id)->order_by('position','desc');
	  		$query=$this->db->get();
            return $query;

			 //return $query;

				

		}catch(Exception $e){

			throw $e;

		}

		

	}



	/*

		save the uploaded img link into the database. 

	*/

	public function saveProperty_photo($data=array()){

		if(empty($data)){

			//oops

		}

		

		$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');

		//now insert

		//build the insert sql			

		$insert_sql=$this->db->insert_string($this->module4,$data);

		if(!$result=$this->db->query($insert_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

		$id=$this->db->insert_id();

		

		return $id;

	}



	/*

		Update the uploaded photo

	*/	

	public function updateProperty_photo($data=array(),$where='')

	{

		  

		if($where=='') 

			$where = "id='".$data['id']."'"; 



		   			

		$update_sql=$this->db->update_string($this->module,$data,$where);

		 // print_r($update_sql);

		  

		if(!$result=$this->db->query($update_sql))

		{

			throw new Exception($this->db->_error_message());

			return FALSE;

		}

	

		$id=$this->db->insert_id();

	

		return $id;

		

	}



	public function checkAvailiable_photo($id){

		try{

			$query = $this->db->query('SELECT * FROM '.$this->module.' where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}



	}	

	public function getpropertyname($id){
		try{

			$query = $this->db->query('SELECT * FROM '.$this->module.' where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}
	}



	

	public function updatePropertyPhotos($id,$data_id=array(),$order=array())

	{     

  			 $where = array();
  			 for($i=0;$i<count($data_id);$i++) {
  			 	$where[$i] = "id='".$data_id[$i]."'"; 
  			 }
            
  		
                for($i=0;$i<count($data_id);$i++) {
	           
	            $data['position'] = (count($data_id)-$order[$i]-1);
	            $update_sql=$this->db->update_string($this->module4,$data,$where[$i]);
	           
			    if(!$result=$this->db->query($update_sql))
			    {

						throw new Exception($this->db->_error_message());

						return FALSE;
				}
			}



		

	}

		public function getAvailiable_index($id){

		try{

			$query = $this->db->query('SELECT position FROM '.$this->module4.' where property_id='.$id.'');

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}



	}	

	


	

	public function getPropertyPhotosDesc($id)

	{

		try{

	        $this->db->select()->from($this->module4)->where('property_id',$id)->order_by('position','desc');
	  		$query=$this->db->get();
            return $query;		

		}catch(Exception $e){

			throw $e;

		}

		

	}

	
	public function getLocation($table,$column,$content)

	{

		try{



			$query = $this->db->query("SELECT * FROM ".$table." Where ".$column." ='".$content."'");

			return $query;

				

		}catch(Exception $e){

			throw $e;

		}

	}
	public function getAvailiable_0_index($where){

		try{

			$query = $this->db->query('SELECT property_id,image,position FROM '.$this->module4.$where. " ORDER BY position DESC");
			
			return $query;

				

		}catch(Exception $e){

			throw $e;

		}



	}	
	




}

