<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class commissions_m extends CI_Model {
		//declare and assign variable.	
		private $module ='commission';
		private $booking_module ='booking';

		function getBookingsOfProperty($propertyId){
			//print_r('-- MODEL  -- ');
			//print_r($propertyId);

			$query = $this->db->get_where($this->booking_module,array('property_id' => $propertyId));
			//print_r('-- num_rows  -- ');print_r($query->num_rows());
			if($query->num_rows() == 0)
			{
				throw new exception('No bookings for this property yet.');
			    return FALSE;
			}else{
				//print_r('RESULT-->');print_r($query->row_array());
				return $query->result_array();
			}
			
		}

		/*
		save the save_commission info into the database.
		*/
		public function save_commission($data=array())
		{
			if(empty($data)){
				//oops
			}
			
			$data['created_date']=$data['last_modified_date'] = date( 'Y-m-d H:i:s');
			//now insert
			//build the insert sql			
			$insert_sql=$this->db->insert_string($this->module,$data);
			if(!$result=$this->db->query($insert_sql))
			{
				throw new Exception($this->db->_error_message());
				return FALSE;
			}
			$id=$this->db->insert_id();
			
			return $id;
		}

		function  list_of_commissions(){
			$query="SELECT ".$this->module.".id,".$this->module.".property_id,commision_percentage,vat_on_commission,minimum_commision,minimum_owner_account_value,overseas_tax,commission_on_net_booking_val,net_booking_value,property_name FROM ".$this->module.",property WHERE ".$this->module.".property_id=property.property_id";
			$result=$this->db->query($query);
			return $result->result_array();
	    }
		
	}

?>	