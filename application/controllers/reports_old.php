<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reports extends CI_Controller {

	/*
		defining a construt method that is invoke to check whether the user has logged in
	*/
	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=reports&err=login_required');
            }
        }

    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function booking_reports()
	{
		$this->load->model('booking_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Booking Reports';
		try{

			$data = $this->booking_m->list_of_bookings();
			$response ['response']['records'] = $data;


		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/booking_reports',$response);
	}

	/*
		download the booking summary in excel format
	*/
	function download_booking_summary_to_excel()
	{
		$this->load->library("PHPExcel");
		$this->load->model('booking_m');
		$phpExcel = new PHPExcel();
		$prestasi = $phpExcel->setActiveSheetIndex(0);
		
		//merger
		$phpExcel->getActiveSheet()->mergeCells('A1:J1');
		
		//manage row hight
		$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		
		//style alignment
		$styleArray = array(
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,),
		);
		
		$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$phpExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
		
		//border
		$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'E3E3E3',
				),
			),
		);
		
		//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
		
		//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
		$prestasi->setCellValue('A1', 'Booking Reports');
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Id');
		$prestasi->setCellValue('B2', 'Customer');
		$prestasi->setCellValue('C2', 'Adults');
		$prestasi->setCellValue('D2', 'Children');
		$prestasi->setCellValue('E2', 'Infants');
		$prestasi->setCellValue('F2', 'Arrival Date');
		$prestasi->setCellValue('G2', 'Departure Date');
		$prestasi->setCellValue('H2', 'Arrival Time');
		$prestasi->setCellValue('I2', 'Departure Time');
		$prestasi->setCellValue('J2', 'Property');
		$data = $this->booking_m->list_of_bookings();
		$no=0;
		$rowexcel = 2;
		
		foreach($data as $row)
		{
			$no++;
			$rowexcel++;
						$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'J'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'J'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $no);
			$prestasi->setCellValue('B'.$rowexcel, $row['customer_name']);
			$prestasi->setCellValue('C'.$rowexcel, $row['adults']);
			$prestasi->setCellValue('D'.$rowexcel, $row['children']);
			$prestasi->setCellValue('E'.$rowexcel, $row['infants']);
			$prestasi->setCellValue('F'.$rowexcel, $row['arrival_date']);
			$prestasi->setCellValue('G'.$rowexcel, $row['departure_date']);
			$prestasi->setCellValue('H'.$rowexcel, $row['arival_time']);
			$prestasi->setCellValue('I'.$rowexcel, $row['departure_time']);
			$prestasi->setCellValue('J'.$rowexcel, $row['property_name']);
		}
		$prestasi->setTitle('Booking Reports');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Booking Reports.xls\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
		$objWriter->save("php://output");	

	}	

	public function pricing_report()
	{
		$start_date=$this->input->post('start_date');

		$this->load->model('pricing_m');
		$this->load->model('property_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Reports';
		
		$response = array();
		
		if(!isset($start_date) || $start_date ==''){		
			$property_id=$this->input->post('property_id');
			try{

				$data = $this->property_m->property_list();			
				$response ['response']['properties'] = $data->result_array();

				if($property_id == '' || empty($property_id)){
					$res = $this->pricing_m->list_pricing();				
					$response ['response']['data']=$res;
				}else{
					$res = $this->pricing_m->list_pricing_id($property_id);				
					$response ['response']['data']=$res;
				}	


			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			}
			
			$this->load->view('app/pricing_reports',$response);
			
		}else{
			
			$data = $this->input->post(NULL, TRUE);
			$this->load->library('form_validation');
			$this->form_validation->set_rules('start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('end_date', 'End Date', 'required');
		

			if ( $this->form_validation->run() == FALSE ){
				//we triggerd validation error
				$response['status']='error';
				$response['response']=validation_errors();
			}else{

				$s_date=$this->input->post('start_date');
				$start_date=date('Y-m-d', strtotime($s_date));			
				$e_date=$this->input->post('end_date');
				$end_date=date('Y-m-d', strtotime($e_date));
				$property_id=$this->input->post('property_id');
				
				try{

					$res = $this->pricing_m->filter_pricing_for_reports($start_date,$end_date,$property_id);				
					$response ['response']['data']=$res->result_array();	

					$data = $this->property_m->property_list();			
					$response ['response']['properties'] = $data->result_array();
					

				}catch(Exception $e){
						//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}

			}
			
			$this->load->view('app/pricing_reports',$response);			
		}
		
	}	


	/*
		download the booking summary in excel format
	*/
	function download_pricing_summary_to_excel()
	{
		$this->load->library("PHPExcel");
		$this->load->model('pricing_m');
		$phpExcel = new PHPExcel();
		$prestasi = $phpExcel->setActiveSheetIndex(0);
		
		//merger
		$phpExcel->getActiveSheet()->mergeCells('A1:E1');
		
		//manage row hight
		$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		
		//style alignment
		$styleArray = array(
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,),
		);
		
		$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$phpExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($styleArray);
		
		//border
		$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'E3E3E3',
				),
			),
		);
		
		//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
		
		//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);		
		$prestasi->setCellValue('A1', 'Pricing Reports');
		$phpExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Property Id');
		$prestasi->setCellValue('B2', 'Start Date');
		$prestasi->setCellValue('C2', 'End Date');
		$prestasi->setCellValue('D2', 'Price');
		$prestasi->setCellValue('E2', 'Shortbreaks');
		
		$data = $this->pricing_m->list_pricing();
		$no=0;
		$rowexcel = 2;
		
		foreach($data as $row)
		{
			$no++;
			$rowexcel++;
						$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'E'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'E'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $row['property_id']);
			$prestasi->setCellValue('B'.$rowexcel, $row['start_date']);
			$prestasi->setCellValue('C'.$rowexcel, $row['end_date']);
			$prestasi->setCellValue('D'.$rowexcel, $row['price']);
			$prestasi->setCellValue('E'.$rowexcel, $row['isShortbreak']);
			
		}
		$prestasi->setTitle('Pricing Reports');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Pricing Reports.xls\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
		$objWriter->save("php://output");	

	}	
}