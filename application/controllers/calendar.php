<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class calendar extends CI_Controller {

	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {

                redirect('/login?returl=calendar&err=login_required');
            }
        }

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$response['navigation']['tab'] = 'Calendar';
		$response['mode']=$_GET['type'];
		$response['calendar_booking']=1;//switch on bookings tpo be created from calendar
		$this->load->model('property_m');

		$data = $this->property_m->property_list();
		$response ['response']['properties'] = $data->result_array();
		$response ['booking']['property_id']=isset($_GET['property_id']) ? $_GET['property_id']:'';
		$this->load->view('app/calendar',$response);
	}

	function get_bookings(){
		$this->load->model('booking_m');
		$response ['status']='success';


		$start = strftime('%Y-%m-%d',$_GET['start']);
		
		$end = strftime('%Y-%m-%d',$_GET['end']);
		
		
		if(isset($_GET['property_id'])){
			
			//for the method you will recive 
			$property=isset($_GET['property_id']) ? $_GET['property_id']:'';
	
			try{
				$result=$this->booking_m->filter_bookings_by_date_property($start,$end,$property);
				$response ['response']['records']=$result;
			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			}
		}else{
			
			try{

				$results=$this->booking_m->list_booking_user($start,$end);
				$res=array();
				$value=array();
				foreach($results as $result){
					$property_id=$result->property_id;
					$res[]=$this->booking_m->filter_bookings_by_date_property_user($start,$end,$property_id);				
					
				}

				if($res[0] == null || $res[0] == '' || empty($res[0])){
					unset($res[0]);
					$value[] = array_values($res);
					
				}else{
					$value[]=$res;
				}

				//$new_array_without_nulls = array_filter($res);
				$response ['response']['records']=$value;
				
				
			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			}
		}

		IF(IS_AJAX) echo json_encode($response);
	}

	public function get_booked_property_bookingdetails(){
		
		$response['status']='success';
		$this->load->model('booking_m');
			
		$start=$_GET['start'];
		$end=$_GET['end'];
		$property_id=$_GET['property_id'];
		
		try{

			$res = $this->booking_m->filter_bookings_by_date_property($start,$end,$property_id);

			$response ['response']['records']=$res;			

		}catch(Exception $e){
				//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		
		if(IS_AJAX) echo json_encode($response);
	}

	function get_pricing(){
		//for the method you will recive 
		$start=$_GET['start_date'];
		$end=$_GET['end_date'];
		$id=$_GET['property_id'];
		
		$this->load->model('pricing_m');
		$response ['status']='success';


		try{
			$result=$this->pricing_m->filter_pricing($start,$end,$id);
			$shortbreak=$this->pricing_m->list_of_shortbreak($id);

			$response ['response']['records']=$result;
			$response ['response']['shortbreak']=$shortbreak;
			
		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		IF(IS_AJAX) echo json_encode($response);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */