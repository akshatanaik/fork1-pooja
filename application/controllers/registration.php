<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registration extends CI_Controller {


	/**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
		parent::__construct();
		//Load the session library - If session lib is autoloaded remove this from here
		$this->load->library('session');

    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{
		$response['status']='success';
		$response['saveurl']=site_url('/registration/save');
		$this->load->view('app/reg',$response);
		
	}

	public function facebook(){
		

		$this->config->load("facebook",TRUE);
        $fb_config = $this->config->item('facebook');
        $this->load->library('Facebook', $fb_config);

		/*$fb_config = array(
		    'appId'  => '272469016271687',
		    'secret' => 'b2ead965f802b010c4ee04bc1c7f1536'
		);

		$this->load->library('facebook', $fb_config);
		*/

		$fb_user_chk = $this->facebook->getUser();

		if ($fb_user_chk) {
            try {
                $fb_user = $this->facebook->api('/me');
                //print_r($fb_user);
                $user['facebook_user_id']=$fb_user['id'];
			    $user['firstname']=$fb_user['first_name'];
			    $user['lastname']=$fb_user['last_name'];
			    $user['email']=$fb_user['email'];
			    $user['username']=isset($fb_user['username'])? $fb_user['username']:$user['email'];
			    //$user['profile_image_url']=$image;
			    $user['login_provider']='facebook';
			    $user['login_provider_username']=$user['username'];
			    
			    //Added because after register we re redirecting directly to dashboard.
			    //$this->sessions->setsessiondata('logged_in',TRUE);

			    //set the required data in sessions so that they can be used later
				$this->sessions->setsessiondata(array('logged_in'=>TRUE,'user_id'=>$user['facebook_user_id']));

				$this->sessions->setsessiondata(array('firstname'=>$user['firstname'],'lastname'=>$user['lastname'],'login_provider'=>$user['login_provider']));

			    $response['status']='success';
			    $response['response']['user']=$user;
			    
			    $this->load->view('app/register', $response);
			    return;
                    
            } catch (FacebookApiException $e) {
                $fb_user = null;
            }
        }

        if ($fb_user_chk) {
           
        } else {
            redirect( $this->facebook->getLoginUrl(array('scope' => 'public_profile,email,user_about_me' )));
        }
	}

	public function twitter(){
		//Form'submitted - TODO: Insert form validation
	    $this->load->library('tweet');
	    if (!$this->tweet->logged_in()) {
			$this->tweet->set_callback(site_url('registration/twitter'));
			$this->tweet->login();
			return;
	    }

	    $user = $this->tweet->call('get', 'account/verify_credentials');
	    $user['twitter_user_id']=$user->id_str;
	    $user['firstname']=$user->name;
	    $user['email']=$user['email'];
	    $user['username']=$user->screen_name;
	    $user['login_provider']='twitter';
	    $user['login_provider_username']=$user['username'];
	    
	    $response['status']='success';
		$response['response']['user']=$user;

		$this->load->view('app/register', $response);
	 
	}

	public function google(){
		//echo('hi');
		$access_token  = $this->sessions->getsessiondata('access_token');
		//var_dump($access_token);
		if($access_token){
		    try {
	        	
	        	//$google_user = $openid->getAttributes();
			  
	            $user['google_user_id']=$this->sessions->getsessiondata('google_user_id');
			    $user['firstname']=$this->sessions->getsessiondata('firstname');
			    $user['lastname']=$this->sessions->getsessiondata('lastname');
			    $user['email']=$this->sessions->getsessiondata('email');
			    $user['username']=isset($user['username'])? $user['username']: $user['email'];			    //$user['profile_image_url']=$image;
			   
			    $user['login_provider']='google';
			    $user['login_provider_username']=$user['username'];
			    
			    $response['status']='success';
			    $response['response']['user']=$user;
			    //var_dump( $user);
			    $this->load->view('app/register', $response);
			    return;
	                
	        } catch (Exception $e) {
	            $fb_user = null;
	        }
    	}
		
	}

	public function save(){

    	$response['status']='success';


		$this->load->model('users_m');
			//Validations		

		$this->load->library('form_validation');
		$user=$this->input->post(NULL, TRUE);
		
		
		$user['role']='owner';
		$user['isactive']=1;//Form'submitted - TODO: Insert form validation
	   
		//print_r('user===>');print_r($user);
		if ( $user['login_provider']=='facebook' ||  $user['login_provider']=='twitter' || $user['login_provider']=='google') {
			$user['username']=$user['login_provider_username'];
		}

		if(isset($user['password']))$user['password']=md5($user['password']);
		$user['activation_code']=uniqid ();

		unset($user['password_confirmation']);

		//check for duplicate username:-
    	$user_record=$this->users_m->get_user_by_username($user['username']);

    	if($user_record){
    		$response['status']='error';
    		$response['response']='Duplicate username found';
    		if(IS_AJAX) echo json_encode($response);
    		return;
    	}

    	$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|trim');
		$this->form_validation->set_rules('password_confirmation', 'Password', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('password', 'Confirm Password', 'trim|required|min_length[5]|max_length[50]|matches[password_confirmation]');
		/*$this->form_validation->set_rules('phone', 'phone', 'trim|numeric|exact_length[10]');
		$this->form_validation->set_rules('zip_postal_code', 'Zip code', 'trim|alphanumeric|exact_length[5]');*/

		

		if ( $this->form_validation->run() == FALSE ){
            //we triggerd validation error
            $response['status']='error';
            $response['response']=validation_errors();
            
            
        }else{
        	
        	$password = $this->input->post('password');
        	$user['username'] = trim($user['username']);
       
			$res =$this->users_m->create($user);

			//Added since now user willl be directly logged in after sign up
			$res = 	$this->users_m->verify_login($user['username'],$password);

			//once a user is created send an email to the user with  temp password & to activate the account
			//on success send email :
			$this->load->library('email');
			$this->email->from('support@bookingbrain.co.uk', 'Booking brain');
			$this->email->to($user['email']);
			$this->email->set_mailtype('html');
			$this->email->subject('Welcome to the bookingbrain.co.uk');	
			$this->email->message($this->load->view('app/emailtemplates/registration_email',$user,true));
			$this->email->send();
		}
		if(IS_AJAX) echo json_encode($response);
	}

	public function registration_complete(){
		//complete the registration  process
		//when the user click on the link  activate make the account active
		$activation_code=isset($_GET['activation_code']) ? $_GET['activation_code']:'';
		$email=isset($_GET['username']) ? $_GET['username']:'';
	
		if($activation_code!='' && $email!=''){
			$this->load->model('users_m');
			//activate the account:
			$result =$this->users_m->filter_user_list(array('activation_code'=>$activation_code,'email'=>$email));
			if(count($result)>0){
				
				$user=$result[0];
				//so once the user record is found update registration complete & remove the
				$user['is_account_activated']=1;
				$user['isactive']=1;
				
				$this->users_m->update($user);
				
				$this->load->view('app/registration_complete');
			}
		}
	}

	public function registration_success(){
		$this->load->view('app/registration_success');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */