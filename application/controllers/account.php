<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class account extends CI_Controller {



	/*

		defining a construt method that is invoke to check whether the user has logged in

	*/

	public function __construct()

	{

        parent::__construct();



        // Check that the user is logged in

        if (!$this->sessions->getsessiondata('logged_in') ) {

            // Prevent infinite loop by checking that this isn't the login controller

            if ($this->router->class != 'login')            {

                redirect('/login?returl=users&err=login_required');

            }

        }



    }


	public function account_Profile(){
       $this->load->model('users_m');
		$response ['status']='success';
		
		try{

			$data = $this->users_m->getUser($this->sessions->getsessiondata('user_id'));
			$response ['response']['user'] = $data;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		
		$this->load->view('app/account',$response);
	
		

	}
	
	public function update_user(){
		$this->load->model('users_m');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('company_name','Company Name','trim|alpha|max_lenght[250]');
		$this->form_validation->set_rules('contact_name','Contact Name','trim|alpha|max_length[250]');
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required|alpha|max_length[50]');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required|alpha|max_length[50]');
		$this->form_validation->set_rules('address','Address','trim|alpha_numeric|max_length[300]');
		$this->form_validation->set_rules('zip_postal_code','Zip/Pin Code','trim|numeric|exact_length[5]');
		$this->form_validation->set_rules('country','Country','trim|alpha|max_length[50]');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|trim');
		$this->form_validation->set_rules('add_booking_email', 'Additional Booking Email Address', 'valid_email|trim');
		$this->form_validation->set_rules('company_website','Website URL','trim|max_length[256]|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check');
		$this->form_validation->set_rules('phone', 'Telephone Number', 'numeric|trim');
		$this->form_validation->set_rules('id', 'Profile Number', 'numeric|trim');
		$this->form_validation->set_rules('company_id', 'Company Registration Number', 'trim|required|numeric|xss_clean|min_length[6]|max_length[7]');
		$this->form_validation->set_rules('vat_number', 'VAT Number', 'trim|required|numeric|xss_clean|min_length[9]|max_length[10]');



		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{
		
			try{
				$user=$this->input->post(NULL, TRUE);
			
				$data = $this->users_m->update($user);
				
				$response['response'] ='Saved';
				$response['status']='success'; 
		   
				
			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			
			}
		}
		if(IS_AJAX) echo json_encode($response);
		
		}

		public function valid_url_format($str)
		{
			$pattern ="|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i";
			if(!preg_match($pattern, $str))
			{
				$this->set_message('valid_url_format','The URL you entered is not Correct.');
				return FALSE;
			}

			return TRUE;
		}
}
	

/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */