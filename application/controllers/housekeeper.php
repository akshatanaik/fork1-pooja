<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class housekeeper extends CI_Controller {

	/*
		defining a construt method that is invoke to check whether the user has logged in
	*/
	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=users&err=login_required');
            }
        }

    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/

	/*
		Form to add a new housekeeper.
	*/
	public function index()
	{

		$this->load->model('housekeeper_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Housekeeper';
		try{

			$data = $this->housekeeper_m->list_of_active_housekeepers();
			$response ['response']['records'] = $data;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/housekeepers',$response);
	
	}

	/*
		save the housekeeper data into the databaase.
	*/
	public function save_housekeeper_details()
	{
		
		//Validations		
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		$details=$this->input->post(NULL,TRUE);

		$this->form_validation->set_rules('firstname', 'First Name', 'required|trim');
		$this->form_validation->set_rules('lastname', 'Last Name', 'required|trim');
		//$this->form_validation->set_rules('company_name', 'Company Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|trim');
		//$this->form_validation->set_rules('password', 'Password', 'required|trim');

		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{
			$permissions_name['name'] = array();	
			$this->load->model('housekeeper_m');
			$this->load->helper('password_helper');

			if($response['status']=='success')
			{
		
				$this->load->library('sessions');

				$owner_id = $this->sessions->getsessiondata('user_id');
			
				$housekeeper_details['role']='housekeeper';
				$housekeeper_details['firstname']=trim($details['firstname']);
				$housekeeper_details['lastname']=trim($details['lastname']);
				$housekeeper_details['company_name']=$details['company_name'];
				$housekeeper_details['email']=trim($details['email']);
				
				$housekeeper_details['phone']=$details['phone'];
				$housekeeper_details['address']=$details['address'];
				//assigning email value to username
				$housekeeper_details['username']=$housekeeper_details['email'];
				
				//commented because they want randomly generated passowrd.
				//$housekeeper_details['username']=trim($details['username']);
				//$housekeeper_details['password']=md5($details['password']);
				$housekeeper_details['notes']=$details['notes'];
				$housekeeper_details['activation_code']=uniqid ();
				$housekeeper_details['owner_id'] = $owner_id;
				
				$housekeeper_details['password'] = get_random_password();
				//print_r('generated_password'); print_r($housekeeper_details['password']);
				//$pwd=md5($password['password']);
				
				/*permissions*/
				if($details['booking_reference'] == 'Allow'){
					$permissions_name['name'][]='manage_booking_reference';
				}else{
					$permissions_name['name'][]='deny_booking_reference';
				}
				if($details['arrival_time'] == 'Allow'){
					$permissions_name['name'][]='manage_arrival_time';
				}else{
					$permissions_name['name'][]='deny_arrival_time';
				}
				if($details['departure_time'] == 'Allow'){
					$permissions_name['name'][]='manage_departure_time';
				}else{
					$permissions_name['name'][]='deny_departure_time';
				}
				if($details['customers_telephone_no'] == 'Allow'){
					$permissions_name['name'][]='manage_customers_telephone_no';
				}else{
					$permissions_name['name'][]='deny_customers_telephone_no';
				}
				if($details['customers_town_or_city'] == 'Allow'){
					$permissions_name['name'][]='manage_customers_town_or_city';
				}else{
					$permissions_name['name'][]='deny_customers_town_or_city';
				}
				if($details['customers_comments'] == 'Allow'){
					$permissions_name['name'][]='manage_customers_comments';
				}else{
					$permissions_name['name'][]='deny_customers_comments';
				}
				if($details['customers_country'] == 'Allow'){
					$permissions_name['name'][]='manage_customers_country';
				}else{
					$permissions_name['name'][]='deny_customers_country';
				}				

				try{

					if($this->input->post('id')!=null && $this->input->post('id')!='')
					{
						$housekeeper_details['id']=$this->input->post('id');
						$res =$this->housekeeper_m->update($housekeeper_details);

						$permissions['user_id']=$this->input->post('id');
						// added by sheetal
						//$res=$this->housekeeper_m->save_housekeeper_details($housekeeper_details);
						
						$this->housekeeper_m->delete_housekeeper_relation($housekeeper_details['id']);
					
						
						$target= $details['assign_Property'];
						
						for($i=0;$i<count($target);$i++) {
								$prop_relation['user_id'] = $housekeeper_details['id'];
								$prop_relation['property_id'] = $target[$i];
								$res1=$this->housekeeper_m->save_housekeeper_relation($prop_relation);
						}
						//
						//insert each permission separately into the database
						$length=count($permissions_name['name']);
						
							for($j=0;$j<count($length);$j++){
									$permissions['permission_name']=$permissions_name['name'][$j];
									$permission_res=$this->housekeeper_m->update_permissions($permissions);								
							}

							for($i=0;$i<$length;$i++){
							
								$permissions['permission_name']=$permissions_name['name'][$i];
								$permission_res=$this->housekeeper_m->save_permissions($permissions);
							}


					}else{

						//check for duplicate username:-
						$this->load->model('users_m');
				    	$user_record=$this->users_m->get_user_by_username($housekeeper_details['email']);

				    	if($user_record){
				    		$response['status']='error';
				    		$response['response']='Duplicate email found';
				    		if(IS_AJAX) echo json_encode($response);
				    		return;
				    	}
						//added by sheetal				    
						$res=$this->housekeeper_m->save_housekeeper_details($housekeeper_details);
						
						$permissions['user_id']=$res;
						
						$target= $details['assign_Property'];

						for($i=0;$i<count($target);$i++) {
								$prop_relation['user_id'] = $res;
								$prop_relation['property_id'] = $target[$i];
								$res1=$this->housekeeper_m->save_housekeeper_relation($prop_relation);
						}
						//
						//insert each permission separately into the database
						$length=count($permissions_name['name']);
						for($i=0;$i<$length;$i++){
							
							$permissions['permission_name']=$permissions_name['name'][$i];
							$permission_res=$this->housekeeper_m->save_permissions($permissions);
						}
						
						$subject = $this->input->post('email_subject');
						$housekeeper_details['email_body'] = $this->input->post('email_body');
					
						//once a housekeeper is added send an email to the user  to activate the account

						$this->load->library('email');
						$this->email->from('support@bookingbrain.co.uk', 'Booking brain');
						$this->email->to($housekeeper_details['email']);
						$this->email->set_mailtype('html');
						if($subject) $this->email->subject($subject);	
						else $this->email->subject('Welcome to the bookingbrain.co.uk');

						
						$this->email->message($this->load->view('app/emailtemplates/registration_email',$housekeeper_details,true));
						$this->email->send();

						$response['response'] ='An email has been sent.';
					}

				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}

			}
		}
		
		if(IS_AJAX) echo json_encode($response);
	}

	/*
		House keeper account activation
	*/
	public function accountactivation()
	{
		
		$user['email']=$_GET['email'];
		$response['status']='success';
		
		try{
			$this->load->model('housekeeper_m');
		  	$updateData=array('isactive'=>1);
		  	$where = "email='".$user['email']."'"; 				
		  	$data = $this->housekeeper_m->update($updateData,$where);
		
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		
		if($response['status']=='success')
		    $this->load->view('app/login');	
				
	}

	public function  add_new_housekeeper(){

		$response ['status']='success';
		$response ['navigation']['tab'] = 'New Housekeeper';

		$this->load->model('housekeeper_m');
		$this->load->model('property_m');

		$response ['status']='success';
		try{
			
			
			$data = $this->property_m->property_list();
			$response ['response']['properties'] = $data->result_array();
			}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		$this->load->view('app/new_housekeeper',$response);
	}


	public function edit_housekeeper(){
		
		if(!isset($_GET['id']) || $_GET['id']==''){
			$response ['status']='error';
			$response ['response']='Oops!Unable to edit..No record found';
			$this->load->view('app/new_housekeeper',$response);
			return;
		}

		$this->load->model('housekeeper_m');
		$this->load->model('property_m');
		$response ['status']='success';
		$response ['navigation']['tab'] = 'Housekeeper > Edit Housekeeper';
		try{
			$user_permissions = $this->housekeeper_m->get_user_permissions($_GET['id']);	
			
			$housekeeper = $this->housekeeper_m->getHousekeeper($_GET['id']);
			$user_id1 = $housekeeper['id'];
			//print_r($user_id1);
			//housekeeper properties
			$housekeeper_properties = $this->housekeeper_m->getHousekeeperProperties($user_id1);
			//print_r($housekeeper_properties);
			//$data = $this->property_m->propertyListforHousekeeper1($housekeeper_properties['property_id']);
			$response['response']['allproperties'] = $housekeeper_properties;
			//print_r($data);

			// housekeeper all properties
			$data1 = $this->property_m->property_list();
			$response ['response']['properties'] = $data1->result_array();

		
			$response ['response']['housekeeper'] = $housekeeper;

			$response ['response']['user_permissions'] = $user_permissions;




		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/new_housekeeper',$response);
	}

	//Delete method
	/*public function delete()
	{
		$this->load->model('housekeeper_m');
		$response['status']='success';
		try{			  
			$this->housekeeper_m->delete($_GET['id']);

			$response['status']='success';
			$response['response']='Record is Deleted';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
	}*/

		/*
		download the booking summary in excel format
	*/
	function download_to_excel()
	{
		$this->load->library("PHPExcel");
		$this->load->model('housekeeper_m');
		$phpExcel = new PHPExcel();
		$prestasi = $phpExcel->setActiveSheetIndex(0);
		
		//merger
		$phpExcel->getActiveSheet()->mergeCells('A1:F1');
		
		//manage row hight
		$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		
		//style alignment
		$styleArray = array(
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,),
		);
		
		$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$phpExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($styleArray);
		
		//border
		$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'E3E3E3',
				),
			),
		);
		
		//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
		
		//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$prestasi->setCellValue('A1', 'Housekeeper Summary');
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Id');
		$prestasi->setCellValue('B2', 'Email');
		$prestasi->setCellValue('C2', 'Address');
		$prestasi->setCellValue('D2', 'Phone');
		$prestasi->setCellValue('E2', 'Company Name');
		$prestasi->setCellValue('F2', 'Role');

		$data = $this->housekeeper_m->list_of_housekeepers();
		$no=0;
		$rowexcel = 2;
		
		foreach($data as $row)
		{
			$no++;
			$rowexcel++;
						$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'F'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'F'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $no);
			$prestasi->setCellValue('B'.$rowexcel, $row['email']);
			$prestasi->setCellValue('C'.$rowexcel, $row['address']);
			$prestasi->setCellValue('D'.$rowexcel, $row['phone']);
			$prestasi->setCellValue('E'.$rowexcel, $row['company_name']);
			$prestasi->setCellValue('F'.$rowexcel, $row['role']);
		}
		$prestasi->setTitle('Housekeeper Summary');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Housekeeper Summary.xls\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
		$objWriter->save("php://output");	

	}	

	public function deactivate_housekeeper()
	{
		$this->load->model('housekeeper_m');
		$response['status']='success';
		try{			  
			$this->housekeeper_m->deactivate_housekeeper($_GET['id']);

			$response['status']='success';
			$response['response']='Record is Deactivated';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
		
	}

	public function activate_housekeeper()
	{
		$this->load->model('housekeeper_m');
		$response['status']='success';
		try{			  
			$this->housekeeper_m->activate_housekeeper($_GET['id']);

			$response['status']='success';
			$response['response']='Record is Activated';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
		
	}

	public function reset_password()
	{
		$response['status']='success';
		
		try{
			$this->load->helper('password_helper');
			$this->load->model('housekeeper_m');	
			
			$password['password'] = get_random_password();
			
			$pwd=md5($password['password']);
			$res=$this->housekeeper_m->reset_password($_GET['id'],$pwd);
			//var_dump($res);
			$password['firstname']	  =  $res[0]['firstname'];
			//$password['password']	  =  $res[0]['password'];
			$this->load->library('email');
			$this->email->from('support@bookingbrain.co.uk', 'Booking brain');
			$this->email->to($_GET['email']);
			$this->email->set_mailtype('html');
			$this->email->subject('Welcome to the bookingbrain.co.uk');	
			$this->email->message($this->load->view('app/emailtemplates/resetpassword_email',$password,true));
			$this->email->send();

			$response['status']='success';
			$response['response'] ='An email has been sent.';
			
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		
		if(IS_AJAX) echo json_encode($response);	  
	}

	public function inactive_housekeeper()
	{
		$this->load->model('housekeeper_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Housekeepers';
		try{

			$data = $this->housekeeper_m->list_of_inactive_housekeepers();
			$response ['response']['records'] = $data;

			$response['active'] = 'inactive';

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/housekeepers',$response);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */