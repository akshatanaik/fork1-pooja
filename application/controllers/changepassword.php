<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changepassword extends CI_Controller {

    /*
        defining a construt method that is invoke to check whether the user has logged in
    */
    public function __construct()
    {
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=bookings&err=login_required');
            }
        }

    }



    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
    */
    public function index()
    {
       $this->load->view('app/change_password');
  
    }

    public function chpassword(){
        //echo('change password entry-->');
        //Validations       
        $this->load->library('form_validation');
        $password_data = $this->input->post(NULL, TRUE);
        
        $this->form_validation->set_rules('inputCurrent_Password', 'Current Password', 'required');
        $this->form_validation->set_rules('inputNew_Password', 'New Password', 'required');
        $this->form_validation->set_rules('inputConfirm_Password', 'Confirm New Password', 'required||matches[inputNew_Password]'); 
        $response['status']='success';
        if ( $this->form_validation->run() == FALSE ){
            //we triggerd validation error
            $response['status']='error';
            $response['response']=validation_errors();
            
            
        }else{
            //echo('else-->');
            $this->load->model('users_m');
                try{
                
                    //first reset the password
                    $passwrd = $this->input->post('inputCurrent_Password');
                    
                    $new_password = $this->input->post('inputNew_Password');
                
                    $password_data = $this->users_m->change_password($passwrd,$new_password);
                  
                    $password['firstname'] = $password_data['firstname'];
                    $password['password'] = $password_data['password'];
                    $password['email'] = $password_data['email'];
                
                    //now send email to that user with new password
                        $this->load->library('email');
                        $this->email->from('support@bookingbrain.co.uk', 'Booking brain');
                        $this->email->to($password['email']);
                        $this->email->set_mailtype('html');
                        $this->email->subject('Welcome to the bookingbrain.co.uk'); 
                        $this->email->message($this->load->view('app/emailtemplates/resetpassword_email',$password,true));
                        $this->email->send();

                        $response['status']='success';
                        $response['response'] ='An email has been sent.';
                
                }catch(Exception $e){
                    //exception means some kind of error from the system
                    $response['status']='error';
                    $response['response']=$e->getMessage();
                }
        }
        //print_r($response);
        if(IS_AJAX) echo json_encode($response);
    }

   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */