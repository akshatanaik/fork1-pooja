<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class property extends CI_Controller {

	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
       if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login');
            }
        }

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	/*
		 displays all the property list stored in the database.
	*/
	public function propertyList()
	{
		
		$this->load->model('property_m');

		$this->load->library('sessions');

		$response ['status']='success';
		try{

			$list_of_properties = $this->property_m->property_list();
			$response ['response']['records'] = $list_of_properties->result_array();
			$response ['response']['size'] =$list_of_properties->num_rows();
			$properties = $list_of_properties->result_array();
			$flag = 0;
			if($properties)
			{
				$where = " Where (property_id = '".$properties[0]['property_id']."'";
				$flag =1;
			}
		 	foreach($properties as $rec)
		 	{
		 		
		 		 $where .= " OR property_id = '".$rec['property_id']."'";
		 	
		 	}
		 	$where.=")  ";
		 	$checkIndex=$this->property_m->getAvailiable_0_index($where);
			$index1 = $checkIndex->result_array();
			$response ['response']['images']=$index1;
			//print_r($where);
			//print_r($index1);

			//$index2=$checkIndex->num_rows();
			$misc['navigation']['tab'] = 'My Properties';

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		//var_dump($response['response']['records']);

		$this->load->view('app/property_list',array_merge($response,$misc));
	}


	/*
		 displays all the property list related to the housekeeper stored in the database.
	*/
	public function propertyListforHousekeeper(){
	
		$this->load->model('property_m');
		$response ['status']='success';
		
		try{

			$data = $this->property_m->getListofOwners();

			$response ['response']['records'] = $data->result_array();

			/* propertylist of each owner which is related to housekeeper.*/
			foreach($response ['response']['records'] as $rec){
				$user_id=$rec['user_id'];

				$data = $this->property_m->propertyListforHousekeeper($user_id);

				$response ['response']['records'] = $data->result_array();
			}

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		if(IS_AJAX) json_encode($response);
		else
		$this->load->view('app/property_list',$response);
	}

	/*
		Detailed property page
	*/
	public function getPropertyDetails($id)
	{
		$this->load->helper(array('form', 'url'));

		$this->load->model('property_m');

		$misc['navigation']['tab'] = 'Property Details';

		$response ['status']='success';
		try{
			
			$data = $this->property_m->getPropertyDetails($id);
			//var_dump($data);
			
			$response ['response']['records'] = $data->result_array();
			$response ['response']['size'] =$data->num_rows();
			//var_dump($response ['response']['size']);
			
		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		$this->load->view('app/property_details',array_merge($response,$misc));
	}


	/*
		loads the view page
	*/
	public function getLocationDetails($id){
		$this->load->model('property_m');

		$misc['navigation']['tab'] = 'Location Details';

		$response ['status']='success';
		try{

			$propertydetails = $this->property_m->getPropertyDetails($id);			
			$data = $this->property_m->getLocationDetails($id);

			
			$response ['response']['records'] = $data->result_array();
			$response ['response']['property'] = $propertydetails->result_array();

			$length=count($response ['response']['property']);
			
			for($i=0;$i<$length;$i++){
					$response ['response']['records'][$i]['property_name']=$response ['response']['property'][$i]['property_name'] ;
			
			}

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/property_location',array_merge($response,$misc));
	}



	/*
		 form to add new property
	*/
	public function addNewProperty()
	{
		$misc['navigation']['tab'] = 'New Property';
		
		$response['response']['title']='Add New Property';
		$response ['response']['records'] = null;
		$response['add_method_property']='saveProperty';
		
		
		$this->load->view('app/add_newproperty.php',array_merge($response,$misc));
	}

	/*
		 form to add new property
	*/
	public function editProperty($id)
	{
		$this->load->model('property_m');

		$misc['navigation']['tab'] = 'Edit Property';

		$response ['status']='success';
		try{
			
			$propertydetails = $this->property_m->getPropertyDetails($id);			
			$data = $this->property_m->getLocationDetails($id);

			
			$response ['response']['records'] = $data->result_array();
			$response ['response']['property'] = $propertydetails->result_array();

			$response['response']['title']='Edit Property';
			$response['edit_method_property']='updateProperty';
			

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		
		$this->load->view('app/add_newproperty.php',array_merge($response,$misc));
	}


	/*
		save the property details into the database.
	*/
	public function saveProperty()
	{
		
		$property_data = $this->input->post(NULL, TRUE);
		//Validations		
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		
	
		/*$this->form_validation->set_rules('inputProperty_id', 'Property Id', 'required|trim');*/
		$this->form_validation->set_rules('inputProperty_name', 'Property Name', 'required|trim');
		$this->form_validation->set_rules('inputMax_guest', 'Maximum guests', 'numeric|trim');
		$this->form_validation->set_rules('inputProperty_address', 'Property Address', 'alpha|trim');
		$this->form_validation->set_rules('inputPostcode', 'Postcode', 'trim|alphanumeric');
		$this->form_validation->set_rules('inputWebsite', 'Website', 'valid_url|trim');
		/*$this->form_validation->set_rules('inputContinent', 'Continent', 'required|trim|alpha');
		$this->form_validation->set_rules('inputCountry', 'Country', 'required|trim|alpha');*/
		
    	
    	$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
			
		}else{
				
			$this->load->model('property_m');
			$this->load->model('pricing_m');

			if($response['status']=='success')
			{
				$this->load->library('sessions');
				//format the data :-
				$property['user_id']=$this->sessions->getsessiondata('user_id');
				$property['property_id']=str_pad(mt_rand(1,99999),5,'0',STR_PAD_LEFT);  ;
				$property['property_name']=$property_data['inputProperty_name'];
				$property['property_type']=$property_data['inputProperty_type'];
				$property['max_guests']=$property_data['inputMax_guest'];
				$property['main_description']=$property_data['inputMain_description'];
				$property['search_result_desc']=$property_data['inputSearch_result'];
			

				$propertyfeatures['user_id']=$this->sessions->getsessiondata('user_id');
				$propertyfeatures['property_id']=$property['property_id'];
				$propertyfeatures['home_size']=$this->input->post('inputHome_size');
				$propertyfeatures['single_beds']=$this->input->post('inputSingle_bed');
				$propertyfeatures['family_bathrooms']=$this->input->post('inputFamily_bathrooms');
				$propertyfeatures['dining_seats']=$this->input->post('inputDining_seats');
				$propertyfeatures['double_beds']=$this->input->post('inputDouble_beds');
				$propertyfeatures['en_suites']=$this->input->post('inputEn_suites');
				$propertyfeatures['lounge_seats']=$this->input->post('inputLounge_seats');
				$propertyfeatures['sofa_beds']=$this->input->post('inputSofa_beds');
				$propertyfeatures['shower_bathrooms']=$this->input->post('inputShower_rooms');
				$propertyfeatures['cots']=$this->input->post('inputCots');
				$propertyfeatures['total_bathrooms']=$this->input->post('inputTotal_bathrooms');
				$propertyfeatures['cooker']=$this->input->post('inputCooker');
				$propertyfeatures['tv']=$this->input->post('inputTv');
				$propertyfeatures['log_fire']=$this->input->post('inputLog_fire');
				$propertyfeatures['fridge']=$this->input->post('inputFridge');
				$propertyfeatures['satellite_tv']=$this->input->post('inputSatellite_tv');
				$propertyfeatures['central_heating']=$this->input->post('inputCentral_heating');
				$propertyfeatures['freezer']=$this->input->post('inputFreezer');
				$propertyfeatures['video_player']=$this->input->post('inputVideo_player');
				$propertyfeatures['ac']=$this->input->post('inputAc');
				$propertyfeatures['microwave']=$this->input->post('inputMicrowave');
				$propertyfeatures['dvd_player']=$this->input->post('inputDvd_player');
				$propertyfeatures['linen']=$this->input->post('inputLinen');
				$propertyfeatures['toaster']=$this->input->post('inputToaster');
				$propertyfeatures['cd_playe']=$this->input->post('inputCd_player');
				$propertyfeatures['towel']=$this->input->post('inputTowels');
				$propertyfeatures['kettle']=$this->input->post('inputKettle');
				$propertyfeatures['internet']=$this->input->post('InputInternet');
				$propertyfeatures['sauna']=$this->input->post('inputSauna');
				$propertyfeatures['dishwasher']=$this->input->post('inputDishwasher');
				$propertyfeatures['wifi']=$this->input->post('inputWifi');
				$propertyfeatures['gym']=$this->input->post('inputGym');
				$propertyfeatures['washing_machine']=$this->input->post('inputWashing_machine');
				$propertyfeatures['telephone']=$this->input->post('inputTelephone');
				$propertyfeatures['table_tennis']=$this->input->post('inputTable_tennis');
				$propertyfeatures['clothes_dryer']=$this->input->post('inputClothes_dryer');
				$propertyfeatures['fax']=$this->input->post('inputFax_machine');
				$propertyfeatures['pool_or_snooker']=$this->input->post('inputPool_or_snooker_table');
				$propertyfeatures['iron']=$this->input->post('inputIron');
				$propertyfeatures['hair_dryer']=$this->input->post('inputHair_dryer');
				$propertyfeatures['games_room']=$this->input->post('inputGames_room');
				$propertyfeatures['high_chair']=$this->input->post('inputHigh_chair');
				$propertyfeatures['safe']=$this->input->post('inputSafe');
				$propertyfeatures['staffed_property']=$this->input->post('inputStaffed_property');
				$propertyfeatures['cleaning_services']=$this->input->post('inputCleaning_services');
				$propertyfeatures['ironing_board']=$this->input->post('inputIroning_board');
				$propertyfeatures['fan']=$this->input->post('inputFan');
				$propertyfeatures['shared_outdoor_pool_heated']=$this->input->post('inputShared_outdoor_pool_heated');
				$propertyfeatures['shared_tennis_court']=$this->input->post('inputShared_tennis_court');
				$propertyfeatures['solarium_or_roof_terrace']=$this->input->post('inputSolarium_or_roof_terrace');
				$propertyfeatures['shared_outdoor_pool_unheated']=$this->input->post('inputShared_outdoor_pool_unheated');
				$propertyfeatures['private_tennis_court']=$this->input->post('inputPrivate_tennis_court');
				$propertyfeatures['balcony_or_terrace']=$this->input->post('inputBalancy_or_terrace');
				$propertyfeatures['private_outdoor_pool_heated']=$this->input->post('inputPrivate_outdoor_pool_heated');
				$propertyfeatures['shared_garden']=$this->input->post('inputShared_garden');
				$propertyfeatures['sea_view']=$this->input->post('inputSea_view');
				$propertyfeatures['private_outdoor_pool_unheated']=$this->input->post('inputPrivate_outdoor_pool_unheated');
				$propertyfeatures['private_garden']=$this->input->post('inputPrivate_garden');
				$propertyfeatures['private_fishinglake_or_river']=$this->input->post('inputPrivate_fishinglake_or_river');
				$propertyfeatures['private_indoor_pool']=$this->input->post('inputPrivate_indoor_pool');
				$propertyfeatures['climbing_frame']=$this->input->post('inputClimbing_frame');
				$propertyfeatures['boat']=$this->input->post('inputBoat');
				$propertyfeatures['shared_indoor_pool']=$this->input->post('inputShared_indoor_pool');
				$propertyfeatures['swing_set']=$this->input->post('inputSwing_set');
				$propertyfeatures['bicycles']=$this->input->post('inputBicycle');
				$propertyfeatures['childrens_pool']=$this->input->post('inputChildrens_pool');
				$propertyfeatures['trampoline']=$this->input->post('inputTrampoline');
				$propertyfeatures['parking']=$this->input->post('inputParking');
				$propertyfeatures['jacuzzi_or_hot_tub']=$this->input->post('inputJacuzzi_or_hot_tub');
				$propertyfeatures['bbq']=$this->input->post('inputBbq');
				$propertyfeatures['secure_parking']=$this->input->post('inputSecure_parking');

				$propertysuitability['user_id']=$this->sessions->getsessiondata('user_id');
				$propertysuitability['property_id']=$property['property_id'];
				$propertysuitability['long_term_lets']=$this->input->post('inputLong_term_lets');
				$propertysuitability['corporate_bookings']=$this->input->post('inputCorporate_bookings');
				$propertysuitability['house_swap']=$this->input->post('inputHouse_swap');
				$propertysuitability['short_breaks']=$this->input->post('inputShort_breaks');
				$propertysuitability['hen_or_stack_breaks']=$this->input->post('inputHen_or_stag_breaks');
				$propertysuitability['children']=$this->input->post('inputChildren');
				$propertysuitability['pets']=$this->input->post('inputPets');
				$propertysuitability['smokers']=$this->input->post('inputSmokers');
				$propertysuitability['restricted_mobility']=$this->input->post('inputRestricted_mobility');
				$propertysuitability['wheelchair_users']=$this->input->post('inputWheelchair_users');
				$propertysuitability['website']=$this->input->post('inputWebsite');


				$propertylocation['user_id']=$this->sessions->getsessiondata('user_id');
				$propertylocation['property_id']=$property['property_id'];
				$propertylocation['continent']=$property_data['inputContinent'];

				//print_r($property_data);	
				if (array_key_exists('inputCountry', $property_data))
					$propertylocation['country']=$property_data['inputCountry'];
				

				if (array_key_exists('inputRegion', $property_data)) 
					$propertylocation['region']=$property_data['inputRegion'];
				

				if (array_key_exists('inputSubregion', $property_data)) 
					$propertylocation['subregion']=$property_data['inputSubregion'];
				

				if (array_key_exists('inputTown', $property_data)) 
					$propertylocation['town']=$property_data['inputTown'];
				

				if (array_key_exists('inputSuburb', $property_data))
					$propertylocation['suburb']=$property_data['inputSuburb'];
				

				$propertylocation['property_address']=$property_data['inputProperty_address'];
				$propertylocation['postcode']=$property_data['inputPostcode'];
				$propertylocation['region_description']=$property_data['inputRegion_description'];
				$propertylocation['area_description']=$property_data['inputArea_description'];
				$propertylocation['airport']=$property_data['inputAirport'];
				$propertylocation['ferry_port']=$property_data['inputFerryport'];
				$propertylocation['train_station']=$property_data['inputTrain_station'];
				$propertylocation['car']=$property_data['inputCar'];
				$propertylocation['how_to_get_there']=$property_data['inputHow_to_get_there'];

			
				$propertylocation['golf_course_within_15km']=$this->input->post('inputGolf_course_15mins_walk');				
				$propertylocation['golf_course_within_30km']=$this->input->post('inputGolf_course_30mins_drive');
				$propertylocation['tennis_court']=$this->input->post('inputTennis_court');
				$propertylocation['skiing']=$this->input->post('inputSkiing');
				$propertylocation['water_sport']=$this->input->post('inputWater_sports');
				$propertylocation['water_park']=$this->input->post('inputWater_park');
				$propertylocation['horse_riding']=$this->input->post('inputHorse_riding');
				$propertylocation['beach_front']=$this->input->post('inputBeach_front');
				$propertylocation['fishing']=$this->input->post('inputFishing');

				$propertylocation['nearest_beach']=$property_data['inputNearest_beach'];
				$propertylocation['nearest_shop']=$property_data['inputNearest_shops'];

				$propertylocation['walking_holiday']=$this->input->post('inputWalking_holidays');
				$propertylocation['cycling_holiday']=$this->input->post('inputCycling_holidays');
				$propertylocation['rural_countryside_retreats']=$this->input->post('inputRural_or_countryside_retreats');
				$propertylocation['beach_lakeside_relaxation']=$this->input->post('inputBeach_or_lakeside_relaxation');
				$propertylocation['city_breaks']=$this->input->post('inputCity_breaks');
				$propertylocation['winter_sun']=$this->input->post('inputWinter_sun');
				$propertylocation['night_life']=$this->input->post('inputNight_life');


				try{
						
					$propertydetails = $this->property_m->savePropertyDetail($property);
					$propertyfacility = $this->property_m->savePropertyFeatures($propertyfeatures);
					$propertysuitability = $this->property_m->savePropertySuitability($propertysuitability);
					$propertylocation = $this->property_m->savePropertyLocationDetails($propertylocation);

					//get current date
					$date = date("Y-m-d");
					//get end date of next 3 months
					$newdate = strtotime ( '+12 month' , strtotime ( $date ) ) ;
					$newdate = date ( 'Y-m-j' , $newdate );
					
					//get all the dates between two dates
					$start_date = date('Y-m-d', strtotime($date));
			        $end_date =  date('Y-m-d', strtotime($newdate));
			        $day = 86400;
			        $format = 'Y-m-d'; 
			        $sTime = strtotime($start_date); 
			        $eTime = strtotime($end_date); 
			        $numDays = round(($eTime - $sTime) / $day) + 1;  
			        $days = array(); 
			        $basic_pricing=array(); 
			        for ($d = 0; $d < $numDays; $d++) {  
			            $days[] = date($format, ($sTime + ($d * $day))); 
			            $date1=date('l', strtotime($days[$d]));
			           
				        if($date1 == "Monday" ){
				        	$basic_pricing[$d]['start_date']=$days[$d];

				        	//get the end date
				        	$add_days = 6;
							$end_date = date('Y-m-d',strtotime($days[$d]) + (24*3600*$add_days));
							$basic_pricing[$d]['end_date']=$end_date;

				            $basic_pricing[$d]['price']=000.000;
				            $basic_pricing[$d]['status']='Available';
				            $basic_pricing[$d]['property_id']=$property['property_id'];
				        	
				        }

				        /*if($date1 == "Friday" ){
				        	$basic_pricing[$d]['start_date']=$days[$d];

				        	//get the end date
				        	$add_days = 2;
							$end_date = date('Y-m-d',strtotime($days[$d]) + (24*3600*$add_days));
							$basic_pricing[$d]['end_date']=$end_date;


				            $basic_pricing[$d]['price']=000.000;
				            $basic_pricing[$d]['status']='Available';
				            $basic_pricing[$d]['property_id']=$property['property_id'];

 				        }*/
			        }	       

			       
					$res=$this->pricing_m->savePricingDetail($basic_pricing);
                    
					$response['response'] ='Saved';
						
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}
					  
		}  
				
		if(IS_AJAX) echo json_encode($response);
	}


	/**/
	public function updateProperty($id)
	{
		/*$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		
		
		$this->form_validation->set_rules('inputProperty_name', 'Property Name', 'required|trim');
		$this->form_validation->set_rules('inputMax_guest', 'Maximum guests', 'required|trim');
		$this->form_validation->set_rules('inputProperty_address', 'Property Address', 'required|trim');
		$this->form_validation->set_rules('inputPostcode', 'Postcode', 'required|trim|alphanumeric');


		
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{*/
			$property_data = $this->input->post(NULL, TRUE);
			$response['status']='success';	
			$this->load->model('property_m');

			if($response['status']=='success')
			{
				//format the data :-
				$property['property_name']=$property_data['inputProperty_name'];
				$property['property_type']=$property_data['inputProperty_type'];
				$property['max_guests']=$property_data['inputMax_guest'];
				$property['main_description']=$property_data['inputMain_description'];
				$property['search_result_desc']=$property_data['inputSearch_result'];
				
				$propertyfeatures['home_size']=$this->input->post('inputHome_size');
				$propertyfeatures['single_beds']=$this->input->post('inputSingle_bed',TRUE)==null ? 0 : 1;
				$propertyfeatures['family_bathrooms']=$this->input->post('inputFamily_bathrooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['dining_seats']=$this->input->post('inputDining_seats',TRUE)==null ? 0 : 1;
				$propertyfeatures['double_beds']=$this->input->post('inputDouble_beds',TRUE)==null ? 0 : 1;
				$propertyfeatures['en_suites']=$this->input->post('inputEn_suites',TRUE)==null ? 0 : 1;
				$propertyfeatures['lounge_seats']=$this->input->post('inputLounge_seats',TRUE)==null ? 0 : 1;
				$propertyfeatures['sofa_beds']=$this->input->post('inputSofa_beds',TRUE)==null ? 0 : 1;
				$propertyfeatures['shower_bathrooms']=$this->input->post('inputShower_rooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['cots']=$this->input->post('inputCots',TRUE)==null ? 0 : 1;
				$propertyfeatures['total_bathrooms']=$this->input->post('inputTotal_bathrooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['cooker']=$this->input->post('inputCooker',TRUE)==null ? 0 : 1;
				$propertyfeatures['tv']=$this->input->post('inputTv',TRUE)==null ? 0 : 1;
				$propertyfeatures['log_fire']=$this->input->post('inputLog_fire',TRUE)==null ? 0 : 1;
				$propertyfeatures['fridge']=$this->input->post('inputFridge',TRUE)==null ? 0 : 1;
				$propertyfeatures['satellite_tv']=$this->input->post('inputSatellite_tv',TRUE)==null ? 0 : 1;
				$propertyfeatures['central_heating']=$this->input->post('inputCentral_heating',TRUE)==null ? 0 : 1;
				$propertyfeatures['freezer']=$this->input->post('inputFreezer',TRUE)==null ? 0 : 1;
				$propertyfeatures['video_player']=$this->input->post('inputVideo_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['ac']=$this->input->post('inputAc',TRUE)==null ? 0 : 1;
				$propertyfeatures['microwave']=$this->input->post('inputMicrowave',TRUE)==null ? 0 : 1;
				$propertyfeatures['dvd_player']=$this->input->post('inputDvd_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['linen']=$this->input->post('inputLinen',TRUE)==null ? 0 : 1;
				$propertyfeatures['toaster']=$this->input->post('inputToaster',TRUE)==null ? 0 : 1;
				$propertyfeatures['cd_playe']=$this->input->post('inputCd_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['towel']=$this->input->post('inputTowels',TRUE)==null ? 0 : 1;
				$propertyfeatures['kettle']=$this->input->post('inputKettle',TRUE)==null ? 0 : 1;
				$propertyfeatures['internet']=$this->input->post('InputInternet',TRUE)==null ? 0 : 1;
				$propertyfeatures['sauna']=$this->input->post('inputSauna',TRUE)==null ? 0 : 1;
				$propertyfeatures['dishwasher']=$this->input->post('inputDishwasher',TRUE)==null ? 0 : 1;
				$propertyfeatures['wifi']=$this->input->post('inputWifi',TRUE)==null ? 0 : 1;
				$propertyfeatures['gym']=$this->input->post('inputGym',TRUE)==null ? 0 : 1;
				$propertyfeatures['washing_machine']=$this->input->post('inputWashing_machine',TRUE)==null ? 0 : 1;
				$propertyfeatures['telephone']=$this->input->post('inputTelephone',TRUE)==null ? 0 : 1;
				$propertyfeatures['table_tennis']=$this->input->post('inputTable_tennis',TRUE)==null ? 0 : 1;
				$propertyfeatures['clothes_dryer']=$this->input->post('inputClothes_dryer',TRUE)==null ? 0 : 1;
				$propertyfeatures['fax']=$this->input->post('inputFax_machine',TRUE)==null ? 0 : 1;
				$propertyfeatures['pool_or_snooker']=$this->input->post('inputPool_or_snooker_table',TRUE)==null ? 0 : 1;
				$propertyfeatures['iron']=$this->input->post('inputIron',TRUE)==null ? 0 : 1;
				$propertyfeatures['hair_dryer']=$this->input->post('inputHair_dryer',TRUE)==null ? 0 : 1;
				$propertyfeatures['games_room']=$this->input->post('inputGames_room',TRUE)==null ? 0 : 1;
				$propertyfeatures['high_chair']=$this->input->post('inputHigh_chair',TRUE)==null ? 0 : 1;
				$propertyfeatures['safe']=$this->input->post('inputSafe',TRUE)==null ? 0 : 1;
				$propertyfeatures['staffed_property']=$this->input->post('inputStaffed_property',TRUE)==null ? 0 : 1;
				$propertyfeatures['cleaning_services']=$this->input->post('inputCleaning_services',TRUE)==null ? 0 : 1;
				$propertyfeatures['ironing_board']=$this->input->post('inputIroning_board',TRUE)==null ? 0 : 1;
				$propertyfeatures['fan']=$this->input->post('inputFan',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_outdoor_pool_heated']=$this->input->post('inputShared_outdoor_pool_heated',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_tennis_court']=$this->input->post('inputShared_tennis_court',TRUE)==null ? 0 : 1;
				$propertyfeatures['solarium_or_roof_terrace']=$this->input->post('inputSolarium_or_roof_terrace',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_outdoor_pool_unheated']=$this->input->post('inputShared_outdoor_pool_unheated',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_tennis_court']=$this->input->post('inputPrivate_tennis_court',TRUE)==null ? 0 : 1;
				$propertyfeatures['balcony_or_terrace']=$this->input->post('inputBalancy_or_terrace',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_outdoor_pool_heated']=$this->input->post('inputPrivate_outdoor_pool_heated',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_garden']=$this->input->post('inputShared_garden',TRUE)==null ? 0 : 1;
				$propertyfeatures['sea_view']=$this->input->post('inputSea_view',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_outdoor_pool_unheated']=$this->input->post('inputPrivate_outdoor_pool_unheated',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_garden']=$this->input->post('inputPrivate_garden',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_fishinglake_or_river']=$this->input->post('inputPrivate_fishinglake_or_river',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_indoor_pool']=$this->input->post('inputPrivate_indoor_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['climbing_frame']=$this->input->post('inputClimbing_frame',TRUE)==null ? 0 : 1;
				$propertyfeatures['boat']=$this->input->post('inputBoat',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_indoor_pool']=$this->input->post('inputShared_indoor_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['swing_set']=$this->input->post('inputSwing_set',TRUE)==null ? 0 : 1;
				$propertyfeatures['bicycles']=$this->input->post('inputBicycle',TRUE)==null ? 0 : 1;
				$propertyfeatures['childrens_pool']=$this->input->post('inputChildrens_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['trampoline']=$this->input->post('inputTrampoline',TRUE)==null ? 0 : 1;
				$propertyfeatures['parking']=$this->input->post('inputParking',TRUE)==null ? 0 : 1;
				$propertyfeatures['jacuzzi_or_hot_tub']=$this->input->post('inputJacuzzi_or_hot_tub',TRUE)==null ? 0 : 1;
				$propertyfeatures['secure_parking']=$this->input->post('inputSecure_parking',TRUE)==null ? 0 : 1;

				$propertysuitability['long_term_lets']=$this->input->post('inputLong_term_lets',TRUE)==null ? 0 : 1;
				$propertysuitability['corporate_bookings']=$this->input->post('inputCorporate_bookings',TRUE)==null ? 0 : 1;
				$propertysuitability['house_swap']=$this->input->post('inputHouse_swap',TRUE)==null ? 0 : 1;
				$propertysuitability['short_breaks']=$this->input->post('inputShort_breaks',TRUE)==null ? 0 : 1;
				$propertysuitability['hen_or_stack_breaks']=$this->input->post('inputHen_or_stag_breaks',TRUE)==null ? 0 : 1;
				$propertysuitability['children']=$property_data['inputChildren'];
				$propertysuitability['pets']=$property_data['inputPets'];
				$propertysuitability['smokers']=$property_data['inputSmokers'];
				$propertysuitability['restricted_mobility']=$property_data['inputRestricted_mobility'];
				$propertysuitability['wheelchair_users']=$property_data['inputWheelchair_users'];
				$propertysuitability['website']=$property_data['inputWebsite'];

				$propertylocation['continent']=$property_data['inputContinent'];
				$propertylocation['country']=$property_data['inputCountry'];
				$propertylocation['region']=$property_data['inputRegion'];
				$propertylocation['subregion']=$property_data['inputSubregion'];
				$propertylocation['town']=$property_data['inputTown'];
				$propertylocation['suburb']=$property_data['inputSuburb'];
				$propertylocation['property_address']=$property_data['inputProperty_address'];
				$propertylocation['postcode']=$property_data['inputPostcode'];
				$propertylocation['region_description']=$property_data['inputRegion_description'];
				$propertylocation['area_description']=$property_data['inputArea_description'];
				$propertylocation['airport']=$property_data['inputAirport'];
				$propertylocation['ferry_port']=$property_data['inputFerryport'];
				$propertylocation['train_station']=$property_data['inputTrain_station'];
				$propertylocation['car']=$property_data['inputCar'];
				$propertylocation['how_to_get_there']=$property_data['inputHow_to_get_there'];

				$propertylocation['golf_course_within_15km']=$this->input->post('inputGolf_course_15mins_walk',TRUE)==null ? 0 : 1;
				$propertylocation['golf_course_within_30km']=$this->input->post('inputGolf_course_30mins_drive',TRUE)==null ? 0 : 1;
				$propertylocation['tennis_court']=$this->input->post('inputTennis_court',TRUE)==null ? 0 : 1;
				$propertylocation['skiing']=$this->input->post('inputSkiing',TRUE)==null ? 0 : 1;
				$propertylocation['water_sport']=$this->input->post('inputWater_sports',TRUE)==null ? 0 : 1;
				$propertylocation['water_park']=$this->input->post('inputWater_park',TRUE)==null ? 0 : 1;
				$propertylocation['horse_riding']=$this->input->post('inputHorse_riding',TRUE)==null ? 0 : 1;
				$propertylocation['beach_front']=$this->input->post('inputBeach_front',TRUE)==null ? 0 : 1;
				$propertylocation['fishing']=$this->input->post('inputFishing',TRUE)==null ? 0 : 1;

				$propertylocation['nearest_beach']=$property_data['inputNearest_beach'];
				$propertylocation['nearest_shop']=$property_data['inputNearest_shops'];

				$propertylocation['walking_holiday']=$this->input->post('inputWalking_holidays',TRUE)==null ? 0 : 1;
				$propertylocation['cycling_holiday']=$this->input->post('inputCycling_holidays',TRUE)==null ? 0 : 1;
				$propertylocation['rural_countryside_retreats']=$this->input->post('inputRural_or_countryside_retreats',TRUE)==null ? 0 : 1;
				$propertylocation['beach_lakeside_relaxation']=$this->input->post('inputBeach_or_lakeside_relaxation',TRUE)==null ? 0 : 1;
				$propertylocation['city_breaks']=$this->input->post('inputCity_breaks',TRUE)==null ? 0 : 1;
				$propertylocation['winter_sun']=$this->input->post('inputWinter_sun',TRUE)==null ? 0 : 1;
				$propertylocation['night_life']=$this->input->post('inputNight_life',TRUE)==null ? 0 : 1;
				


				try{
					$where = "property_id='".$id."'";	
					
					$propertydetails = $this->property_m->updatePropertyDetails($property,$where);
					$propertyfeatures = $this->property_m->updatePropertyFeatures($propertyfeatures,$where);
					$propertysuitability = $this->property_m->updatePropertySuitability($propertysuitability,$where);
					$propertylocation = $this->property_m->updateLocationDetails($propertylocation,$where);
					//$response['data']['id']=$res;
							
					$response['response'] ='Updated';
							
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}
					  
		//} 
		if(IS_AJAX) echo json_encode($response);
	}



	/*
		edit form for property details page
	*/
	public function editPropertyDetails($id)
	{
		
		$this->load->model('property_m');
		
		$misc['navigation']['tab'] = 'Property Details';

		$response ['status']='success';
		
		try
		{
			
			$data = $this->property_m->getPropertyDetails($id);
			$response ['response']['records'] = $data->result_array();
			
		}catch(Exception $e){
			
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		
		$this->load->view('app/edit_propertydetails',array_merge($response,$misc));
	}



	/*
		Update the edit form details of a property into the database
	*/
	public function updatePropertyDetails($id)
	{
			
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		$property_data = $this->input->post(NULL, TRUE);
		
		$this->form_validation->set_rules('inputProperty_name', 'Property Name', 'required|trim');
		$this->form_validation->set_rules('inputMax_guest', 'Maximum guests', 'required|trim');

		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{
				
			$this->load->model('property_m');

			if($response['status']=='success')
			{
				//format the data :-
				$property['property_name']=$property_data['inputProperty_name'];
				$property['property_type']=$property_data['inputProperty_type'];
				$property['max_guests']=$property_data['inputMax_guest'];
				$property['main_description']=$property_data['inputMain_description'];
				$property['search_result_desc']=$property_data['inputSearch_result'];
				
				$propertyfeatures['home_size']=$this->input->post('inputHome_size');
				$propertyfeatures['single_beds']=$this->input->post('inputSingle_bed',TRUE)==null ? 0 : 1;
				$propertyfeatures['family_bathrooms']=$this->input->post('inputFamily_bathrooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['dining_seats']=$this->input->post('inputDining_seats',TRUE)==null ? 0 : 1;
				$propertyfeatures['double_beds']=$this->input->post('inputDouble_beds',TRUE)==null ? 0 : 1;
				$propertyfeatures['en_suites']=$this->input->post('inputEn_suites',TRUE)==null ? 0 : 1;
				$propertyfeatures['lounge_seats']=$this->input->post('inputLounge_seats',TRUE)==null ? 0 : 1;
				$propertyfeatures['sofa_beds']=$this->input->post('inputSofa_beds',TRUE)==null ? 0 : 1;
				$propertyfeatures['shower_bathrooms']=$this->input->post('inputShower_rooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['cots']=$this->input->post('inputCots',TRUE)==null ? 0 : 1;
				$propertyfeatures['total_bathrooms']=$this->input->post('inputTotal_bathrooms',TRUE)==null ? 0 : 1;
				$propertyfeatures['cooker']=$this->input->post('inputCooker',TRUE)==null ? 0 : 1;
				$propertyfeatures['tv']=$this->input->post('inputTv',TRUE)==null ? 0 : 1;
				$propertyfeatures['log_fire']=$this->input->post('inputLog_fire',TRUE)==null ? 0 : 1;
				$propertyfeatures['fridge']=$this->input->post('inputFridge',TRUE)==null ? 0 : 1;
				$propertyfeatures['satellite_tv']=$this->input->post('inputSatellite_tv',TRUE)==null ? 0 : 1;
				$propertyfeatures['central_heating']=$this->input->post('inputCentral_heating',TRUE)==null ? 0 : 1;
				$propertyfeatures['freezer']=$this->input->post('inputFreezer',TRUE)==null ? 0 : 1;
				$propertyfeatures['video_player']=$this->input->post('inputVideo_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['ac']=$this->input->post('inputAc',TRUE)==null ? 0 : 1;
				$propertyfeatures['microwave']=$this->input->post('inputMicrowave',TRUE)==null ? 0 : 1;
				$propertyfeatures['dvd_player']=$this->input->post('inputDvd_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['linen']=$this->input->post('inputLinen',TRUE)==null ? 0 : 1;
				$propertyfeatures['toaster']=$this->input->post('inputToaster',TRUE)==null ? 0 : 1;
				$propertyfeatures['cd_playe']=$this->input->post('inputCd_player',TRUE)==null ? 0 : 1;
				$propertyfeatures['towel']=$this->input->post('inputTowels',TRUE)==null ? 0 : 1;
				$propertyfeatures['kettle']=$this->input->post('inputKettle',TRUE)==null ? 0 : 1;
				$propertyfeatures['internet']=$this->input->post('InputInternet',TRUE)==null ? 0 : 1;
				$propertyfeatures['sauna']=$this->input->post('inputSauna',TRUE)==null ? 0 : 1;
				$propertyfeatures['dishwasher']=$this->input->post('inputDishwasher',TRUE)==null ? 0 : 1;
				$propertyfeatures['wifi']=$this->input->post('inputWifi',TRUE)==null ? 0 : 1;
				$propertyfeatures['gym']=$this->input->post('inputGym',TRUE)==null ? 0 : 1;
				$propertyfeatures['washing_machine']=$this->input->post('inputWashing_machine',TRUE)==null ? 0 : 1;
				$propertyfeatures['telephone']=$this->input->post('inputTelephone',TRUE)==null ? 0 : 1;
				$propertyfeatures['table_tennis']=$this->input->post('inputTable_tennis',TRUE)==null ? 0 : 1;
				$propertyfeatures['clothes_dryer']=$this->input->post('inputClothes_dryer',TRUE)==null ? 0 : 1;
				$propertyfeatures['fax']=$this->input->post('inputFax_machine',TRUE)==null ? 0 : 1;
				$propertyfeatures['pool_or_snooker']=$this->input->post('inputPool_or_snooker_table',TRUE)==null ? 0 : 1;
				$propertyfeatures['iron']=$this->input->post('inputIron',TRUE)==null ? 0 : 1;
				$propertyfeatures['hair_dryer']=$this->input->post('inputHair_dryer',TRUE)==null ? 0 : 1;
				$propertyfeatures['games_room']=$this->input->post('inputGames_room',TRUE)==null ? 0 : 1;
				$propertyfeatures['high_chair']=$this->input->post('inputHigh_chair',TRUE)==null ? 0 : 1;
				$propertyfeatures['safe']=$this->input->post('inputSafe',TRUE)==null ? 0 : 1;
				$propertyfeatures['staffed_property']=$this->input->post('inputStaffed_property',TRUE)==null ? 0 : 1;
				$propertyfeatures['cleaning_services']=$this->input->post('inputCleaning_services',TRUE)==null ? 0 : 1;
				$propertyfeatures['ironing_board']=$this->input->post('inputIroning_board',TRUE)==null ? 0 : 1;
				$propertyfeatures['fan']=$this->input->post('inputFan',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_outdoor_pool_heated']=$this->input->post('inputShared_outdoor_pool_heated',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_tennis_court']=$this->input->post('inputShared_tennis_court',TRUE)==null ? 0 : 1;
				$propertyfeatures['solarium_or_roof_terrace']=$this->input->post('inputSolarium_or_roof_terrace',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_outdoor_pool_unheated']=$this->input->post('inputShared_outdoor_pool_unheated',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_tennis_court']=$this->input->post('inputPrivate_tennis_court',TRUE)==null ? 0 : 1;
				$propertyfeatures['balcony_or_terrace']=$this->input->post('inputBalancy_or_terrace',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_outdoor_pool_heated']=$this->input->post('inputPrivate_outdoor_pool_heated',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_garden']=$this->input->post('inputShared_garden',TRUE)==null ? 0 : 1;
				$propertyfeatures['sea_view']=$this->input->post('inputSea_view',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_outdoor_pool_unheated']=$this->input->post('inputPrivate_outdoor_pool_unheated',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_garden']=$this->input->post('inputPrivate_garden',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_fishinglake_or_river']=$this->input->post('inputPrivate_fishinglake_or_river',TRUE)==null ? 0 : 1;
				$propertyfeatures['private_indoor_pool']=$this->input->post('inputPrivate_indoor_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['climbing_frame']=$this->input->post('inputClimbing_frame',TRUE)==null ? 0 : 1;
				$propertyfeatures['boat']=$this->input->post('inputBoat',TRUE)==null ? 0 : 1;
				$propertyfeatures['shared_indoor_pool']=$this->input->post('inputShared_indoor_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['swing_set']=$this->input->post('inputSwing_set',TRUE)==null ? 0 : 1;
				$propertyfeatures['bicycles']=$this->input->post('inputBicycle',TRUE)==null ? 0 : 1;
				$propertyfeatures['childrens_pool']=$this->input->post('inputChildrens_pool',TRUE)==null ? 0 : 1;
				$propertyfeatures['trampoline']=$this->input->post('inputTrampoline',TRUE)==null ? 0 : 1;
				$propertyfeatures['parking']=$this->input->post('inputParking',TRUE)==null ? 0 : 1;
				$propertyfeatures['jacuzzi_or_hot_tub']=$this->input->post('inputJacuzzi_or_hot_tub',TRUE)==null ? 0 : 1;
				$propertyfeatures['secure_parking']=$this->input->post('inputSecure_parking',TRUE)==null ? 0 : 1;

				$propertysuitability['long_term_lets']=$this->input->post('inputLong_term_lets',TRUE)==null ? 0 : 1;
				$propertysuitability['corporate_bookings']=$this->input->post('inputCorporate_bookings',TRUE)==null ? 0 : 1;
				$propertysuitability['house_swap']=$this->input->post('inputHouse_swap',TRUE)==null ? 0 : 1;
				$propertysuitability['short_breaks']=$this->input->post('inputShort_breaks',TRUE)==null ? 0 : 1;
				$propertysuitability['hen_or_stack_breaks']=$this->input->post('inputHen_or_stag_breaks',TRUE)==null ? 0 : 1;
				$propertysuitability['children']=$property_data['inputChildren'];
				$propertysuitability['pets']=$property_data['inputPets'];
				$propertysuitability['smokers']=$property_data['inputSmokers'];
				$propertysuitability['restricted_mobility']=$property_data['inputRestricted_mobility'];
				$propertysuitability['wheelchair_users']=$property_data['inputWheelchair_users'];
				$propertysuitability['website']=$property_data['inputWebsite'];


				try{
					$where = "property_id='".$id."'";	
					
					$propertydetails = $this->property_m->updatePropertyDetails($property,$where);
					$propertyfeatures = $this->property_m->updatePropertyFeatures($propertyfeatures,$where);
					$propertysuitability = $this->property_m->updatePropertySuitability($propertysuitability,$where);
					//$response['data']['id']=$res;
							
					$response['response'] ='Updated';
							
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}
					  
		} 
		if(IS_AJAX) echo json_encode($response);
	}	

	
	/*
		edit form for location details page
	*/
	public function editLocationDetails($id)
	{
		$this->load->model('property_m');

		$misc['navigation']['tab'] = 'Location Details';

		$response ['status']='success';
		
		try{
			
			$data = $this->property_m->getLocationDetails($id);
			$response ['response']['records'] = $data->result_array();
			
		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		
		$this->load->view('app/edit_locationdetails',array_merge($response,$misc ));
	}


	
	/*
		Form to add a new housekeeper.
	*/
	public function addhousekeeper()
	{
		$misc['navigation']['tab'] = 'Housekeeper';
		$this->load->view('app/housekeepers_addform',array_merge($misc));
	}

	/*
		save the housekeeper data into the databaase.
	*/
	public function saveHousekeeperDetails()
	{
		
		//Validations		
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		$details=$this->input->post(NULL,TRUE);

		$this->form_validation->set_rules('inputCompany_name', 'Company Name', 'required|trim');
		$this->form_validation->set_rules('inputEmail', 'Email Address', 'required|trim');
		$this->form_validation->set_rules('inputPassword', 'Password', 'required|trim');

		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{
				
			$this->load->model('users_m');

			if($response['status']=='success')
			{
		
				$this->load->library('sessions');
				$user_id=$this->sessions->getsessiondata('id');
		
				$housekeeper_details['user_id']=$user_id;
				$housekeeper_details['profile']='Housekeeper';
				$housekeeper_details['company_name']=$details['inputCompany_name'];
				//$housekeeper_details['contact']=$details['inputContact'];
				$housekeeper_details['email']=$details['inputEmail'];
				$housekeeper_details['telephone']=$details['inputTelephone'];
				$housekeeper_details['mobile']=$details['inputMobile'];
				$housekeeper_details['address']=$details['inputAddress'];
				$housekeeper_details['password']=md5($details['inputPassword']);
				$housekeeper_details['notes']=$details['inputNotes'];
				$housekeeper_details['email_subject']=$details['inputEmail_subject'];
				$housekeeper_details['email_body']=$details['inputEmail_body'];
				$housekeeper_details['booking_reference']=$details['inputBooking_reference'];
				$housekeeper_details['arrival_time']=$details['inputArrival_time'];
				$housekeeper_details['departure_time']=$details['inputDeparture_time'];
				$housekeeper_details['customers_telephone_no']=$details['inputCustomers_telephone_no'];
				$housekeeper_details['customers_mobile_telephone_no']=$details['inputCustomers_mobile_telephone_no'];
				$housekeeper_details['customers_town_or_city']=$details['inputCustomers_town_or_city'];
				$housekeeper_details['customers_comments']=$details['inputCustomers_comments'];
				$housekeeper_details['customers_country']=$details['inputCustomers_country'];

				try{
					$res=$this->users_m->create($housekeeper_details);
					
					//on success send email :
					$this->load->library('email');
					$this->email->from('support@bookingbrain.co.uk', 'booking brain');
					$this->email->to($details['inputEmail']);
					$this->email->set_mailtype('html');
					$this->email->subject($details['inputEmail_subject']);	
					$this->email->message($this->load->view('app/emails/HousekeeperRegistration_layout',$details,true));
					$this->email->send();

					$response['response'] ='An email has been sent.';

				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}

			}
		}
		
		if(IS_AJAX) echo json_encode($response);
	}

	/*
		House keeper account activation
	*/
	public function accountactivation()
	{
		
		$user['email']=$_GET['email'];
		$response['status']='success';
		
		try{
			$this->load->model('users_m');
		  	$updateData=array('isactive'=>1);
		  	$where = "email='".$user['email']."'"; 				
		  	$data = $this->users_m->update($updateData,$where);
		
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		
		if($response['status']=='success')
		    $this->load->view('app/login');	
				
	}

	/*
		Delete property from db
	*/
	//Delete method
	public function delete()
	{
		$this->load->model('property_m');
		$response['status']='success';
		try{			  
			$this->property_m->delete($_GET['id']);

			$response['status']='success';
			$response['response']='Property is Deleted';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
	}

	/*
		Uploading photos for particular id.
	*/
	public function propertyPhotos($id)
	{
			
		$this->load->helper('form');
		$this->load->model('property_m');

		$misc['navigation']['tab'] = 'Photos';
		
		$response ['status']='success';
		try{

			$data = $this->property_m->getPropertyPhotos($id);
			$response ['response']['records'] = $data->result_array();
			$response ['response']['size'] =$data->num_rows();
			
		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		
		$error = array('error'=>'');
		$this->load->view('app/property_photos',array_merge($response,$error,$misc));
	}

	/*
		save the uploaded file into the database.
	*/
	public function do_upload($id)
	{
		$name_array = array(); 
		$count = count($_FILES['userfile']['size']); 
		foreach($_FILES as $key=>$value) 
		
		for($s=0; $s<=$count-1; $s++) { 
			$_FILES['userfile']['name']=$value['name'][$s]; 
			$_FILES['userfile']['type'] = $value['type'][$s]; 
			$_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s]; 
			$_FILES['userfile']['error'] = $value['error'][$s]; 
			$_FILES['userfile']['size'] = $value['size'][$s]; 
			$config['upload_path']='./property_images/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg'; 
			$config['max_size']	= '3000'; 
			$config['max_width'] = '1024'; 
			$config['max_height'] = '768'; 
			$this->load->library('upload', $config); 
			
			 if(!$this->upload->do_upload()){
			$error = array('error'=>$this->upload->display_errors());
			$this->load->view('app/property_photos',$error);
			}else{
			//$file_data =$this->upload->data();
			$this->upload->do_upload(); 
			$file_data =$this->upload->data();
			
			$data['property_id']=$id;

		
			$data['image']=base_url().'property_images/'. $file_data['file_name'];
                
			//add the image link into the database
			$this->load->model('property_m');
			
			$checkIndex=$this->property_m->getAvailiable_index($id);
			$index1 = $checkIndex->result_array();
			$index2=$checkIndex->num_rows();
     
            $pos = array();
			for($i=0;$i<$index2;$i++){

		         $pos[$i]=$index1[$i]['position'];

			}
			if($index2 == null) {
                   $max = 0;
			} else {
			  $max = max($pos) + 1;	
			}
			
			$data['position']= $max;
			
			$checkAvaliable_photo=$this->property_m->checkAvailiable_photo($id);
			$image = $checkAvaliable_photo->result_array();
			$image_size=$checkAvaliable_photo->num_rows();

            //print_r($data);
			if($image_size == 0){
				$where = "property_id='".$id."'";
				$img_link = $this->property_m->updateProperty_photo($data,$where);
				$img_link = $this->property_m->saveProperty_photo($data);
			}else{	
				$img_link = $this->property_m->saveProperty_photo($data);
			}
			
			}
            
		} 
		$this->propertyPhotos($id);
		
	
	}


	public function updatePropertyPhoto()
	{
		$prop_id=$_GET['id'];
		$data_id = explode(',',$_GET['data_id']);
		$order = explode(',',$_GET['order']);
        
        $this->load->model('property_m');
        $img_link = $this->property_m->updatePropertyPhotos($prop_id,$data_id,$order);

		$this->load->helper('form');
	

		$data = $this->property_m->getPropertyPhotosDesc($prop_id);
		$response ['response']['records'] = $data->result_array();
		$response ['response']['size'] =$data->num_rows();
			
        $error = array('error'=>'');
		$misc['navigation']['tab'] = 'Update Property';

		$response ['status']='success'; 
	   
		$this->load->view('app/property_photos',array_merge($response,$error,$misc));
       
	}

	public function updateLocationDetails($id)
	{
		//Validations		
		/*$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');
		
		
		
		$this->form_validation->set_rules('inputProperty_address', 'Property Address', 'required|trim');
		$this->form_validation->set_rules('inputPostcode', 'Postcode', 'required|trim|alphanumeric');
		$this->form_validation->set_rules('inputContinent', 'Continent', 'required|trim|alpha');
		$this->form_validation->set_rules('inputCountry', 'Country', 'required|trim|alpha');

		
		
		if ( $this->form_validation->run() == FALSE ){
			
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
			
		}else{*/
			$location_data = $this->input->post(NULL, TRUE);
			$response['status']='success';	
			$this->load->model('property_m');

			if($response['status']=='success')
			{
				//format the data :-
				$propertylocation['continent']=$location_data['inputContinent'];
				$propertylocation['country']=$location_data['inputCountry'];
				$propertylocation['region']=$location_data['inputRegion'];
				$propertylocation['subregion']=$location_data['inputSubregion'];
				$propertylocation['town']=$location_data['inputTown'];
				$propertylocation['suburb']=$location_data['inputSuburb'];
				$propertylocation['property_address']=$location_data['inputProperty_address'];
				$propertylocation['postcode']=$location_data['inputPostcode'];
				$propertylocation['region_description']=$location_data['inputRegion_description'];
				$propertylocation['area_description']=$location_data['inputArea_description'];
				$propertylocation['airport']=$location_data['inputAirport'];
				$propertylocation['ferry_port']=$location_data['inputFerryport'];
				$propertylocation['train_station']=$location_data['inputTrain_station'];
				$propertylocation['car']=$location_data['inputCar'];
				$propertylocation['how_to_get_there']=$location_data['inputHow_to_get_there'];

				$propertylocation['golf_course_within_15km']=$this->input->post('inputGolf_course_15mins_walk',TRUE)==null ? 0 : 1;
				$propertylocation['golf_course_within_30km']=$this->input->post('inputGolf_course_30mins_drive',TRUE)==null ? 0 : 1;
				$propertylocation['tennis_court']=$this->input->post('inputTennis_court',TRUE)==null ? 0 : 1;
				$propertylocation['skiing']=$this->input->post('inputSkiing',TRUE)==null ? 0 : 1;
				$propertylocation['water_sport']=$this->input->post('inputWater_sports',TRUE)==null ? 0 : 1;
				$propertylocation['water_park']=$this->input->post('inputWater_park',TRUE)==null ? 0 : 1;
				$propertylocation['horse_riding']=$this->input->post('inputHorse_riding',TRUE)==null ? 0 : 1;
				$propertylocation['beach_front']=$this->input->post('inputBeach_front',TRUE)==null ? 0 : 1;
				$propertylocation['fishing']=$this->input->post('inputFishing',TRUE)==null ? 0 : 1;

				$propertylocation['nearest_beach']=$location_data['inputNearest_beach'];
				$propertylocation['nearest_shop']=$location_data['inputNearest_shops'];

				$propertylocation['walking_holiday']=$this->input->post('inputWalking_holidays',TRUE)==null ? 0 : 1;
				$propertylocation['cycling_holiday']=$this->input->post('inputCycling_holidays',TRUE)==null ? 0 : 1;
				$propertylocation['rural_countryside_retreats']=$this->input->post('inputRural_or_countryside_retreats',TRUE)==null ? 0 : 1;
				$propertylocation['beach_lakeside_relaxation']=$this->input->post('inputBeach_or_lakeside_relaxation',TRUE)==null ? 0 : 1;
				$propertylocation['city_breaks']=$this->input->post('inputCity_breaks',TRUE)==null ? 0 : 1;
				$propertylocation['winter_sun']=$this->input->post('inputWinter_sun',TRUE)==null ? 0 : 1;
				$propertylocation['night_life']=$this->input->post('inputNight_life',TRUE)==null ? 0 : 1;
				

				try{
					
					$where = "property_id='".$id."'";	
					
					$res = $this->property_m->updateLocationDetails($propertylocation,$where);
				
					$response['data']['id']=$res;
					$response['response'] ='Updated';
							
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}
					  
		//} 
		
		if(IS_AJAX) echo json_encode($response);
	}


	 function get_Location(){
 		
 		$this->load->model('property_m');
		$content = $_POST['content1'];
        $table = $_POST['table']; 
        $column =$_POST['column'];
		$data = $this->property_m->getLocation($table,$column,$content);
		$response ['response']['records'] = $data->result_array();
		$response ['response']['size'] =$data->num_rows();
     
		$output[]=$response;
      
		echo json_encode($output);

	}

}