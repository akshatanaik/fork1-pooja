<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bookings extends CI_Controller {

	/*
		defining a construt method that is invoke to check whether the user has logged in
	*/
	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=bookings&err=login_required');
            }
        }

    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{
		$this->load->model('booking_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Booking Summary';
		try{

			$data = $this->booking_m->list_of_bookings();
			$response ['response']['records'] = $data;
        

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/bookings',$response);
	}

	public function  add_new_booking(){

		$this->load->model('property_m');



		$response ['status']='success';
		try{

			$data = $this->property_m->property_list();
			$response ['response']['properties'] = $data->result_array();


		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		

		if(!IS_AJAX)$this->load->view('app/new_booking',$response);
		else
			echo $this->load->view('app/booking_form_partial',$response,TRUE);	
	}

	public function  new_booking(){

		$this->load->model('property_m');

		$url_id=$_GET['property_id'];
		

		$response ['status']='success';
		try{

			$data = $this->property_m->property_details($url_id);
			$response ['response']['properties'] = $data->result_array();


		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		if(!IS_AJAX)$this->load->view('app/new_booking',$response);
		else
			echo $this->load->view('app/booking_form_partial',$response,TRUE);	
	}

	public function edit_booking(){
		
		if(!isset($_GET['id']) || $_GET['id']==''){
			$response ['status']='error';
			$response ['response']='Oops!Unable to edit..No record found';
			$this->load->view('app/new_booking',$response);
			return;
		}

		$this->load->model('booking_m');
		$this->load->model('property_m');
		$response ['status']='success';
		try{

			$booking = $this->booking_m->getBooking_for_edit($_GET['id']);
			
			$data = $this->property_m->property_list();
			$response ['response']['properties'] = $data->result_array();

			$response ['response']['booking'] = $booking;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		//print_r($response);
		$this->load->view('app/new_booking',$response);
	}

	//invoice
	public function invoice(){
		
		if(!isset($_GET['id']) || $_GET['id']==''){
			$response ['status']='error';
			$response ['response']='Oops!Unable to edit..No record found';
			$this->load->view('app/new_booking',$response);
			return;
		}

		$this->load->model('booking_m');
		$this->load->model('property_m');
		$response ['status']='success';
		try{

			$booking = $this->booking_m->getBooking_for_edit($_GET['id']);	
			
			$response ['response']['booking'] = $booking;

			$property_id=$booking['property_id'];

			$data = $this->property_m->getpropertyname($property_id);
			$response ['response']['properties'] = $data->result_array();			

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		//print_r($response);
		$this->load->view('app/booking_invoice',$response);
	}

	public function  transaction_details(){
		$this->load->view('app/booking_transaction_details');
	}

	public function  booking_logs(){
		$this->load->view('app/booking_logs');
	}

	public function  pdfgeneration(){		

		if(!isset($_GET['id']) || $_GET['id']==''){
			$response ['status']='error';
			$response ['response']='Oops!Unable to edit..No record found';
			$this->load->view('app/new_booking',$response);
			return;
		}

		$this->load->model('booking_m');
		$this->load->model('property_m');
		$response ['status']='success';
		try{

			$booking = $this->booking_m->getBooking_for_edit($_GET['id']);	
			
			$response ['response']['booking'] = $booking;

			$property_id=$booking['property_id'];

			$data = $this->property_m->getpropertyname($property_id);
			$response ['response']['properties'] = $data->result_array();			

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}		

		$this->load->view('app/pdfgeneration',$response);
	}

	 //Delete method
	public function delete()
	{
		$this->load->model('booking_m');
		$response['status']='success';
		try{			  
			$this->booking_m->delete($_GET['id']);

			$response['status']='success';
			$response['response']='Record is Deleted';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
	}

	/**
	 * 
	 */

	public function update_dates(){
		$booking_data = $this->input->post(NULL, TRUE);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('inputArival_date', 'Arival Date', 'required');
		$response['status']='success';

		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();
		}else{

			$this->load->model('booking_m');
			$arrival_date=$this->input->post('inputArival_date');
			$booking['arrival_date']=date('Y-m-d', strtotime($arrival_date));
			$departure_date=$this->input->post('inputDeparture_date');
			$booking['departure_date']=date('Y-m-d', strtotime($departure_date));
			$booking['property_id']=$booking_data['inputProperty_name'];
			$booking['id']=$this->input->post('id');
			try{

				$res = $this->booking_m->update($booking);
				$response['data']['id']=$res;
				$response['response'] ='Saved';

			}catch(Exception $e){
					//exception means some kind of error from the system
				$response['status']='error';
				$response['response']=$e->getMessage();
			}


		}
		if(IS_AJAX) echo json_encode($response);
	}

	public function save(){
		//Validations		
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');

		$booking_data = $this->input->post(NULL, TRUE);

		$this->form_validation->set_rules('inputProperty_name', 'Property Name', 'required|trim');
		$this->form_validation->set_rules('inputCustomer_name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('inputArival_date', 'Arival Date', 'required|callback__date_check');
		$this->form_validation->set_rules('inputDeparture_date', 'Departure Date', 'required|callback__date_check');
		//$this->form_validation->set_rules('inputArival_time', 'Arrival Time', 'required|trim');
		//$this->form_validation->set_rules('inputDeparture_time', 'Departure Time', 'required|trim');
		//$this->form_validation->set_rules('inputAdults', 'No of Adults', 'required');


    	$response['status']='success';

		if ( $this->form_validation->run() == FALSE ){

			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();


		}else{

			$this->load->model('booking_m');

			if($response['status']=='success')
			{

				$booking['property_id']=$booking_data['inputProperty_name'];
				$arrival_date=$this->input->post('inputArival_date');
				$booking['arrival_date']=date('Y-m-d', strtotime($arrival_date));
				$booking['customer_name']=$booking_data['inputCustomer_name'];
				$departure_date=$this->input->post('inputDeparture_date');
				$booking['departure_date']=date('Y-m-d', strtotime($departure_date));
				$booking['arival_time']=$booking_data['inputArival_time'];
				$booking['departure_time']=$booking_data['inputDeparture_time'];
				$booking['adults']=$booking_data['inputAdults'];
				//$booking['childrens']=$booking_data['inputChildrens'];
				$booking['children']=$booking_data['children'];
				$booking['infants']=$booking_data['inputInfants'];
				//$booking['nights']=$booking_data['inputNights'];
				$booking['nights']= $departure_date - $arrival_date;
				$booking['status']=$booking_data['inputStatus'];
				$booking['cost']=$booking_data['inputCost'];

				try{
					$todaysdate = date('Y-m-d');
					$datetime1 = new DateTime($booking['arrival_date']);
					$datetime2 = new DateTime($booking['departure_date']);

   					$difference=date_diff($datetime1,$datetime2);
    			    $diff1=$difference->days+1;

    			    
    			    $weekday = array(7);
					$weekday[0]=  "Sun";
					$weekday[1] = "Mon";
					$weekday[2] = "Tue";
					$weekday[3] = "Wed";
					$weekday[4] = "Thu";
					$weekday[5] = "Fri";
					$weekday[6] = "Sat";

					$res='';
					$date1 = date('D',strtotime($booking['arrival_date']));
					

					$days_ago = date('Y-m-d', strtotime('-4 days', strtotime($booking['arrival_date'])));
					

					$days_after = date('Y-m-d', strtotime('+2 days', strtotime($booking['arrival_date'])));
					
					if($date1 == "Fri" and $diff1==3)
					{
					$res=$this->booking_m->filter_shortbreak($booking['arrival_date'],$days_after);

					}
					else if($date1 == "Fri")
					{
						
					$res=$this->booking_m->filter_shortbreak($days_ago,$days_after);

					}
					else 
					{
					$res=$this->booking_m->filter_shortbreak($booking['arrival_date'],$booking['departure_date']);
				    }
                     
                    
					if($res != null)
					{
					 
                      $shortbreak=$res[0]['isShortbreak'];
                    }
                    else 
                    {
                    	
                    	$shortbreak = 0;
                    }
					if ($todaysdate > $booking['arrival_date'] || $todaysdate > $booking['departure_date']) {
					    throw new Exception('You cannot book for past dates.');
					    return FALSE;
					}
					else {
						$interval = $datetime1->diff($datetime2);
						$flag = 0;
						
						 if($shortbreak==1 && $date1 == "Fri" && $diff1 != 3)
						 {
						 	
						 	throw new Exception("you can book for 2 nights");
							exit();
						 }
						
						 if($shortbreak==1 && $date1 == "Mon" && $diff1 != 4)
						 {
						 	
						 	throw new Exception("you can book for 3 nights");
							exit();
						 }
						 
						 
							if(($shortbreak==0 && $date1 == "Fri" && $diff1 == 3) || ($shortbreak==0 && $date1 == "Mon" && $diff1 == 4))
								{
									
									
						 		 	$res = $this->booking_m->create($booking);
						 		 	$response['data']['id']=$res;
							        $response['response'] ='Saved';
						 		 	
						 		 }
								else
						 		 {
							if($interval->format('%R%a days') == '+6 days')
							{

							if($this->input->post('id')!=null && $this->input->post('id')!='')
							{
								if($booking['property_id']!=''){$id = $booking['property_id'];}	
								else $id=$this->input->post('id');
								$where = "property_id='".$id."' AND arrival_date='".$booking['arrival_date']."'" ;
								$res =$this->booking_m->update($booking,$where);
							}
							else{

							if($date1 != "Mon" && $date1 != "Fri")
							{
							throw new Exception("Booking should be either from monday or Friday");
							exit();
						 	}else
									$res = $this->booking_m->create($booking);
							}

							$response['data']['id']=$res;
							$response['response'] ='Saved';
						}else
						{
							throw new Exception('Booking should be minimum  7 days.');
							return FALSE;
						}
						
					}
					}
					
					
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}

		}
		if(IS_AJAX) echo json_encode($response);
	}
	//callback function to validate date.
	public function _date_check($date){
	    //$this->form_validation->set_message('_date_check', 'Is check in callback even being called?');
	    if (preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}+$/', $date))return TRUE;
		
		else
		{
			$this->form_validation->set_message('_date_check', 'The %s field must contain a valid date(xx-xx-xxxx).');
			return FALSE;
		}
	} 


	/*
		download the booking summary in excel format
	*/
	function download_to_excel()
	{
		$this->load->library("PHPExcel");
		$this->load->model('booking_m');
		$phpExcel = new PHPExcel();
		$prestasi = $phpExcel->setActiveSheetIndex(0);
		
		//merger
		$phpExcel->getActiveSheet()->mergeCells('A1:J1');
		
		//manage row hight
		$phpExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
		
		//style alignment
		$styleArray = array(
			'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,),
		);
		
		$phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);	
		$phpExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
		
		//border
		$styleArray1 = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		  )
		);
		
		//background
		$styleArray12 = array(
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'startcolor' => array(
					'rgb' => 'E3E3E3',
				),
			),
		);
		
		//freeepane
		$phpExcel->getActiveSheet()->freezePane('A3');
		
		//coloum width
		$phpExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4.1);
		$phpExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$phpExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$phpExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$phpExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
		$prestasi->setCellValue('A1', 'Booking Summary');
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray);
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray1);
		$phpExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($styleArray12);
		$prestasi->setCellValue('A2', 'Id');
		$prestasi->setCellValue('B2', 'Customer');
		$prestasi->setCellValue('C2', 'Adults');
		$prestasi->setCellValue('D2', 'Children');
		$prestasi->setCellValue('E2', 'Infants');
		$prestasi->setCellValue('F2', 'Arrival Date');
		$prestasi->setCellValue('G2', 'Departure Date');
		$prestasi->setCellValue('H2', 'Arrival Time');
		$prestasi->setCellValue('I2', 'Departure Time');
		$prestasi->setCellValue('J2', 'Property');
		$data = $this->booking_m->list_of_bookings();
		$no=0;
		$rowexcel = 2;
		
		foreach($data as $row)
		{
			$no++;
			$rowexcel++;
						$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'J'.$rowexcel)->applyFromArray($styleArray);
			$phpExcel->getActiveSheet()->getStyle('A'.$rowexcel,'J'.$rowexcel)->applyFromArray($styleArray1);
			$prestasi->setCellValue('A'.$rowexcel, $no);
			$prestasi->setCellValue('B'.$rowexcel, $row['customer_name']);
			$prestasi->setCellValue('C'.$rowexcel, $row['adults']);
			$prestasi->setCellValue('D'.$rowexcel, $row['children']);
			$prestasi->setCellValue('E'.$rowexcel, $row['infants']);
			$prestasi->setCellValue('F'.$rowexcel, $row['arrival_date']);
			$prestasi->setCellValue('G'.$rowexcel, $row['departure_date']);
			$prestasi->setCellValue('H'.$rowexcel, $row['arival_time']);
			$prestasi->setCellValue('I'.$rowexcel, $row['departure_time']);
			$prestasi->setCellValue('J'.$rowexcel, $row['property_name']);
		}
		$prestasi->setTitle('Booking Summary');
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=\"Booking Summary.xls\"");
		header("Cache-Control: max-age=0");
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, "Excel5");
		$objWriter->save("php://output");	

	}	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */