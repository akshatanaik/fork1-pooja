<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users extends CI_Controller {

	/*
		defining a construt method that is invoke to check whether the user has logged in
	*/
	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=users&err=login_required');
            }
        }

    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{
		$this->load->model('users_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Users';
		try{

			$data = $this->users_m->list_of_active_users();
			$response ['response']['records'] = $data;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/users',$response);
	}

	public function new_user(){
		$response ['status']='success';
		$response ['navigation']['tab'] = 'New User';

		$this->load->view('app/new_user',$response);
	}

	public function edit_user(){
		if(!isset($_GET['id']) || $_GET['id']==''){
			$response ['status']='error';
			$response ['response']='Oops!Unable to edit..No record found';
			$this->load->view('app/new_user',$response);
			return;
		}
		$this->load->model('users_m');
		$user = $this->users_m->getUser($_GET['id']);
		$response ['response']['user'] = $user;

		$response ['status']='success';
		$response ['navigation']['tab'] = 'User > Edit User';

		$this->load->view('app/new_user',$response);
	}

	public function save(){
	    $this->load->helper('password_helper');

    	$response['status']='success';


		$this->load->model('users_m');

		$user=$this->input->post(NULL, TRUE);


		if($this->input->post('id')!=null && $this->input->post('id')!=''){
				$user['id']=$this->input->post('id');
				$res =$this->users_m->update($user);
		}else{
				$password=get_random_password();
				$user['password']=md5($password);
				$user['activation_code']=uniqid ();
				$res =$this->users_m->create($user);

				//once a user is created send an email to the user with  temp password & to activate the account

				$this->load->library('email');
				$this->email->from('support@bookingbrain.co.uk', 'Booking brain');
				$this->email->to($user['email']);
				$this->email->set_mailtype('html');
				$this->email->subject('Welcome to the bookingbrain.co.uk');	
				$this->email->message($this->load->view('app/emailtemplates/registration_email',$user,true));
				$this->email->send();
		}

		if(IS_AJAX) echo json_encode($response);
	}

	public function deactivate_user()
	{
		$this->load->model('users_m');
		$response['status']='success';
		try{			  
			$this->users_m->deactivate_user($_GET['id']);

			$response['status']='success';
			$response['response']='Record is Deactivated';
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		  
		if(IS_AJAX) echo json_encode($response);
		
	}

	public function reset_password()
	{
		$response['status']='success';
		
		try{
			$this->load->helper('password_helper');
			$this->load->model('users_m');	
			
			$password=md5(get_random_password());	 
			$res=$this->users_m->reset_password($_GET['id'],$password);

			$this->load->library('email');
			$this->email->from('support@bookingbrain.co.uk', 'Booking brain');
			$this->email->to($_GET['email']);
			$this->email->set_mailtype('html');
			$this->email->subject('Welcome to the bookingbrain.co.uk');	
			$this->email->message($this->load->view('app/emailtemplates/resetpassword_email',$password,true));
			$this->email->send();

			$response['status']='success';
			$response['response'] ='An email has been sent.';
			
		}
		catch(Exception $e){
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		
		if(IS_AJAX) echo json_encode($response);	  
	}

	public function inactive_users()
	{
		$this->load->model('users_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Users';
		try{

			$data = $this->users_m->list_of_inactive_users();
			$response ['response']['records'] = $data;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/users',$response);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */