<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pricing extends CI_Controller {

	/*
		defining a construt method that is invoke to check whether the user has logged in
	*/
	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=pricing&err=login_required');
            }
        }

    }


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing';

		$this->load->model('property_m');
		
		$list_of_properties = $this->property_m->property_list();
		$response ['response']['records'] = $list_of_properties->result_array();

		$this->load->view('app/pricing',$response);
	}

	public function get_pricing()
	{
		$id=$_GET['property_id'];
		$availability=array();
		
		//load the model and get the pricing data from db and loop it for thhose records
		$this->load->model('pricing_m');
		$results = $this->pricing_m->get_pricing($id);
		$cleaning_days=$this->pricing_m->list_of_pricing($id);

		if(!empty($cleaning_days)){
			for($i =0; $i < count($results); $i++ ){

				//get the start date,endate and amount
				$startdate=$results[$i]['start_date'];
				$enddate=$results[$i]['end_date'];
				$weekrate=$results[$i]['price'];
				$status=$results[$i]['status'];
							
				//for given startdate and enddate get the dates in between
				$start_date = date('Y-m-d', strtotime($startdate));
				$end_date =  date('Y-m-d', strtotime($enddate));
				$day = 86400; // Day in seconds  
				$format = 'Y-m-d'; // Output format (see PHP date funciton)  
				$sTime = strtotime($start_date); // Start as time  
				$eTime = strtotime($end_date); // End as time  
				$numDays = round(($eTime - $sTime) / $day) + 1;  
				
				$days = array();  
				$array=array();
				
				for ($d = 0; $d < $numDays; $d++ ) {

					$days[] = date($format, ($sTime + ($d * $day))); 
	            	$date1=date('F', strtotime($days[$d]));
	            	$dayname=date('l', strtotime($days[$d]));

	   			    for($j =0; $j < count($cleaning_days); $j++ ){    
				        if($date1 == $cleaning_days[$j]['month']){			        	
				        	$array[] =array(date($format, ($sTime + ($d * $day))),$days['price']= $weekrate,$days['status']= $status,$days['month']=$cleaning_days[$j]['month'],$days['sun']=$cleaning_days[$j]['sun'],$days['mon']=$cleaning_days[$j]['mon'],$days['tues']=$cleaning_days[$j]['tues'],$days['wed']=$cleaning_days[$j]['wed'],$days['thur']=$cleaning_days[$j]['thur'] ,$days['fri']=$cleaning_days[$j]['fri'],$days['sat']=$cleaning_days[$j]['sat'],$days['dayname']=$dayname);			        	
						}
					}
				}

				foreach($array as $date){				
					$availability[$date[0]]['price']=$date[1];				
					$availability [$date[0]]['status']=strtolower($date[2]);
					$availability [$date[0]]['bind']=$date[2]=='Booked' ?0:1;				
					$availability[$date[0]]['month']=$date[3];
					$availability[$date[0]]['sunday']=$date[4];
					$availability[$date[0]]['mon']=$date[5];
					$availability[$date[0]]['tues']=$date[6];
					$availability[$date[0]]['wed']=$date[7];
					$availability[$date[0]]['thur']=$date[8];
					$availability[$date[0]]['fri']=$date[9];
					$availability[$date[0]]['sat']=$date[10];
					$availability[$date[0]]['dayname']=$date[11];
				}
				
			}
		}else{
			for($i =0; $i < count($results); $i++ ){
				//get the start date,endate and amount
				$startdate=$results[$i]['start_date'];
				$enddate=$results[$i]['end_date'];
				$weekrate=$results[$i]['price'];
				$status=$results[$i]['status'];
				
							
				//for given startdate and enddate get the dates in between
				$start_date = date('Y-m-d', strtotime($startdate));
				$end_date =  date('Y-m-d', strtotime($enddate));
				$day = 86400; // Day in seconds  
				$format = 'Y-m-d'; // Output format (see PHP date funciton)  
				$sTime = strtotime($start_date); // Start as time  
				$eTime = strtotime($end_date); // End as time  
				$numDays = round(($eTime - $sTime) / $day) + 1;  
				
				$days = array();  
				$array=array();
				
				for ($d = 0; $d < $numDays; $d++ ) {  
					$array[] =array($days[] = date($format, ($sTime + ($d * $day))),
					$days['price']= $weekrate,$days['status']= $status);
				}
				
				foreach($array as $date){				
					$availability[$date[0]]['price']=$date[1];				
					$availability[$date[0]]['status']=strtolower($date[2]);
					$availability[$date[0]]['bind']=$date[2]=='Booked' ?0:1;
				}
			}
		}
		
		echo json_encode($availability);
	}

	/* Added to display only updated pricing */
	function get_updated_pricing(){
		//for the method you will recive 
		$start=strftime('%Y-%m-%d',$_GET['start']);
		$end=strftime('%Y-%m-%d',$_GET['end']);
		$property=isset($_GET['property_id']) ? $_GET['property_id']:'';
		$this->load->model('pricing_m');
		$response ['status']='success';

		try{
			$result=$this->pricing_m->filter_pricing_by_date_property($start,$end,$property);
			$response ['response']['records']=$result;

			$shortbreak_price =$this->pricing_m->get_shortbreaks_price($property);
			$response ['response']['shortbreak_price']=$shortbreak_price;
			

			//print_r('==satrt==');print_r($start);
			//print_r('==end==');print_r($end);

			$pricing_settings =$this->pricing_m->get_pricing_settings($property);
			$response ['response']['pricing_settings']=$pricing_settings;
			
			//print_r($response ['response']['pricing_settings']);

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		IF(IS_AJAX) echo json_encode($response);
		
	}

	/* Added to display only updated pricing */
	/*function get_shortbreaks(){
		//for the method you will recive 
		$start=strftime('%Y-%m-%d',$_GET['start']);
		$end=strftime('%Y-%m-%d',$_GET['end']);
		$property=isset($_GET['property_id']) ? $_GET['property_id']:'';
		$this->load->model('pricing_m');
		$response ['status']='success';

		try{
			$result=$this->pricing_m->get_shortbreaks($property);
			$response ['response']['records']=$result;
		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}

		IF(IS_AJAX) echo json_encode($response);
		
	}*/
		
	//callback function to validate date.
	public function _date_check($date){
	    //$this->form_validation->set_message('_date_check', 'Is check in callback even being called?');
	    if (preg_match('/^[0-9]{2}-[0-9]{2}-[0-9]{4}+$/', $date))return TRUE;
		
		else
		{
			$this->form_validation->set_message('_date_check', 'The %s field must contain a valid date(xx-xx-xxxx).');
			return FALSE;
		}
	} 

	public function save_pricing()
	{	
		//Validations		
		$this->load->library('form_validation');
	
		//$pricing=$this->input->post(NULL,TRUE);

		$this->form_validation->set_rules('start_date', 'Start Date', 'required|trim|callback__date_check');
		$this->form_validation->set_rules('end_date', 'End Date', 'required|trim|callback__date_check');
		$this->form_validation->set_rules('price', 'Price', 'required|numeric|trim');

		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();			
		}else{
			try{
				$todaysdate = date('Y-m-d');
				$start_date=date('Y-m-d', strtotime($this->input->post('start_date')));
				$end_date=date('Y-m-d', strtotime($this->input->post('end_date')));
				$price=$this->input->post('price');
				$id=$this->input->post('propertyId');

				if ($todaysdate > $start_date || $todaysdate > $end_date) {
				    throw new Exception('You cannot select past dates.');
				    return FALSE;
				}else {
	        		$st_date1 = strtotime($start_date);
	        		$st_date2 = date("l", $st_date1);
	        		$st_date3 = strtolower($st_date2);

	        		$et_date1 = strtotime($end_date);
	        		$et_date2 = date("l", $et_date1);
	        		$et_date3 = strtolower($et_date2);
	        		//echo $date3;
	        		if(($st_date3 != "monday") ||  ($et_date3 != "sunday")){
	            		throw new Exception('Please select price from Monday to Sunday');
						return FALSE;
	        		} else {
	            		$response ['status']='success';
						//load the model and get the pricing data from db and loop it for thhose records
						$this->load->model('pricing_m');
						
						$results = $this->pricing_m->save_pricing($start_date,$end_date,$price,$id);
					}
				}
			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			}
		}
		if(IS_AJAX) echo json_encode($response);	

	}

	public function save_shortbreak_upd()
	{
		//Validations		
		$this->load->library('form_validation');
	
		//$pricing=$this->input->post(NULL,TRUE);

		$this->form_validation->set_rules('start_date', 'Start Date', 'trim');
		$this->form_validation->set_rules('end_date', 'End Date', 'trim');
		

		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();			
		}else{
			
			try{
				$todaysdate = date('Y-m-d');
				$start_date=date('Y-m-d', strtotime($this->input->post('start_date_sb')));
				$end_date=date('Y-m-d', strtotime($this->input->post('end_date_sb')));
				$id=$this->input->post('propertyId');
					
					if ($todaysdate > $start_date || $todaysdate > $end_date) {
						throw new Exception('You cannot select past dates.');
						return FALSE;
					}else {
						$st_date1 = strtotime($start_date);
						$st_date2 = date("l", $st_date1);
						$st_date3 = strtolower($st_date2);
	
						$et_date1 = strtotime($end_date);
						$et_date2 = date("l", $et_date1);
						$et_date3 = strtolower($et_date2);
						//echo $date3;
						if(($st_date3 != "monday") ||  ($et_date3 != "sunday")){
							throw new Exception('Please select shortbreak from Monday to Sunday');
							return FALSE;
						} else {
							$response ['status']='success';
							//load the model and get the pricing data from db and loop it for thhose records
							$this->load->model('pricing_m');
							
							$results = $this->pricing_m->save_shortbreak_upd($start_date,$end_date,$id);
						}
					}
			}catch(Exception $e){
				$response ['status']='error';
				$response ['response']=$e->getMessage();
			}
		}
		if(IS_AJAX) echo json_encode($response);	

	}

	public function pricinglist()
	{
		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Summary';
		$this->load->model('pricing_m');

		$response ['record'] =$_GET['property_id'];

		$data = $this->pricing_m->list_of_pricing($response ['record']);

		$shortbreak_data=$this->pricing_m->list_of_shortbreak($response ['record']);
		$response ['response']['shortbreak'] = $shortbreak_data;
		
		$response ['response']['records'] = $data;
		
		$this->load->view('app/pricing_summary',$response);
	}

	public function add_pricing()
	{
		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Settings';
		$response['method']='save_pricing_settings';
		
		$this->load->model('pricing_m');
		
		$response ['record'] =$_GET['property_id'];

		$data = $this->pricing_m->list_of_pricing($response ['record']);
		$shortbreak = $this->pricing_m->list_of_shortbreak($response ['record']);
		
		$response ['response']['records'] = $data;
		$response ['response']['shortbreak'] = $shortbreak;

		$this->load->view('app/pricing_settings',$response);
	}

	/*public function save_pricing_settings()
	{
		$data = $this->input->post(NULL, TRUE);	
		$response['status']='success';
	
		$this->load->model('pricing_m');

		try{
			//$response ['record'] =$_GET['property_id'];
			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$data['month'];
			$settings['weekday_start']=$data['weekday_start'];
			$settings['weekend_start']=$data['weekend_start'];	

			$data = $this->pricing_m->save_price_settings($settings);
			
			$response['response'] ='Save';
			$response['property_id'] =$_GET['property_id'];
				
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
			
		if(IS_AJAX) echo json_encode($response);
	}*/

	public function save_pricing_settings()
	{
		$data = $this->input->post(NULL, TRUE);	
		$response['status']='success';
	
		$this->load->model('pricing_m');

		try{
			//$response ['record'] =$_GET['property_id'];			
			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('jan');
			$settings['sun']=$this->input->post('jan_sun');
			$settings['mon']=$this->input->post('jan_mon');
			$settings['tues']=$this->input->post('jan_tues');
			$settings['wed']=$this->input->post('jan_wed');
			$settings['thur']=$this->input->post('jan_thur');
			$settings['fri']=$this->input->post('jan_fri');
			$settings['sat']=$this->input->post('jan_sat');	

			$data = $this->pricing_m->delete($_GET['property_id']);
			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('feb');
			$settings['sun']=$this->input->post('feb_sun');
			$settings['mon']=$this->input->post('feb_mon');
			$settings['tues']=$this->input->post('feb_tues');
			$settings['wed']=$this->input->post('feb_wed');
			$settings['thur']=$this->input->post('feb_thur');
			$settings['fri']=$this->input->post('feb_fri');
			$settings['sat']=$this->input->post('feb_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('march');
			$settings['sun']=$this->input->post('march_sun');
			$settings['mon']=$this->input->post('march_mon');
			$settings['tues']=$this->input->post('march_tues');
			$settings['wed']=$this->input->post('march_wed');
			$settings['thur']=$this->input->post('march_thur');
			$settings['fri']=$this->input->post('march_fri');
			$settings['sat']=$this->input->post('march_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('april');
			$settings['sun']=$this->input->post('april_sun');
			$settings['mon']=$this->input->post('april_mon');
			$settings['tues']=$this->input->post('april_tues');
			$settings['wed']=$this->input->post('april_wed');
			$settings['thur']=$this->input->post('april_thur');
			$settings['fri']=$this->input->post('april_fri');
			$settings['sat']=$this->input->post('april_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('may');
			$settings['sun']=$this->input->post('may_sun');
			$settings['mon']=$this->input->post('may_mon');
			$settings['tues']=$this->input->post('may_tues');
			$settings['wed']=$this->input->post('may_wed');
			$settings['thur']=$this->input->post('may_thur');
			$settings['fri']=$this->input->post('may_fri');
			$settings['sat']=$this->input->post('may_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('june');
			$settings['sun']=$this->input->post('june_sun');
			$settings['mon']=$this->input->post('june_mon');
			$settings['tues']=$this->input->post('june_tues');
			$settings['wed']=$this->input->post('june_wed');
			$settings['thur']=$this->input->post('june_thur');
			$settings['fri']=$this->input->post('june_fri');
			$settings['sat']=$this->input->post('june_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('july');
			$settings['sun']=$this->input->post('july_sun');
			$settings['mon']=$this->input->post('july_mon');
			$settings['tues']=$this->input->post('july_tues');
			$settings['wed']=$this->input->post('july_wed');
			$settings['thur']=$this->input->post('july_thur');
			$settings['fri']=$this->input->post('july_fri');
			$settings['sat']=$this->input->post('july_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('aug');
			$settings['sun']=$this->input->post('aug_sun');
			$settings['mon']=$this->input->post('aug_mon');
			$settings['tues']=$this->input->post('aug_tues');
			$settings['wed']=$this->input->post('aug_wed');
			$settings['thur']=$this->input->post('aug_thur');
			$settings['fri']=$this->input->post('aug_fri');
			$settings['sat']=$this->input->post('aug_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('sep');
			$settings['sun']=$this->input->post('sep_sun');
			$settings['mon']=$this->input->post('sep_mon');
			$settings['tues']=$this->input->post('sep_tues');
			$settings['wed']=$this->input->post('sep_wed');
			$settings['thur']=$this->input->post('sep_thur');
			$settings['fri']=$this->input->post('sep_fri');
			$settings['sat']=$this->input->post('sep_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('oct');
			$settings['sun']=$this->input->post('oct_sun');
			$settings['mon']=$this->input->post('oct_mon');
			$settings['tues']=$this->input->post('oct_tues');
			$settings['wed']=$this->input->post('oct_wed');
			$settings['thur']=$this->input->post('oct_thur');
			$settings['fri']=$this->input->post('oct_fri');
			$settings['sat']=$this->input->post('oct_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('nov');
			$settings['sun']=$this->input->post('nov_sun');
			$settings['mon']=$this->input->post('nov_mon');
			$settings['tues']=$this->input->post('nov_tues');
			$settings['wed']=$this->input->post('nov_wed');
			$settings['thur']=$this->input->post('nov_thur');
			$settings['fri']=$this->input->post('nov_fri');
			$settings['sat']=$this->input->post('nov_sat');	

			$data = $this->pricing_m->save_price_settings($settings);

			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$this->input->post('dec');
			$settings['sun']=$this->input->post('dec_sun');
			$settings['mon']=$this->input->post('dec_mon');
			$settings['tues']=$this->input->post('dec_tues');
			$settings['wed']=$this->input->post('dec_wed');
			$settings['thur']=$this->input->post('dec_thur');
			$settings['fri']=$this->input->post('dec_fri');
			$settings['sat']=$this->input->post('dec_sat');	

			$data = $this->pricing_m->save_price_settings($settings);		

			
			$response['response'] ='Saved';
			$response['property_id'] =$_GET['property_id'];
				
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
			
		if(IS_AJAX) echo json_encode($response);
	}

	/*public function edit_pricing_settings()
	{

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Settings';
		$response['method']='update_pricing_settings';
		
		$this->load->model('pricing_m');
		
		$response ['record'] =$_GET['property_id'];
		$response['month']=$_GET['month'];
		//var_dump($response['month']);

		$data = $this->pricing_m->get_pricing_settings($response ['record'],$response ['month']);
		
		$response ['response']['records'] = $data;
		//var_dump($response ['response']['records']);

		$this->load->view('app/pricing_settings',$response);
	}*/

	/*public function update_pricing_settings()
	{
		$data = $this->input->post(NULL, TRUE);	
		$response['status']='success';
	
		$this->load->model('pricing_m');

		try{
			//$response ['record'] =$_GET['property_id'];
			$settings['property_id']=$_GET['property_id'];
			$settings['month']=$data['month'];
			$settings['weekday_start']=$data['weekday_start'];
			$settings['weekend_start']=$data['weekend_start'];
			
			$where = "month='".$data['inputMonth']."'";
			$data = $this->pricing_m->update_pricing_settings($settings,$where);
			
			$response['response'] ='Updated';
			$response['property_id'] =$_GET['property_id'];
				
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
			
		if(IS_AJAX) echo json_encode($response);
	}*/

	/*public function add_shortbreak()
	{
		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Shortbreak';
		$response['method']='save_shortbreak';
		
		$this->load->model('pricing_m');
		
		$response ['record'] =$_GET['property_id'];

		$data = $this->pricing_m->list_of_shortbreak($response ['record']);
		
		$response ['response']['records'] = $data;

		$this->load->view('app/pricing_shortbreak',$response);
	}*/

	public function save_shortbreak()
	{	
		$this->load->library('form_validation');
	
		//$pricing=$this->input->post(NULL,TRUE);
		//echo('hi');
		$this->form_validation->set_rules('midweek_percentage', 'Midweek %', 'numeric|callback__percent_check');
		$this->form_validation->set_rules('weekend_percentage', 'Weekend %', 'numeric|callback__percent_check');
		$this->form_validation->set_rules('additional_charge', 'Additional Charge', 'numeric');
		$this->form_validation->set_rules('min_price', 'Minimum Price', 'numeric');
		$this->form_validation->set_rules('allow_short_breaks', '\'Days prior to arrival to always allow shortbreaks\'', 'numeric');


		$response['status']='success';
		
		if ( $this->form_validation->run() == FALSE ){
			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();			
		}else{
			$data = $this->input->post(NULL, TRUE);	
			$response['status']='success';
		
			$this->load->model('pricing_m');

			try{
				//$response ['record'] =$_GET['property_id'];
				$shortbreak['property_id']=$_GET['property_id'];
				$shortbreak['short_break_pricing_model']=$data['short_break_pricing_model'];
				$shortbreak['midweek_arrive']=$data['midweek_arrive'];
				$shortbreak['midweek_depart']=$data['midweek_depart'];
				$shortbreak['midweek_percentage']=$data['midweek_percentage'];
				$shortbreak['midweek_min_nights']=$data['midweek_min_nights'];
				$shortbreak['weekend_arrive']=$data['weekend_arrive'];
				$shortbreak['weekend_depart']=$data['weekend_depart'];
				$shortbreak['weekend_percentage']=$data['weekend_percentage'];
				$shortbreak['weekend_min_nights']=$data['weekend_min_nights'];	
				$shortbreak['additional_charge']=$data['additional_charge'];
				$shortbreak['min_price']=$data['min_price'];
				$shortbreak['allow_short_breaks']=$data['allow_short_breaks'];

				//Check if id is already present in db.
				$pricing_data = $this->pricing_m->list_of_prpId_in_pricing_shortbreaks($shortbreak['property_id']);
				if (in_array($shortbreak['property_id'], $pricing_data)){

					$where = "property_id='".$shortbreak['property_id']."'";
					$data = $this->pricing_m->update_shortbreaks($shortbreak,$where);
				}
				else{
					$data = $this->pricing_m->save_shortbreaks($shortbreak);
				}
				
				$response['response'] ='Saved';
				$response['property_id'] =$_GET['property_id'];
				
			}catch(Exception $e){
				//exception means some kind of error from the system
				$response['status']='error';
				$response['response']=$e->getMessage();
			}
		}	
		if(IS_AJAX) echo json_encode($response);
	}


	
	//callback function to validate percentage.
	public function _percent_check($num){
		
	   
	    if ($num <= 100)return TRUE;
		else
		{
			$this->form_validation->set_message('_percent_check', 'The %s field must contain a valid percentage.');
			return FALSE;
		}
	} 

	/*public function edit_shortbreak()
	{

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Pricing Shortbreak';
		$response['method']='update_shortbreak';
		
		$this->load->model('pricing_m');
		
		$response ['record'] =$_GET['property_id'];

		$data = $this->pricing_m->list_of_shortbreak($response ['record']);
		
		$response ['response']['records'] = $data;

		$this->load->view('app/pricing_shortbreak',$response);
	}

	public function update_shortbreak()
	{
		$data = $this->input->post(NULL, TRUE);	
		$response['status']='success';
	
		$this->load->model('pricing_m');

		try{
			//$response ['record'] =$_GET['property_id'];
			$shortbreak['property_id']=$_GET['property_id'];
			$shortbreak['short_break_pricing_model']=$data['short_break_pricing_model'];
			$shortbreak['midweek_arrive']=$data['midweek_arrive'];
			$shortbreak['midweek_depart']=$data['midweek_depart'];
			$shortbreak['midweek_percentage']=$data['midweek_percentage'];
			$shortbreak['midweek_min_nights']=$data['midweek_min_nights'];
			$shortbreak['weekend_arrive']=$data['weekend_arrive'];
			$shortbreak['weekend_depart']=$data['weekend_depart'];
			$shortbreak['weekend_percentage']=$data['weekend_percentage'];
			$shortbreak['weekend_min_nights']=$data['weekend_min_nights'];	
			$shortbreak['additional_charge']=$data['additional_charge'];
			$shortbreak['min_price']=$data['min_price'];
			$shortbreak['allow_short_breaks']=$data['allow_short_breaks'];

			$where = "property_id='".$shortbreak['property_id']."'";
			$data = $this->pricing_m->update_shortbreaks($shortbreak,$where);
			
			$response['response'] ='Updated';
			$response['property_id'] =$_GET['property_id'];
				
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
			
		if(IS_AJAX) echo json_encode($response);
	}*/

	
}

/* End of file pricing.php */
/* Location: ./application/controllers/pricing.php */