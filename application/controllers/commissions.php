<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class commissions extends CI_Controller {

	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {

                redirect('/login?returl=calendar&err=login_required');
            }
        }

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('commissions_m');

		$response ['status']='success';
		$response ['navigation']['tab'] = 'Commissions';
		try{

			$data = $this->commissions_m->list_of_commissions();
			$response ['response']['records'] = $data;

		}catch(Exception $e){
			$response ['status']='error';
			$response ['response']=$e->getMessage();
		}
		$this->load->view('app/commissions_list',$response);
	}

	public function add_commissions()
	{
		$this->load->model('property_m');
		
		$response['status'] = 'success';
		try{
			$propertyList = $this->property_m->property_list();
			$response['response']['properties'] = $propertyList->result_array();
            

			$this->load->view('app/commissions',$response);	
		}catch(Exception $e){
			$response['status'] = 'error';
			$response['response'] = $e->getMessage();	
		}
		
		if(IS_AJAX) echo json_encode($response);
	}



	function getBookingsOfProperty($propertyId=''){
		//print_r('--getBookingsOfProperty CONTROLLER --');
		$this->load->model('commissions_m');
		$response['status']='success';
		try{
			$propertyId = $_GET['property_id'];
			//print_r($propertyId);

			$bookings = $this->commissions_m->getBookingsOfProperty($propertyId);
			//$response['response']['netBookingValue'] = $bookings;
			//print_r($response['response']['netBookingValue']);
			$cost = array();
			for($i = 0;$i< sizeof($bookings);$i++){
				$cost[] = $bookings[$i]['cost'];
			}

			$netBookingValue = array_sum($cost)/sizeof($bookings);
			//print_r($netBookingValue);
			$response['response']['netBookingValue'] = $netBookingValue;

			//$this->load->view('app/commissions',$response);*/	

		}catch(Exception $e){
			$response['status']= 'error';
			$response['response'] = $e->getMessage();
		}
		IF(IS_AJAX) echo json_encode($response);

	}

	public function save(){
		//Validations		
		$this->load->library('form_validation');
		$this->form_validation->set_message('unique', 'The %s already exists');
		$this->form_validation->set_message('integer', 'The %s field must contain numbers.');

		$commission_data = $this->input->post(NULL, TRUE);

		$this->form_validation->set_rules('commision_percentage', 'Commision Percentage', 'required|trim');
		$this->form_validation->set_rules('vat_on_commission', 'Vat % on commission', 'required|trim');
		$this->form_validation->set_rules('minimum_commision', 'Minimum Commision', 'required');
		$this->form_validation->set_rules('minimum_owner_account_value', 'Minimum Owner Account Value', 'required');
		
    	$response['status']='success';

		if ( $this->form_validation->run() == FALSE ){

			//we triggerd validation error
			$response['status']='error';
			$response['response']=validation_errors();


		}else{

			$this->load->model('commissions_m');

			if($response['status']=='success')
			{

				$commission['property_id']=$commission_data['inputProperty_name'];
				$commission['commision_percentage']=$commission_data['commision_percentage'];		
				$commission['vat_on_commission']=$commission_data['vat_on_commission'];
				$commission['minimum_commision']=$commission_data['minimum_commision'];
				$commission['minimum_owner_account_value']=$commission_data['minimum_owner_account_value'];
				$commission['overseas_tax']=$commission_data['overseas_tax'];
				$commission['commission_on_net_booking_val']=$commission_data['commission_on_net_booking_val'];
				$commission['net_booking_value']=$commission_data['net_booking_value'];

				try{
					$res = $this->commissions_m->save_commission($commission);
					
					$response['data']['id']=$res;
					$response['response'] ='Saved';
					
				}catch(Exception $e){
					//exception means some kind of error from the system
					$response['status']='error';
					$response['response']=$e->getMessage();
				}
			}

		}
		if(IS_AJAX) echo json_encode($response);
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */