<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_profile extends CI_Controller {

    /*
        defining a construt method that is invoke to check whether the user has logged in
    */
    public function __construct()
    {
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {
                redirect('/login?returl=bookings&err=login_required');
            }
        }

    }


   


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -  
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
    */
    public function index()
    {
        $this->load->model('users_m');
        $id = $this->sessions->getsessiondata('user_id');
        $response['response']['user']=$this->users_m->getUser($id);
        $this->load->view('app/user_profile',$response);
  
    }

    
   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */