<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

	/**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
		parent::__construct();
		//Load the session library - If session lib is autoloaded remove this from here
		$this->load->library('session');

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//is there any flash message to be displayed 
		if(isset($_GET['err']) && $_GET['err']){
			if($_GET['err']=='login_required'){
				$data['flash_message']='Looks like you need to login to access the page';
			}else if($_GET['err']=='facebook_login_failed'){
				$data['flash_message']='Facebook authentication failed..Facebook user not registered';
			}else if($_GET['err']=='twitter_login_failed'){
				$data['flash_message']='Twitter authentication failed';
			}else if($_GET['err']=='google_login_failed'){
				$data['flash_message']='Google authentication failed.. Google user not registered';
			}

		}


		$data['returl']=isset($_GET['returl']) ? $_GET['returl']:'dashboard';
		$this->load->view('app/login_page',$data);
	}

	/**
	 *  function to authenticate the user
	 *
	 */
	public function authenticate(){
		$this->load->model('users_m');
		
		$response['status']='success';
		try{
			$username=$this->input->post('username');
			$password=$this->input->post('password');

			$res = 	$this->users_m->verify_login($username,$password);
		}catch(Exception $e){
			//exception means some kind of error from the system
			$response['status']='error';
			$response['response']=$e->getMessage();
		}
		if(IS_AJAX) echo json_encode($response);
	}

	//Forgot Password Method.
	public function forgot_password()
	{
		
		$response['status']='success';
		$this->load->helper('password_helper');

		$this->load->model('users_m');
		
		try{
				
				//first reset the password
				$user_record = $this->users_m->forgot_password($this->input->post('username_or_email'));
				
				
			    //now send email to that user with new password
				$this->load->library('email');
				$this->email->from('support@bookingbrain.co.uk', 'Booking Brain');
				$this->email->to($user_record['email']); 
				$this->email->set_mailtype('html');
				$this->email->subject('Your password has been reset');
				$this->email->message($this->load->view('app/emailtemplates/resetpassword_email',$user_record,true));	
				$this->email->send();
				

				$this->load->view('site/home');

			

				
		}catch(Exception $e){
			//exception means some kind of error from the system

			$response['status']='error';
			$response['response']=$e->getMessage();
		}

				
		
			
	}


 	/**
     * Logs user in with facebook
     */
    public function facebook() {
		
		$this->config->load("facebook",TRUE);
        $fb_config = $this->config->item('facebook');
        $this->load->library('Facebook', $fb_config);

		/*$fb_config = array(
		    'appId'  => '272469016271687',
		    'secret' => 'b2ead965f802b010c4ee04bc1c7f1536'
		);

		$this->load->library('facebook', $fb_config);
		*/
	
		$fb_user_chk = $this->facebook->getUser();

		if ($fb_user_chk) {
            try {
                $fb_user = $this->facebook->api('/me');
                $this->load->model('users_m');
		    	//$verify_result = $this->users_m->verify_login_facebook($fb_user['id']);
              
		    	$user['facebook_user_id']=$fb_user['id'];
			    $user['firstname']=$fb_user['first_name'];
			    $user['lastname']=$fb_user['last_name'];
			    $user['email']=$fb_user['email'];
			    $user['username']=isset($fb_user['username'])? $fb_user['username']:$user['email'];
			    $user['profile_image_url']="https://graph.facebook.com/".$fb_user['id']."/picture?type=large";
			    $user['login_provider']='facebook';
			    $user['login_provider_username']=$user['username'];

			 
		    	$verify_result = $this->users_m->verify_facebook_user($user);


			    redirect('/dashboard');
            } catch (Exception $e) {
                //$fb_user = null;
                //redirect('/login?err=facebook_login_failed');
            }
        }

        if ($fb_user_chk) {
           
        } else {
            redirect( $this->facebook->getLoginUrl(array('scope' => 'public_profile,email,user_about_me' )));
        }
		
		
    }

    
    /**
     * Logs user in with google
     */
    public function google() {

		require_once 'src/Google_Client.php'; // include the required calss files for google login
		require_once 'src/contrib/Google_PlusService.php';
		require_once 'src/contrib/Google_Oauth2Service.php';

		$client = new Google_Client();
		$client->setApplicationName("Booking Brain"); 
		$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/plus.me')); 

		//local
		/*$client->setClientId('919307454631-g51bu1v3cf31q0i84p1k1g82m2a5k8d5.apps.googleusercontent.com'); 
		$client->setClientSecret('v_ECVEMlaAC11ZIfmGNRHjEh'); 
		$client->setRedirectUri('http://localhost/booking-brain/index.php/login/google');*/

		#server
		$client->setClientId('919307454631-j6uaddctfjs6v96mdp85bq6n012ht03c.apps.googleusercontent.com'); 
		$client->setClientSecret('K5uGq1G9JkUZZd13asNiu5UR'); 
		$client->setRedirectUri('https://bookingbrain.koncero.com/index.php/login/google');

		//$client->setDeveloperKey('AIzaSyAKaPCut1KRk2z--TranSvpOYJFuibAtoE'); // Developer key
		$plus       = new Google_PlusService($client);
		$oauth2     = new Google_Oauth2Service($client); // Call the OAuth2 class for get email address

		if(isset($_GET['code'])) {
		    $client->authenticate(); // Authenticate
		    $this->sessions->setsessiondata('access_token',$client->getAccessToken());
		    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
		}

		$access_token  = $this->sessions->getsessiondata('access_token');
		//echo('access_token');var_dump($this->sessions->getsessiondata('access_token')); 
		
		if(isset($access_token)) {
		    $client->setAccessToken($access_token);
		}
		 
		if ($client->getAccessToken()) {
			try {
				$user = $oauth2->userinfo->get();
			    //echo('user--->');var_dump($user); 
				
				/*$me = $plus->people->get('me');
				echo('me--->');var_dump($me); 
				$optParams = array('maxResults' => 100);
				
				$activities = $plus->activities->listActivities('me', 'public',$optParams);
				echo('activities-->');var_dump($me); 
				// The access token may have been updated lazily.
				$this->sessions->setsessiondata('access_token',$client->getAccessToken());
				  
				
				echo('email-->');var_dump($email); */

				/*$google_user_id = $user['id'];
				$firstname = $user['given_name'];
				$lastname = $user['family_name'];
				$email= filter_var($user['email'], FILTER_SANITIZE_EMAIL);*/

				  $this->sessions->setsessiondata(array(
			        	'google_user_id'=>$user['id'],
			        	'firstname'=>$user['given_name'],
			        	'lastname'=>$user['family_name'],
			        	'email'=>filter_var($user['email'], FILTER_SANITIZE_EMAIL)));
				
				/*echo('google_user_id-->');var_dump($user['id']); 
				echo('google_user_id-->');var_dump($user['given_name']); 
				echo('google_user_id-->');var_dump($user['family_name']); 
				echo('google_user_id-->');var_dump($user['email']);*/ 
				$this->load->model('users_m');
				$this->users_m->verify_login_google($user['id']);
				redirect('/dashboard');

			    } catch (Exception $e) {
		            redirect('/login?err=google_login_failed');
		        }
		} else {
		  
		}


		/*if(isset($me)){ 
		 
		  $this->sessions->setsessiondata('gplusuer',$me);
		}*/
		 

		//echo('hiiii');var_dump($this->sessions->getsessiondata('access_token')); 
		//echo('hiiii-----');var_dump($this->sessions->getsessiondata('gplusuer')); 

    }

    /**
     * Logs user in with twitter
     */
    public function twitter() {
    	
		$this->load->library('tweet');
		if (!$this->tweet->logged_in()) {
		    $this->tweet->set_callback(site_url('login/twitter'));
		    $this->tweet->login();
		    return;
		}

		$tw_user = $this->tweet->call('get', 'account/verify_credentials');
		if (empty($tw_user)) {
		    redirect('/login?err=twitter_login_failed');
		}else{
		    $this->load->model('users_m');
		    $verify_result = $this->users_m->verify_login_twitter($tw_user->id_str);

		    if ($verify_result){
			    //redirect to home page
			    redirect('/dashboard');
		    }
		}
		//Go to the registeration page
		redirect('/login?err=twitter_login_failed');
    }

   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */