<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registration extends CI_Controller {


	/**
     * Constructor - Access Codeigniter's controller object
     * 
     */
    function __construct() {
		parent::__construct();
		//Load the session library - If session lib is autoloaded remove this from here
		$this->load->library('session');

    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	*/
	public function index()
	{
		$response['status']='success';
		$response['saveurl']=site_url('/registration/save');
		$this->load->view('app/register',$response);
		
	}

	public function facebook(){
		

		$this->config->load("facebook",TRUE);
        $fb_config = $this->config->item('facebook');
        $this->load->library('Facebook', $fb_config);

		/*$fb_config = array(
		    'appId'  => '272469016271687',
		    'secret' => 'b2ead965f802b010c4ee04bc1c7f1536'
		);

		$this->load->library('facebook', $fb_config);
		*/

		$fb_user_chk = $this->facebook->getUser();

		if ($fb_user_chk) {
            try {
                $fb_user = $this->facebook->api('/me');
                //print_r($fb_user);
                $user['facebook_user_id']=$fb_user['id'];
			    $user['firstname']=$fb_user['first_name'];
			    $user['lastname']=$fb_user['last_name'];
			    $user['email']=$fb_user['email'];
			    $user['username']=isset($fb_user['username'])? $fb_user['username']:$user['email'];
			    //$user['profile_image_url']=$image;
			    $user['login_provider']='facebook';
			    $user['login_provider_username']=$user['username'];
			    
			    $response['status']='success';
			    $response['response']['user']=$user;
			    
			    $this->load->view('app/register', $response);
			    return;
                    
            } catch (FacebookApiException $e) {
                $fb_user = null;
            }
        }

        if ($fb_user_chk) {
           
        } else {
            redirect( $this->facebook->getLoginUrl(array('scope' => 'public_profile,email,user_about_me' )));
        }
	}

	public function twitter(){
		//Form'submitted - TODO: Insert form validation
	    $this->load->library('tweet');
	    if (!$this->tweet->logged_in()) {
			$this->tweet->set_callback(site_url('registration/twitter'));
			$this->tweet->login();
			return;
	    }

	    $user = $this->tweet->call('get', 'account/verify_credentials');
	    $user['twitter_user_id']=$user->id_str;
	    $user['firstname']=$user->name;
	    $user['email']=$user['email'];
	    $user['username']=$user->screen_name;
	    $user['login_provider']='twitter';
	    $user['login_provider_username']=$user['username'];
	    
	    $response['status']='success';
		$response['response']['user']=$user;

		$this->load->view('app/register', $response);
	 
	}

	public function save(){

    	$response['status']='success';


		$this->load->model('users_m');
			//Validations		

		$this->load->library('form_validation');
		$user=$this->input->post(NULL, TRUE);
		
		

		$user['isactive']=1;//Form'submitted - TODO: Insert form validation
	   
		
		if ( $user['login_provider']=='facebook' ||  $user['login_provider']=='twitter') {
			$user['username']=$user['login_provider_username'];
		}

		if(isset($user['password']))$user['password']=md5($user['password']);
		$user['activation_code']=uniqid ();

		unset($user['password_confirmation']);

		//check for duplicate username:-
    	$user_record=$this->users_m->get_user_by_username($user['username']);

    	if($user_record){
    		$response['statu