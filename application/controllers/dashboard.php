<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller {

	public function __construct()
	{
        parent::__construct();

        // Check that the user is logged in
        if (!$this->sessions->getsessiondata('logged_in') ) {
            // Prevent infinite loop by checking that this isn't the login controller
            if ($this->router->class != 'login')            {

                redirect('/login?returl=dashboard&err=login_required');
            }
        }

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$misc['navigation']['tab'] = 'Dashboard';

		$this->load->model('booking_m');
		$misc['bookings'] = $this->booking_m->get_latest_bookings();

		//Added on 19/8 to check if user has logged-in for first time.
		$this->load->model('users_m');
		$data = $this->users_m->user_logged_in_for_1st_time();
		
		if (!$this->sessions->getsessiondata('login_if_first') ) {
			if (sizeof($data) == 1){ $this->sessions->setsessiondata('login_if_first','YES'); }	
		}
		$this->load->view('app/dashboard',$misc);

		 
	}

	public function get_propertywise_booking_stats(){

		$this->load->model('booking_m');

		echo json_encode($this->booking_m->get_bookingcount_groupedby_property());
		

		//get bookings count grouped by property
		
	}

	public function get_staff_web_bookings_stats(){
		$this->load->model('booking_m');

		$data=$this->booking_m->get_bookingcount_groupedby_date();
		echo json_encode($data);
	}

	public function search(){
		$misc['navigation']['tab'] = 'Dashboard';

		$this->load->model('booking_m');
		$misc['bookings'] = $this->booking_m->get_latest_bookings();

		$search_data = $this->input->post(NULL,TRUE);
		//print_r($search_data);
		$this->load->model('booking_m');
		if($search_data['searching'] == 'yes'){
			//print_r($search_data);
			$search_data = $this->booking_m->search($search_data);
			$misc ['response']['search_data'] = $search_data->result_array();

			//print_r($misc ['response']['search_data']);
		} 
		$this->load->view('app/dashboard',$misc);
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */