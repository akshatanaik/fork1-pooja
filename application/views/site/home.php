<?= $this->load->view('site/layouts/header'); ?>
	<link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">


<div class="slider-home">



<div class="container">



<!-- Slogan

================================================== -->

 <div class="row">

 

 <div class="divider"></div>

 <div class="span12 promo-slogan">

   <h1>Hello and welcome to <span>Booking Brain</span> , a way to manage and book the property </h1>

 

 </div>

 <div class="divider"></div>







<!-- Slider

================================================== -->

  <div class="span12 cont-bs">

   <!-- Slideshow -->

    <ul class="rslides rslides1">

      <li><img src="<?= INCLUDES ?>site/img/slider-img03.jpg" alt="" /><div class="caption">This is a caption 1</div></li>

      <li><img src="<?= INCLUDES ?>site/img/slider-img02.jpg" alt="" /><div class="caption">This is a caption 2</div></li>

      <li><img src="<?= INCLUDES ?>site/img/slider-img03.jpg" alt="" /><div class="caption">This is a caption 3</div></li>

    </ul>

  </div>



 </div>

</div>



</div>



<div class="container">

 

<!-- Home content

================================================== -->

<div class="divider"></div> 



<div class="row">



 <div class="span3 block-info-h">

      <img src="<?= INCLUDES ?>site/img/create-icon.png" alt="">

      <h3><a href="#">Property Management</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

    </div>

    <div class="span3 block-info-h">

      <img src="<?= INCLUDES ?>site/img/develop-icon.png" alt="">

      <h3><a href="#">Booking</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

    </div>

    <div class="span3 block-info-h">

      <img src="<?= INCLUDES ?>site/img/usability-icon.png" alt="">

      <h3><a href="#">East to use</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

    </div>

    <div class="span3 block-info-h">

      <img src="<?= INCLUDES ?>site/img/responsive.png" alt="">

      <h3><a href="#">Reports & Dashboards</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

    </div>

 

</div>



 

  

 <div class="divider"></div>    

  

 <!-- Latest project

================================================== -->  

  

  <div class="row">

  <div class="span12 divider-strip"><h4>Latest projects</h4><span class="strip-block"></span></div>

  </div>



  <ul class="thumbnails">

    <li class="span3 item-block">

     <a href="<?= INCLUDES ?>site/img/bootstrap-mdo-sfmoma-01.jpg" class="zoom" rel="prettyPhoto" title="Image Title"></a>

     <a href="portfolio.html" class="link"></a>

      <a class="thumbnail" href="portfolio.html">

        <img src="<?= INCLUDES ?>site/img/example-sites/example1.jpg" alt="example-item">

      </a>

      <div class="desc">

      <a href="portfolio.html"> Images </a>

       <p> <em>Portfolio Item Images </em></p>

      </div>

    </li>

    <li class="span3 item-block">

     

       <iframe src="http://player.vimeo.com/video/28220269?title=0&amp;byline=0&amp;portrait=0" width="270" height="135" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

      <div class="desc">

      <a href="portfolio.html">Vimeo Video </a>

       <p> <em>Portfolio Item Images</em> </p>

      </div>

    </li>

   <li class="span3 item-block">

     <a href="<?= INCLUDES ?>site/img/bootstrap-mdo-sfmoma-01.jpg" class="zoom" rel="prettyPhoto" title="Image Title"></a>

     <a href="portfolio.html" class="link"></a>

      <a class="thumbnail" href="portfolio.html">

        <img src="<?= INCLUDES ?>site/img/example-sites/example1.jpg" alt="example-item">

      </a>

      <div class="desc">

      <a href="portfolio.html"> Images </a>

       <p> <em>Portfolio Item Images</em> </p>

      </div>

    </li>

    <li class="span3 item-block">

     <div class="row">

      <div class="span3">

     <video src="<?= INCLUDES ?>site/media/VH_videoAsset.flv" type="video/flv" controls="controls"></video>

     </div>

     </div>

      <div class="desc">

      <a href="portfolio.html"> Local Video </a>

       <p> <em>Portfolio Item Images</em> </p>

      </div>

    </li>

  </ul>

  

 <!-- Our Services

================================================== --> 

  <div class="row">

  <div class="span12 divider-strip"><h4>Our Services</h4><span class="strip-block"></span></div>

  </div>

  

  <div class="row">

   <div class="span4">

    <video src="<?= INCLUDES ?>site/media/VH_videoAsset.flv" type="video/flv" controls="controls"></video>

   </div>

   <div class="span8">

    <div class="row">

    

    <div class="span4 block-info pull-left">

      <img class="pull-left" src="<?= INCLUDES ?>site/img/create-icon.png" alt="">

      <h3><a href="#">web design</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>

    </div>

    <div class="span4 block-info pull-left">

      <img class="pull-left" src="<?= INCLUDES ?>site/img/develop-icon.png" alt="">

      <h3><a href="#">development</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>

    </div>

    <div class="span4 block-info pull-left">

      <img class="pull-left" src="<?= INCLUDES ?>site/img/usability-icon.png" alt="">

      <h3><a href="#">usability</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>

    </div>

    <div class="span4 block-info pull-left">

      <img class="pull-left" src="<?= INCLUDES ?>site/img/responsive.png" alt="">

      <h3><a href="#">responsive</a></h3>

      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>

    </div>

    

    </div>

    </div>

    </div>

    <div class="divider"></div> 

  

  

  <!-- Latest blog

================================================== -->

  

    <div class="row">

  

    <div class="span6">

    <div class="divider-strip block-title"><h4>Latest blog</h4><span class="strip-block"></span></div>

      <div class="row">

    <div class="span3 blog-home">

     <img src="<?= INCLUDES ?>site/img/blog-examples/blog-home.jpg" alt="">

     <ul class="meta pull-left"><li class="data">21 may<br>2012</li><li class="post-format image"><span></span></li></ul>

     <h3><a href="#">Lorem ipsum dolor sit amet</a></h3> 

     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa...</p>

     <a href="blog.html" class="btn">Read more</a>

    </div>

    <div class="span3 blog-home">

    <iframe src="http://player.vimeo.com/video/28220269?title=0&amp;byline=0&amp;portrait=0" width="270" height="100" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

    <ul class="meta pull-left"><li class="data">21 may<br>2012</li><li class="post-format video"><span></span></li></ul>

     <h3><a href="#">Lorem ipsum dolor sit amet</a></h3> 

     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa...</p>

     <a href="blog.html" class="btn">Read more</a>

    </div>

    </div>

    </div>

    

    <div class="span6">

    

    <div class="divider-strip block-title"><h4>Testimonials</h4><span class="strip-block"></span></div>

      <div class="testimonialswrap" data-autorotate="3000">

		

		<ul class="testimonials-slider" id="testimonials">

			<li class="testimonials-slide">	

				 <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

<small>John Doe</small></blockquote>

			</li>

			<li class="testimonials-slide">	

				<blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

<small>John Doe</small></blockquote>

			</li>

			<li class="testimonials-slide">	

				<blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

<small>John Doe</small></blockquote>

			</li>

			<li class="testimonials-slide">	

				<blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

<small>John Doe</small></blockquote>

			</li>

			<li class="testimonials-slide">	

				<blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>

<small>John Doe</small></blockquote>

			</li>

		</ul>

        

        <div class="testimonials-controls">

			<a href="#testimonials" class="next-l">1</a>

			<a href="#testimonials" class="prev-l">2</a>

		</div>

        

	</div>

    </div>



    </div>



<div class="row">

 <div class="divider"></div>    

<!-- Promo

================================================== -->

 

 <div class="well no-padding-lr span12">

 <div class="row promo-buy">

  <div class="span9"><h1 class="center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </h1></div>

  <div class="span3"><a class="btn btn-large btn-success right margin-right" href="#"><i class="icon-shopping-cart icon-white"></i> Get Started »</a></div>

 </div>

 </div>

  

   

  </div>

  

  <div class="divider"></div>



    </div>

     <div id="regModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/'); ?>';" >×</button>
            <h3 id="myModalLabel">Sign Up</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/modal.php') ?>
            </div>
        </div>
    </div>
        
     <style type="text/css">
     #loginModal,#regModal{width:320px; left:60%;}
     .modal-footer{background-color:#fff; border-top: medium none;}
     .form-horizontal{padding-left: 30px;}
     .modal.fade.in{top:40%;}
     .form-error{ color:red; font-size: 12px; padding-left:30px;}

     
     </style>
    </div>

    <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/'); ?>';" >×</button>
            <h3 id="myModalLabel">Login</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/login.php') ?>
            </div>
        </div>
    </div>
        
   
    </div>



<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-1.10.2.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/bootstrap.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/dataTables/jquery.dataTables.min.js'></script>  
<script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 
<script type='text/javascript' src='<?= INCLUDES ?>app/js/cookies.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/actions.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/settings.js'></script>

<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/stepywizard/jquery.stepy.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/register.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/login.js'></script>


    
<?= $this->load->view('site/layouts/footer'); ?>