<?= $this->load->view('site/layouts/header'); ?>
<link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">


<div class="slider-cont">

  <div class="container">

    <header id="pagehead">

      <h1>Help <small>&nbsp; &frasl; &nbsp;&nbsp;Booking Brain. </small></h1>

    </header>

  </div>

</div>



<div class="slider-home">

  <div class="container">

    <!-- Content================================================== --> 

    	<section>

	    	<!-- Help =========-->

 			<div class="row">

				<div class="span12">

					<div class="row">



						<div class="span12" style="margin-bottom:20px;">

							<div class="divider-strip block-title"><h2>Help</h2><span class="strip-block"></span></div>

							<p style="font-size:18px;"><em>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</em></p>

							<p style="font-size:18px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p> 

						</div>



					</div>

				</div>

			</div>



			<!-- FAQ =========-->

 			<div class="row">

				<div class="span12">

					<div class="row">



						<div class="span12" style="margin-bottom:20px;">

							<div class="divider-strip block-title"><h2>FAQ</h2><span class="strip-block"></span></div>

							<p style="font-size:18px;"><em>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</em></p>

							<p style="font-size:18px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p> 

						</div>



					</div>

				</div>

			</div>

		</section>

   

    <div class="divider"></div>

 </div><!-- /container -->


 <div id="regModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/website/help'); ?>';" >×</button>
            <h3 id="myModalLabel">Sign Up</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/modal.php') ?>
            </div>
        </div>
    </div>
        
     <style type="text/css">
     #loginModal,#regModal{width:320px; left:60%;}
     .modal-footer{background-color:#fff; border-top: medium none;}
     .form-horizontal{padding-left: 30px;}
     .modal.fade.in{top:40%;}
     .form-error{ color:red; font-size: 12px; padding-left:30px;}

     
     </style>
    </div>

    <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/website/help'); ?>';" >×</button>
            <h3 id="myModalLabel">Login</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/login.php') ?>
            </div>
        </div>
    </div>
        
   
    </div>

<?= $this->load->view('site/layouts/footer'); ?>