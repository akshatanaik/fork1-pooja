<?= $this->load->view('site/layouts/header'); ?>
<link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">


<div class="slider-cont">

  <div class="container">

    <header id="pagehead">

      <h1>Tour <small>&nbsp; &frasl; &nbsp;&nbsp;Booking Brain. </small></h1>

    </header>

  </div>

</div>



<div class="slider-home">

  	<div class="container">

	    <section>

	    	<!-- Calendar =========-->

 			<div class="row">

				<div class="span12">

					<div class="row">



						<div class="span12" style="margin-bottom:20px;">

							<div class="divider-strip block-title"><h2>Calendar</h2><span class="strip-block"></span></div>

							<p style="font-size:18px;"><em>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</em></p>

							<p style="font-size:18px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p> 

						</div>



					</div>

				</div>

			</div>



			<!-- Booking =========-->

			<div class="row">

				<div class="span12">

					<div class="row">



						<div class="span12" style="margin-bottom:20px;">

							<div class="divider-strip block-title"><h2>Booking</h2><span class="strip-block"></span></div>

							<p style="font-size:18px;"><em>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</em></p>

							<p style="font-size:18px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p> 

						</div>



					</div>

				</div>

			</div>



			<!-- Property =========-->

			<div class="row">

				<div class="span12">

					<div class="row">



						<div class="span12" style="margin-bottom:20px;">

							<div class="divider-strip block-title"><h2>Property</h2><span class="strip-block"></span></div>

							<p style="font-size:18px;"><em>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</em></p>

							<p style="font-size:18px;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p> 

						</div>



					</div>

				</div>

			</div>



		</section>



		<div class="divider"></div>

	</div><!-- /container -->

<?= $this->load->view('site/layouts/footer'); ?>