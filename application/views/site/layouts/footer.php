     <!-- Footer
      ================================================== -->
      <footer>
      <div class="container">
      <div class="row">
      
      <div class="span4">
      <h3>About</h3>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
      <h3>Connect With Us</h3>
      <!-- social begin here -->
                    <ul class="socicon left">
                       <li>
                            <a href="#" class="share-icon">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="google">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="facebook">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="twitter">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="flickr">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dribbble">
                            </a>
                        </li>
                        <li>
                            <a href="#" class="linkedin">
                            </a>
                        </li>
                        <li class="last">
                            <a href="#" class="vimeo">
                            </a>
                        </li>
                       
                    </ul>
                    
      </div>
      
      <!-- tweets begin here -->
      <div class="span4">
      <h3>Latest Tweets</h3>
       <div class="tweets">
                        <p>
                            Loading Tweets...
                        </p>
                        <ul id="tweet-list">
                        </ul>
                    </div>
      </div>
      
      <div class="span4">
      <!-- flickr begin here -->
      <h3>From Flickr</h3>
      
      <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;source=user&amp;user=52617155@N08&amp;layout=x&amp;display=random&amp;size=s"></script> 

      </div>
      
      <div class="span12 copy">
       &copy; 2012 NLINE. All Rights Reserved.
      </div>

      </div>
      </div>
      </footer>

    <!-- JavaScript files begin-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?= INCLUDES ?>site/js/jquery.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.form.js"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.ufvalidator-1.0.5.js"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.easing.1.3.js"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.cycle.all.js"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.prettyPhoto.js"></script>
    <script src="<?= INCLUDES ?>site/js/google-code-prettify/prettify.js"></script>
    <script src="<?= INCLUDES ?>site/js/bootstrap.min.js"></script>
    <script src="<?= INCLUDES ?>site/js/application.js"></script>
    <script src="<?= INCLUDES ?>site/js/responsiveslides.min.js"></script>
    <script src="<?= INCLUDES ?>site/build/mediaelement-and-player.min.js"></script>
    <script src="<?= INCLUDES ?>site/js/gmap3.min.js"></script>
    <script src="<?= INCLUDES ?>site/js/carusel.js"></script>
    <script src="<?= INCLUDES ?>site/js/custom.js"></script>
    <script src="<?= INCLUDES ?>site/js/jquery.ui.totop.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    	$().UItoTop({ easingType: 'easeOutQuart' });
    });
    </script>
    <script type="text/javascript" src="<?= INCLUDES ?>site/js/sslider-min.js"></script>
	<script type="text/javascript">	
		
		/mobile/i.test(navigator.userAgent) && !location.hash && setTimeout(function () {
		  if (!pageYOffset) window.scrollTo(0, 1);
		}, 1000);
		
		$(document).ready(function(){
			var options = {
				nextButton: true,
				prevButton: true,
				pauseButton: true,
				animateStartingFrameIn: true,
				transitionThreshold: 250,
				afterLoaded: function(){
					$("#nav").fadeIn(100);
					$("#nav li:nth-child("+(sequence.settings.startingFrameID)+") img").addClass("active");
				},
				beforeNextFrameAnimatesIn: function(){
					$("#nav li:not(:nth-child("+(sequence.nextFrameID)+")) img").removeClass("active");
					$("#nav li:nth-child("+(sequence.nextFrameID)+") img").addClass("active");
				}
			};

			var sequence = $("#sequence").sequence(options).data("sequence");
			
			$("#nav li").click(function(){
				if(!sequence.active){
					$(this).children("img").removeClass("active").children("img").addClass("active");
					sequence.nextFrameID = $(this).index()+1;
					sequence.goTo(sequence.nextFrameID);
				}
			});
		});
	</script>
    


  </body>
</html>