<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Booking Brain</title>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
    <!-- CSS files begin-->
    
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700|Open+Sans+Condensed:700,300,300italic|Open+Sans:400,300italic,400italic,600,600italic,700,700italic,800,800italic|PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="<?= INCLUDES ?>site/css/bootstrap.css" rel="stylesheet">
    <link href="<?= INCLUDES ?>site/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?= INCLUDES ?>site/css/responsiveslides.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= INCLUDES ?>site/css/prettyPhoto.css" type='text/css'>
	<link rel="stylesheet" type="text/css" media="screen" href="<?= INCLUDES ?>site/css/slide-in.css" />
	<!--[if lt IE 9]><link rel="stylesheet" type="text/css" media="screen" href="<?= INCLUDES ?>site/css/slide-in.ie.css" /><![endif]-->
    <link href="<?= INCLUDES ?>site/css/style.css" rel="stylesheet">
    <!-- Color Style Setting CSS file-->
    <link href="<?= INCLUDES ?>site/css/color-theme/color-fblue.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="<?= INCLUDES ?>site/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= INCLUDES ?>site/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= INCLUDES ?>site/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= INCLUDES ?>site/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?= INCLUDES ?>site/ico/apple-touch-icon-57-precomposed.png">
   
</head>

<body>

<!-- Head
================================================== -->
<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="span6">
                <!-- text widget begin here -->
                <ul class="info-text pull-left">
                    <li>
                        <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget.</a>
                    </li>
                    <li>
                        <a href="#">
                            This informational text widget 
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Lorem ipsum dolor sit
                        </a>
                        amet, consectetuer adipiscing elit. Aenean commodo ligula eget.
                    </li>
                </ul>
            </div>
            <div class="span6">
                <!-- social begin here -->
                <ul class="socicon right top-w">
                    <li>
                        <a href="#" class="share-icon">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="google">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="facebook">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="twitter">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="flickr">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="dribbble">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="linkedin">
                        </a>
                    </li>
                    <li class="last">
                        <a href="#" class="vimeo">
                        </a>
                    </li>
                </ul>
             
            </div>
        </div>
    </div>
</div>
<!-- Logo / Menu
================================================== -->
<header class="header">
    <div class="container strip-line">
        <div class="row">
            <div class="span4">
                <a href="index.html" class="logo">
                    <img src="<?= INCLUDES ?>site/img/logo.png" alt="">
                </a>
            </div>
            <div class="span8">
            <?php   $url = $_SERVER["REQUEST_URI"]; $tokens = explode('/', $url);
				    $lastElementofPath = $tokens[sizeof($tokens)-1];
					if ($this->sessions->getsessiondata('logged_in') && $lastElementofPath == 'login') {}else{ ?>
                    <nav>
                        <ul class="right">
                            <li class="current">
                                <a href="<?= site_url('/'); ?>">home</a>
                            </li>
                            <li>
                                <a href="<?= site_url('/website/tour'); ?>">tour</a>                        
                            </li>
                            <li>
                                <a href="<?= site_url('/website/pricing'); ?>">pricing</a>                        
                            </li>
                            <li>
                                <a href="<?= site_url('/website/help'); ?>">help</a>
                            </li>
                            <li>
                                <!---<a href="<?= site_url('/login/'); ?>">login</a>-->
                                <a href="javascript:void(0);" onclick="calLoginModal()">login</a>  
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="calModal()">get started</a>  
                                     <!--<ul style="display: none;">
                                    <li>
                                        <a href="<?= site_url('/registration/'); ?>">
                                            Register
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/registration/facebook'); ?>">
                                            Register with Facebook
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/registration/twitter'); ?>">
                                            Register with Twitter
                                        </a>
                                    </li>-->
                                   
    
                            </li>            
                        </ul>
                    </nav>
                <?php } ?>    
            </div>
        </div>
    </div>
</header>
