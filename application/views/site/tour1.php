<?= $this->load->view('site/layouts/header'); ?>
<link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">


<div class="slider-cont">

  <div class="container">

    <header id="pagehead">

      <h1>Tour <small>&nbsp; &frasl; &nbsp;&nbsp;Booking Brain. </small></h1>

    </header>

  </div>

</div>



<div class="slider-home">

  	<div class="container">

	   <section>

			<div class="row">



				<div class="span12">

					<div class="row">





						<div class="span12">

							<div class="row">

								<div class="span3">

									<ul class="blog-meta meta pull-left">

										<li class="data"><h3>Calendar</h3></li>

									</ul><br/><br/><br/>

										<p>Calendar has two drop down menus Grid & Month</p>

										<p>Owners can make a booking from here by clicking on a date which pops up a screen to asks number of nights guests are staying.</p>									

								</div>



								<div class="span9">

									<img src="<?= INCLUDES ?>site/img/booking.png" alt="calendar" width="1200" height="550">									

								</div>

							</div>

							<div class="divider-post"></div>

						</div>

					</div>

				</div>



				<div class="span12">

					<div class="row">





						<div class="span12">

							<div class="row">

								<div class="span3">

									<ul class="blog-meta meta pull-left">

										<li class="data"><h3>Booking</h3></li>								

									</ul>

									<br/><br/><br/>

									<p>Owner can add a new booking</p>

									<p>owner can view the booking summary</p>

								</div>



								<div class="span9">

									<img src="<?= INCLUDES ?>site/img/booking.png" alt="booking" width="1200" height="550">

								</div>

							</div>

							<div class="divider-post"></div>

						</div>

					</div>

				</div>





				<div class="span12">

					<div class="row">





						<div class="span12">

							<div class="row">

								<div class="span3">

									<ul class="blog-meta meta pull-left">

										<li class="data"><h3>Property</h3></li>										

									</ul>

									<br/><br/><br/>

									<p>All the information is added about the owners properties</p>

									<p>Owner can drop a pin on a map to show the location of their property</p>

									<p>The owner can upload up to 20 photos of their property. This can be dragged and dropped into the browser, deleted and reordered easily.</p>

									<p>Customers can set their own prices. By ticking the boxes below they can decide which days of the week for a particular month they are happy to accept a change over day. </p>

									<p>The owners can set the prices for their cottage quickly and easily but choose the weekly price for any given period. Here they can set when they want to accept the short breaks</p>

									<p>The owner can add details about their housekeeper to automatically send out an invitation for them to log in.</p>

								</div>



								<div class="span9">									

									<img src="<?= INCLUDES ?>site/img/property.png" alt="property" width="1200" height="550">								

								</div>

							</div>

							<div class="divider-post"></div>

						</div>

					</div>

				</div>



			</div>



		</section>



		<div class="divider"></div>

	</div><!-- /container -->

	<div id="regModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/website/tour'); ?>';" >×</button>
            <h3 id="myModalLabel">Sign Up</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/modal.php') ?>
            </div>
        </div>
    </div>
        
     <style type="text/css">
     #loginModal,#regModal{width:320px; left:60%;}
     .modal-footer{background-color:#fff; border-top: medium none;}
     .form-horizontal{padding-left: 30px;}
     .modal.fade.in{top:40%;}
     .form-error{ color:red; font-size: 12px; padding-left:30px;}

     
     </style>
    </div>

    <div id="loginModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="location.href = '<?= site_url('/website/tour'); ?>';" >×</button>
            <h3 id="myModalLabel">Login</h3>
        </div>
        <div class="row-fluid" >
            <div class="block-fluid">
                <?= $this->load->view('app/login.php') ?>
            </div>
        </div>
    </div>
        
   
    </div>


<?= $this->load->view('site/layouts/footer'); ?>