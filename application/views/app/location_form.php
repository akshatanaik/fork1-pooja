<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

   
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    

    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



    <link href="<?= INCLUDES ?>/app/css/login_styles.css" rel="stylesheet" type="text/css" />



    <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>



     <!-- for validation -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>



    <!-- for ajax form submit -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/ajax_form.js' charset='utf-8'></script>

   

   

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 



   

    <style>



           [class*="block"] [class*="isw-"], [class*="block"] [class*="isb-"]

        {

            margin-left: 5px;

            margin-right: 1px;

            padding: 18px 0 0;

        }



        .block, .block-fluid 

        {

            margin-bottom: -11px;

        }



        .form-error{ color:red; font-size: 10px;}

    </style>

    

</head>

<body>

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>

    

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1><?= $response['title']; ?></h1>

                </div>



                <div class="block-fluid tabs">

                    <ul>

                        <li><a href="#property_location">Property location</a></li>

                        <li><a href="#localarea">Local area</a></li>

                        <li><a href="#how_to_get_there">How to get there</a></li>

                        <li><a href="#whats_nearby">What's_nearby</a></li>

                        <li><a href="#holiday_types">Holiday types</a></li>

                    </ul>



                    <?php $id = $this->uri->segment(3); ?>

                     

                    <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                        <?php $method=$edit_method_location;  ?>

                        <form id="location" class="form-horizontal" action="<?= site_url('/property/'.$method.'/'.$id)?>" method="POST" onsubmit="return locationdetails(this);">

                    <?php } else{ ?>

                        <?php $method=$add_method_location;  ?>

                         <form id="location" class="form-horizontal" action="<?= site_url('/property/'.$method.'/'.$id)?>" method="POST" onsubmit="return locationdetails(this);">

                    <?php } ?>



                        <div id="property_location">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>Property Location</h1>

                                    </div>

                           

                                    <div class="block-fluid">                        

                                        <div class="row-form clearfix">

                                            <div class="span3">Continent<em style="color:#F00;">*</em></div>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>     

                                                <div class="span6">        

                                                    <select name="inputContinent" id="inputContinent" data-validation="required" data-validation-error-msg="Continent field cannot be empty." >

                                                        <option value="<?= $rec['continent'] ?>"><?= $rec['continent'] ?></option>

                                                        <option value="europe">Europe</option>

                                                        <option value="asia">Asia</option>

                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">        

                                                    <select name="inputContinent" id="inputContinent" data-validation="required" data-validation-error-msg="Continent field cannot be empty.">

                                                        <option value="">Choose</option>

                                                        <option value="europe">Europe</option>

                                                        <option value="asia">Asia</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                       

                                        </div>                        



                                        <div class="row-form clearfix">

                                            <div class="span3">Country<em style="color:#F00;">*</em></div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputCountry" id="inputCountry" data-validation="required" data-validation-error-msg="Country field cannot be empty.">

                                                        <option value="<?= $rec['country'] ?>"><?= $rec['country'] ?></option>

                                                        <option value="england">England</option>

                                                        <option value="usa">USA</option>

                                                        <option value="uk">UK</option>

                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">        

                                                    <select name="inputCountry" id="inputCountry" data-validation="required" data-validation-error-msg="Country field cannot be empty.">

                                                        <option value="">Choose</option>

                                                        <option value="england">England</option>

                                                        <option value="usa">USA</option>

                                                        <option value="uk">UK</option>

                                                    </select>

                                                </div>

                                            <?php } ?>

                                                                        

                                        </div>                                                                                    



                                        <div class="row-form clearfix">

                                            <div class="span3">Region</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputRegion" id="inputRegion" >

                                                        <option value="<?= $rec['region'] ?>"><?= $rec['region'] ?> </option>

                                                        <option value="west_country">West country</option>

                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?> 

                                                <div class="span6">        

                                                    <select name="inputRegion" id="inputRegion" >

                                                        <option value="">Choose</option>

                                                        <option value="west_country">West country</option>

                                                    </select>

                                                </div>

                                            <?php } ?>                            

                                        </div>                                                                                 



                                        <div class="row-form clearfix">

                                            <div class="span3">Subregion</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputSubregion" id="inputSubregion" >

                                                        <option value="<?= $rec['subregion'] ?>"><?= $rec['subregion'] ?></option>

                                                        <option value="somerset">Somerset</option>

                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">        

                                                    <select name="inputSubregion" id="inputSubregion" >

                                                        <option value="">Choose</option>

                                                        <option value="somerset">Somerset</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                          

                                        </div>      



                                        <div class="row-form clearfix">

                                            <div class="span3">Town</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputTown" id="inputTown" >

                                                        <option value="<?= $rec['town'] ?>"><?= $rec['town'] ?></option>

                                                        <option value="minehead">Minehead</option>

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?> 

                                                <div class="span6">        

                                                    <select name="inputTown" id="inputTown" >

                                                        <option value="">Choose</option>

                                                        <option value="minehead">Minehead</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                           

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Suburb(optional)</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputSuburb" id="inputSuburb" > 

                                                        <option value="<?= $rec['suburb'] ?>"><?= $rec['suburb'] ?></option>

                                                        <option value="Studio">Studio</option>

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputSuburb" id="inputSuburb" > 

                                                        <option value="">Choose</option>

                                                        <option value="Studio">Studio</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                            

                                        </div> 



                                        <div class="row-form clearfix">

                                            <div class="span3">Property address<em style="color:#F00;">*</em></div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['property_address'] ?>"  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty."/></div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty."/></div>

                                            <?php } ?>

                                        </div> 



                                        <div class="row-form clearfix">

                                            <div class="span3">Postcode<em style="color:#F00;">*</em></div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['postcode'] ?>"  type="text" name="inputPostcode" id="inputPostcode" data-validation="required" data-validation-error-msg="Property code field cannot be empty."/></div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputPostcode" id="inputPostcode" data-validation="required" data-validation-error-msg="Post code field cannot be empty."/></div>

                                            <?php } ?>

                                        </div> 



                                        <div class="row-fluid">

                                            <div class="span12">

                                                <div class="head clearfix">

                                                    <div class="isw-graph"></div>

                                                    <h1>Property Map</h1>

                                                </div>

                                                <div class="block">

                                                    <iframe  width="1125" height="250" frameborder="0" style="border:0"

                                                      src="https://www.google.com/maps/embed/v1/search?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q=<?= $rec['property_address'] ?>">

                                                    </iframe>

                                                </div> 

                                            </div> 

                                        </div>                    

                                    </div>

                                </div>

                            </div>

                        </div>

                        

                        <div id="localarea">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>The local area</h1>

                                    </div>

                                    <div class="block-fluid">

                                        

                                        <div class="row-form clearfix">

                                            <div class="span3">The region</div>

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"> <?= $rec['region_description'] ?></textarea> </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"></textarea> </div>



                                            <?php } ?>                                            

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">The area</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"><?= $rec['area_description'] ?></textarea> </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"></textarea> </div>

                                            <?php } ?>

                                        </div>                            

                                    </div>  

                                </div>

                            </div>

                        </div>

              

                        <div id="how_to_get_there">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-users"></div>

                                        <h1>How to get there</h1>

                                    </div>

                                

                                    <div class="block-fluid">                               

                                        <div class="row-form clearfix">

                                            <div class="span3">Airport</div>

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                 <div class="span6"><input  value="<?= $rec['airport'] ?>" type="text" name="inputAirport" id="inputAirport"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><input type="text" name="inputAirport" id="inputAirport"/></div>  km

                                             <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Ferry port</div>

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['ferry_port'] ?>" type="text" name="inputFerryport" id="inputFerryport"/></div>  km

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputFerryport" id="inputFerryport"/></div>  km

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Train station </div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['train_station'] ?>" type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km

                                            <?php } ?>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">Car required</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputCar" id="inputCar" >

                                                         <option value="<?= $rec['car'] ?>"><?= $rec['car'] ?> </option>

                                                        <option value="car_not_necessary">Car not necessary</option>

                                                        <option value="car_necessary">Car necessary</option>

                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                               <div class="span6">        

                                                    <select name="inputCar" id="inputCar" >

                                                         <option value="">Choose</option>

                                                        <option value="car_not_necessary">Car not necessary</option>

                                                        <option value="car_necessary">Car necessary</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                        

                                        </div>

                                        <div class="row-form clearfix">



                                            <div class="span3">More tips of how to get there</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"><?= $rec['how_to_get_there'] ?></textarea> </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"></textarea> </div>

                                            <?php } ?>

                                        </div> 

                                    </div>

                                </div>

                            </div>

                        </div>

                    

                        <div id="whats_nearby">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>What's nearby</h1>

                                    </div>

                                    <div class="block-fluid">

                                        <div class="row-form clearfix">

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['golf_course_within_15km'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputGolf_course_15mins_walk"/>  Golf course within 15 mins walk

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                   

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                                    </label>                                                   

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['golf_course_within_30km'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputGolf_course_30mins_drive"/> Golf course  within 30 mins drive

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                    

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                                     </label>                                                    

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputTennis_court"/> Tennis courts nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                                    </label>                                                    

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['skiing'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputSkiing"/> Skiing:property is in a ski resort

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                                 </label>

                                                 <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                                     </label>

                                                </div>

                                            <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['water_sport'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputWater_sports"/>Water sports nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                                </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['water_park'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputWater_park"/> Water park nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                                 </label>

                                                 <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['horse_riding'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputHorse_riding"/> Horse riding nearby 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                                

                                            

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                 <?php if($rec['beach_front'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputBeach_front"/> Beachfront property

                                                    </label>

                                                <?php }else{ ?>



                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                                 </label>

                                                <?php } ?>

                                            </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                                 </label>



                                                </div>

                                            <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['fishing'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputFishing"/> Fishing nearby

                                                    </label>

                                                    <?php }else{ ?>

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                                    </label>

                                                    <?php } ?>

                                                    

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                               

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                                    </label>

                                                </div>

                                            <?php } ?>

                                                

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest Beach</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['nearest_beach'] ?>" type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><input type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km

                                                <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest shops</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['nearest_shop'] ?>" type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km

                                            <?php } ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        



                        <div id="holiday_types">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>Holiday types</h1>

                                    </div>

                                    <div class="block-fluid">

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['walking_holiday'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Walking holidays

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">                                                   

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                                    </label>                                                    

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['cycling_holiday'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>Cycling holidays

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                                     </label>

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <label class="checkbox inline">

                                                    <?php if($rec['rural_countryside_retreats'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Rural or countryside retreats

                                                    </label>

                                                <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                                    </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                             <div class="span3">                                               

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                                    </label>

                                               

                                            </div>

                                            <?php }?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['beach_lakeside_relaxation'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Beach or lakeside relaxation

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                    

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                                     </label>

                                                </div>

                                            <?php } ?>



                                        </div>



                                        <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['city_breaks'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>City breaks

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                               

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                                    </label>                                                

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['winter_sun'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Winter sun

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                                     </label>                                                 

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['night_life'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Nightlife

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                                </label>                                                

                                            </div>

                                            <?php } ?>

                                        </div>

                                    </div>   

                                </div>

                            </div>

                             <div class="footer tar">

                                <button class="btn" id="save_details">Save</button>

                            </div>

                        </div>

                    </form>

                </div>

             </div>

         </div>   

    </div>

</body>

</html>

