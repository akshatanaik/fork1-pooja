<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

    

     <style>

        [class*="span"] {

            float: left;

            margin-left: 0px;

            min-height: 1px;

        }

        .span12 {

            width: 1090px;

        }

        .span3 {

            width: 240px;

        }



        .block .footer, .block-fluid .footer {

            background-color: #F2F2F2;

            border-top: 1px solid #DAE1E8;

            margin-top: -1px;

            padding: 5px 5px 4px;

            text-align: right;

        }



           .form-error{ color:red; font-size: 10px;}





    </style>

</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">                

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!--   -->

            <div class="workplace">

            	<?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                <?php foreach($response['records'] as $rec): ?>

                <?php $id = $this->uri->segment(3); ?>

                <form class="form-horizontal" action="<?= site_url('/property/updatePropertyDetails/'.$id)?>" method="POST" onsubmit="return propertydetails(this);">

                     <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-favorite"></div>

                            <h1>Edit Property Details</h1>



                             <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                <button class="btn btn-success">Update</button>

                                <a class="btn" href="<?= site_url('property/propertyList');?>">Cancel</a>

                            </div> 

                        </div>

                        <!-- build  the form -->

                          <div class="block-fluid tabs">

                                 <ul >

                                    <li><a href="#essentials">Essentials<em style="color:#F00;">*</em></a></li>

                                    <li><a href="#facilities">Facilities and Features</a></li>

                                    <li><a href="#suitability">Suitability</a></li>

                                    <li><a href="#website">Website</a></li>

                                </ul> 



                                <div id="essentials">

                                		<div class="row-form clearfix">

                                            <div class="span3">Property Name<em style="color:#F00;">*</em></div>

                                            <div class="span6"><input value="<?= $rec['property_name'] ?>"  type="text" name="inputProperty_name" id="inputProperty_name" data-validation="required" data-validation-error-msg="Property name field cannot be empty."/></div>

                                        </div>                         



                                        <div class="row-form clearfix">

                                            <div class="span3">Property type</div>

                                            <div class="span6">        

                                                <select name="inputProperty_type" id="inputProperty_type">

                                                    <?php if($rec['property_type'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                    <?php } else { ?>

                                                        <option value="<?= $rec['property_type'] ?>"><?= $rec['property_type'] ?></option>

                                                    <?php } ?>                                                    

                                                   

                                                    <option value="Studio">Studio</option>

                                                </select>

                                            </div>                            

                                        </div>                                                                                    



                                        <div class="row-form clearfix">

                                            <div class="span3">Maximum guests<em style="color:#F00;">*</em></div>

                                            <div class="span6">        

                                                <select name="inputMax_guest" id="inputMax_guest" data-validation="required" data-validation-error-msg="Max guest field cannot be empty.">

                                                     <?php if($rec['max_guests'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                    <?php } else { ?>

                                                        <option value="<?= $rec['max_guests'] ?>"><?= $rec['max_guests'] ?> </option>

                                                    <?php } ?>                                                    

                                                    

                                                    <option value="1">1 guests</option>

                                                    <option value="2">2 guests</option>

                                                    <option value="3">3 guests</option>

                                                </select>

                                            </div>                            

                                        </div>                                                                                 



                                        <div class="row-form clearfix">

                                            <div class="span3">Main Description</div>

                                            <div class="span6">        

                                                <textarea name="inputMain_description" id="inputMain_description"><?= $rec['main_description'] ?></textarea>

                                            </div>

                                        </div>      



                                        <div class="row-form clearfix">

                                            <div class="span3">Search result summary</div>

                                            <div class="span6">        

                                                <textarea name="inputSearch_result" id="inputSearch_result"><?= $rec['search_result_desc'] ?></textarea>

                                            </div>

                                        </div>                    

                                </div>



                                <div id="facilities">

                             			<div class="row-form clearfix">

                                            <div class="span2">Home size in Sq.m</div>

                                            <div class="span6"><input value="<?= $rec['home_size'] ?>"  type="text" name="inputHome_size" id="inputHome_size"/> </div>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span4"><h4>Beds</h4></div>

                                            <div class="span4"><h4>Bathrooms</h4> </div>

                                            <div class="span3"><h4>Seating</h4> </div>

                                        </div>

                                         <div class="row-form clearfix">

                                            

                                            <div class="span2">Single beds</div>

                                            <div class="span2">        

                                                <select name="inputSingle_bed" id="inputSingle_bed" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['single_beds'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                        

                                                        <?php  if($rec['single_beds'] == 1){ ?>

                                                            <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['single_beds'] == 2){ ?>

                                                            <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>                                                   

                                            

                                                </select>

                                            </div> 

                                            

                                            <div class="span2">Family bathrooms</div>

                                            <div class="span2">        

                                                <select name="inputFamily_bathrooms" id="inputFamily_bathrooms" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['family_bathrooms'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                         

                                                         <?php  if($rec['family_bathrooms'] == 1){ ?>

                                                            <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['family_bathrooms'] == 2){ ?>

                                                            <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div>

                                            

                                            <div class="span2">Dining seats</div>

                                            <div class="span2">        

                                                <select name="inputDining_seats" id="inputDining_seats" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['dining_seats'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                        

                                                        <?php  if($rec['dining_seats'] == 1){ ?>

                                                            <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['dining_seats'] == 2){ ?>

                                                            <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                    

                                                    

                                                </select>

                                            </div> 

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            <div class="span2">Double beds</div>

                                            <div class="span2">        

                                                <select name="inputDouble_beds" id="inputDouble_beds" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['double_beds'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                         

                                                         <?php  if($rec['double_beds'] == 1){ ?>

                                                            <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['double_beds'] == 2){ ?>

                                                            <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div> 

                                            <div class="span2">En suites </div>

                                            <div class="span2">        

                                                <select name="inputEn_suites" id="inputEn_suites" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['en_suites'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                        

                                                        <?php  if($rec['en_suites'] == 1){ ?>

                                                            <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['en_suites'] == 2){ ?>

                                                            <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                    

                                                    

                                                </select>

                                            </div> 

                                            <div class="span2">Lounge seats </div>

                                            <div class="span2">        

                                                <select name="inputLounge_seats" id="inputLounge_seats" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['lounge_seats'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                        

                                                        <?php  if($rec['lounge_seats'] == 1){ ?>

                                                            <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['lounge_seats'] == 2){ ?>

                                                            <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                    

                                                    

                                                </select>

                                            </div>  

                                         </div> 

                                          

                                         <div class="row-form clearfix">

                                            <div class="span2">Sofa beds </div>

                                            <div class="span2">        

                                                <select name="inputSofa_beds" id="inputSofa_beds" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['sofa_beds'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                         

                                                         <?php  if($rec['sofa_beds'] == 1){ ?>

                                                            <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?>   </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['sofa_beds'] == 2){ ?>

                                                            <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div> 

                                            <div class="span2">Shower rooms </div>

                                            <div class="span2">        

                                                <select name="inputShower_rooms" id="inputShower_rooms" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['shower_bathrooms'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                         

                                                         <?php  if($rec['shower_bathrooms'] == 1){ ?>

                                                            <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?>   </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['shower_bathrooms'] == 2){ ?>

                                                            <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div> 



                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span2"> Cots</div>

                                            <div class="span2">        

                                                <select name="inputCots" id="inputCots" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['cots'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                         

                                                         <?php  if($rec['cots'] == 1){ ?>

                                                            <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?>   </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['cots'] == 2){ ?>

                                                            <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div> 

                                            <div class="span2">Total bathrooms </div>

                                            <div class="span2">        

                                                <select name="inputTotal_bathrooms" id="inputTotal_bathrooms" style=" margin: 0px 0px 0px -55px;">

                                                    <?php if($rec['total_bathrooms'] == null) { ?>

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php } else { ?>

                                                        

                                                        <?php  if($rec['total_bathrooms'] == 1){ ?>

                                                            <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?>   </option>

                                                            <option value="2">2 </option>

                                                            <option value="3">3 </option>

                                                        <?php }elseif($rec['total_bathrooms'] == 2){ ?>

                                                            <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="3">3 </option>

                                                        <?php }else{ ?>

                                                            <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?>   </option>

                                                            <option value="1">1 </option>

                                                            <option value="2">2 </option>

                                                        <?php } ?>

                                                    <?php } ?>

                                                   

                                                    

                                                </select>

                                            </div> 



                                        </div>        

                                        

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Indoors</h4> 

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                           

                                            <div class="span3">

                                                <?php if($rec['cooker'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Cooker

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCooker" id="inputCooker" value="1" />Cooker 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['tv'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  TV

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTv" id="inputTv" value="1"/>TV

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['log_fire'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Log fire

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputLog_fire" id="inputLog_fire" value="1"/>Log fire 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['fridge'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Fridge

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFridge" id="inputFridge" value="1"/>Fridge 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                            

                                            <div class="span3">

                                                <?php if($rec['satellite_tv'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Satellite tv

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSatellite_tv" id="inputSatellite_tv" value="1"/>Satellite tv 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['central_heating'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Central heating

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCentral_heating" id="inputCentral_heating" value="1"/>Central heating 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['freezer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Freezer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFreezer" id="inputFreezer" value="1"/>Freezer 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['video_player'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Video player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputVideo_player" id="inputVideo_player" value="1"/>Video player 

                                                </label>

                                                <?php } ?>

                                            </div>



                                        </div>

                                        <div class="row-form clearfix">

                                            

                                            <div class="span3">

                                                <?php if($rec['ac'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Air conditioning

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputAc" id="inputAc" value="1"/>Air conditioning 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['microwave'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Microwave

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputMicrowave" id="inputMicrowave" value="1"/>Microwave 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['dvd_player'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  DVD player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputDvd_player" id="inputDvd_player" value="1"/>DVD player 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['linen'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Linen provided

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputLinen" id="inputLinen" value="1"/>Linen provided 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['toaster'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Toaster

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputToaster" id="inputToaster" value="1"/>Toaster 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['cd_playe'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> CD player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCd_player" id="inputCd_player" value="1"/>CD player 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['towel'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Towels provided

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTowels" id="inputTowels" value="1"/>Towels provided 

                                                </label>

                                                <?php } ?>

                                            </div> 



                                            <div class="span3">

                                                <?php if($rec['kettle'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Kettle 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputKettle" id="inputKettle" value="1"/>Kettle 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['internet'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Internet access

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="InputInternet" id="InputInternet" value="1"/>Internet access 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['sauna'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Sauna

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSauna" id="inputSauna" value="1"/>Sauna 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['dishwasher'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Dishwasher

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputDishwasher" id="inputDishwasher" value="1"/>Dishwasher 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['wifi'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Wi-Fi avaliable

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWifi" id="inputWifi" value="1"/>Wi-Fi avaliable 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['gym'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Gym

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGym" id="inputGym" value="1"/>Gym 

                                                </label>

                                                <?php } ?>

                                            </div> 



                                            <div class="span3">

                                                <?php if($rec['washing_machine'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Washing machine

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWashing_machine" id="inputWashing_machine" value="1"/>Washing machine 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['telephone'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Telephone

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTelephone" id="inputTelephone" value="1"/>Telephone 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['table_tennis'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Table-tennis

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTable_tennis" id="inputTable_tennis" value="1"/>Table-tennis 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                           

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['clothes_dryer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Clothes dryer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputClothes_dryer" id="inputClothes_dryer" value="1"/>Clothes dryer 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['fax'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Fax machine 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFax_machine" id="inputFax_machine" value="1"/>Fax machine 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['pool_or_snooker'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Pool or Snooker table

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPool_or_snooker_table" id="inputPool_or_snooker_table" value="1"/>Pool or Snooker table 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['iron'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Iron

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputIron" id="inputIron" value="1"/>Iron 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['hair_dryer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Hair dryer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputHair_dryer" id="inputHair_dryer" value="1"/>Hair dryer 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['games_room'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Games room

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGames_room" id="inputGames_room" value="1"/>Games room 

                                                </label>

                                                <?php } ?>

                                            </div>



                                             <div class="span3">

                                                <?php if($rec['high_chair'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  High chair

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputHigh_chair" id="inputHigh_chair" value="1"/>High chair 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['safe'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Safe

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSafe" id="inputSafe" value="1"/>Safe 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['staffed_property'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Staffed property

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputStaffed_property" id="inputStaffed_property" value="1"/>Staffed property 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['cleaning_services'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Cleaning services

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCleaning_services" id="inputCleaning_services" value="1"/>Cleaning services 

                                                </label> 

                                                <?php } ?>                                   

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['ironing_board'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Ironing board

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputIroning_board" id="inputIroning_board" value="1"/>Ironing board

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['fan'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Fan

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFan" id="inputFan" value="1"/>Fan 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                    



                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Outdoors</h4> 

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['shared_outdoor_pool_heated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared Outdoor pool(heated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_outdoor_pool_heated" id="inputShared_outdoor_pool_heated" value="1"/>Shared Outdoor pool(heated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['shared_tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared tennis court 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_tennis_court" id="inputShared_tennis_court" value="1"/>Shared tennis court 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['solarium_or_roof_terrace'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Solarium or roof terrace

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSolarium_or_roof_terrace" id="inputSolarium_or_roof_terrace" value="1"/>Solarium or roof terrace 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['shared_outdoor_pool_unheated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared Outdoor pool(unheated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_outdoor_pool_unheated" id="inputShared_outdoor_pool_unheated" value="1"/>Shared Outdoor pool(unheated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            

                                            <div class="span3">

                                                <?php if($rec['private_tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Private tennis court

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_tennis_court" id="inputPrivate_tennis_court" value="1"/>Private tennis court 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['balcony_or_terrace'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Balcony or terrace

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBalancy_or_terrace" id="inputBalancy_or_terrace" value="1"/>Balcony or terrace 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['private_outdoor_pool_heated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private outdoor pool(heated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_outdoor_pool_heated" id="inputPrivate_outdoor_pool_heated" value="1"/>Private outdoor pool(heated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['shared_garden'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared garden

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_garden" id="inputShared_garden" value="1"/>Shared garden 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            

                                            <div class="span3">

                                                <?php if($rec['sea_view'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Sea view

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSea_view" id="inputSea_view" value="1"/>Sea view 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['private_outdoor_pool_unheated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private outdoor pool(unheated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_outdoor_pool" id="inputPrivate_outdoor_pool" value="1"/>Private outdoor pool(unheated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['private_garden'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private garden

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_garden" id="inputPrivate_garden" value="1"/>Private garden 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['private_fishinglake_or_river'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private fishinglake or river

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_fishinglake_or_river" id="inputPrivate_fishinglake_or_river" value="1"/>Private fishinglake or river 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['private_indoor_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private indoor pool

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_indoor_pool" id="inputPrivate_indoor_pool" value="1"/>Private indoor pool 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['climbing_frame'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Climbing frame

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputClimbing_frame" id="inputClimbing_frame" value="1"/>Climbing frame 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['boat'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Boat avaliable 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBoat" id="inputBoat" value="1"/>Boat avaliable 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['shared_indoor_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared indoor pool 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_indoor_pool" id="inputShared_indoor_pool" value="1"/>Shared indoor pool  

                                                </label>

                                                <?php } ?>

                                            </div>

                                         </div>

                                         

                                         <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['swing_set'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Swing set

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSwing_set" id="inputSwing_set" value="1"/>Swing set 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['bicycles'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Bicycle avaliable

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBicycle" id="inputBicycle" value="1"/>Bicycle avaliable

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['childrens_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>   Children's pool

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputChildrens_pool" id="inputChildrens_pool" value="1"/> Children's pool

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['trampoline'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Trampoline

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTrampoline" id="inputTrampoline" value="1"/> Trampoline

                                                </label>

                                                <?php } ?>

                                            </div>

                                         </div>

                                         

                                         <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['parking'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Parking

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputParking" id="inputParking" value="1"/> Parking

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['jacuzzi_or_hot_tub'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Jacuzzi or hot tub

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputJacuzzi_or_hot_tub" id="inputJacuzzi_or_hot_tub" value="1"/> Jacuzzi or hot tub

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['bbq'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  BBQ

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBbq" id="inputBbq" value="1"/> BBQ

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['secure_parking'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Secure parking

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSecure_parking" id="inputSecure_parking" value="1"/> Secure parking

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                         </div>

                                </div>



                                <div id="suitability">

                                	<div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['long_term_lets'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Long term lets(over 1-month) 

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputLong_term_lets" id="inputLong_term_lets" value="1"/>Long term lets(over 1-month)   

                                            </label>

                                            <?php } ?>

                                        </div>

                                        

                                        <div class="span3">

                                            <?php if($rec['corporate_bookings'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  Corporate bookings

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCorporate_bookings" id="inputCorporate_bookings" value="1"/> Corporate bookings

                                            </label>

                                            <?php } ?>

                                        </div>

                                        

                                        <div class="span3">

                                            <?php if($rec['house_swap'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  House swap

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHouse_swap" id="inputHouse_swap" value="1"/> House swap

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['short_breaks'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Short breaks(1-4 days) 

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShort_breaks" id="inputShort_breaks" value="1"/>Short breaks(1-4 days) 

                                            </label>

                                            <?php } ?>

                                        </div>

                                     </div>

                                     <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <?php if($rec['hen_or_stack_breaks'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  Hen or Stag breaks

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHen_or_stag_breaks" id="inputHen_or_stag_breaks" value="1"/> Hen or Stag breaks

                                            </label>

                                            <?php } ?>

                                        </div>

                                     </div>

                                     <div class="row-form clearfix">

                                        <div class="span3">Children</div>

                                        <div class="span6">        

                                            <select name="inputChildren" id="inputChildren" >

                                                <?php if($rec['children'] == null) { ?>

                                                    <option value="Choose">Choose</option>

                                                <?php } else { ?>

                                                    <option value="<?= $rec['children'] ?>"><?= $rec['children'] ?> </option>

                                                <?php } ?>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Pets </div>

                                        <div class="span6">        

                                            <select name="inputPets" id="inputPets" >

                                                <?php if($rec['pets'] == null) { ?>

                                                    <option value="Choose">Choose</option>

                                                <?php } else { ?>

                                                    <option value=""><?= $rec['pets'] ?> </option>

                                                <?php } ?>

                                                

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Smokers </div>

                                        <div class="span6">        

                                            <select name="inputSmokers" id="inputSmokers" >

                                                <?php if($rec['smokers'] == null) { ?>

                                                    <option value="Choose">Choose</option>

                                                <?php } else { ?>

                                                    <option value=""><?= $rec['smokers'] ?> </option>

                                                <?php } ?>

                                                

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Restricted mobility </div>

                                        <div class="span6">        

                                            <select name="inputRestricted_mobility" id="inputRestricted_mobility">

                                                <?php if($rec['restricted_mobility'] == null) { ?>

                                                    <option value="Choose">Choose</option>

                                                <?php } else { ?>

                                                     <option value=""><?= $rec['restricted_mobility'] ?> </option>

                                                <?php } ?>

                                              

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Wheelchair users</div>

                                        <div class="span6">        

                                            <select name="inputWheelchair_users" id="inputWheelchair_users" >

                                                <?php if($rec['wheelchair_users'] == null) { ?>

                                                    <option value="Choose">Choose</option>

                                                <?php } else { ?>

                                                    <option value=""><?= $rec['wheelchair_users'] ?> </option>

                                                <?php } ?>

                                                 

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>                                     

                                </div>



                                <div id="website">

                                	<p>Link to your personal website from your listing. All we ask is that you link back to us in return. 			Please use this html code to set up the link.</p>

                                    <div class="row-form clearfix">

                                        <div class="span3">Your website http://</div>

                                        <div class="span6"><input value="<?= $rec['website'] ?>"  type="text" name="inputWebsite" id="inputWebsite"/></div>

                                    </div>

                                </div>

                          </div>

                    </div>

                   </form>

                  <?php endforeach; ?>

                  <?php } ?>

            </div>

        </div>   

    </div>

     <!-- for validation -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>



     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 



</body>

</html>

