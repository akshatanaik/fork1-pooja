<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />


    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

   <?= $this->load->view('app/layouts/assets'); ?> 

    <!-- include all css and js file -->

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/plugins/uniform/uniform.js"></script>
    <script type='text/javascript' src="<?= INCLUDES ?>app/js/account.js"></script>

	
</style>
</head>

<body data-baseurl="<?php echo base_url(); ?>">

 <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   



        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>User Account</h1>

                </div> 

                <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>
                <?php $user = isset($response['user'])?$response['user']:array(); ?>
                  
              <div class="row-fluid">

                    <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-chats"></div>

                            <h1>User Account</h1>

                            <ul class="buttons">

                               <?php 

                               $id = $this->sessions->getsessiondata('user_id'); ?>

                                                      

                            </ul>       

                        </div>
     				<div class="alert alert-error" id="errormsg" style="visibility:hidden;display:none;"></div> 
                    <div class="alert alert-success" id="successmsg" style="visibility:hidden;display:none;"></div>    

                        <div class="block-fluid clearfix">


                            <form  class="form-horizontal" method="POST" id="usr_registration" action="<?= site_url('/account/update_user')?>" onsubmit="return account.update();">
                            
                                 <input type="hidden" name="id" id="id" value="<?= isset($user['id'])?$user['id']:'' ?> ">
                                    <div class="row-form clearfix" style=" float:30px;">

                                        <div class="span3">Company Name</div>

                                        <div class="span9"><input type= "text" value="<?= isset($user['company_name'])?$user['company_name']:'' ?>" id="company_name" name="company_name"/></div>

                                      </div>

                                      <div class="row-form clearfix">

                                        <div class="span3">Contact Name</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['contact_name'])?$user['contact_name']:'' ?>" id="contact_name" name="contact_name"/></div>

                                        </div>

                                     <div class="row-form clearfix">

                                        <div class="span3">Firstname</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['firstname'])?$user['firstname']:'' ?>" id="firstname" name="firstname" /></div>

                                      </div>

                                      <div class="row-form clearfix">

                                        <div class="span3">Lastname</div>

                                        <div class="span9"><input type= "text" value="<?= isset($user['lastname'])?$user['lastname']:'' ?>" id="lastname" name="lastname" /></div>

                                      </div>


                                      <div class="row-form clearfix">

                                        <div class="span3">Address</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['address'])?$user['address']:'' ?>" id="address" name="address" /></div>

                                      </div>

                                      <div class="row-form clearfix">

                                        <div class="span3">Zip/Pin code</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['zip_postal_code'])?$user['zip_postal_code']:'' ?>" id="zip_postal_code" name="zip_postal_code" /></div>

                                      </div>


                                      <div class="row-form clearfix">

                                        <div class="span3">Country</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['country'])?$user['country']:'' ?>" id="country" name="country" /></div>

                                       </div>

                                   

                                        <div class="row-form clearfix">

                                        <div class="span3">Email Address</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['email'])?$user['email']:'' ?>" id="email" name="email" /></div>

                                        </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Additional Booking Email Address</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['add_booking_email'])?$user['add_booking_email']:'' ?>" id="booking_email" name="booking_email" /></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Website URL</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['company_website'])?$user['company_website']:'' ?>" id="company_website" name="company_website" /></div>

                                    </div>


                                    <div class="row-form clearfix">

                                        <div class="span3">Telephone Number</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['phone'])?$user['phone']:''?>" id="phone" name="phone" /></div>

                                    </div> 


                                    <div class="row-form clearfix">

                                        <div class="span3">Profile Number</div>

                                        <div class="span9"><input type="text" value="<?= isset($user['id'])?$user['id']:'' ?>" id="id" name="id" /></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Company Registration Number<em style="color:#Ff0000;">*</em></div>

                                        <div class="span9"><input type="text" value="<?= isset($user['company_id'])?$user['company_id']:''?>" id="company_id" name="company_id" /></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">VAT Number<em style="color:#Ff0000;">*</em></div>

                                        <div class="span9"><input type="text" value="<?= isset($user['vat_number'])?$user['vat_number']:'' ?>" id="vat_number" name="vat_number" /></div>

                                    </div>
                                    
     								<div class="footer tar">

                                          <!--  <button class="btn" id="save_details">Save</button> -->
                                            <input type="submit" class="btn btn-success"value="<?= isset($user['id']) && $user['id'] !='' ? 'Update':'Register' ?>" />
                                            <a class="btn" href="http://localhost/booking-brain/index.php/dashboard">Cancel</a>

                                     </div>   

                              
								
                            </form>
                           

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</body>

</html>