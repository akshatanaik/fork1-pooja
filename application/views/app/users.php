<!-- bookings view -->

<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain-Users</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

    

</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">                

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- here let us build add the graphs & data  -->

            <div class="workplace">

            

            	<!-- display the list of bookings -->

            	 <div class="page-header">

                    <h1>Users</h1>

                </div>                  

                

                <div class="row-fluid">



                    <div class="span12">                    

                        <div class="head clearfix">

                            <div class="isw-grid"></div>

                            <h1>All Users</h1>      

                            <ul class="buttons">

                                <li><a href="#" class="isw-download"></a></li>                                                        

                                <li>

                                    <a href="#" class="isw-settings"></a>

                                    <ul class="dd-list">

                                        <li><a href="<?= site_url('/users/new_user') ?>"><span class="isw-plus"></span> New User</a></li>

                                        <li><a href="<?= site_url('/users/inactive_users') ?>"><span class="isw-plus"></span>Inactive Users</a></li>

                                        <li><a href="<?= site_url('/users/active_users') ?>"><span class="isw-plus"></span>Active Users</a></li>

                                        

                                    </ul>

                                </li>

                            </ul>                        

                        </div>

                        <div class="block-fluid table-sorting clearfix">

                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">

                                <thead>

                                    <tr>

                                        <th width="10%"></th>

                                        <th width="13%">ID</th>

                                        <th width="25%">Name</th>

                                        <th width="15%">Email</th>

                                        <th width="12%">Phone</th>

                                        <th width="15%">Company Name</th>

                                        <th width="15%">Website</th>

                                        

                                        

                                    </tr>

                                </thead>

                                <tbody>

                                     <?php foreach($response['records'] as $rec): ?>

                                     <?php if($rec['isactive'] == 1){ ?>

                                        <tr>

                                            <td><div class="btn-group" style="font-size: 10px;">

                                            <button style="font-size: 10px;" class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>

                                            <ul class="dropdown-menu" style="font-size: 10px;">

                                                <li><a href="<?= site_url('/users/edit_user') ?>?id=<?= $rec['id'] ?>"  >Edit</a></li>

                                                <li><a href="javascript:void(0);" onclick="deactivate('<?= $rec['id'] ?>')">Disable</a></li>

                                                <li><a href="javascript:void(0);" onclick="reset_password('<?= $rec['id'] ?>','<?= $rec['email'] ?>')" >Reset Password</a></li>

                                                

                                            </ul>

                                        </div></td>

                                            <td><?= $rec['id'] ?></td>

                                            <td><?= $rec['firstname'].' '.$rec['lastname'] ?></td>

                                            

                                            <td><?= $rec['email'] ?></td> 

                                            <td><?= $rec['phone'] ?></td>

                                            <td><?= $rec['company_name'] ?></td>

                                            <td><?= $rec['company_website'] ?></td> 

                                            

                                        </tr>

                                    <?php }else{ ?>

                                        <tr>

                                            <td><div class="btn-group" style="font-size: 10px;">

                                            <button style="font-size: 10px;" class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>

                                            <ul class="dropdown-menu" style="font-size: 10px;">

                                                <li><a href="<?= site_url('/users/edit_user') ?>?id=<?= $rec['id'] ?>"  >Edit</a></li>

                                                <li><a href="javascript:void(0);" onclick="deactivate('<?= $rec['id'] ?>')">Disable</a></li>

                                                <li><a href="javascript:void(0);" onclick="reset_password('<?= $rec['id'] ?>','<?= $rec['email'] ?>')" >Reset Password</a></li>

                                                

                                            </ul>

                                        </div></td>

                                            <td><?= $rec['id'] ?></td>

                                            <td><?= $rec['firstname'].' '.$rec['lastname'] ?></td>

                                            

                                            <td><?= $rec['email'] ?></td> 

                                            <td><?= $rec['phone'] ?></td>

                                            <td><?= $rec['company_name'] ?></td>

                                            <td><?= $rec['company_website'] ?></td> 

                                            

                                        </tr>

                                    <?php } ?>

                                    <?php endforeach; ?>

                                </tbody>

                        </div>

                    </div>



                </div>



            </div>



        </div>   

    </div>



    <script src="<?= INCLUDES ?>app/js/users.js"></script>

    

    <!-- modal to deactive user -->

     <div style="display:none;" id="deacitive-confirm" title="Deactivate?">

          <p>

             Are you sure? You want to deactivate this user.   

         </p>

    </div> 



    <!-- modal to reset password -->

    <div style="display:none;" id="reset-confirm" title="Reset Password?">

          <p>

             You want to reset password for this user?   

         </p>

    </div> 



    <!-- let us load a view to add new user so that we open a modal -->



    <script>

    	$("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});

    </script>

</body>

</html>