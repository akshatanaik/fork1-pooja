<!-- bookings view -->

<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain-Bookings</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



    

</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">                

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- here let us build add the graphs & data  -->

            <div class="workplace">

            

                <!-- display the list of bookings -->

                 <div class="page-header">

                    <h1>Pricing</h1>

                </div>                  

                

                <div class="row-fluid">



                    <div class="span12">                    

                        <div class="head clearfix">

                            <div class="isw-grid"></div>

                            <h1>Start Day for full week bookings Summary</h1>      

                           <ul class="buttons">                                                                                   

                                <li>

                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $record ?>" class="isw-settings"></a>

                                </li>

                            </ul>                        

                        </div>

                        <div class="block-fluid table-sorting clearfix">

                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">

                                <thead>

                                    <tr>

                                        <th width="25%">Month</th>

                                        <th width="25%">Weekday Start</th>

                                        <th width="25%">Weekend Start</th> 

                                        <th width="10%">Edit</th>                                                                         

                                    </tr>

                                </thead>

                                <tbody>

                                     <?php foreach($response['records'] as $rec): ?>

                                        <tr>

                                            <td><?= $rec['month'] ?></td>

                                            <td><?= $rec['weekday_start'] ?></td>

                                            

                                            <td><?= $rec['weekend_start'] ?></td> 

                                            <td><a href="<?= site_url('/pricing/edit_pricing_settings/') ?>?property_id=<?= $record ?>&&month=<?=  $rec['month'] ?>"><span class="icon-pencil"></span></a></td>                                            

                                                                              

                                        </tr>

                                    <?php endforeach; ?>

                                </tbody>

                                </table>

                        </div>

                    </div>

                </div>



                <div class="row-fluid">

                    <div class="span12">                    

                        <div class="head clearfix">

                            <div class="isw-grid"></div>

                            <h1>Short break Summary</h1> 

                            <?php if(isset($response['shortbreak']) &&  !empty($response['shortbreak'])) {  ?>     

                               

                            <?php }else{ ?> 

                                <ul class="buttons">                                                                                   

                                    <li>

                                        <a href="<?= site_url('/pricing/add_shortbreak') ?>?property_id=<?= $record ?>" class="isw-settings"></a>

                                    </li>

                                </ul>



                            <?php } ?>                       

                        </div>

                        <div class="block-fluid table-sorting clearfix">

                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable1">

                                <thead>

                                    <tr>

                                        <th width="15%">Short break pricing model</th>

                                        <th width="10%">Midweek</th>

                                        <th width="10%">Midweek percentage</th> 

                                        <th width="15%">Weekend</th>

                                        <th width="10%">Weekend percentage</th>

                                        <th width="10%">Additional charge</th>

                                        <th width="10%">Minimum price</th> 

                                        <th width="5%">Edit</th>                                 

                                    </tr>

                                </thead>

                                <tbody>

                                     <?php foreach($response['shortbreak'] as $rec): ?>

                                        <tr>

                                            <td><?= $rec['short_break_pricing_model'] ?></td>

                                            <td>    

                                                    Arrive:  <?= $rec['midweek_arrive'] ?>.<br/>

                                                    Depart:  <?= $rec['midweek_depart'] ?>.<br/>

                                                Min nights:  <?= $rec['midweek_min_nights'] ?>.

                                            </td>

                                            

                                            <td><?= $rec['midweek_percentage'] ?></td> 

                                            <td>    

                                                    Arrive:  <?= $rec['weekend_arrive'] ?>.<br/>

                                                    Depart:  <?= $rec['weekend_depart'] ?>.<br/>

                                                Min nights:  <?= $rec['weekend_min_nights'] ?>.

                                            </td>

                                            

                                            <td><?= $rec['weekend_percentage'] ?></td> 

                                            <td><?= $rec['additional_charge'] ?></td>

                                            <td><?= $rec['min_price'] ?></td>

                                            <td><a href="<?= site_url('/pricing/edit_shortbreak/') ?>?property_id=<?= $record ?>"><span class="icon-pencil"></span></a></td>

                                                                              

                                        </tr>

                                    <?php endforeach; ?>

                                </tbody>

                                </table>

                        </div>

                    </div>



                </div>



            </div>



        </div>   

    </div>



   

    <script> 

        $("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null]});

        $("#tSortable1").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null]});

    </script>









</body>

</html>