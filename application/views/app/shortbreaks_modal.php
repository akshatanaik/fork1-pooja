    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
    </style>
    <script>
        $(function() {
            $( "#end_date_sb" ).datepicker({ dateFormat: 'dd/mm/yy' });
        });      
   </script>
    <?php 
    $propertyId = $_GET['property_id'];
    $shortbreak = isset($response['shortbreak']) ? $response['shortbreak']:array(); ?>
            <form id="shortbreak_form" class="form-horizontal" action="<?= site_url('/pricing/save_shortbreak_upd')?>" method="POST" >
                <input type="hidden" id="propertyId" name="propertyId" value="<?= isset($propertyId)? $propertyId:'' ?>">
               
                <div class="block-fluid">                    
                    <div class="row-form clearfix">
                        <div class="span3">Start Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span3"><input value="<?= isset($shortbreak['start_date'])? $shortbreak['start_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Start Date" type="text" name="start_date_sb" id="start_date_sb"/></div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">End Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span3"><input value="<?= isset($shortbreak['end_date'])? $shortbreak['end_date']:'' ?>" data-validation="required" data-validation-error-msg="Please select End Date" type="text" name="end_date_sb" id="end_date_sb"/></div>
                    </div> 
                </div>
         
            </form>