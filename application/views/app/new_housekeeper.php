<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    

    <title>Booking Brain</title>
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">


    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



     <link href="<?= INCLUDES ?>/app/css/login_styles.css" rel="stylesheet" type="text/css" />





     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>





     <!-- for validation -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>

    

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>



    <!-- for ajax form submit -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/ajax_form.js' charset='utf-8'></script>

    

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 





    

      <style>

        [class*="span"] {

            float: left;

            margin-left: 0px;

            min-height: 1px;

        }

        .span12 {

            width: 1090px;

        }

        .span3 {

            width: 240px;

        }



        .block .footer, .block-fluid .footer {

            background-color: #F2F2F2;

            border-top: 1px solid #DAE1E8;

            margin-top: -1px;

            padding: 5px 5px 4px;

            text-align: right;

        }



           .form-error{ color:red; font-size: 10px;}



    </style>



</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   

        

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



           

            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>Add/Edit Housekeeper</h1>

                </div> 

                <div class="row-fluid">

                <div class="span12"> 

                    <div class="head clearfix">

                        <div class="isw-calendar"></div>

                        <h1>Add/Edit Housekeeper</h1>

                    </div>

                     <?php if($status=='error'){ ?>

                        <div id="error"><?= $response ?></div>

                     <?php }else{ ?>

                     <?= $this->load->view('app/housekeeper_form') ?>

                     <?php } ?>

                </div>

                </div>



            </div>

        </div>

    </div>

    <script>

       

    </script>

</body>

</html>