<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

  
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>



    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script>

    

    <style>

        [class*="block"] [class*="isw-"], [class*="block"] [class*="isb-"]

        {

                margin-left: 5px;

                margin-right: 1px;

                padding: 18px 0 0;

        }



        .block, .block-fluid 

        {

                margin-bottom: -11px;

        }



        [class*="span"]

        {

            float: left;

            margin-left: 0;

            min-height: 1px;

        }

    </style> 

    

</head>

<body>

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   

        

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>Property Details</h1>

                </div>

                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                <?php foreach($response['records'] as $rec): ?>

                <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-favorite"></div>

                            <h1> <?= $rec['property_name'] .'('. ($rec['property_id']) .')' ?></h1>
                            <a class="btn" style="margin-left:840px;" href="<?= site_url('property/propertyList');?>">Cancel</a>

                        </div>



                        <div class="block-fluid tabs">

                            <ul>

                                <li><a href="#essentials">Essentials</a></li>

                                <li><a href="#facilities">Facilities and Features</a></li>

                                <li><a href="#suitability">Suitability</a></li>

                                <li><a href="#website">Website</a></li>

                            </ul>



                            <?php $id = $this->uri->segment(3); ?>

                    

                            <div id="essentials">

                                <div class="row-fluid">

                                    <div class="span12"> 

                                                    

                                        <div class="row-form clearfix">

                                            <div class="span3">Property Name</div>

                                            <div class="span6"> <?= $rec['property_name'] ?></div>

                                        </div>                         



                                        <div class="row-form clearfix">

                                            <div class="span3">Property type</div>

                                            <div class="span6"><?= $rec['property_type'] ?></div>                            

                                        </div>                                                                                    



                                        <div class="row-form clearfix">

                                            <div class="span3">Maximum guests</div>

                                            <div class="span6"><?= $rec['max_guests'] ?></div>                            

                                        </div>                                                                                 



                                        <div class="row-form clearfix">

                                            <div class="span3">Main Description</div>

                                            <div class="span6"><?= $rec['main_description'] ?></div>

                                        </div>      



                                        <div class="row-form clearfix">

                                            <div class="span3">Search result summary</div>

                                            <div class="span6"><?= $rec['search_result_desc'] ?></div>

                                        </div>              

                                    </div>

                                 </div>

                            </div><!-- essentials -->

               

                   

                            <div id="facilities">

                                <div class="row-fluid">

                                    <div class="span12"> 

                               

                                    

                                        <div class="row-form clearfix">

                                            <div class="span3">Home size in Sq.m</div>

                                            <div class="span6"><?= $rec['home_size'] ?></div>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span4"><h4>Beds</h4></div>

                                            <div class="span4"><h4>Bathrooms</h4> </div>

                                            <div class="span4"><h4>Seating</h4> </div>

                                        </div>

                                         <div class="row-form clearfix">

                                            

                                            <div class="span2">Single beds</div>

                                            <div class="span2"><?= $rec['single_beds'] ?></div> 

                                            

                                            <div class="span2">Family bathrooms</div>

                                            <div class="span2"><?= $rec['family_bathrooms'] ?></div>

                                            

                                            <div class="span2">Dining seats</div>

                                            <div class="span2"><?= $rec['dining_seats'] ?></div> 

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                            <div class="span2">Double beds</div>

                                            <div class="span2"><?= $rec['double_beds'] ?></div> 

                                            

                                            <div class="span2">En suites </div>

                                            <div class="span2"><?= $rec['family_bathrooms'] ?></div> 

                                            

                                            <div class="span2">Lounge seats </div>

                                            <div class="span2"><?= $rec['en_suites'] ?></div>  

                                         </div> 

                                      

                                         <div class="row-form clearfix">

                                            <div class="span2">Sofa beds </div>

                                            <div class="span2"><?= $rec['sofa_beds'] ?></div> 

                                            

                                            <div class="span2">Shower rooms </div>

                                            <div class="span2"><?= $rec['shower_bathrooms'] ?></div> 

                                        </div>  

                                    

                                        <div class="row-form clearfix">

                                            <div class="span2"> Cots</div>

                                            <div class="span2"><?= $rec['cots'] ?></div> 

                                            

                                            <div class="span2">Total bathrooms </div>

                                            <div class="span2"><?= $rec['total_bathrooms'] ?></div> 



                                        </div>        

                                    

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Indoors</h4> 

                                            </div>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                           

                                            <div class="span3">

                                                <?php if($rec['cooker'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Cooker

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCooker" id="inputCooker" value="1" />cooker 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['tv'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Tv

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTv" id="inputTv" value="1"/>Tv

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['log_fire'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Log fire

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputLog_fire" id="inputLog_fire" value="1"/>Log fire 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['fridge'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  fridge

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFridge" id="inputFridge" value="1"/>fridge 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                        

                                            <div class="span3">

                                                <?php if($rec['satellite_tv'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Satellite tv

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSatellite_tv" id="inputSatellite_tv" value="1"/>Satellite tv 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['central_heating'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Central heating

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCentral_heating" id="inputCentral_heating" value="1"/>Central heating 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['freezer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  freezer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFreezer" id="inputFreezer" value="1"/>freezer 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['video_player'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Video player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputVideo_player" id="inputVideo_player" value="1"/>Video player 

                                                </label>

                                                <?php } ?>

                                            </div>



                                        </div>

                                        <div class="row-form clearfix">

                                        

                                            <div class="span3">

                                                <?php if($rec['ac'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Air conditioning

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputAc" id="inputAc" value="1"/>Air conditioning 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['microwave'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Microwave

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputMicrowave" id="inputMicrowave" value="1"/>Microwave 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['dvd_player'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  DVD player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputDvd_player" id="inputDvd_player" value="1"/>DVD player 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['linen'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Linen provided

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputLinen" id="inputLinen" value="1"/>Linen provided 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['toaster'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Toaster

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputToaster" id="inputToaster" value="1"/>Toaster 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['cd_playe'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> CD player

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCd_player" id="inputCd_player" value="1"/>CD player 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['towel'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Towels provided

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTowels" id="inputTowels" value="1"/>Towels provided 

                                                </label>

                                                <?php } ?>

                                            </div> 



                                            <div class="span3">

                                                <?php if($rec['kettle'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Kettle 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputKettle" id="inputKettle" value="1"/>Kettle 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['internet'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Internet access

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="InputInternet" id="InputInternet" value="1"/>Internet access 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['sauna'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Sauna

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSauna" id="inputSauna" value="1"/>Sauna 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['dishwasher'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Dishwasher

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputDishwasher" id="inputDishwasher" value="1"/>Dishwasher 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['wifi'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Wi-Fi avaliable

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWifi" id="inputWifi" value="1"/>Wi-Fi avaliable 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['gym'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Gym

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGym" id="inputGym" value="1"/>Gym 

                                                </label>

                                                <?php } ?>

                                            </div> 



                                            <div class="span3">

                                                <?php if($rec['washing_machine'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Washing machine

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWashing_machine" id="inputWashing_machine" value="1"/>Washing machine 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['telephone'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Telephone

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTelephone" id="inputTelephone" value="1"/>Telephone 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['table_tennis'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Table-tennis

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTable_tennis" id="inputTable_tennis" value="1"/>Table-tennis 

                                                </label>

                                                <?php } ?>

                                            </div>                                

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['clothes_dryer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Clothes dryer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputClothes_dryer" id="inputClothes_dryer" value="1"/>Clothes dryer 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['fax'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Fax machine 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFax_machine" id="inputFax_machine" value="1"/>Fax machine 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['pool_or_snooker'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Pool or Snooker table

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPool_or_snooker_table" id="inputPool_or_snooker_table" value="1"/>Pool or Snooker table 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['iron'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Iron

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputIron" id="inputIron" value="1"/>Iron 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['hair_dryer'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Hair dryer

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputHair_dryer" id="inputHair_dryer" value="1"/>Hair dryer 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['games_room'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Games room

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGames_room" id="inputGames_room" value="1"/>Games room 

                                                </label>

                                                <?php } ?>

                                            </div>



                                             <div class="span3">

                                                <?php if($rec['high_chair'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  High chair

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputHigh_chair" id="inputHigh_chair" value="1"/>High chair 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['safe'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Safe

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSafe" id="inputSafe" value="1"/>Safe 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                        <div class="row-form clearfix">

                                             <div class="span3">

                                                <?php if($rec['staffed_property'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Staffed property

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputStaffed_property" id="inputStaffed_property" value="1"/>Staffed property 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['cleaning_services'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Cleaning services

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCleaning_services" id="inputCleaning_services" value="1"/>Cleaning services 

                                                </label> 

                                                <?php } ?>                                   

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['ironing_board'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Ironing board

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputIroning_board" id="inputIroning_board" value="1"/>Ironing board

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['fan'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  fan

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFan" id="inputFan" value="1"/>fan 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                



                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Outdoors</h4> 

                                            </div>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['shared_outdoor_pool_heated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared Outdoor pool(heated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_outdoor_pool_heated" id="inputShared_outdoor_pool_heated" value="1"/>Shared Outdoor pool(heated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['shared_tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared tennis court 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_tennis_court" id="inputShared_tennis_court" value="1"/>Shared tennis court 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['solarium_or_roof_terrace'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Solarium or roof terrace

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSolarium_or_roof_terrace" id="inputSolarium_or_roof_terrace" value="1"/>Solarium or roof terrace 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['shared_outdoor_pool_unheated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared Outdoor pool(unheated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_outdoor_pool_unheated" id="inputShared_outdoor_pool_unheated" value="1"/>Shared Outdoor pool(unheated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                        

                                            <div class="span3">

                                                <?php if($rec['private_tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Private tennis court

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_tennis_court" id="inputPrivate_tennis_court" value="1"/>Private tennis court 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['balcony_or_terrace'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Balcony or terrace

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBalancy_or_terrace" id="inputBalancy_or_terrace" value="1"/>Balcony or terrace 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['private_outdoor_pool_heated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private outdoor pool(heated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_outdoor_pool_heated" id="inputPrivate_outdoor_pool_heated" value="1"/>Private outdoor pool(heated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['shared_garden'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared garden

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_garden" id="inputShared_garden" value="1"/>Shared garden 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                        

                                            <div class="span3">

                                                <?php if($rec['sea_view'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Sea view

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSea_view" id="inputSea_view" value="1"/>Sea view 

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['private_outdoor_pool_unheated'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private outdoor pool(unheated)

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_outdoor_pool" id="inputPrivate_outdoor_pool" value="1"/>Private outdoor pool(unheated) 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['private_garden'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private garden

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_garden" id="inputPrivate_garden" value="1"/>Private garden 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['private_fishinglake_or_river'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private fishinglake or river

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_fishinglake_or_river" id="inputPrivate_fishinglake_or_river" value="1"/>Private fishinglake or river 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['private_indoor_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Private indoor pool

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputPrivate_indoor_pool" id="inputPrivate_indoor_pool" value="1"/>Private indoor pool 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['climbing_frame'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Climbing frame

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputClimbing_frame" id="inputClimbing_frame" value="1"/>Climbing frame 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['boat'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Boat avaliable 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBoat" id="inputBoat" value="1"/>Boat avaliable 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['shared_indoor_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Shared indoor pool 

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputShared_indoor_pool" id="inputShared_indoor_pool" value="1"/>Shared indoor pool  

                                                </label>

                                                <?php } ?>

                                            </div>

                                        </div>

                                     

                                         <div class="row-form clearfix">

                                            <div class="span3">

                                                <?php if($rec['swing_set'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Swing set

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSwing_set" id="inputSwing_set" value="1"/>Swing set 

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['bicycles'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Bicycle avaliable

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBicycle" id="inputBicycle" value="1"/>Bicycle avaliable

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['childrens_pool'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>   Children's pool

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputChildrens_pool" id="inputChildrens_pool" value="1"/> Children's pool

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['trampoline'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Trampoline

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputTrampoline" id="inputTrampoline" value="1"/> Trampoline

                                                </label>

                                                <?php } ?>

                                            </div>

                                         </div>

                                     

                                         <div class="row-form clearfix">

                                           <div class="span3">

                                            <?php if($rec['parking'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Parking

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputParking" id="inputParking" value="1"/> Parking

                                                </label>

                                                <?php } ?>

                                            </div>



                                            <div class="span3">

                                                <?php if($rec['jacuzzi_or_hot_tub'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Jacuzzi or hot tub

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputJacuzzi_or_hot_tub" id="inputJacuzzi_or_hot_tub" value="1"/> Jacuzzi or hot tub

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                            <div class="span3">

                                                <?php if($rec['bbq'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  BBQ

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBbq" id="inputBbq" value="1"/> BBQ

                                                </label>

                                                <?php } ?>

                                            </div>

                                        

                                            <div class="span3">

                                                <?php if($rec['secure_parking'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Secure parking

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputSecure_parking" id="inputSecure_parking" value="1"/> Secure parking

                                                </label>

                                                <?php } ?>

                                            </div>

                                            

                                         </div>

                                    </div>

                                </div>

                            </div><!-- facilities -->



                            <div id="suitability">

                                <div class="row-fluid" id="suitability">

                                    <div class="span12">

                                

                                     <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['long_term_lets'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Long term lets(over 1-month) 

                                                </label>

                                            <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputLong_term_lets" id="inputLong_term_lets" value="1"/>Long term lets(over 1-month)   

                                                </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['corporate_bookings'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/>  Corporate bookings

                                                    </label>

                                            <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCorporate_bookings" id="inputCorporate_bookings" value="1"/> Corporate bookings

                                                </label>

                                            <?php } ?>

                                        </div>



                                         <div class="span3">

                                            <?php if($rec['house_swap'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  house swap

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHouse_swap" id="inputHouse_swap" value="1"/> house swap

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['short_breaks'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Short breaks(1-4 days) 

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShort_breaks" id="inputShort_breaks" value="1"/>Short breaks(1-4 days) 

                                            </label>

                                            <?php } ?>

                                        </div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['hen_or_stack_breaks'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  Hen or Stag breaks

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHen_or_stag_breaks" id="inputHen_or_stag_breaks" value="1"/> Hen or Stag breaks

                                            </label>

                                            <?php } ?>

                                        </div>

                                    </div>

                                    

                                    <div class="row-form clearfix">

                                        <div class="span3">Children</div>

                                        <div class="span6"><?= $rec['children'] ?></div>                            

                                    </div>  

                                    

                                    <div class="row-form clearfix">

                                        <div class="span3">Pets </div>

                                        <div class="span6"><?= $rec['pets'] ?></div>                            

                                    </div> 



                                    <div class="row-form clearfix">

                                        <div class="span3">Smokers </div>

                                        <div class="span6"><?= $rec['smokers'] ?></div>                            

                                    </div> 



                                    <div class="row-form clearfix">

                                        <div class="span3">Restricted mobility </div>

                                        <div class="span6"><?= $rec['restricted_mobility'] ?></div>                            

                                    </div>



                                     <div class="row-form clearfix">

                                        <div class="span3">Wheelchair users</div>

                                        <div class="span6"><?= $rec['wheelchair_users'] ?></div>                            

                                    </div> 

                                </div>

                            </div>

                        </div>

                      

                        <div id="website">

                            <div class="row-fluid">

                                <div class="span12"> 

                                

                                    <p>Link to your personal website from your listing. All we ask is that you link back to us in return. Please use this html code to set up the link.</p>

                                    <div class="row-form clearfix">

                                        <div class="span3">Your website http://</div>

                                        <div class="span6"><?= $rec['website'] ?></div>

                                    </div>                                        

                                </div>

                            </div>

                        </div>

                            </div>

                        </div>

                        </div>

                    </div>

                </div><!-- block fluid tabs -->

                <?php endforeach; ?>

                <?php }else{ ?>

                     <p style="text-align:center; font-size:18px;"> No property details. </p>

                <?php } ?>

            </div><!-- workplace -->

         </div> <!-- content -->  

    </div><!-- Wraper -->

</body>

</html>

