<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    <style>
        .table span[class^="icon"] {
            margin-top: -25px;
            margin-left: 20px;
        }
        .avaliablity{
            background-color:#9172EC;
        }
        .propertydetails{
            background-color:#82CAFA;
        }

        .photos{
           background-color: #1589FF;
        }

        .prices{
            background-color:#E8ADAA;
        }
        .locationdetails{
            background-color:#C12267;
        }

        .booking{
             background-color:#42BEC2;
        }

        .icon-home {
            background-position: 5px -20px;
            width:25px;
            height:25px;
        }

        .icon-book {
            background-position: -44px -44px;
            width:25px;
            height:25px;
        }
        .icon-camera {
            background-position: -115px -44px;
            width:25px;
            height:25px;
        }

        .icon-calendar {
            background-position: -186px -116px;
            width:25px;
            height:25px;
        }
        .icon-globe {
            background-position: -331px -140px;
            width:25px;
            height:25px;
        }

        .span_hover:hover {
            display: inline-block;
            outline: none;
            cursor: pointer;
            text-decoration: none;
            font: Arial, Helvetica, sans-serif;
            padding: 10px 40px; 
            text-shadow: 0 1px 1px rgba(0,0,0,.3);
            -webkit-border-radius: 1.0em; 
            -moz-border-radius: .5em;
            border-radius: .5em;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
            -moz-box-shadow: 1 1px 2px rgba(0,0,0,.2);
            box-shadow: 2px 2px rgba(184,184,184,1);
            width: 31.6239%;
            background: rgba(248,248,248,1);
        }
  
    </style> 
    
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>    
        
        <div class="menu">                
            <!-- include navigation -->
            <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            
            <!-- Main content -->
            <div class="workplace">
                <div class="page-header">
                    <h1>Property List</h1>
                </div>

                <div class="row-fluid" id="property">
                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>
                <?php foreach($response['records'] as $rec): ?>
                <?php $id = $rec['property_id'] ?>
                 <?php $role=$this->sessions->getsessiondata('role'); ?>
                <div class="row-fluid">
                    <div class="span12"> 
                        <div class="head clearfix">
                            <div class="isw-favorite"></div>
                            <h1><?= $rec['property_name'] .'('. ($rec['property_id']) .')' ?></h1>

                            <ul class="buttons">
                                <li><a href="<?= site_url('/property/editProperty/'.$id) ?>" class="isw-edit" class="tip" title="Edit"></a></li>
                                <li><a href="javascript:void(0)" onclick="deleteProperty('<?= $rec['property_id'] ?>')" class="isw-delete" class="tip" title="Delete"></a></li>
                            </ul>        
                        </div>

                        <div class="block-fluid">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table listUsers">
                                <tbody>

                                    <tr>
                                        <td width="150" height="150">
                                            <?php if($response['images']) 
                                            {
                                                $arr = $response['images'];
                                                $flag1 = 0;
                                                foreach ($arr as $img) {
                                                    if($id == $img['property_id'])
                                                    {
                                                        $rec['image'] = $img['image'];
                                                        break;
                                                    }
                                                }
                                            }
                                            
                                           if($rec['image'] != null) { ?>
                                                <a href="#"><img src="<?= $rec['image'] ?>" style="width:150px; height:150px;" class="img-polaroid"/></a>
                                            <?php }else{ ?>
                                                <h5 style="text-align:center;">No Photo Available</h5>
                                            <?php } ?>
                                        </td>
                                        <td>

                                            <div class="row-fluid" style="margin-left:10px;">
                                                <div class="span4 span_hover" onclick="location.reload();location.href='<?= site_url('/calendar'); ?>?type=month&property_id=<?= $rec['property_id']; ?>'" >

                                                    <i class="icon-calendar icon-white avaliablity" ></i> Availability
                                                    </div>
                                                <div class="span4 span_hover" onclick="location.reload();location.href='<?= site_url('/property/editPropertyDetails/'.$id); ?>'" >
                                                        <i class="icon-home icon-white propertydetails"></i> Property Details
                                                           
                                                    </div>

                                                    <!--onclick="window.open('<?= site_url('/property/propertyPhotos/'.$id); ?>')" -->
                                                <div class="span4 span_hover" onclick="location.reload();location.href='<?= site_url('/property/propertyPhotos/'.$id); ?>'" >
                                                     <i class="icon-camera icon-white  photos"></i> Photos
                                                </div>
                                            </div> <br/>

                                            <div class="dr"><span></span></div>

                                            <div class="row-fluid" style="margin-left:10px;">
                                                <div class="span4 span_hover" onclick="location.reload();location.href='<?= site_url('/pricing') ?>?property_id=<?= $rec['property_id']; ?>&type=undefined'" >
                                                     <i class="icon-calendar icon-white prices"></i> Prices
                                               </div>

                                               <div class="span4 span_hover" style="display:inline-block;" onclick="location.reload();location.href='<?= site_url('/property/editLocationDetails/'.$id); ?>'" >
                                                         <i class="icon-globe icon-white  locationdetails"></i> Location Details
                                                </div>

                                                <div class="span4 span_hover" onclick="location.reload();location.href='<?= site_url('/bookings/new_booking'); ?>?property_id=<?= $id ?>'" >  
                                                     <i class="icon-book icon-white booking"></i> New Booking
                                               </div>
                                            </div> 

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> 
                <?php endforeach; ?>
                <?php }else{ ?>
                    <p style="text-align:center; font-size:18px;"> No property list. </p>
                <?php } ?>
                </div>
                 <!-- Pagination -->
                 <div class="alt_page_navigation" id="pagination"></div> 

            </div>

         </div>
    </div>

     <link type="text/css" rel="stylesheet" href="<?= INCLUDES ?>app/css/pajinate.css" />
    <script type='text/javascript' src="<?= INCLUDES ?>app/js/jquery.pajinate.js"></script>
    <script type='text/javascript' src="<?= INCLUDES ?>app/js/jquery.pajinate.min.js"></script>

    <!-- pagination -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('.workplace').pajinate({
                items_per_page : 3,
                item_container_id : '#property',
                nav_panel_id : '.alt_page_navigation'
            });
        }); 
    </script> 

     <div style="display:none;" id="dialog-confirm" title="Delete?">
      <p>
        Are you sure? Do you want to delete this property?
     </p>
    </div>

   

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 


</body>
</html>
