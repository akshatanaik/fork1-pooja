<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <title>Booking Brain</title>
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    <link rel='stylesheet' type='text/css' href='<?= INCLUDES ?>/app/css/fullcalendar.print.css' media='print' />
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />

     <script>
        $( document ).ready(function() {
            getMonth();
        });
        function getMonth(){
            for(i = 0; i <= 11; i++){
                for(j = 0; j < i; j++){
                    $('#booking_calendar' + i).fullCalendar('next');
                }
            }
        }
    </script>

    <style>
        [class*="span"] {
            float: left;
            margin-left: 0;
            min-height: 1px;
        }

        .span12 {

            width: 1090px;

        }

        .span3 {

            width: 240px;

        }


    </style>
</head>
<?php  $property = $booking['property_id']; ?>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper">
            <!-- include header -->
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>
            <!-- display property filter as well -->
             <!-- <div class="row-fluid"> 
                  <div class="row-form clearfix">
                        <div class="span1">Property</div>
                        <div class="span6">
                            <select onchange="bookingCalendar.availability_property_wise(this.value)">
                                    <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                        <option value="">--Choose a property to filter calendar --</option>
                                        <?php foreach($response['properties'] as $res => $rec): ?>
                                            <option <?= isset($property) && $property==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                        <?php endforeach; ?>
                                    <?php } ?>
                                </select>
                        </div>
                    </div> 
            </div> -->
            <div class="dr"><span></span></div>
            <!--   -->
            <div class="workplace">
                     <div class="span12">
                         <?php if($mode != 'year'){ ?>
                        <div class="head clearfix">
                            <div class="isw-calendar"></div>
                            <h1>Calendar</h1>
                        </div>

                        <?php }if($mode == 'month') {?>
                            <div class="block-fluid">
                                <div id="booking_calendar" class="fc"></div>
                            </div>
                         <?php }elseif($mode == 'year'){ ?>
                           <table>
                                <tbody>
                                    <tr>
                                        <td class="calendar">
                                            <div class="span4">
                                                <div class="head clearfix">
                                                    <div class="isw-calendar"></div>
                                                    <h1>Calendar</h1>
                                                </div>
                                               
                                                    <div class="block-fluid">
                                                        <div id="booking_calendar0" class="fc"></div>
                                                    </div>
                                               
                                            </div>
                                        </td>
                                        <td class="calendar">
                                            <div class="span4">
                                                <div class="head clearfix">
                                                    <div class="isw-calendar"></div>
                                                    <h1>Calendar</h1>
                                                </div>
                                               <div class="block-fluid">
                                               
                                                        <div id="booking_calendar1" class="fc"></div>
                                                    </div>
                                                
                                            </div>
                                        </td>
                                        <td class="calendar"> 
                                            <div class="span4">
                                                <div class="head clearfix">
                                                    <div class="isw-calendar"></div>
                                                    <h1>Calendar</h1>
                                                </div>
                                               
                                                    <div class="block-fluid">
                                                        <div id="booking_calendar2" class="fc"></div>
                                                    </div>
                                             
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                          
                                                <div class="block-fluid">
                                                    <div id="booking_calendar3" class="fc"></div>
                                                </div>
                                          
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                            
                                                <div class="block-fluid">
                                                    <div id="booking_calendar4" class="fc"></div>
                                                </div>
                                            
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                          
                                                <div class="block-fluid">
                                                    <div id="booking_calendar5" class="fc"></div>
                                                </div>
                                         
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                           
                                                <div class="block-fluid">
                                                    <div id="booking_calendar6" class="fc"></div>
                                                </div>
                                           
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                           
                                                <div class="block-fluid">
                                                    <div id="booking_calendar7" class="fc"></div>
                                                </div>
                                          
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                         
                                                <div class="block-fluid">
                                                    <div id="booking_calendar8" class="fc"></div>
                                                </div>
                                         
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                          
                                                <div class="block-fluid">
                                                    <div id="booking_calendar9" class="fc"></div>
                                                </div>
                                          
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                          
                                                <div class="block-fluid">
                                                    <div id="booking_calendar10" class="fc"></div>
                                                </div>
                                          
                                        </div>
                                    </td>
                                    <td class="calendar">
                                        <div class="span4">
                                            <div class="head clearfix">
                                                <div class="isw-calendar"></div>
                                                <h1>Calendar</h1>
                                            </div>
                                         
                                                <div class="block-fluid">
                                                    <div id="booking_calendar11" class="fc"></div>
                                                </div>
                                          
                                        </div>
                                    </td>
                                </tr> 
                                </tbody>
                            </table>
                      
                        <?php }else{ ?>
                        <table class="fc-header" style="width:100%;margin-top:0px;">
                        
                        <tr style="border:1px solid #ccc;background-color:#f1f1f1;">

                            <td style="width:10%;">
                                <div style="margin-bottom:515px;"> 
                                     <p style="font-weight: bold;margin-left:32px;height:40px;">Properties
                                     </p> 
                                      <div> 
                                         <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                              <?php foreach($response['properties'] as $res => $rec): ?>
                                                  <div class="head clearfix" style="border:#f1f1f1;height:50px;padding-left:50px;color: #fff;" <?= isset($property) && $property==$rec['property_id'] ? 'selected="selected"':'' ?>  id="<?= $rec['property_id']?>"> 
                                                     <?= $rec['property_name']?>
                                                 </div>  
                                          <?php endforeach; ?>
                                        <?php } ?>
                                    </div>
                                
                                </div>     
                            </td>
                            <td  style="width:90%;">
                                <div class="block-fluid"  style="padding-top:0px;margin-top:0px;">
                                    <div id="booking_calendar_week" class="fc"></div>
                                </div>
                            </td>
                            
                        </tr>
                   
                        </table>
                        <?php } ?>
                    </div>
                   

            </div>
            

        </div>   
    </div>

    <!-- KEEP THE BOOKING FORM HIDDEN-->
    <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Booking</h3>
            </div>
            <div class="row-fluid" style="height:400px; overflow: scroll; overflow-x:hidden;">
                <div class="block-fluid">
                 <?= $this->load->view('app/booking_form_partial') ?>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick="bookingCalendar.save();return false;" id="save" class="btn btn-success" >Save</button>
                <button onclick="bookingCalendar.update();return false;" id="edit" class="btn btn-success" >Update</button>
                <button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
 
            </div>
    </div>

    <!-- KEEP THE BOOKING FORM HIDDEN-->
    <div id="bModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">List of Booked Properties</h3>
            </div>
            <div class="row-fluid" style="height:400px; overflow: scroll; overflow-x:hidden;">
                <div class="block-fluid">
                    <div class="row-form clearfix" id="property_name"></div> 
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
    </div>


<!-- for calendar load the necessary js -->
<script type="text/javascript" src="<?= INCLUDES ?>app/js/plugins/fullcalendar/fullcalendar.min.js" ></script>
<script type="text/javascript" src="<?= INCLUDES ?>app/js/bookingCalendar.js" ></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
<script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.timepicker.min.js"></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/booking.js'></script> 
<script>
<?php if($mode == 'month') {?>
    bookingCalendar.init('month');
<?php }elseif($mode == 'year'){ ?>
     bookingCalendar.init('year');
<?php }else{ ?>
    bookingCalendar.init('week');                     
<?php } ?>
</script>



</body>
</html>
