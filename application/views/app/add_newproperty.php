 <?php

try {
   //For Localhost 
   // $objDb = new PDO('mysql:host=localhost;dbname=booking_brain_db', 'root', '');

    //For Server 
    $objDb = new PDO('mysql:host=internal-db.s75223.gridserver.com;dbname=db75223_booking_brain_db', 'db75223', 'Vbjq2euO');
    $objDb->exec('SET CHARACTER SET utf8');
    $sql = "SELECT continent
         FROM cont_country group by continent";
    $statement = $objDb->query($sql);
    $list = $statement->fetchAll(PDO::FETCH_ASSOC);
   
    
} catch(PDOException $e) {
    echo 'There was a problem';
}

?>


<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    

    <title>Booking Brain</title>
    <link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">


    <!-- include all css and js file -->
    
    <?= $this->load->view('app/layouts/assets'); ?>


    <script type='text/javascript'>       
     
        function get_property_address(){
            var addrs =document.getElementById('inputProperty_address').value ;

            document.getElementById("iframe").src = "https://www.google.com/maps/embed/v1/place?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q="+ addrs;
            
        }        
    
    </script>

    <style>

        [class*="span"] {

            float: left;

            margin-left: 0px;

            min-height: 1px;

        }

        .span12 {

            width: 1090px;

        }

        .span3 {

            width: 240px;

        }



        .block .footer, .block-fluid .footer {

            background-color: #F2F2F2;

            border-top: 1px solid #DAE1E8;

            margin-top: -1px;

            padding: 5px 5px 4px;

            text-align: right;

        }



        .form-error{ color:red; font-size: 10px;}

      #inputCountry, #inputRegion, #inputSubregion, #inputTown, #inputSuburb
      {
          display:none;
          visibility:"hidden";
          
     }
     select {
            width:100px; 
            overflow:hidden; 
            white-space:pre; 
            text-overflow:ellipsis;
            -webkit-appearance: none;

        }
        option {
                  border: solid 1px #DDDDDD; 
        }


      

    </style>
 


</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">                

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!--   -->

            <div class="workplace">

                <?php $id = $this->uri->segment(3); ?>

                 <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                 <?php $method= $edit_method_property;?>

                    <form class="form-horizontal" action="<?= site_url('/property/'.$method.'/'.$id)?>" method="POST" onsubmit="return propertydetails(this);">

                <?php }else{ ?>

                <?php $method= $add_method_property;?>

                    <form class="form-horizontal" action="<?= site_url('/property/'.$method.'')?>" method="POST" onsubmit="return propertydetails(this);">

                <?php } ?>

                     <div class="span11">

                        <div class="head clearfix">

                            <div class="isw-favorite"></div>

                            <h1><?= $response['title'] ?></h1>



                            <!--<div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                <button class="btn btn-success">Save</button>

                                  <a class="btn" href="<?= site_url('property/propertyList');?>">Cancel</a>

                            </div>-->

                            

                        </div>

                        <!-- build  the form -->

                          <div class="block-fluid tabs" id="tabs">

                                 <ul >

                                    <li><a href="#essentials">Essentials</a></li>

                                    <li><a href="#facilities">Facilities and Features</a></li>

                                    <li><a href="#suitability">Suitability</a></li>

                                    <li><a href="#website">Website</a></li>                                    

                                    <li><a href="#property_location">Property location</a></li>

                                    <li><a href="#localarea">Local area</a></li>

                                    <li><a href="#how_to_get_there">How to get there</a></li>

                                    <li><a href="#whats_nearby">What's nearby</a></li>

                                    <li><a href="#holiday_types">Holiday types</a></li>

                                </ul> 



                                

                                    <div id="essentials">

                                        <div class="row-form clearfix">

                                            <div class="span3">Property Name<em style="color:#Ff0000;">*</em></div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>  

                                                <div class="span6"><input value="<?= $rec['property_name'] ?>"  type="text" name="inputProperty_name" id="inputProperty_name" data-validation="required" data-validation-error-msg="Property Name field can not be empty."/></div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input value=""  type="text" name="inputProperty_name" id="inputProperty_name" data-validation="required" data-validation-error-msg="Property Name field can not be empty."/></div>

                                            <?php } ?>

                                        </div>                         



                                        <div class="row-form clearfix">

                                            <div class="span3">Property type</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>  

                                                <div class="span6">        
                                                    <select name="inputProperty_type" id="inputProperty_type">                                                                                                    

                                                        <option value="<?= $rec['property_type'] ?>"><?= $rec['property_type'] ?></option>
                                                        <?php if($rec['property_type'] == 'Cottage'){ ?>
                                                            <option value="House">House</option>
                                                            <option value="Farm House">Farm House</option>
                                                            <option value="Apartment">Apartment</option>
                                                            <option value="Studio">Studio</option>
                                                        <?php }else if($rec['property_type'] == 'House'){ ?>
                                                            <option value="Cottage">Cottage</option>
                                                            <option value="Farm House">Farm House</option>
                                                            <option value="Apartment">Apartment</option>
                                                            <option value="Studio">Studio</option>
                                                        <?php }else if($rec['property_type'] == 'Farm House'){ ?>
                                                            <option value="Cottage">Cottage</option>
                                                            <option value="House">House</option>
                                                            <option value="Apartment">Apartment</option>
                                                            <option value="Studio">Studio</option>
                                                        <?php }else if($rec['property_type'] == 'Apartment'){ ?>
                                                            <option value="Cottage">Cottage</option>
                                                            <option value="House">House</option>
                                                            <option value="Farm House">Farm House</option>
                                                            <option value="Studio">Studio</option>
                                                        <?php } else{ ?>
                                                            <option value="Cottage">Cottage</option>
                                                            <option value="House">House</option>
                                                            <option value="Farm House">Farm House</option>
                                                            <option value="Apartment">Apartment</option>
                                                        <?php } ?>                                                      
                                                    </select>
                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputProperty_type" id="inputProperty_type">

                                                        <option value="Choose">Choose</option>                                                       
                                                        <option value="Cottage">Cottage</option>
                                                        <option value="House">House</option>
                                                        <option value="Farm House">Farm House</option>
                                                        <option value="Apartment">Apartment</option>
                                                        <option value="Studio">Studio</option>


                                                    </select>

                                                </div>

                                            <?php } ?>                            

                                        </div>                                                                                    



                                        <div class="row-form clearfix">

                                            <div class="span3">Maximum Guests</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>  

                                                <div class="span6">        

                                                <select name="inputMax_guest" id="inputMax_guest" data-validation="required" data-validation-error-msg="Maximum Guests field can not be empty.">
                                                    <?php for($i=1;$i<=30;$i++) { ?>
                                                        <option <?= isset($rec['max_guests']) && $rec['max_guests']==$i ? 'selected="selected"':'' ?> value="<?= $i ?>"><?= $i ?></option>
                                                    <?php } ?>                                                 

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputMax_guest" id="inputMax_guest" data-validation="required" data-validation-error-msg="Maximum Guests field can not be empty.">

                                                        <option value="">Choose </option>
                                                        <option value="1">1 </option>
                                                        <option value="2">2 </option>
                                                        <option value="3">3 </option>
                                                        <option value="1">4 </option>
                                                        <option value="2">5 </option>
                                                        <option value="3">6 </option>
                                                        <option value="1">7 </option>
                                                        <option value="2">8 </option>
                                                        <option value="3">9 </option>
                                                        <option value="1">10 </option>
                                                        <option value="2">11 </option>
                                                        <option value="3">12 </option>
                                                        <option value="1">13 </option>
                                                        <option value="2">14 </option>
                                                        <option value="3">15 </option>
                                                        <option value="1">16 </option>
                                                        <option value="2">17 </option>
                                                        <option value="3">18 </option>
                                                        <option value="1">19 </option>
                                                        <option value="2">20 </option>
                                                        <option value="3">21 </option>
                                                        <option value="1">22 </option>
                                                        <option value="2">23 </option>
                                                        <option value="3">24 </option>
                                                        <option value="3">25 </option>
                                                        <option value="1">26 </option>
                                                        <option value="2">27 </option>
                                                        <option value="3">28 </option>
                                                        <option value="1">29 </option>
                                                        <option value="2">30 </option>
                                                       
                                                    </select>

                                                </div>

                                            <?php } ?>                            

                                        </div>                                                                                 



                                        <div class="row-form clearfix">

                                            <div class="span3">Main Description</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>  

                                                <div class="span6">        

                                                <textarea name="inputMain_description" id="inputMain_description"><?= $rec['main_description'] ?></textarea>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <textarea name="inputMain_description" id="inputMain_description"></textarea>

                                                </div>

                                            <?php } ?>

                                        </div>      



                                        <div class="row-form clearfix">

                                            <div class="span3">Search result summary</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                            <div class="span6"> 

                                                <textarea name="inputSearch_result" id="inputSearch_result"><?= $rec['search_result_desc'] ?></textarea>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                            <div class="span6">        

                                                <textarea name="inputSearch_result" id="inputSearch_result"></textarea>

                                            </div>

                                            <?php } ?>

                                        </div> 
                                        
                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>

                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>                                  

                                    </div><!-- essentials -->



                                    <div id="facilities">

                                        <div class="row-form clearfix">

                                            <div class="span2">Home size in Sq.m</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>  

                                                <div class="span6"><input value="<?= $rec['home_size'] ?>"  type="text" name="inputHome_size" id="inputHome_size"/> </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input type="text" name="inputHome_size" id="inputHome_size"/> </div>

                                            <?php } ?>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span4"><h4>Beds</h4></div>

                                            <div class="span4"><h4>Bathrooms</h4></div>

                                            <div class="span3"><h4>Seating</h4></div>

                                        </div>

                                         <div class="row-form clearfix">

                                            

                                            <div class="span2">Single beds</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputSingle_bed" id="inputSingle_bed" style=" margin: 0px 0px 0px -55px;">                                                    

                                                   <?php  if($rec['single_beds'] == 1){ ?>

                                                        <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['single_beds'] == 2){ ?>

                                                        <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['single_beds'] ?>"><?= $rec['single_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputSingle_bed" id="inputSingle_bed"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?> 

                                    

                                            <div class="span2">Family bathrooms</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputFamily_bathrooms" id="inputFamily_bathrooms"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                    <?php  if($rec['family_bathrooms'] == 1){ ?>

                                                        <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['family_bathrooms'] == 2){ ?>

                                                    <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['family_bathrooms'] ?>"><?= $rec['family_bathrooms'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputFamily_bathrooms" id="inputFamily_bathrooms"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?>

                                    

                                            <div class="span2">Dining seats</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputDining_seats" id="inputDining_seats"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                    <?php  if($rec['dining_seats'] == 1){ ?>

                                                        <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['dining_seats'] == 2){ ?>

                                                        <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['dining_seats'] ?>"><?= $rec['dining_seats'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputDining_seats" id="inputDining_seats"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div> 

                                            <?php } ?>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                            <div class="span2">Double beds</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputDouble_beds" id="inputDouble_beds"  style=" margin: 0px 0px 0px -55px;">

                                                    

                                                    <?php  if($rec['double_beds'] == 1){ ?>

                                                        <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['double_beds'] == 2){ ?>

                                                        <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['double_beds'] ?>"><?= $rec['double_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputDouble_beds" id="inputDouble_beds"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?>



                                            <div class="span2">En suites </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span2">        

                                                    <select name="inputEn_suites" id="inputEn_suites"  style=" margin: 0px 0px 0px -55px;">                                                        

                                                        <?php  if($rec['en_suites'] == 1){ ?>

                                                        <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['en_suites'] == 2){ ?>

                                                        <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['en_suites'] ?>"><?= $rec['en_suites'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                    </select>

                                                </div>                                                 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputEn_suites" id="inputEn_suites"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div> 

                                            <?php } ?>



                                            <div class="span2">Lounge seats </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span2">        

                                                <select name="inputLounge_seats" id="inputLounge_seats"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                    <?php  if($rec['lounge_seats'] == 1){ ?>

                                                        <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['lounge_seats'] == 2){ ?>

                                                        <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['lounge_seats'] ?>"><?= $rec['lounge_seats'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputLounge_seats" id="inputLounge_seats"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?>  

                                         </div> 

                                      

                                         <div class="row-form clearfix">

                                            <div class="span2">Sofa beds </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputSofa_beds" id="inputSofa_beds"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                    <?php  if($rec['sofa_beds'] == 1){ ?>

                                                        <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['sofa_beds'] == 2){ ?>

                                                        <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['sofa_beds'] ?>"><?= $rec['sofa_beds'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputSofa_beds" id="inputSofa_beds"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose</option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?>



                                            <div class="span2">Shower rooms </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputShower_rooms" id="inputShower_rooms"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                    <?php  if($rec['shower_bathrooms'] == 1){ ?>

                                                        <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['shower_bathrooms'] == 2){ ?>

                                                        <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['shower_bathrooms'] ?>"><?= $rec['shower_bathrooms'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputShower_rooms" id="inputShower_rooms"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose   </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?> 



                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span2"> Cots</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                <select name="inputCots" id="inputCots"  style=" margin: 0px 0px 0px -55px;">                                                    

                                                   <?php  if($rec['cots'] == 1){ ?>

                                                        <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?></option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['cots'] == 2){ ?>

                                                        <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['cots'] ?>"><?= $rec['cots'] ?></option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                </select>

                                            </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputCots" id="inputCots"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose   </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div> 

                                            <?php } ?>



                                            <div class="span2">Total bathrooms </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span2">        

                                                    <select name="inputTotal_bathrooms" id="inputTotal_bathrooms"  style=" margin: 0px 0px 0px -55px;">                                                        

                                                        <?php  if($rec['total_bathrooms'] == 1){ ?>

                                                        <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?> </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    <?php }elseif($rec['total_bathrooms'] == 2){ ?>

                                                        <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="3">3 </option>

                                                    <?php }else{ ?>

                                                        <option value="<?= $rec['total_bathrooms'] ?>"><?= $rec['total_bathrooms'] ?> </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                    <?php } ?>

                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span2">        

                                                    <select name="inputTotal_bathrooms" id="inputTotal_bathrooms"  style=" margin: 0px 0px 0px -55px;">

                                                        <option value="Choose">Choose   </option>

                                                        <option value="1">1 </option>

                                                        <option value="2">2 </option>

                                                        <option value="3">3 </option>

                                                    </select>

                                                </div>

                                            <?php } ?> 



                                        </div>        

                                    

                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Indoors</h4> 

                                            </div>

                                        </div>

                                

                                        <div class="row-form clearfix">

                                           

                                           <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['cooker'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Cooker

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCooker" id="inputCooker" value="1" />Cooker 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" name="inputCooker" id="inputCooker" value="1" />Cooker

                                                        </label>

                                                </div>

                                            <?php } ?>



                                           <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['tv'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  TV

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTv" id="inputTv" value="1"/>TV

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                     <label class="checkbox inline">

                                                            <input type="checkbox" name="inputTv" id="inputTv" value="1"/>TV

                                                    </label>

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['log_fire'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Log fire

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLog_fire" id="inputLog_fire" value="1"/>Log fire 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLog_fire" id="inputLog_fire" value="1"/>Log fire 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['fridge'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Fridge

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFridge" id="inputFridge" value="1"/>Fridge 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFridge" id="inputFridge" value="1"/>Fridge 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['satellite_tv'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Satellite tv

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSatellite_tv" id="inputSatellite_tv" value="1"/>Satellite tv 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSatellite_tv" id="inputSatellite_tv" value="1"/>Satellite tv 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['central_heating'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Central heating

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCentral_heating" id="inputCentral_heating" value="1"/>Central heating 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCentral_heating" id="inputCentral_heating" value="1"/>Central heating 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>   

                                                <div class="span3">

                                                    <?php if($rec['freezer'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Freezer

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFreezer" id="inputFreezer" value="1"/>Freezer 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFreezer" id="inputFreezer" value="1"/>Freezer 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?> 

                                                <div class="span3">

                                                    <?php if($rec['video_player'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Video player

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputVideo_player" id="inputVideo_player" value="1"/>Video player 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputVideo_player" id="inputVideo_player" value="1"/>Video player 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                        </div>

                                        <div class="row-form clearfix">

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?> 

                                                <div class="span3">

                                                    <?php if($rec['ac'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Air conditioning

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputAc" id="inputAc" value="1"/>Air conditioning 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputAc" id="inputAc" value="1"/>Air conditioning 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span3">

                                                    <?php if($rec['microwave'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Microwave

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputMicrowave" id="inputMicrowave" value="1"/>Microwave 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputMicrowave" id="inputMicrowave" value="1"/>Microwave 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['dvd_player'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  DVD player

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputDvd_player" id="inputDvd_player" value="1"/>DVD player 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputDvd_player" id="inputDvd_player" value="1"/>DVD player 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['linen'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Linen provided

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLinen" id="inputLinen" value="1"/>Linen provided 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLinen" id="inputLinen" value="1"/>Linen provided 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                           <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                   <div class="span3">

                                                    <?php if($rec['toaster'] != 0) { ?>

                                                            <label class="checkbox inline">

                                                                <input type="checkbox" checked="checked"/>  Toaster

                                                            </label>

                                                        <?php }else{ ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" name="inputToaster" id="inputToaster" value="1"/>Toaster 

                                                        </label>

                                                        <?php } ?>

                                                    </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                            <input type="checkbox" name="inputToaster" id="inputToaster" value="1"/>Toaster 

                                                        </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['cd_playe'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> CD player

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCd_player" id="inputCd_player" value="1"/>CD player 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCd_player" id="inputCd_player" value="1"/>CD player 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['towel'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Towels provided

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTowels" id="inputTowels" value="1"/>Towels provided 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTowels" id="inputTowels" value="1"/>Towels provided 

                                                    </label>

                                                </div>

                                            <?php } ?> 



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['kettle'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Kettle 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputKettle" id="inputKettle" value="1"/>Kettle 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputKettle" id="inputKettle" value="1"/>Kettle 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span3">

                                                    <?php if($rec['internet'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Internet access

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="InputInternet" id="InputInternet" value="1"/>Internet access 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="InputInternet" id="InputInternet" value="1"/>Internet access 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['sauna'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Sauna

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSauna" id="inputSauna" value="1"/>Sauna 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSauna" id="inputSauna" value="1"/>Sauna 

                                                    </label>

                                                 </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['dishwasher'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Dishwasher

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputDishwasher" id="inputDishwasher" value="1"/>Dishwasher 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputDishwasher" id="inputDishwasher" value="1"/>Dishwasher 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['wifi'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Wi-Fi avaliable

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWifi" id="inputWifi" value="1"/>Wi-Fi avaliable 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWifi" id="inputWifi" value="1"/>Wi-Fi avaliable 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                           <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                               <div class="span3">

                                                <?php if($rec['gym'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Gym

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGym" id="inputGym" value="1"/>Gym 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3"> 

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGym" id="inputGym" value="1"/>Gym 

                                                    </label>

                                                 </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['washing_machine'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Washing machine

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWashing_machine" id="inputWashing_machine" value="1"/>Washing machine 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWashing_machine" id="inputWashing_machine" value="1"/>Washing machine 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['telephone'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Telephone

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTelephone" id="inputTelephone" value="1"/>Telephone 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTelephone" id="inputTelephone" value="1"/>Telephone 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['table_tennis'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Table-tennis

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTable_tennis" id="inputTable_tennis" value="1"/>Table-tennis 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTable_tennis" id="inputTable_tennis" value="1"/>Table-tennis 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                           

                                        </div>

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['clothes_dryer'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Clothes dryer

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputClothes_dryer" id="inputClothes_dryer" value="1"/>Clothes dryer 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputClothes_dryer" id="inputClothes_dryer" value="1"/>Clothes dryer 

                                                    </label>

                                                </div>

                                            <?php } ?> 

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['fax'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Fax machine 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFax_machine" id="inputFax_machine" value="1"/>Fax machine 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFax_machine" id="inputFax_machine" value="1"/>Fax machine 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                        

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['pool_or_snooker'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Pool or Snooker table

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPool_or_snooker_table" id="inputPool_or_snooker_table" value="1"/>Pool or Snooker table 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPool_or_snooker_table" id="inputPool_or_snooker_table" value="1"/>Pool or Snooker table 

                                                    </label>

                                                </div>

                                            <?php } ?>





                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['iron'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Iron

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputIron" id="inputIron" value="1"/>Iron 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputIron" id="inputIron" value="1"/>Iron 

                                                    </label>

                                                </div>

                                            <?php } ?>  

                                            

                                            

                                        </div>

                                        <div class="row-form clearfix">

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span3">

                                                    <?php if($rec['hair_dryer'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Hair dryer

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHair_dryer" id="inputHair_dryer" value="1"/>Hair dryer 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHair_dryer" id="inputHair_dryer" value="1"/>Hair dryer 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['games_room'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Games room

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGames_room" id="inputGames_room" value="1"/>Games room 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGames_room" id="inputGames_room" value="1"/>Games room 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span3">

                                                    <?php if($rec['high_chair'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  High chair

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHigh_chair" id="inputHigh_chair" value="1"/>High chair 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHigh_chair" id="inputHigh_chair" value="1"/>High chair 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['safe'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Safe

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSafe" id="inputSafe" value="1"/>Safe 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSafe" id="inputSafe" value="1"/>Safe 

                                                    </label>

                                                </div>

                                            <?php } ?>                  

                                        </div>

                                        <div class="row-form clearfix">

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                 <div class="span3">

                                                    <?php if($rec['staffed_property'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Staffed property

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputStaffed_property" id="inputStaffed_property" value="1"/>Staffed property 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputStaffed_property" id="inputStaffed_property" value="1"/>Staffed property 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['cleaning_services'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Cleaning services

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCleaning_services" id="inputCleaning_services" value="1"/>Cleaning services 

                                                    </label> 

                                                    <?php } ?>                                   

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCleaning_services" id="inputCleaning_services" value="1"/>Cleaning services 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['ironing_board'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Ironing board

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputIroning_board" id="inputIroning_board" value="1"/>Ironing board

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputIroning_board" id="inputIroning_board" value="1"/>Ironing board

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['fan'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Fan

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFan" id="inputFan" value="1"/>Fan 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFan" id="inputFan" value="1"/>Fan 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                        </div>

                                    



                                        <div class="row-form clearfix">

                                           <div class="span3">

                                               <h4>Outdoors</h4> 

                                            </div>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['shared_outdoor_pool_heated'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Shared Outdoor pool(heated)

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_outdoor_pool_heated" id="inputShared_outdoor_pool_heated" value="1"/>Shared Outdoor pool(heated) 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_outdoor_pool_heated" id="inputShared_outdoor_pool_heated" value="1"/>Shared Outdoor pool(heated) 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['shared_tennis_court'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Shared tennis court 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_tennis_court" id="inputShared_tennis_court" value="1"/>Shared tennis court 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_tennis_court" id="inputShared_tennis_court" value="1"/>Shared tennis court 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['solarium_or_roof_terrace'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Solarium or roof terrace

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSolarium_or_roof_terrace" id="inputSolarium_or_roof_terrace" value="1"/>Solarium or roof terrace 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSolarium_or_roof_terrace" id="inputSolarium_or_roof_terrace" value="1"/>Solarium or roof terrace 

                                                    </label>

                                                 </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['shared_outdoor_pool_unheated'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Shared Outdoor pool(unheated)

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_outdoor_pool_unheated" id="inputShared_outdoor_pool_unheated" value="1"/>Shared Outdoor pool(unheated) 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_outdoor_pool_unheated" id="inputShared_outdoor_pool_unheated" value="1"/>Shared Outdoor pool(unheated) 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_tennis_court'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Private tennis court

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_tennis_court" id="inputPrivate_tennis_court" value="1"/>Private tennis court 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_tennis_court" id="inputPrivate_tennis_court" value="1"/>Private tennis court 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['balcony_or_terrace'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Balcony or terrace

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBalancy_or_terrace" id="inputBalancy_or_terrace" value="1"/>Balcony or terrace 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBalancy_or_terrace" id="inputBalancy_or_terrace" value="1"/>Balcony or terrace 

                                                    </label>

                                                 </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_outdoor_pool_heated'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Private outdoor pool(heated)

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_outdoor_pool_heated" id="inputPrivate_outdoor_pool_heated" value="1"/>Private outdoor pool(heated) 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_outdoor_pool_heated" id="inputPrivate_outdoor_pool_heated" value="1"/>Private outdoor pool(heated) 

                                                    </label>

                                                 </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['shared_garden'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Shared garden

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_garden" id="inputShared_garden" value="1"/>Shared garden 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_garden" id="inputShared_garden" value="1"/>Shared garden 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        

                                        <div class="row-form clearfix">

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['sea_view'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Sea view

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSea_view" id="inputSea_view" value="1"/>Sea view 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSea_view" id="inputSea_view" value="1"/>Sea view 

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_outdoor_pool_unheated'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Private outdoor pool(unheated)

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_outdoor_pool" id="inputPrivate_outdoor_pool" value="1"/>Private outdoor pool(unheated) 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_outdoor_pool" id="inputPrivate_outdoor_pool" value="1"/>Private outdoor pool(unheated) 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_garden'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Private garden

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_garden" id="inputPrivate_garden" value="1"/>Private garden 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_garden" id="inputPrivate_garden" value="1"/>Private garden 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_fishinglake_or_river'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Private fishinglake or river

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_fishinglake_or_river" id="inputPrivate_fishinglake_or_river" value="1"/>Private fishinglake or river 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_fishinglake_or_river" id="inputPrivate_fishinglake_or_river" value="1"/>Private fishinglake or river 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                    

                                        <div class="row-form clearfix">

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['private_indoor_pool'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Private indoor pool

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_indoor_pool" id="inputPrivate_indoor_pool" value="1"/>Private indoor pool 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputPrivate_indoor_pool" id="inputPrivate_indoor_pool" value="1"/>Private indoor pool 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['climbing_frame'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Climbing frame

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputClimbing_frame" id="inputClimbing_frame" value="1"/>Climbing frame 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                              <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputClimbing_frame" id="inputClimbing_frame" value="1"/>Climbing frame 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['boat'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Boat avaliable 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBoat" id="inputBoat" value="1"/>Boat avaliable 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBoat" id="inputBoat" value="1"/>Boat avaliable 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['shared_indoor_pool'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Shared indoor pool 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_indoor_pool" id="inputShared_indoor_pool" value="1"/>Shared indoor pool  

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShared_indoor_pool" id="inputShared_indoor_pool" value="1"/>Shared indoor pool  

                                                    </label>

                                                </div>

                                            <?php } ?>

                                         </div>

                                     

                                         <div class="row-form clearfix">

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['swing_set'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Swing set

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSwing_set" id="inputSwing_set" value="1"/>Swing set 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSwing_set" id="inputSwing_set" value="1"/>Swing set 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['bicycles'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Bicycle avaliable

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBicycle" id="inputBicycle" value="1"/>Bicycle avaliable

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBicycle" id="inputBicycle" value="1"/>Bicycle avaliable

                                                    </label>

                                                </div>

                                            <?php } ?>



                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['childrens_pool'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>   Children's pool

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputChildrens_pool" id="inputChildrens_pool" value="1"/> Children's pool

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputChildrens_pool" id="inputChildrens_pool" value="1"/> Children's pool

                                                    </label>

                                                </div>

                                            <?php } ?>

                                           

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['trampoline'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Trampoline

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTrampoline" id="inputTrampoline" value="1"/> Trampoline

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputTrampoline" id="inputTrampoline" value="1"/> Trampoline

                                                    </label>

                                                </div>

                                            <?php } ?>

                                         </div>

                                         

                                         <div class="row-form clearfix">

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['swing_set'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Parking  

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputParking" id="inputParking" value="1"/>Parking

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputParking" id="inputParking" value="1"/> Parking

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['bicycles'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Jacuzzi or hot tub

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputJacuzzi_or_hot_tub" id="inputJacuzzi_or_hot_tub" value="1"/>Jacuzzi or hot tub

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputJacuzzi_or_hot_tub" id="inputJacuzzi_or_hot_tub" value="1"/>Jacuzzi or hot tub

                                                    </label>

                                                </div>

                                            <?php } ?>



                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['childrens_pool'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> BBQ

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBbq" id="inputBbq" value="1"/> BBQ

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBbq" id="inputBbq" value="1"/> BBQ

                                                    </label>

                                                </div>

                                            <?php } ?>

                                           

                                            

                                              <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['trampoline'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Secure parking

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSecure_parking" id="inputSecure_parking" value="1"/> Secure parking

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputSecure_parking" id="inputSecure_parking" value="1"/> Secure parking

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                         </div>  
                                         
                                         <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>                                    

                                    </div>



                                    <div id="suitability">

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['long_term_lets'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Long term lets(over 1-month) 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLong_term_lets" id="inputLong_term_lets" value="1"/>Long term lets(over 1-month)   

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputLong_term_lets" id="inputLong_term_lets" value="1"/>Long term lets(over 1-month)   

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['corporate_bookings'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Corporate bookings

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCorporate_bookings" id="inputCorporate_bookings" value="1"/> Corporate bookings

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCorporate_bookings" id="inputCorporate_bookings" value="1"/> Corporate bookings

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['house_swap'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  House swap

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHouse_swap" id="inputHouse_swap" value="1"/> House swap

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHouse_swap" id="inputHouse_swap" value="1"/> House swap

                                                    </label>

                                                </div>

                                            <?php } ?>

                                            

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['short_breaks'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Short breaks(1-4 days) 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShort_breaks" id="inputShort_breaks" value="1"/>Short breaks(1-4 days) 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputShort_breaks" id="inputShort_breaks" value="1"/>Short breaks(1-4 days) 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">                                        

                                           <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['hen_or_stack_breaks'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>  Hen or Stag breaks

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHen_or_stag_breaks" id="inputHen_or_stag_breaks" value="1"/> Hen or Stag breaks

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHen_or_stag_breaks" id="inputHen_or_stag_breaks" value="1"/> Hen or Stag breaks

                                                    </label>

                                                </div>

                                            <?php } ?>

                                         </div>

                                         <div class="row-form clearfix">

                                           <div class="span3">Children</div>

                                             <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputChildren" id="inputChildren" >

                                                        <option value="<?= $rec['children'] ?>"><?= $rec['children'] ?> </option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">

                                                    <select name="inputChildren" id="inputChildren" >

                                                        <option value="">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php } ?>                              

                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span3">Pets </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputPets" id="inputPets" >

                                                        <option value=""><?= $rec['pets'] ?> </option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputPets" id="inputPets" >

                                                        <option value="">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>

                                                                       

                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span3">Smokers </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputSmokers" id="inputSmokers" >

                                                        <option value=""><?= $rec['smokers'] ?> </option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputSmokers" id="inputSmokers" >

                                                        <option value="">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php } ?>                            

                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span3">Restricted mobility </div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputRestricted_mobility" id="inputRestricted_mobility">

                                                       <option value=""><?= $rec['restricted_mobility'] ?> </option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputRestricted_mobility" id="inputRestricted_mobility">

                                                       <option value="">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php } ?>                            

                                        </div>  

                                        <div class="row-form clearfix">

                                            <div class="span3">Wheelchair users</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputWheelchair_users" id="inputWheelchair_users" >

                                                         <option value=""><?= $rec['wheelchair_users'] ?> </option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputWheelchair_users" id="inputWheelchair_users" >

                                                         <option value="">Choose</option>

                                                        <option value="1">1</option>

                                                        <option value="2">2</option>

                                                    </select>

                                                </div>

                                            <?php } ?>                        

                                        </div>
                                        
                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>                          

                                    </div>



                                    <div id="website">

                                        <p>Link to your personal website from your listing. All we ask is that you link back to us in return. Please use this html code to set up the link.</p>

                                        <div class="row-form clearfix">

                                            <div class="span3">Your website http://</div>

                                            <?php if(isset($response['property']) &&  !empty($response['property'])) {  ?>

                                            <?php foreach($response['property'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['website'] ?>"  type="text" name="inputWebsite" id="inputWebsite"/></div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input type="text" name="inputWebsite" id="inputWebsite"/></div>

                                            <?php } ?>

                                        </div> 
                                        
                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>   

                                    </div>


                                
                                    <div id="property_location">

                                        <div class="row-form clearfix">

                                            <div class="span3">Continent</div>

                                           <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>     

                                                <div class="span6">        

                                                    <select name="inputContinent" id="inputContinent" onclick="calljavascriptfunction(id,'cont_country','continent','inputCountry','country');" data-validation="required" data-validation-error-msg="Continent field cannot be empty." >

                                                        <option value="<?= $rec['continent'] ?>"><?= $rec['continent'] ?></option>

                                                         <?php if (!empty($list)) { ?>
                                                               <?php foreach($list as $row) { ?>
                                                                    <option value="<?php echo $row['continent']; ?>">
                                                                        <?php echo $row['continent']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                        <?php } ?>

                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">        

                                                    <select name="inputContinent" id="inputContinent" onclick="calljavascriptfunction(id,'cont_country','continent','inputCountry','country');" data-validation="required" data-validation-error-msg="Continent field cannot be empty.">

                                                    
                                                         <?php if (!empty($list)) { ?>
                                                               <?php foreach($list as $row) { ?>
                                                                    <option value="<?php echo $row['continent']; ?>">
                                                                        <?php echo $row['continent']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                        <?php } ?>

                                                    </select>

                                                </div> 

                                            <?php } ?>                            

                                      <!--  </div>                      


                                        
                                        <div class="row-form"> -->
                                        
                                           <div class="span3"> </div> 

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select class="block" name="inputCountry" id="inputCountry" onclick="calljavascriptfunction(id,'country_region','country','inputRegion','region');" data-validation="required" data-validation-error-msg="Country field cannot be empty.">

                                                        <option value="<?= $rec['country'] ?>"><?= $rec['country'] ?></option>

                                                       

                                                    </select>

                                               </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">       

                                                    <select   name="inputCountry" id="inputCountry" onclick="calljavascriptfunction(id,'country_region','country','inputRegion','region');" data-validation="required" data-validation-error-msg="Country field cannot be empty." disabled>
                                                        <option value="">Choose</option>
                                                       
                                                    </select>

                                             </div> 

                                            <?php } ?>                           
                                    <!--  </div>                                                                                     

                                  

                                        <div class="row-form clearfix"> -->

                                            <div class="span3"> </div> 

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputRegion" id="inputRegion" onclick="calljavascriptfunction(id,'region_subregion','region','inputSubregion','subregion');">

                                                        <option value="<?= $rec['region'] ?>"><?= $rec['region'] ?> </option>

                                                      
                                                    
                                                    </select>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?> 

                                                <div class="span6">        

                                                    <select name="inputRegion" id="inputRegion" onclick="calljavascriptfunction(id,'region_subregion','region','inputSubregion','subregion');" disabled>

                                                    </select>

                                                </div>

                                            <?php } ?>                           

                                      <!--  </div>                                                                                 

                                

                                        <div class="row-form clearfix"> -->

                                            <div class="span3"></div> 

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputSubregion" id="inputSubregion" onclick="calljavascriptfunction(id,'subregion_town','subregion','inputTown','town');">

                                                        <option value="<?= $rec['subregion'] ?>"><?= $rec['subregion'] ?></option>


                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>  

                                                <div class="span6">        

                                                    <select name="inputSubregion" id="inputSubregion" onclick="calljavascriptfunction(id,'subregion_town','subregion','inputTown','town');" disabled>

                                                    </select>

                                                </div> 

                                            <?php } ?>                            

                                      <!--  </div>      



                                        <div class="row-form clearfix">  -->

                                            <div class="span3"> </div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?> 

                                                <div class="span6">        

                                                    <select name="inputTown" id="inputTown" >

                                                        <option value="<?= $rec['town'] ?>"><?= $rec['town'] ?></option>

                                                        

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?> 

                                                <div class="span6">        

                                                    <select name="inputTown" id="inputTown" disabled>

                                                      <!--  <option value="">Choose</option>

                                                        <option value="minehead">Minehead</option>
                                                      -->
                                                    </select>

                                                </div> 

                                            <?php } ?>                             

                                       <!-- </div>



                                        <div class="row-form clearfix"> 

                                            <div class="span3">Suburb(optional)</div> -->

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputSuburb" id="inputSuburb" > 

                                                        <option value="<?= $rec['suburb'] ?>"><?= $rec['suburb'] ?></option>

                                                       

                                                    </select>

                                                </div> 

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6">        

                                                    <select name="inputSuburb" id="inputSuburb" > 

                                                        <option value="">Choose</option>

                                                        <option value="Studio">Studio</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                            

                                       </div> 



                                        <div class="row-form clearfix"> 

                                            <div class="span3">Property address</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['property_address'] ?>"  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty."/></div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty." onchange="get_property_address()"/></div>

                                            <?php } ?>

                                        </div> 



                                        <div class="row-form clearfix">

                                            <div class="span3">Postcode</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['postcode'] ?>"  type="text" name="inputPostcode" id="inputPostcode" data-validation="required" data-validation-error-msg="Property code field cannot be empty."/></div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputPostcode" id="inputPostcode" data-validation="required" data-validation-error-msg="Post code field cannot be empty."/></div>

                                            <?php } ?>

                                        </div> 



                                        <div class="row-fluid">

                                            <div class="span12">

                                                <div class="head clearfix">

                                                    <div class="isw-graph"></div>

                                                    <h1>Property Map</h1>

                                                </div>

                                                 <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <iframe width="1065" height="290" frameborder="0" style="border:0"

                                                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q=<?= $rec['property_address'] ?>

                                                        ">

                                                    </iframe> 
                                                 <?php endforeach; ?>

                                                <?php }else{ ?> 
                                                    
                                                    <iframe id="iframe" width="1065" height="290" frameborder="0" style="border:0"

                                                        src="">

                                                    </iframe>
                                                
                                                <?php } ?>                                             

                                            </div>

                                        </div>
                                        
                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>   

                                    </div>



                                    <div id="localarea">

                                        <div class="row-form clearfix">

                                            <div class="span3">The region</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"> <?= $rec['region_description'] ?></textarea> </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"></textarea> </div>



                                            <?php } ?> 

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">The area</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"><?= $rec['area_description'] ?></textarea> </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"></textarea> </div>

                                            <?php } ?>

                                        </div>

                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>  

                                    </div>



                                    <div id="how_to_get_there">

                                        <div class="row-form clearfix">

                                            <div class="span3">Airport</div>

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                 <div class="span6"><input  value="<?= $rec['airport'] ?>" type="text" name="inputAirport" id="inputAirport"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><input type="text" name="inputAirport" id="inputAirport"/></div>  km

                                             <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Ferry port</div>

                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['ferry_port'] ?>" type="text" name="inputFerryport" id="inputFerryport"/></div>  km

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputFerryport" id="inputFerryport"/></div>  km

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Train station </div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['train_station'] ?>" type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km

                                            <?php } ?>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">Car required</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6">        

                                                    <select name="inputCar" id="inputCar" >

                                                         <option value="<?= $rec['car'] ?>"><?= $rec['car'] ?> </option>

                                                        <option value="car_not_necessary">Car not necessary</option>

                                                        <option value="car_necessary">Car necessary</option>

                                                    </select>

                                                </div> 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                               <div class="span6">        

                                                    <select name="inputCar" id="inputCar" >

                                                         <option value="">Choose</option>

                                                        <option value="car_not_necessary">Car not necessary</option>

                                                        <option value="car_necessary">Car necessary</option>

                                                    </select>

                                                </div> 

                                            <?php } ?>                                   

                                        </div>

                                        <div class="row-form clearfix">



                                            <div class="span3">More tips of how to get there</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"><?= $rec['how_to_get_there'] ?></textarea> </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"></textarea> </div>

                                            <?php } ?>

                                        </div>

                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>  

                                    </div>



                                    <div id="whats_nearby">

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span4">

                                                    <?php if($rec['golf_course_within_15km'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputGolf_course_15mins_walk"/>  Golf course within 15 mins walk

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">                                                   

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                                    </label>                                                   

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span4">

                                                    <?php if($rec['golf_course_within_30km'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputGolf_course_30mins_drive"/> Golf course  within 30 mins drive

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">                                                    

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                                     </label>                                                    

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['tennis_court'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputTennis_court"/> Tennis courts nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                                    </label>                                                    

                                                </div>

                                            <?php } ?>



                                            
                                        </div>



                                        <div class="row-form clearfix">
                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span4">

                                                <?php if($rec['skiing'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputSkiing"/> Skiing:property is in a ski resort

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                                 </label>

                                                 <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                                     </label>

                                                </div>

                                            <?php } ?>


                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span4">

                                                <?php if($rec['water_sport'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputWater_sports"/>Water sports nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                                </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                                </label>

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <?php if($rec['water_park'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputWater_park"/> Water park nearby

                                                    </label>

                                                <?php }else{ ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                                 </label>

                                                 <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                                    </label>

                                                </div>

                                            <?php } ?>



                                            
                                        </div>



                                        <div class="row-form clearfix">
                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span4">

                                                    <?php if($rec['horse_riding'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked" name="inputHorse_riding"/> Horse riding nearby 

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                                    </label>

                                                </div>

                                            <?php } ?>

                                                

                                            

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span4">

                                                 <?php if($rec['beach_front'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputBeach_front"/> Beachfront property

                                                    </label>

                                                <?php }else{ ?>



                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                                 </label>

                                                <?php } ?>

                                            </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span4">

                                                    <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                                 </label>



                                                </div>

                                            <?php } ?>


                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['fishing'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked" name="inputFishing"/> Fishing nearby

                                                    </label>

                                                    <?php }else{ ?>

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                                    </label>

                                                    <?php } ?>

                                                    

                                                </div>

                                             <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                               

                                                     <label class="checkbox inline">

                                                        <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                                    </label>

                                                </div>

                                            <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest Beach</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['nearest_beach'] ?>" type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span6"><input type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km

                                                <?php } ?>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest shops</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input  value="<?= $rec['nearest_shop'] ?>" type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km

                                            <?php } ?>

                                        </div>

                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                            <input type="button" class="btn" id="btnNext" value="Next"/>


                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>  

                                    </div>



                                    <div id="holiday_types">

                                       <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['walking_holiday'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Walking holidays

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">                                                   

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                                    </label>                                                    

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['cycling_holiday'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>Cycling holidays

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                                     </label>

                                                </div>

                                            <?php } ?>



                                             <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                            <div class="span3">

                                                <label class="checkbox inline">

                                                    <?php if($rec['rural_countryside_retreats'] != 0) { ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" checked="checked"/> Rural or countryside retreats

                                                    </label>

                                                <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                                    </label>

                                                <?php } ?>

                                            </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                             <div class="span3">                                               

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                                    </label>

                                               

                                            </div>

                                            <?php }?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['beach_lakeside_relaxation'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Beach or lakeside relaxation

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                                    

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                                     </label>

                                                </div>

                                            <?php } ?>

                                        </div>

                                        <div class="row-form clearfix">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['city_breaks'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/>City breaks

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">                                               

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                                    </label>                                                

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['winter_sun'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Winter sun

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                                     </label>

                                                     <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                 <div class="span3">

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                                     </label>                                                 

                                                </div>

                                            <?php } ?>



                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span3">

                                                    <?php if($rec['night_life'] != 0) { ?>

                                                        <label class="checkbox inline">

                                                            <input type="checkbox" checked="checked"/> Nightlife

                                                        </label>

                                                    <?php }else{ ?>

                                                    <label class="checkbox inline">

                                                        <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                                    </label>

                                                    <?php } ?>

                                                </div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                                </label>                                                

                                            </div>

                                            <?php } ?>

                                        </div>

                                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                                           <!-- <input type="button" class="btn" id="btnNext" value="Next"/>-->


                                            <button class="btn btn-success">Save</button>

                                            <a class="btn btn-danger" href="<?= site_url('property/propertyList');?>">Cancel</a>

                                        </div>  

                                    </div>                               

                                </div>

                        </div>

                </form>

            </div>

        </div>   

    </div>



     <!-- for validation -->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>



     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 

    
    <script type="text/javascript">
        var currentTab = 0;
        $(function () {
            $("#tabs").tabs({
                select: function (e, i) {
                    currentTab = i.index;
                }
            });
        });
        $("#btnNext").live("click", function () {
            var tabs = $('#tabs').tabs();
            var c =  $("#tabs >ul >li").size();//$('#tabs').tabs("length");
            currentTab = currentTab == (c - 1) ? currentTab : (currentTab + 1);
            tabs.tabs("option", "active", currentTab);
        });


        function calljavascriptfunction(id,table,column,destination,dest_output){


              $.ajax({
                 type : 'POST',
                 data : 'content1='+ $('#'+id).val() + '&table='+ table + '&column='+ column,
                 url : 'get_Location',
                  dataType: 'json',
                 success : function(data){
                       
                         document.getElementById(destination).disabled = false;
                         document.getElementById(destination).style.display = "block";
                          document.getElementById(destination).style.visibility = "visible";
                         $("#"+destination).empty();
                         var output = $.map(data, function(response){
                   
                         var records = $.map(response, function(records){
                         var record = $.map(records, function(record) {
                         for(i=0;i<record.length;i++){
                            $('#'+destination).append('<option value="' + record[i][dest_output]+ '">' + record[i][dest_output] + '</option>');
                        }
                    
                });
              });
                });
              
       
                }
             });
            }
    </script>


  
</body>



</html>

