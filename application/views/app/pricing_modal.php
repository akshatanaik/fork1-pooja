    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
    </style>
    <script>
        $(function() {
            $( "#start_date" ).datepicker({ 
                dateFormat: 'dd-mm-yy',
                onClose:function(selectedDate){
                    $( "#end_date" ).datepicker("option","minDate",selectedDate);
                }
            });

            $( "#end_date" ).datepicker({ 
                dateFormat: 'dd-mm-yy',
                onClose:function(selectedDate){
                    $( "#start_date" ).datepicker("option","maxDate",selectedDate);
                }
            }); 
        });

   </script>
   <script type="text/javascript">
    //var model = @Html.Raw(Json.Encode(do));
    </script>
    <?php 
    $propertyId = $_GET['property_id'];
    $pricing = isset($response['pricing']) ? $response['pricing']:array(); ?>
            
            <form id="pricing_form" class="form-horizontal" action="<?= site_url('/pricing/save_pricing')?>" method="POST" >
                
                <input type="hidden" id="id" name="id" value="<?= isset($pricing['id'])? $pricing['id']:'' ?>">
                <input type="hidden" id="propertyId" name="propertyId" value="<?= isset($propertyId)? $propertyId:'' ?>">
               
                <div class="block-fluid"> 
                    <div class="row-form clearfix">
                        <div class="span3">Start Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span3"><input value="<?= isset($pricing['start_date'])? $pricing['start_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Start Date" type="text" name="start_date" id="start_date"/></div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">End Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span3"><input value="<?= isset($pricing['end_date'])? $pricing['end_date']:'' ?>" data-validation="required" data-validation-error-msg="Please select End Date" type="text" name="end_date" id="end_date" /></div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Status</div>
                        <div class="span6">
                            <select name="inputStatus" id="inputStatus">
                                <option value="available">Available</option>
                                <option value="booked">Booked</option>
                                <option value="special">Special</option>
                                 <option value="unavailable">Unavailable</option>
                            </select>
                        </div>
                    </div>
                   
                     <div class="row-form clearfix">
                        <div class="span3">Price (in &dollar;)<em style="color:#Ff0000;">*</em></div>
                        <div class="span6"><input value="<?= isset($pricing['price'])?$pricing['price']:'' ?>" type="text" name="price" id="price"/></div>
                    </div> 
                </div>
         
            </form>