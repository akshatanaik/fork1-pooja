                <script src="<?= INCLUDES ?>app/js/housekeeper.js"></script>
                <style type="text/css">
                    .form-error{ color:red; font-size: 10px;}
                </style>
                 <?php $housekeeper = isset($response['housekeeper']) ? $response['housekeeper']:array(); ?>

                 <form class="form-horizontal" action="<?= site_url('housekeeper/save_housekeeper_details') ?>" method="POST" onsubmit="return housekeeper(this);">
                     <div class="span12">
                       
                        
                        <!-- build  the form -->
                        <div class="block-fluid tabs">
                                 <ul >
                                    <li><a href="#personal_details">Personal Details</a></li>
                                    <li><a href="#permissions">Permissions</a></li>
                                </ul> 
                                <input type="hidden" name="username" id="username" value="<?= isset($user['username'])?$user['username']:'' ?> ">

                                <input type="hidden" id="id" name="id" value="<?= isset($housekeeper['id'])? $housekeeper['id']:'' ?>">

                                <div id="personal_details">
                                    <div class="row-form clearfix">
                                        <div class="span3">First Name<em style="color:#Ff0000;">*</em></div>
                                        <div class="span6"><input value="<?= isset($housekeeper['firstname'])?$housekeeper['firstname']:'' ?> "  type="text" name="firstname" id="firstname"/></div>
                                    </div>
                                    <div class="row-form clearfix">
                                        <div class="span3">Last Name<em style="color:#Ff0000;">*</em></div>
                                        <div class="span6"><input value="<?= isset($housekeeper['lastname'])?$housekeeper['lastname']:'' ?> "  type="text" name="lastname" id="lastname"/></div>
                                    </div>

                                    <div class="row-form clearfix">
                                        <div class="span3">Company Name</div>
                                        <div class="span6"><input value="<?= isset($housekeeper['company_name'])? $housekeeper['company_name']:'' ?>"  type="text" name="company_name" id="company_name" /></div>
                                    </div> 
                                
                                 
                                    <div class="row-form clearfix">
                                        <div class="span3">Phone</div>
                                        <div class="span6"><input value="<?= isset($housekeeper['phone'])? $housekeeper['phone']:'' ?>" type="text" name="phone" id="phone"/></div>
                                    </div> 
                                    <div class="row-form clearfix">
                                        <div class="span3">Address</div>
                                        <div class="span6"><input value="<?= isset($housekeeper['address'])? $housekeeper['address']:'' ?>" type="text" name="address" id="address"/></div>
                                    </div> 

                                    <div class="row-form clearfix">
                                        <div class="span3">Email Address<em style="color:#F00;">*</em></div>
                                        <div class="span6"><input value="<?= isset($housekeeper['email'])? $housekeeper['email']:'' ?>"  onblur="$('#username').val(this.value);" on type="text" name="email" id="email" /></div>
                                    </div> 

                                    <?php 

                                    if(!isset($housekeeper['id'])){ ?>
                                     <!--<div class="row-form clearfix">
                                        <div class="span3">Username<em style="color:#F00;">*</em></div>
                                        <div class="span6">
                                            <input value="<?= isset($housekeeper['username'])? $housekeeper['username']:'' ?>" data-validation="required" data-validation-error-msg="Please enter username" type="text" name="username" id="username"/>
                                        </div>
                                    </div> 
                                    
                                   <div class="row-form clearfix">
                                        <div class="span3">Password<em style="color:#F00;">*</em></div>
                                        <div class="span6"><input type="password" name="password" id="password" /></div>
                                    </div>-->

                                    <div class="row-form clearfix">
                                        <div class="span3">Email subject</div>
                                        <div class="span6"><input type="text" name="email_subject" id="email_subject"/></div>
                                    </div> 
                                    <div class="row-form clearfix">
                                        <div class="span3">Email body</div>
                                        <div class="span6"><textarea type="text" name="email_body" id="email_body"></textarea></div>
                                    </div>

                                    <?php }/*else{ ?>

                                    <!--<input type="hidden" name="username" id="username" value="<?= isset($housekeeper['username'])?$housekeeper['username']:'' ?> ">
                                    <input type="hidden" name="password" id="password" value="<?= isset($housekeeper['password'])?$housekeeper['password']:'' ?> ">-->
                                  
                                    <?php } */?>

                                    <div class="row-form clearfix">
                                        <div class="span3">Notes</div>
                                        <div class="span6"><textarea  type="text" name="notes" id="notes"><?php echo(isset($housekeeper['notes'])?$housekeeper['notes']:''); ?></textarea></div>
                                    </div> 
                                     <?php $allproperties=null;$i=0;
                                                if(isset($response['allproperties'])) {
                                                foreach($response['allproperties'] as $res => $rec): 
                                                $allproperties[$i]=$rec['property_id'];
                                                $i++;
                                           endforeach;
                                           } ?>
                                 <div class="row-form clearfix">
                                            <div class="span3">Assign Property<em style="color:#F00;">*</em></div>
                                        
                                            <div class="span6">
                                            <select type="multiple"  multiple="multiple" name="assign_Property[]"   id="assign_Property" type="multiple" value="<?= isset($housekeeper['property_id'])? $housekeeper['property_id']:'' ?>"  onchange="bookingCalendar.pricing(this.value)" data-validation="required" data-validation-error-msg="Please select property to Assign">
                                            
                                                    <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                                     
                                                    <?php foreach($response['properties'] as $res => $rec): ?>
                                                            <option <?php echo (isset($allproperties) && (in_array($rec['property_id'], $allproperties)))?'selected' : '' ?> value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                                    <?php endforeach; ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        </div>    
                                

                                <div id="permissions">
                                    <div class="row-form clearfix">
                                        <div class="span2">View Options</div>
                                        <div class="span8">       
                                        <table style="width:500px;">
                                            <thead>
                                                <tr>
                                                    <th width='55' style="float:left;">Option</th>
                                                    <th width='50' style="float:right; margin-right:-80px;">Access</th>
                                                </tr>

                                            </thead>
                                            <tbody>
                                                    <?php $user_permissions = isset($response['user_permissions']) ? $response['user_permissions']:array(); ?>
                                                     
                                                    <tr>
                                                    <td>Booking Reference</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the booking reference" name="booking_reference" id="booking_reference" class="validate[required]">
                                                            <?php if($user_permissions[0]['permission_name'] == 'manage_booking_reference') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                            
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                                <tr>
                                                     <td>Arrival Time</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="select the arrival time" name="arrival_time" id="arrival_time" class="validate[required]">
                                                            <?php if($user_permissions[1]['permission_name'] == 'manage_arrival_time') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                </tr>
                                                <tr>
                                                    <td>Departure Time</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the departure time" name="departure_time" id="departure_time" class="validate[required]">
                                                            <?php if($user_permissions[2]['permission_name'] == 'manage_departure_time') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                                <tr>
                                                    <td>Customer's Telephone Number</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the customers telephone no" name="customers_telephone_no" id="customers_telephone_no" class="validate[required]">
                                                            <?php if($user_permissions[3]['permission_name'] == 'manage_customers_telephone_no') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                                <tr>
                                                    <td>Customer's Town/City</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the customers town or city" name="customers_town_or_city" id="customers_town_or_city" class="validate[required]">
                                                            <?php if($user_permissions[4]['permission_name'] == 'manage_customers_town_or_city') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                                <tr>
                                                    <td>Customer Comments</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the customers comments" name="customers_comments" id="customers_comments" class="validate[required]">
                                                            <?php if($user_permissions[5]['permission_name'] == 'manage_customers_comments') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                                <tr>
                                                    <td>Customer's Country</td>
                                                    <td>
                                                        <select data-validation="required" data-validation-error-msg="Select the customers country" name="customers_country" id="customers_country" class="validate[required]">
                                                            <?php if($user_permissions[6]['permission_name'] == 'manage_customers_country') {  ?>
                                                                <option  value="Allow">Allow</option>
                                                                <option  value="Deny">Deny</option>
                                                            <?php }else{ ?>
                                                                <option  value="Deny">Deny</option>
                                                                <option  value="Allow">Allow</option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>      
                                </div>
                            </div>
                            <div class="footer tar">
                                <?php  if(!isset($housekeeper['id'])){ ?>
                                <button class="btn btn-success" id="save_details">Save</button>
                                <?php }else{ ?>
                                <button class="btn btn-success" id="save_details">Update</button>
                                <?php } ?>
                                <!--<a class="btn" href="javascript:window.history.go(-1);">Cancel</a>-->
                                <a class="btn" href="<?= site_url('housekeeper');?>">Cancel</a>
                            </div>  
                        </div>
                    </div>
                </form>
                 <script>
                function populate_username(email){
    
                if(email!='')
                $('#username').val(email);
    
                }
                </script>
            