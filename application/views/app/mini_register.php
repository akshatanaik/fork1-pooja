    


    <?php $user = isset($response['user']) ? $response['user']:array(); ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-chats"></div>
                <h1>Registration</h1>
            </div>
            <div class="block-fluid clearfix">

                <form action="<?= isset($saveurl) ? $saveurl :site_url('/users/save') ?>" method="POST" id="user_registration">
                
                <input type="hidden" name="id" id="id" value="<?= isset($user['id'])?$user['id']:'' ?> ">
                <input type="hidden" name="login_provider" id="login_provider" value="<?= isset($user['login_provider'])?$user['login_provider']:'' ?> ">
                <input type="hidden" name="login_provider_username" id="login_provider_username" value="<?= isset($user['login_provider_username'])?$user['login_provider_username']:'' ?> ">
                <input type="hidden" name="facebook_user_id" id="facebook_user_id" value="<?= isset($user['facebook_user_id'])?$user['facebook_user_id']:'' ?> ">
                <input type="hidden" name="profile_image_url" id="profile_image_url" value="<?= isset($user['profile_image_url'])?$user['profile_image_url']:'' ?> ">
                <input type="hidden" name="twitter_user_id" id="twitter_user_id" value="<?= isset($user['twitter_user_id'])?$user['twitter_user_id']:'' ?> ">
                <input type="hidden" name="google_user_id" id="google_user_id" value="<?= isset($user['google_user_id'])?$user['google_user_id']:'' ?> ">


                     
                             
                                <div class="row-form clearfix">
                                    <div class="span3">First Name<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['firstname'])?$user['firstname']:'' ?> " data-validation="required" data-validation-error-msg="Please enter First Name" type="text" name="firstname" id="firstname"/></div>
                                </div>

                                <div class="row-form clearfix">
                                    <div class="span3">Last Name<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['lastname'])?$user['lastname']:'' ?> " data-validation="required" data-validation-error-msg="Please enter Last Name" type="text" name="lastname" id="lastname"/></div>
                                </div>

                                <div class="row-form clearfix">
                                    <div class="span3">Email Address<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['email'])?$user['email']:'' ?> " onblur="$('#username').val(this.value);" data-validation="required email" data-validation-error-msg="Please enter valid Email Address" on type="text" name="email" id="email"/></div>
                                </div>
                                
                                <!-- Since if its facebook login simple reg form is expected 31/5/14.-->
                                <?php if( !isset($user['id'])  && !isset($user['login_provider'] ) ){ ?>
                               
                      
                           
                                    <input  type="hidden" name="username" id="username"/>
                                    <!--<div class="row-form clearfix">
                                        <div class="span3">Username<em style="color:#Ff0000;">*</em></div>
                                        <div class="span9">
                                            
                                        </div>
                                    </div>-->

                                    <?php if(!$this->sessions->getsessiondata('isadmin')){?>
                                        <div class="row-form clearfix">
                                            <div class="span3">Password<em style="color:#Ff0000;">*</em></div>
                                            <div class="span9">
                                                <input  data-validation-strength="2" data-validation="strength"  type="password" name="password_confirmation" id="password_confirmation"/>
                                            </div>
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">Confirm Password<em style="color:#Ff0000;">*</em></div>
                                            <div class="span9">
                                                <input data-validation="confirmation" type="password" name="password" id="password"/>
                                            </div>
                                        </div>
                                    <?php } ?>
                           
                                <?php } ?>
                        
                                <?php if( isset($user['login_provider'] ) ){ ?>
                                      <input type="hidden" name="username" id="username" value="<?= isset($user['username'])?$user['username']:'' ?> ">
                                <?php }?>

                     

                        <div class="footer tar">
                            <input type="submit" class="btn " id="reg" value="<?= isset($user['id']) && $user['id']!='' ?'Update':'Register' ?>" />
                            <a class="btn" href="javascript:window.history.go(-1);">Cancel</a>
                            <!--<a class="btn" href="<?= site_url('/login');?>">Cancel</a>-->
                        </div>  

                </form>
            </div>
        </div>
         <style>
            #reg{
                border:1px solid #829E18 !important;
                background:-moz-linear-gradient(center top , #ADC800 0%, #829E18 100%) repeat-x scroll 0 0 rgba(0, 0, 0, 0) !important;
                color: #FFFFFF !important;
                text-shadow:0 -1px 0 rgba(0, 0, 0, 0.25) !important;
                height:30px;
              
               
            }
        </style>

   