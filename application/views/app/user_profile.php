<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />


    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/stepywizard/jquery.stepy.js'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/users.js'></script>



</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   



        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>User profile</h1>



                </div> 

                <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>

                <?php $user = isset($response['user']) ? $response['user']:array();?>

                <div class="row-fluid">

                    <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-chats"></div>

                            <h1>User profile</h1>

                            <ul class="buttons">

                               <?php 

                               $id = $this->sessions->getsessiondata('user_id'); ?>

                                <li><a href="<?= site_url('/users/edit_user/?id='.$id) ?>" class="isw-edit"></a></li>                         

                            </ul>       

                        </div>

                        <div class="block-fluid clearfix">



                            <form  method="POST" id="user_registration">

                                <input type="hidden" name="id" id="id" value="<?= isset($user['id'])?$user['id']:'' ?> ">



                                <fieldset title="Step 1">

                                    <legend>Personal details</legend>

                                    <div class="row-form clearfix">

                                        <div class="span3">Firstname</div>

                                        <div class="span9"><?= $user['firstname']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Lastname</div>

                                        <div class="span9"><?= $user['lastname']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Email Address</div>

                                        <div class="span9"><?= $user['email']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Phone</div>

                                        <div class="span9"><?= $user['phone']?></div>

                                    </div> 

                                    <div class="row-form clearfix">

                                        <div class="span3">Address</div>

                                        <div class="span9"><?= $user['address']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">City</div>

                                        <div class="span9"><?= $user['city']?></div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">Country</div>

                                        <div class="span9"><?= $user['country']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Zip/Pin code</div>

                                        <div class="span9"><?= $user['zip_postal_code']?></div>

                                    </div>

                                </fieldset>



                                <fieldset title="Step 2">

                                    <legend>Company Details</legend>

                                    <div class="row-form clearfix">

                                        <div class="span3">Company Id</div>

                                        <div class="span9"><?= $user['company_id']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Company Name</div>

                                        <div class="span9"><?= $user['company_name']?></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Company Website</div>

                                        <div class="span9"><?= $user['company_website']?></div>

                                    </div>

                                </fieldset>

                                <?php if(!isset($user['id'])){ ?>

                                <fieldset title="Step 3">

                                    <legend>Login Details</legend>

                                    <div class="row-form clearfix">

                                        <div class="span3">Username</div>

                                        <div class="span9"><?= $user['username']?></div>

                                    </div>

                                    <?php if(!$this->sessions->getsessiondata('isadmin')){?>

                                    <div class="row-form clearfix">

                                        <div class="span3">Password:</div>

                                        <div class="span9"></div>

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Re-type Password</div>

                                        <div class="span9"></div>

                                    </div>

                                    <?php } ?>

                                </fieldset>

                                <?php } ?>

                                <!--<input type="submit" class="btn finish" value="<?= isset($user['id']) && $user['id']!='' ?'Update':'Register' ?>" />-->

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</body>

</html>