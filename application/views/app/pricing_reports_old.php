<!-- bookings view -->
<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain-Bookings</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.timepicker.min.js"></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/report.js'></script> 

    <style>
        .row-fluid .span12 {           
            margin-left: -0.436%;           
        }

        [class*="span"] {
            float: left;
            margin-left: 0;
            min-height: 1px;
        }

        .help-block {
            display: block;
            margin-bottom: 24px;
            width: 150px;
        }
    </style>

</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
            
            	<!-- display the list of bookings -->
            	 <div class="page-header">
                    <h1>Pricing Reports</h1>
                </div>                  
                <form id="report_form" class="form-horizontal" action="<?= site_url('/reports/pricing_report')?>" method="POST" >
                    <div class="row-fluid">
                        <div class="span12">
                            <div>
                                <div class="span1">Start Date</div>
                                <div class="span1"><input data-validation="required" data-validation-error-msg="Please select Start Date" type="text" name="start_date" id="start_date" style="margin-left:-16px;"/></div>
                            </div>

                            <div style="margin-left:335px;">
                                <div class="span2" style="margin-left:-30px;">End Date</div>
                                <div class="span1"><input data-validation="required" data-validation-error-msg="Please select End Date" type="text" name="end_date" id="end_date" style="margin-left:-42px;"/></div>
                            </div>

                            <div style="margin-left:665px;">
                                <div class="span2" style="margin-left:-40px;">Property</div>
                                <div class="span2">
                                    <select  name="property_id" id="property_id">
                                        <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                            <option value="">Choose a property</option>
                                            <?php foreach($response['properties'] as $rec): ?>
                                                <option  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                            <?php endforeach; ?>
                                        <?php } ?>                                      
                                    </select>
                                </div>
                            </div>

                            <div class="footer tar">
                                <button class="btn btn-success" id="save_details">Generate</button>
                            </div>                        
                        </div>
                    </div>
                </form><br/>

               
                    <div class="span10">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>All Pricing</h1>      
                            <ul class="buttons">
                                <li><a href="<?= site_url('/reports/download_pricing_summary_to_excel') ?>" class="isw-download"></a></li>                                                   
                         
                            </ul>                        
                        </div>
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                <thead>
                                    <tr>  
                                        <th width="10%">Property Id</th>                                     
                                        <th width="10%">Start Date</th>
                                        <th width="10%">End Date</th>
                                        <th width="10%">Price</th>
                                        <th width="10%">Short Break</th>                                                                  
                                    </tr>
                                </thead>
                                <tbody> 
                                    <?php if(isset($response['data']) || !empty($response['data'])){   ?> 
                                    <?php foreach($response['data'] as $rec): ?>                                   
                                        <tr> 
                                            <td><?= $rec['property_id']; ?></td>                                           
                                            <td><?= $rec['start_date']; ?></td>
                                            <td><?= $rec['end_date']; ?></td>
                                            <td><?= $rec['price']; ?></td>

                                            <?php if($rec['isShortbreak'] == 1){ ?>
                                                <td>Yes</td> 
                                            <?php }else{ ?>  
                                                <td>No</td> 
                                            <?php } ?>                                                                       
                                        </tr> 
                                    <?php endforeach; ?>
                                    +69<?php } ?>                               
                                </tbody>
                            </table>
                        </div>
                    </div>
                              

            </div>

        </div>   
    </div>  
    

    <script> 
        $("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});
    </script>
</body>
</html>