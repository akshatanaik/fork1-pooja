<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain-Bookings</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/cleditor/jquery.cleditor.js'></script> 
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/select2/select2.min.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/mail.js'></script>
    
    
    <style>
        [class*="span"] {
            float: left;
            margin-left: 0;
            min-height: 1px;
        }
    </style>
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
                <?php $booking = isset($response['booking']) ? $response['booking']:array();  ?>
                <?php $property =isset($response['properties'])  ? $response['properties']:array(); ?>
               
                <!-- display the list of bookings -->
                 <div class="page-header">
                    <h1>Invoice</h1>
                </div>

                <div class="row-fluid">
                    <div class="span8">
                        <div class="span2">
                            <a href="<?= site_url('/bookings/invoice') ?>?id=<?= $booking['id'] ?>"><button class="btn btn-block" type="button">Invoice</button></a>                           
                        </div>
                        <div class="span2">
                            <a href="<?= site_url('/bookings/edit_booking') ?>?id=<?= $booking['id'] ?>"><button class="btn btn-block" type="button">Edit</button></a>                           
                        </div>
                        <div class="span2">                            
                            <a href="<?= site_url('/bookings/transaction_details') ?>"><button class="btn btn-block" type="button">Transaction</button></a>                            
                        </div>
                        <div class="span2">                          
                            <a href="<?= site_url('/bookings/booking_logs') ?>"><button class="btn btn-block" type="button">Log</button></a>
                        </div>
                        
                    </div>
                </div><br/> 

                <div class="span7">                  

                    <!-- build  the form -->
                    <div class="block-fluid tabs" style="border:1px solid; border-color:grey; border-radius:5px; height:535px;">               
                        <div id="inner_details">                             
                                <h3 style="width:330px; margin-left:30px;">Booking Invoice</h3>
                                <button id="deposit" value="Deposit" style="background-color: #C0C0C0; margin-left:30px;width:85px;" class="btn btn-block" type="button">Deposit</button>
                                <p  style="margin-bottom:0px; width:215px; height:92px; margin-left:415px; margin-top:-60px; text-align:right;"><b><?= $property[0]['property_name']; ?></b><br/>
                                    Exmoor<br/>
                                    Somerset,
                                    TA24 8PB.
                                </p>                                
                                
                                <div>
                                    <div style="margin-left:30px; width:265px;">
                                        <p id="details1" style="margin-left:0px; width:265px;">
                                            <?= $booking['customer_name']; ?>
                                        </p>
                                    </div>
                                    
                                    <div style="margin-left:-20px; width:365px; margin-top:-32px;">
                                        <table id="table-1"  style="border:1px dotted black; margin-top:0px; margin-left:412px; width:65%;" border="1" cellpadding="0" cellspacing="" >
                                            <tr>
                                                <td bgcolor="#999999" width="94" style="color:#fff; text-shadow: 1px 1px 1px #555;" ><b>Booking ID:</b></td>
                                                <td width="114" style="text-align: right;">MSVG-201013</td>
                                            </tr>                                 
                                             
                                            <tr>
                                                <td bgcolor="#999999" width="99" style="color:#fff; text-shadow: 1px 1px 1px #555;"><b>Booking Date:</b></td>
                                                <td width="114" style="text-align: right;"><?= $booking['created_date']; ?></td>
                                            </tr>
                                                
                                            <tr>
                                                <td bgcolor="#999999" style="color:#fff; text-shadow: 1px 1px 1px #555;"><b>Total(GBP)</b></td>
                                                <td style="text-align: right;"><?= $booking['cost']; ?></td>
                                            </tr>
                                        </table><br/><br/>
                                    </div>
                                </div>
                                    
                                <table width="90%" id="table-2" style="border:1px dotted black; margin-left:30px;" border="1" cellpadding="0" cellspacing="">
                                    <thead>
                                        <th bgcolor="#999999" width="30%" valign="middle">Item</th>
                                        <th bgcolor="#999999" width="20%" valign="middle">Rate</th>
                                        <th bgcolor="#999999" width="25%" valign="middle">Amount</th>
                                        <th bgcolor="#999999" width="30%" valign="middle">Payment Summary</th>
                                    </thead>
                                    <tbody>                                         
                                        <tr>
                                            <td ><b><?= $property[0]['property_name']; ?></b><br/>Arrival Date:<?= $booking['arrival_date']; ?><br/>Departure Date: <?= $booking['departure_date']; ?></td>
                                            <td style="text-align:center;"><?= $booking['nights']; ?> Nights @<?= $booking['cost']; ?></td>
                                            <td style="text-align:right;">&dollar;<?= $booking['cost']; ?></td>
                                        </tr>                                              
                                              
                                        <tr>
                                            <?php $arrival_time = $booking['arival_time'];
                                            $time_in_12_hour_format_A  = date("g:i a", strtotime($arrival_time));

                                            $departure_time = $booking['departure_time'];
                                            $time_in_12_hour_format_D  = date("g:i a", strtotime($departure_time));
                                            ?>
                                            <td style="text-align:left;" width="550" height="70" class=" sorting_1">Arrival Time:<?= $time_in_12_hour_format_A  ?><br/>
                                            Departure Time:<?= $time_in_12_hour_format_D ?></td>


                                            
                                            <td></td>
                                            <td width="195">
                                                <div style="width: 50%; float: left; text-align: left;">Total:</div>
                                                <div style="width: 50%; float: left; text-align: right;">&dollar;<?= $booking['cost']; ?></div><br/>
                                             
                                                <div style="width: 75%; float: left; text-align: left;">Amount Paid:</div>
                                                <div style="width: 50%; float: left; text-align: right;"></div><br/>
                                             
                                                <div style="width: 50%; float: left; text-align: left;">Balance-Due:</div>
                                                <div style="width: 50%; float: left; text-align: right;">&dollar;<?= $booking['cost']; ?></div>                                            
                                                  
                                            </td>
                                                <td width="195">
                                                    <div style="width: 50%; float: left; text-align: left;">Date:</div>
                                                    <div style="width: 50%; float: left; text-align: right;"></div><br/>

                                                    <div style="width: 50%; float: left; text-align: left;">Mode of Payment:</div>
                                                    <div style="width: 50%; float: left; text-align: right;"></div><br/>
                                                </td>
                                        </tr>
                                             
                                        <tr>
                                            <td></td>
                                            <td></td>                                                                                       
                                        </tr>
                                    </tbody>
                                </table>
                                 
                            </div><!--inner Details-->    
                        </div>              
                   
                    </div>
                    <div class="span3">
                        <div class="row-form clearfix" style="border-bottom: 0px none;">                            
                            <div class="span3" style="margin-top:-18px;">
                                <textarea placeholder="Add a Note" name="textarea"></textarea>
                            </div>

                            <div class="row-fluid" style="margin-top:95px; margin-left:2px;">
                                <div class="span10">
                                    <button class="btn" type="button">Create</button>
                                    <label class="checkbox inline">
                                        <div class="checker">
                                            <span class="">
                                                <input type="checkbox" checked="checked" value="checkbox2">
                                            </span>
                                        </div>
                                        Show in invoice
                                    </label>                                 
                                </div>
                            </div>
                    </div>
                </div>               
            </div>
        </div>
    </div>

    <div id="sendmail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">New Email</h3>
            </div>        
            <div class="row-fluid">
                <div class="block-fluid">
                    <div class="row-form clearfix">
                        <div class="span3">From:</div>
                        <div class="span9">bookingbrain@bookingbrain.co.uk</div>                    
                    </div>            
                    <div class="row-form clearfix">
                        <div class="span3">To:</div>
                        <div class="span9"><input type="text" name="to" value=""/></div>

                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Attach:</div>
                        <div class="span9"><input type="file" name="files[]"/></div>
                    </div>                
                </div>                
                <div class="block-fluid">            
                    <textarea id="mail_wysiwyg" name="text" style="height: 257px; width: 546px;"></textarea>
                </div>
                <div class="dr"><span></span></div>
                <div class="block">                
                    <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet.</p>
                </div>
            </div>                    
            <div class="modal-footer">
                <button class="btn">Send</button> 
                <button class="btn btn-warning">Save</button>
                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>            
            </div>
        </div>      
</body>
</html>