<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    

    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>





    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>







    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/stepywizard/jquery.stepy.js'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/users.js'></script>

    

</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   

        

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



           

            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>New/Edit User</h1>

                

                </div> 

                <?php if($status=='error'){ ?>

                        <div id="error"><?= $response ?></div>

                <?php }else{ ?>

                <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>

                

                <?= $this->load->view('app/register_form_partial') ?>

                <?php } ?>



                





            </div>

        </div>

    </div>

   

</body>

</html>