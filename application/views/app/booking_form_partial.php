    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
    </style>
    <?php 
    $role = $this->sessions->getsessiondata('role');
    $booking = isset($response['booking']) ? $response['booking']:array(); 
    if(isset($response['booking'])){
    $booking['arrival_date']= date('d-m-Y', strtotime($booking['arrival_date']));
    $booking['departure_date']= date('d-m-Y', strtotime($booking['departure_date']));
    }
    ?>
            <form id="booking_form" class="form-horizontal" action="<?= site_url('/bookings/save/')?>" method="POST" >
                <input type="hidden" id="id" name="id" value="<?= isset($booking['id'])? $booking['id']:'' ?>">
                <input type="hidden" id="role" name="role" value="<?= isset($role)? $role:'' ?>">
                <input type="hidden" id="role" name="role" value="">
                <div class="block-fluid"> 
                    <div class="row-form clearfix">
                        <div class="span3">Property<em style="color:#F00;">*</em></div>
                        <div class="span6">

                        <?php if(count($response['properties']) >1){ ?>
                            <select onchange="bookingCalendar.pricing(this.value)" data-validation="required" data-validation-error-msg="Please select property" name="inputProperty_name" id="inputProperty_name">
                                    <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                        <option value="">--Choose a property--</option>
                                        <?php foreach($response['properties'] as $res => $rec): ?>
                                            <option <?= isset($booking['property_id']) && $booking['property_id']==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                        <?php endforeach; ?>
                                    <?php } ?>
                                </select>
                        <?php } 
                        else { ?>
                              <select onchange="bookingCalendar.pricing(this.value)" data-validation="required" data-validation-error-msg="Please select property" name="inputProperty_name" id="inputProperty_name">
                                    <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                        
                                        <?php foreach($response['properties'] as $res => $rec): ?>
                                            <option <?= isset($booking['property_id']) && $booking['property_id']==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                        <?php endforeach; ?>
                                    <?php } ?>
                                </select>
                        <?php } ?>

                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Customer Name<em style="color:#Ff0000;">*</em></div>
                        <div class="span6"><input value="<?= isset($booking['customer_name'])? $booking['customer_name']:'' ?>" data-validation="required" data-validation-error-msg="Please enter Customer Name" type="text" name="inputCustomer_name" id="inputCustomer_name"/></div>
                    </div>
                    <?php 
                        if(isset($booking['arrival_date'])) {
                    ?>
                    <div class="row-form clearfix">
                        <div class="span3">Arrival Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span2">
                            <input value="<?= isset($booking['arrival_date'])? $booking['arrival_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Arrival Date" type="text" name="inputArival_date1" id="inputArival_date1"  onchange="booking.getNextday(1);" disabled>
                            <input value="<?= isset($booking['arrival_date'])? $booking['arrival_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Arrival Date" type="hidden" name="inputArival_date" id="inputArival_date"  onchange="booking.getNextday(1);">
                        </div>
                    </div>
                    <?php 
                      } else {
                    ?>
                    <div class="row-form clearfix">
                        <div class="span3">Arrival Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span2"><input value="<?= isset($booking['arrival_date'])? $booking['arrival_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Arrival Date" type="text" name="inputArival_date" id="inputArival_date"  onchange="booking.getNextday(1);"></div>
                    </div>

                    <?php
                    }    

                        if(isset($booking['departure_date'])) 
                        {
                           $d_date = $booking['departure_date'];
                           $d = new DateTime($d_date);
                           $date = "7 nights ".$d->format('D j M Y');
                           // arrival_date_new = arrival_date.split("-");
                             //var d = new Date(arrival_date_new[2], arrival_date_new[1] - 1, arrival_date_new[0]);
                        }
                        else
                        {
                            $date=''; 
                        }

                       ?>
                    <div class="row-form clearfix">
                        <?php
                         if(isset($booking['departure_date'])) {
                    ?> 
                        <div class="span3">Departure Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span2"><select  data-validation="required" data-validation-error-msg="Please select Departure Date" type="text" name="inputDeparture_date1" id="inputDeparture_date1" disabled>
                                <option value="<?= isset($booking['departure_date'])? $d_date :'' ?>"><?= $date ?>
                                </option>
                         </select>
                           <input value="<?= isset($booking['departure_date'])? $booking['departure_date']:'' ?>"  data-validation="required" data-validation-error-msg="Please select Arrival Date" type="hidden" name="inputDeparture_date" id="inputDeparture_date"  onchange="booking.getNextday(1);">
                        </div> 
                     <!--  <div class="span2"><input  value="<?= isset($booking['departure_date'])? $booking['departure_date']:'' ?>" data-validation="required" data-validation-error-msg="Please select Departure Date" type="text" name="inputDeparture_date" id="inputDeparture_date">
                        </div> -->
                   
                    <?php } else { ?>
                        <div class="span3">Departure Date<em style="color:#Ff0000;">*</em></div>
                        <div class="span2">
                            <select  data-validation="required" data-validation-error-msg="Please select Departure Date" type="text" name="inputDeparture_date" id="inputDeparture_date" disabled>
                                <option value="<?= isset($booking['departure_date'])? $d_date:'' ?>"><?= $date ?>
                                </option>
                            </select>
                          
                        </div> 
                    <?php } 
                    ?>
                     </div>

                    <div class="row-form clearfix">
                        <div class="span3">Arrival Time<em style="color:#Ff0000;"></em></div>
                        <div class="span2"><input value="<?= isset($booking['arival_time'])? $booking['arival_time']:'' ?>"  type="text" name="inputArival_time" id="inputArival_time"/></div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Departure Time<em style="color:#Ff0000;"></em></div>
                        <div class="span2"><input value="<?= isset($booking['departure_time'])? $booking['departure_time']:'' ?>"  type="text" name="inputDeparture_time" id="inputDeparture_time"/></div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">No of Adults<em style="color:#F00;"></em></div>
                        <div class="span6">
                            <select  name="inputAdults" id="inputAdults" class="validate[required]">
                                <option value="">Choose</option>
                                <?php for($i=1;$i<10;$i++) { ?>
                                    <option <?= isset($booking['adults']) && $booking['adults']==$i ? 'selected="selected"':'' ?> value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <!--<div class="row-form clearfix">
                        <div class="span3">No of Childrens </div>
                        <div class="span6">
                            <select name="inputChildrens" id="inputChildrens" >
                                <option selected="selected" value="0">0</option>
                                <?php for($i=1;$i<10;$i++) { ?>
                                    <option <?= isset($booking['childrens']) && $booking['childrens']==$i ? 'selected="selected"':'' ?> value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>-->

                    <div class="row-form clearfix">
                        <div class="span3">No of Children</div>
                        <div class="span6">
                            <select name="children" id="children" >
                                <option value="">0</option>
                                <?php for($i=1;$i<10;$i++) { ?>
                                    <option <?= isset($booking['children']) && $booking['children']==$i ? 'selected="selected"':'' ?> value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">No of Infants</div>
                        <div class="span6">
                            <select name="inputInfants" id="inputInfants" >
                                <option value="">Choose</option>
                                <?php for($i=1;$i<10;$i++) { ?>
                                    <option <?= isset($booking['infants']) && $booking['infants']==$i ? 'selected="selected"':'' ?> value="<?= $i ?>"><?= $i ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Nights</div>
                        <div class="span6">
                            <input value="<?= isset($booking['nights'])? $booking['nights']:'' ?>" type="text" name="inputNights" id="inputNights" />


                        </div>
                    </div>

                    <div class="row-form clearfix">
                        <div class="span3">Status</div>
                        <div class="span6">
                            <select name="inputStatus" id="inputStatus">
                                <option value="">Choose</option>
                                <option value="confirmed">confirmed</option>
                                <option value="provisional">provisional</option>
                            </select>
                        </div>
                    </div>
                    <div class="row-form clearfix">
                        <div class="span3">Cost (in &dollar;)</div>
                        <div class="span6"><input value="<?= isset($booking['cost'])?$booking['cost']:'' ?>" type="text" name="inputCost" id="inputCost"/></div>
                    </div> 
                    <?php  if(!isset($calendar_booking)) { ?>
                    <div class="footer tar">
                        <?php  if(!isset($booking['id'])){ ?>
                        <button class="btn btn-success" id="save_details">Save</button>
                        <?php }else{ ?>
                        <button class="btn btn-success" id="save_details">Update</button>
                        <?php } ?>
                        <a class="btn" href="javascript:window.history.go(-1);">Cancel</a>
                         <!--<a class="btn" href="<?= site_url('bookings/bookings');?>">Cancel</a>-->
                    </div>  
                    <?php } ?>
                </div>
         
    </form>