<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td align="center">
         <table width="580" border="0" cellspacing="0" cellpadding="0">
          <tr>
               <td  height="60" style="height: 60px;border-top: 1px dotted #d5d5d5;border-bottom: 1px dotted #d5d5d5;">
                  <img src="<?= INCLUDES ?>/app/img/b_logo.png" width="200" height="60" alt="Booking Brain" />
               </td>
            </tr> 
            <tr>
               <td>

                  <table width="580" border="0" cellspacing="20" cellpadding="0">

                     <tr>
                        <td width="580" align="left" valign="top" class="mainbar">
                          <h2 style="font-family: Arial;font-size: 18px;font-weight: normal;color: #000000;margin: 20px 0 8px 0;padding: 0;">
                          Dear <?php echo $firstname ?>,</h2>

                          <!-- HOUSEKEEPER -->
                          <?php if ($role == 'housekeeper'){ ?>
                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                             Thank you for signing up at Booking Brain.Your User Name and Password are given below.
                          </p>
                          
						              <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                          	Username : </strong> <strong><?php echo $email ?></strong>
                          </p>
                          
                          
                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                        	Password : </strong> <strong><?php  echo $password ?></strong>
   						           </p> 
                          

                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
						            Please click on the following link to activate your account: 
                           <br/>
                           <a href="<?php echo ROOT.'index.php/registration/registration_complete?username='.$email .'&activation_code='.$activation_code ?>" target="_blank">
                           Activate</a> 
							  
							  
							<?php  }else{ ?>
                          
                          <!-- USER REG -->
                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                             Thank you for signing up at Booking Brain.
                          </p>
                          <!-- COMMENTED SINCE WE ARE NOT ACTIVATING USER -->
                          <!--<p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
						               Please click on the following link to activate your account: 
                           <br/>
                           <a href="<?php echo ROOT.'index.php/registration/registration_complete?username='.$username .'&activation_code='.$activation_code ?>" target="_blank">
                           Activate</a>-->

                          </p>

                          <?php  } ?>
						  
						                <!-- HOUSEKEEPER -->
						                <?php if(isset($email_body)){
                            echo $email_body; }
                            ?>
                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                          If you have any problems accessing your account, please contact us at support@bookingbrain.co.uk


                           </p>


                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                          Thank you,</p> 
                          <p style="font-family: Arial;font-size: 13px;font-weight: normal;color: #333333;margin: 0 0 20px 0;padding: 0;">
                          <strong>
                          Booking Brain<br>

                        </p>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
