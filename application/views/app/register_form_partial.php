    


    <?php $user = isset($response['user']) ? $response['user']:array(); ?>
    <div class="row-fluid">
        <div class="span12">
            <div class="head clearfix">
                <div class="isw-chats"></div>
                <h1>Registration</h1>
            </div>
            <div class="block-fluid clearfix">

                <form action="<?= isset($saveurl) ? $saveurl :site_url('/users/save') ?>" method="POST" id="user_registration">
                
                <input type="hidden" name="id" id="id" value="<?= isset($user['id'])?$user['id']:'' ?> ">
                <input type="hidden" name="login_provider" id="login_provider" value="<?= isset($user['login_provider'])?$user['login_provider']:'' ?> ">
                <input type="hidden" name="login_provider_username" id="login_provider_username" value="<?= isset($user['login_provider_username'])?$user['login_provider_username']:'' ?> ">
                <input type="hidden" name="facebook_user_id" id="facebook_user_id" value="<?= isset($user['facebook_user_id'])?$user['facebook_user_id']:'' ?> ">
                <input type="hidden" name="profile_image_url" id="profile_image_url" value="<?= isset($user['profile_image_url'])?$user['profile_image_url']:'' ?> ">
                <input type="hidden" name="twitter_user_id" id="twitter_user_id" value="<?= isset($user['twitter_user_id'])?$user['twitter_user_id']:'' ?> ">
                <input type="hidden" name="google_user_id" id="google_user_id" value="<?= isset($user['google_user_id'])?$user['google_user_id']:'' ?> ">


                        <fieldset title="Step 1">
                                <legend>Personal Details</legend>
                                <div class="row-form clearfix">
                                    <div class="span3">First Name<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['firstname'])?$user['firstname']:'' ?> " data-validation="required" data-validation-error-msg="You did not enter a firstname" type="text" name="firstname" id="firstname"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">Last Name<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['lastname'])?$user['lastname']:'' ?> " data-validation="required" data-validation-error-msg="You did not enter a lastname" type="text" name="lastname" id="lastname"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">Email Address<em style="color:#Ff0000;">*</em></div>
                                    <div class="span9"><input value="<?= isset($user['email'])?$user['email']:'' ?> " onblur="$('#username').val(this.value);" data-validation="required email" data-validation-error-msg="Invalid email address" on type="text" name="email" id="email"/></div>
                                </div>
                                 <div class="row-form clearfix">
                                    <div class="span3">Phone</div>
                                    <div class="span9"><input value="<?= isset($user['phone'])?$user['phone']:'' ?>" type="text" name="phone" id="phone"/></div>
                                </div>
                                
                                <div class="row-form clearfix">
                                    <div class="span3">Address</div>
                                    <div class="span9">
                                        <textarea value="<?= isset($user['address'])?$user['address']:'' ?> "  name="address" id="address"></textarea>
                                    </div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">City</div>
                                    <div class="span9"><input value="<?= isset($user['city'])?$user['city']:'' ?>"  type="text" name="city" id="city"/></div>
                                </div>

                                <?php
                                    $country = array(
                                    "" => "--Select--",    
                                    "AF" => "Afghanistan",
                                    "AX" => "Åland Islands",
                                    "AL" => "Albania",
                                    "DZ" => "Algeria",
                                    "AS" => "American Samoa",
                                    "AD" => "Andorra",
                                    "AO" => "Angola",
                                    "AI" => "Anguilla",
                                    "AQ" => "Antarctica",
                                    "AG" => "Antigua and Barbuda",
                                    "AR" => "Argentina",
                                    "AM" => "Armenia",
                                    "AW" => "Aruba",
                                    "AU" => "Australia",
                                    "AT" => "Austria",
                                    "AZ" => "Azerbaijan",
                                    "BS" => "Bahamas",
                                    "BH" => "Bahrain",
                                    "BD" => "Bangladesh",
                                    "BB" => "Barbados",
                                    "BY" => "Belarus",
                                    "BE" => "Belgium",
                                    "BZ" => "Belize",
                                    "BJ" => "Benin",
                                    "BM" => "Bermuda",
                                    "BT" => "Bhutan",
                                    "BO" => "Bolivia",
                                    "BA" => "Bosnia and Herzegovina",
                                    "BW" => "Botswana",
                                    "BV" => "Bouvet Island",
                                    "BR" => "Brazil",
                                    "IO" => "British Indian Ocean Territory",
                                    "BN" => "Brunei Darussalam",
                                    "BG" => "Bulgaria",
                                    "BF" => "Burkina Faso",
                                    "BI" => "Burundi",
                                    "KH" => "Cambodia",
                                    "CM" => "Cameroon",
                                    "CA" => "Canada",
                                    "CV" => "Cape Verde",
                                    "KY" => "Cayman Islands",
                                    "CF" => "Central African Republic",
                                    "TD" => "Chad",
                                    "CL" => "Chile",
                                    "CN" => "China",
                                    "CX" => "Christmas Island",
                                    "CC" => "Cocos (Keeling) Islands",
                                    "CO" => "Colombia",
                                    "KM" => "Comoros",
                                    "CG" => "Congo",
                                    "CD" => "Congo, The Democratic Republic of The",
                                    "CK" => "Cook Islands",
                                    "CR" => "Costa Rica",
                                    "CI" => "Cote D'ivoire",
                                    "HR" => "Croatia",
                                    "CU" => "Cuba",
                                    "CY" => "Cyprus",
                                    "CZ" => "Czech Republic",
                                    "DK" => "Denmark",
                                    "DJ" => "Djibouti",
                                    "DM" => "Dominica",
                                    "DO" => "Dominican Republic",
                                    "EC" => "Ecuador",
                                    "EG" => "Egypt",
                                    "SV" => "El Salvador",
                                    "GQ" => "Equatorial Guinea",
                                    "ER" => "Eritrea",
                                    "EE" => "Estonia",
                                    "ET" => "Ethiopia",
                                    "FK" => "Falkland Islands (Malvinas)",
                                    "FO" => "Faroe Islands",
                                    "FJ" => "Fiji",
                                    "FI" => "Finland",
                                    "FR" => "France",
                                    "GF" => "French Guiana",
                                    "PF" => "French Polynesia",
                                    "TF" => "French Southern Territories",
                                    "GA" => "Gabon",
                                    "GM" => "Gambia",
                                    "GE" => "Georgia",
                                    "DE" => "Germany",
                                    "GH" => "Ghana",
                                    "GI" => "Gibraltar",
                                    "GR" => "Greece",
                                    "GL" => "Greenland",
                                    "GD" => "Grenada",
                                    "GP" => "Guadeloupe",
                                    "GU" => "Guam",
                                    "GT" => "Guatemala",
                                    "GG" => "Guernsey",
                                    "GN" => "Guinea",
                                    "GW" => "Guinea-bissau",
                                    "GY" => "Guyana",
                                    "HT" => "Haiti",
                                    "HM" => "Heard Island and Mcdonald Islands",
                                    "VA" => "Holy See (Vatican City State)",
                                    "HN" => "Honduras",
                                    "HK" => "Hong Kong",
                                    "HU" => "Hungary",
                                    "IS" => "Iceland",
                                    "IN" => "India",
                                    "ID" => "Indonesia",
                                    "IR" => "Iran, Islamic Republic of",
                                    "IQ" => "Iraq",
                                    "IE" => "Ireland",
                                    "IM" => "Isle of Man",
                                    "IL" => "Israel",
                                    "IT" => "Italy",
                                    "JM" => "Jamaica",
                                    "JP" => "Japan",
                                    "JE" => "Jersey",
                                    "JO" => "Jordan",
                                    "KZ" => "Kazakhstan",
                                    "KE" => "Kenya",
                                    "KI" => "Kiribati",
                                    "KP" => "Korea, Democratic People's Republic of",
                                    "KR" => "Korea, Republic of",
                                    "KW" => "Kuwait",
                                    "KG" => "Kyrgyzstan",
                                    "LA" => "Lao People's Democratic Republic",
                                    "LV" => "Latvia",
                                    "LB" => "Lebanon",
                                    "LS" => "Lesotho",
                                    "LR" => "Liberia",
                                    "LY" => "Libyan Arab Jamahiriya",
                                    "LI" => "Liechtenstein",
                                    "LT" => "Lithuania",
                                    "LU" => "Luxembourg",
                                    "MO" => "Macao",
                                    "MK" => "Macedonia, The Former Yugoslav Republic of",
                                    "MG" => "Madagascar",
                                    "MW" => "Malawi",
                                    "MY" => "Malaysia",
                                    "MV" => "Maldives",
                                    "ML" => "Mali",
                                    "MT" => "Malta",
                                    "MH" => "Marshall Islands",
                                    "MQ" => "Martinique",
                                    "MR" => "Mauritania",
                                    "MU" => "Mauritius",
                                    "YT" => "Mayotte",
                                    "MX" => "Mexico",
                                    "FM" => "Micronesia, Federated States of",
                                    "MD" => "Moldova, Republic of",
                                    "MC" => "Monaco",
                                    "MN" => "Mongolia",
                                    "ME" => "Montenegro",
                                    "MS" => "Montserrat",
                                    "MA" => "Morocco",
                                    "MZ" => "Mozambique",
                                    "MM" => "Myanmar",
                                    "NA" => "Namibia",
                                    "NR" => "Nauru",
                                    "NP" => "Nepal",
                                    "NL" => "Netherlands",
                                    "AN" => "Netherlands Antilles",
                                    "NC" => "New Caledonia",
                                    "NZ" => "New Zealand",
                                    "NI" => "Nicaragua",
                                    "NE" => "Niger",
                                    "NG" => "Nigeria",
                                    "NU" => "Niue",
                                    "NF" => "Norfolk Island",
                                    "MP" => "Northern Mariana Islands",
                                    "NO" => "Norway",
                                    "OM" => "Oman",
                                    "PK" => "Pakistan",
                                    "PW" => "Palau",
                                    "PS" => "Palestinian Territory, Occupied",
                                    "PA" => "Panama",
                                    "PG" => "Papua New Guinea",
                                    "PY" => "Paraguay",
                                    "PE" => "Peru",
                                    "PH" => "Philippines",
                                    "PN" => "Pitcairn",
                                    "PL" => "Poland",
                                    "PT" => "Portugal",
                                    "PR" => "Puerto Rico",
                                    "QA" => "Qatar",
                                    "RE" => "Reunion",
                                    "RO" => "Romania",
                                    "RU" => "Russian Federation",
                                    "RW" => "Rwanda",
                                    "SH" => "Saint Helena",
                                    "KN" => "Saint Kitts and Nevis",
                                    "LC" => "Saint Lucia",
                                    "PM" => "Saint Pierre and Miquelon",
                                    "VC" => "Saint Vincent and The Grenadines",
                                    "WS" => "Samoa",
                                    "SM" => "San Marino",
                                    "ST" => "Sao Tome and Principe",
                                    "SA" => "Saudi Arabia",
                                    "SN" => "Senegal",
                                    "RS" => "Serbia",
                                    "SC" => "Seychelles",
                                    "SL" => "Sierra Leone",
                                    "SG" => "Singapore",
                                    "SK" => "Slovakia",
                                    "SI" => "Slovenia",
                                    "SB" => "Solomon Islands",
                                    "SO" => "Somalia",
                                    "ZA" => "South Africa",
                                    "GS" => "South Georgia and The South Sandwich Islands",
                                    "ES" => "Spain",
                                    "LK" => "Sri Lanka",
                                    "SD" => "Sudan",
                                    "SR" => "Suriname",
                                    "SJ" => "Svalbard and Jan Mayen",
                                    "SZ" => "Swaziland",
                                    "SE" => "Sweden",
                                    "CH" => "Switzerland",
                                    "SY" => "Syrian Arab Republic",
                                    "TW" => "Taiwan, Province of China",
                                    "TJ" => "Tajikistan",
                                    "TZ" => "Tanzania, United Republic of",
                                    "TH" => "Thailand",
                                    "TL" => "Timor-leste",
                                    "TG" => "Togo",
                                    "TK" => "Tokelau",
                                    "TO" => "Tonga",
                                    "TT" => "Trinidad and Tobago",
                                    "TN" => "Tunisia",
                                    "TR" => "Turkey",
                                    "TM" => "Turkmenistan",
                                    "TC" => "Turks and Caicos Islands",
                                    "TV" => "Tuvalu",
                                    "UG" => "Uganda",
                                    "UA" => "Ukraine",
                                    "AE" => "United Arab Emirates",
                                    "GB" => "United Kingdom",
                                    "US" => "United States",
                                    "UM" => "United States Minor Outlying Islands",
                                    "UY" => "Uruguay",
                                    "UZ" => "Uzbekistan",
                                    "VU" => "Vanuatu",
                                    "VE" => "Venezuela",
                                    "VN" => "Viet Nam",
                                    "VG" => "Virgin Islands, British",
                                    "VI" => "Virgin Islands, U.S.",
                                    "WF" => "Wallis and Futuna",
                                    "EH" => "Western Sahara",
                                    "YE" => "Yemen",
                                    "ZM" => "Zambia",
                                    "ZW" => "Zimbabwe");
                                    ?>

                                <div class="row-form clearfix">
                                    <div class="span3">Country</div>
                                    <div class="span6">
                                        <select name="country">
                                        <?php foreach($country as $key => $value) {   ?>
                                            <option value="<?= $key ?>" title="<?= htmlspecialchars($value) ?>"><?= htmlspecialchars($value) ?></option>
                                        <?php    }  ?>
                                        </select>
                                    <!--<input value="<?= isset($user['country'])?$user['country']:'' ?>"  type="text" name="country" id="country"/>--></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">Zip/Pin code</div>
                                    <div class="span9"><input value="<?= isset($user['zip_postal_code'])?$user['zip_postal_code']:'' ?>" type="text" name="zip_postal_code" id="zip_postal_code"/></div>
                                </div>
                        </fieldset>
                        <!-- Since if its facebook login simple reg form is expected 31/5/14.-->
                        <?php if( !isset($user['id'])  && !isset($user['login_provider'] ) ){ ?>
                        <fieldset title="Step 2">
                                <legend>Company Details</legend>
                                <div class="row-form clearfix">
                                    <div class="span3">Company Id</div>
                                    <div class="span9"><input value="<?= isset($user['company_id'])?$user['company_id']:'' ?>" type="text" name="company_id" id="company_id" placeholder="eg:mycompany.bookingbrain.co.uk"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">Company Name</div>
                                    <div class="span9"><input value="<?= isset($user['company_name'])?$user['company_name']:'' ?>" type="text" name="company_name" id="company_name"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span3">Company Website</div>
                                    <div class="span9"><input value="<?= isset($user['company_website'])?$user['company_website']:'' ?>" type="text" name="company_website" id="company_website"/></div>
                                </div>
                        </fieldset>
                      
                            <fieldset title="Step 3">
                                    <legend>Login Details</legend>
                                    <div class="row-form clearfix">
                                        <div class="span3">Username<em style="color:#Ff0000;">*</em></div>
                                        <div class="span9">
                                            <input data-validation="required" data-validation-error-msg="You did not enter a username" type="text" name="username" id="username"/>
                                        </div>
                                    </div>
                                    <?php if(!$this->sessions->getsessiondata('isadmin')){?>
                                        <div class="row-form clearfix">
                                            <div class="span3">Password<em style="color:#Ff0000;">*</em></div>
                                            <div class="span9">
                                                <input  data-validation-strength="2" data-validation="strength"  type="password" name="password_confirmation" id="password_confirmation"/>
                                            </div>
                                        </div>
                                        <div class="row-form clearfix">
                                            <div class="span3">Re-type Password<em style="color:#Ff0000;">*</em></div>
                                            <div class="span9">
                                                <input data-validation="confirmation" type="password" name="password" id="password"/>
                                            </div>
                                        </div>
                                    <?php } ?>
                            </fieldset>
                        <?php } ?>
                        <?php if( isset($user['login_provider'] ) ){ ?>
                              <input type="hidden" name="username" id="username" value="<?= isset($user['username'])?$user['username']:'' ?> ">
                        <?php }?>

                        <input type="submit" class="btn finish" id="reg" value="<?= isset($user['id']) && $user['id']!='' ?'Update':'Register' ?>" />
                        <a class="btn finish" style="margin-right:-151px; height:17px; " href="<?= site_url('/');?>">Cancel</a>

                </form>
            </div>
        </div>
         <style>
            #reg{
                border:1px solid #829E18 !important;
                background:-moz-linear-gradient(center top , #ADC800 0%, #829E18 100%) repeat-x scroll 0 0 rgba(0, 0, 0, 0) !important;
                color: #FFFFFF !important;
                text-shadow:0 -1px 0 rgba(0, 0, 0, 0.25) !important;
                height:27px;
                margin-right: 75px;
               
            }
        </style>

   