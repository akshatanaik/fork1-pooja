<!-- commissions view -->
<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    <title>Booking Brain-Commissions</title>
    <link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">
    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
            
                <!-- display the list of commissions -->
                 <div class="page-header">
                    <h1>Commissions</h1>
                </div>                  
                
                <div class="row-fluid">

                    <div class="span12">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>All Commissions</h1>      
                            <ul class="buttons">
                                <li><a href="#" class="isw-download"></a></li>                                                        
                                
                                <li>
                                    <a href="#" class="isw-settings"></a>
                                    <ul class="dd-list">
                                        <li><a href="<?= site_url('/commissions/add_commissions') ?>"><span class="isw-plus"></span> New Commission</a></li>
                                        
                                    </ul>
                                </li>
                            </ul>                        
                        </div>
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th width="3%">ID</th>
                                        <th width="15%">Commision percentage</th>
                                        <th width="10%">VAT% on commission</th>
                                        <th width="10%">Minimum commission</th>
                                        <th width="10%">Minimum owner account value</th>
                                        <th width="10%">Overseas tax</th>
                                        <th width="10%">Commission on net booking value</th>
                                        <th width="25%">Property</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php foreach($response['records'] as $rec): ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group" style="font-size: 10px;">
                                                    <button disabled="disabled" style="font-size: 10px;" class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" style="font-size: 10px;">
                                                        <li><a href="<?= site_url('/commissions/edit_commission') ?>?id=<?= $rec['id'] ?>">Edit</a></li>
                                                        <li><a href="javascript:void(0)" onclick="commissions.delete('<?= $rec['id'] ?>')" id="delete">Delete</a></li>                                              
                                                    </ul>
                                                </div>
                                            </td>
                                            <td><?= $rec['id'] ?></td>
                                            <td><?= $rec['commision_percentage'] ?></td>
                                            <td><?= $rec['vat_on_commission'] ?></td>
                                            <td><?= $rec['minimum_commision'] ?></td> 
                                            <td><?= $rec['minimum_owner_account_value'] ?></td>
                                            <td><?= $rec['overseas_tax'] ?></td>
                                            <td><?= $rec['commission_on_net_booking_val'] ?></td> 
                                            <td><?= $rec['property_name'] ?></td>                                     
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>   
    </div>

    
    <script src="<?= INCLUDES ?>app/js/commissions.js"></script>

     <div style="display:none;" id="dialog-confirm" title="Delete?">
      <p>
        Commission is going to be deleted. Are you sure?    
     </p>
    </div> 

    <script> 
        $("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});
    </script>
    <style>
    .table thead th {
    vertical-align: top;
    }
    </style>



</body>
</html>