<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

   
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 



    

    <style>



          [class*="block"] [class*="isw-"], [class*="block"] [class*="isb-"]

        {

            margin-left: 5px;

            margin-right: 1px;

            padding: 18px 0 0;

        }



        .block, .block-fluid 

        {

            margin-bottom: -11px;

        }



        [class*="span"] 

        {

            float: left;

            margin-left: 0;

            min-height: 1px;

        }

    </style>

    

</head>

<body>

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   

        

        <div class="menu">                

            <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>Location Details</h1>


                </div>



                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                <?php foreach($response['records'] as $rec): ?>

                <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-favorite"></div>

                            <h1> <?= $rec['property_name'] .'('. ($rec['property_id']) .')' ?></h1>
                            <a class="btn" style="margin-left:840px;" href="<?= site_url('property/propertyList');?>">Cancel</a>

                        </div>

                        <div class="block-fluid tabs">

                            <ul>

                                <li><a href="#property_location">Property location</a></li>

                                <li><a href="#localarea">Local area</a></li>

                                <li><a href="#how_to_get_there">How to get there</a></li>

                                <li><a href="#whats_nearby">What's nearby</a></li>

                                <li><a href="#holiday_types">Holiday types</a></li>

                            </ul>

                            <?php $id = $this->uri->segment(3); ?>

                              

                            <div id="property_location">

                                <div class="row-fluid">

                                    <div class="span12"> 

                                                               

                                    <div class="row-form clearfix">

                                        <div class="span3">Continent<em style="color:#F00;">*</em></div>

                                        <div class="span6"><?= $rec['continent'] ?></div>                            

                                    </div>                        



                                    <div class="row-form clearfix">

                                        <div class="span3">Country</div>

                                        <div class="span6"><?= $rec['country'] ?></div>                            

                                    </div>                                                                                    



                                    <div class="row-form clearfix">

                                        <div class="span3">Region</div>

                                        <div class="span6"><?= $rec['region'] ?></div>                            

                                    </div>                                                                                 



                                    <div class="row-form clearfix">

                                        <div class="span3">Subregion</div>

                                        <div class="span6"><?= $rec['subregion'] ?></div>                            

                                    </div>      



                                    <div class="row-form clearfix">

                                        <div class="span3">Town</div>

                                        <div class="span6"><?= $rec['town'] ?></div>                            

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">Suburb(optional)</div>

                                        <div class="span6"><?= $rec['suburb'] ?></div>                            

                                    </div> 



                                    <div class="row-form clearfix">

                                        <div class="span3">Property address</div>

                                        <div class="span6"><?= $rec['property_address'] ?></div>

                                    </div> 



                                    <div class="row-form clearfix">

                                        <div class="span3">Postcodes</div>

                                        <div class="span6"><?= $rec['postcode'] ?></div>

                                    </div> 



                                    <div class="row-fluid">

                                        <div class="span12">

                                            <div class="head clearfix">

                                                <div class="isw-graph"></div>

                                                <h1>Property Map</h1>

                                            </div>

                                            <div class="block">

                                                <iframe  width="1090" height="250" frameborder="0" style="border:0"

                                                      src="https://www.google.com/maps/embed/v1/search?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q=<?= $rec['property_address'] ?>">

                                                </iframe>



                                            </div> 

                                        </div> 

                                    </div>                  

                                

                            </div>

                        </div>

                    </div>

              

                    <div id="localarea">

                        <div class="row-fluid">

                            <div class="span12"> 

                                    

                                    <div class="row-form clearfix">

                                        <div class="span3">The region</div>

                                        <div class="span6"><?= $rec['region_description'] ?></div>

                                    </div>

                                     <div class="row-form clearfix">

                                        <div class="span3">The area</div>

                                        <div class="span6"><?= $rec['area_description'] ?></div>

                                    </div>                       

                                </div>

                        </div>

                    </div>

              

                    <div id="how_to_get_there">

                        <div class="row-fluid">

                            <div class="span12"> 

                                                      

                                    <div class="row-form clearfix">

                                        <div class="span3">Airport</div>

                                        <div class="span6"><?= $rec['airport'] ?> km</div>  

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Ferry port</div>

                                        <div class="span6"><?= $rec['ferry_port'] ?> km</div> 

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">Train station </div>

                                        <div class="span6"><?= $rec['train_station'] ?> km</div> 

                                    </div>

                                     <div class="row-form clearfix">

                                        <div class="span3">Car required</div>

                                        <div class="span6"><?= $rec['car'] ?></div>                            

                                    </div>

                                    <div class="row-form clearfix">



                                        <div class="span3">More tips of how to get there</div>

                                        <div class="span6"><?= $rec['how_to_get_there'] ?></div>

                                    </div> 

                                

                            </div>

                        </div>

                    </div>

                

                    <div id="whats_nearby">

                        <div class="row-fluid">

                            <div class="span12"> 

                               

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['golf_course_within_15km'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>  Golf course within 15 mins walk

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['golf_course_within_30km'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Golf course  within 30 mins drive

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                             </label>

                                             <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['tennis_court'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Tennis courts nearby

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['skiing'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Skiing:property is in a ski resort

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                             </label>

                                             <?php } ?>

                                        </div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['water_sport'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>Water sports nearby

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['water_park'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Water park nearby

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                             </label>

                                             <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['horse_riding'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Horse riding nearby 

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                             <?php if($rec['beach_front'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked" /> Beachfront property

                                                </label>

                                            <?php }else{ ?>



                                            <label class="checkbox inline">

                                                <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                             </label>

                                            <?php } ?>

                                        </div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['fishing'] != 0) { ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" checked="checked"/> Fishing nearby

                                            </label>

                                            <?php }else{ ?>

                                             <label class="checkbox inline">

                                                <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                            </label>

                                            <?php } ?>

                                            

                                        </div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">Nearest Beach</div>

                                        <div class="span6"><?= $rec['nearest_beach'] ?></div>  km

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">Nearest shops</div>

                                        <div class="span6"><?= $rec['nearest_shop'] ?></div>  km

                                    </div>

                                

                            </div>

                        </div>

                    </div>



                    <div id="holiday_types">

                        <div class="row-fluid">

                            <div class="span12"> 

                               

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['walking_holiday'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Walking holidays

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['cycling_holiday'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>Cycling holidays

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                             </label>

                                             <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <?php if($rec['rural_countryside_retreats'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Rural or countryside retreats

                                                </label>

                                            <?php }else{ ?>

                                                <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['beach_lakeside_relaxation'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Beach or lakeside relaxation

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                             </label>

                                             <?php } ?>

                                        </div>

                                    </div>



                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <?php if($rec['city_breaks'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/>City breaks

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                            </label>

                                            <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['winter_sun'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Winter sun

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                             </label>

                                             <?php } ?>

                                        </div>



                                        <div class="span3">

                                            <?php if($rec['night_life'] != 0) { ?>

                                                <label class="checkbox inline">

                                                    <input type="checkbox" checked="checked"/> Nightlife

                                                </label>

                                            <?php }else{ ?>

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                            </label>

                                            <?php } ?>

                                        </div>

                                   

                                </div>   

                            </div>

                        </div>

                    </div>

                </div>

             <?php endforeach; ?>

            <?php }else{ ?>

                <p style="text-align:center; font-size:18px;">  No location details for this property. </p>

            <?php } ?>

            </div>   

        </div>

</body>

</html>

