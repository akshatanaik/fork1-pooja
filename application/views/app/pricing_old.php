<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />


    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

     <link rel='stylesheet' type='text/css' href='<?= INCLUDES ?>/app/css/jquery.dop.BackendBookingCalendarPRO.css'  />



     <style>

        [class*="span"] {

            float: left;

            margin-left: 0;

            min-height: 1px;

        }



     </style>

    

</head>

<?php  $property = isset($_GET['property_id']) ? $_GET['property_id']:'';

       

?>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper">

            <!-- include header -->

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- display property filter as well -->

            <div class="row-fluid"> 

                    <div class="row-form clearfix">

                        <div class="span3">Property</div>

                        <div class="span6">

                            <select >

                                    <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>

                                        <option value="">--Choose a property to set pricing --</option>

                                        <?php foreach($response['properties'] as $res => $rec): ?>

                                            <option <?= isset($property) && $property==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  

                                        <?php endforeach; ?>

                                    <?php } ?>

                                </select>

                        </div>

                    </div>

            </div>

            <div class="dr"><span></span></div>

            <!--   -->

            <div class="workplace">

                     <div class="span12">

                        <div class="head clearfix">

                            <div class="isw-calendar"></div>

                            <h1>Pricing</h1>

                            <ul class="buttons">                                                                                   

                                <li>

                                    <a href="<?= site_url('/pricing/pricing_settings') ?>?property_id=<?= $property ?>" class="isw-settings"></a>

                                </li>

                            </ul>  

                        </div>

                        <div class="block-fluid">

                            <div id="pricing_calendar" ></div>

                        </div>

                    </div>

            </div>





        </div>   

    </div>



   <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.dop.BackendBookingCalendarPRO.js" ></script>



   <script>

        $('#pricing_calendar').DOPBackendBookingCalendarPRO({'ID': '1111','DataURL': "<?= site_url('/pricing/get_pricing')?>?property_id=<?= $property ?>",

                                                    'SaveURL': "<?= site_url('/pricing/save_pricing')?>?property_id=<?= $property ?>"

                                                  });

   </script>









</body>

</html>

