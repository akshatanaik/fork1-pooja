<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Booking Brain</title>
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/plugins/jquery-1.10.2.min.css" />

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/commissions.js'></script>
   
    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
    </style>

    <script>
    function chkd(){
        var isChkd = $("input:checked:checked").map(function(){
            return $(this).is(':checked');
        }).get();
        var hiddenVal = document.getElementById('bookingVal').value;
        if(isChkd == 'true'){
            if(hiddenVal == ''){ 
                $.pnotify({title: 'Error', text:'Select property for commision needs to be calculated.', opacity: .8, type: 'error'});
                document.getElementById("cal_commission").checked = false;    
            }
            var net_booking_val = document.getElementById('net_booking_value').value = hiddenVal;
            if(net_booking_val == ''){

            } 
            console.log('Net booking val'+net_booking_val);
            var commision_percentage = document.getElementById('commision_percentage').value;
            var commision_value = net_booking_val *commision_percentage/100;
            
            document.getElementById('commission_on_net_booking_val').value = commision_value;

        }
    }
    </script>

</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>   
        
        <div class="menu">                
            <!-- include navigation -->
            <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

           
            <!-- Main content -->
            <div class="workplace">
                
                <div class="page-header">
                    <h1>Commissions</h1>
                </div> 
                <div class="row-fluid">
                    <div class="span12"> 
                        <div class="head clearfix">
                            <div class="isw-favorite"></div>
                            <h1>Commissions</h1>
                        </div>
                        <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>
                        <?php $commission = isset($response['commission']) ? $response['commission']:array(); ?>
                        <form id="commissions_form" class="form-horizontal" action="<?= site_url('/commissions/save') ?>" method="POST" onsubmit="return save(this)">
                            <div class="block-fluid">
                            	<div class="row-form clearfix">
                                <div class="span4">Property<em style="color:#F00;">*</em></div>
                                    <input id="bookingVal" type="hidden" value=""/>
                                    <div class="span4">
                                        <select onchange="set_commision(this.value)" data-validation="required" data-validation-error-msg="Please select property" name="inputProperty_name" id="inputProperty_name">
                                            <?php if(isset($response['properties']) &&  !empty($response['properties'])) {  ?>
                                                <option value="">--Choose a property--</option>
                                                <?php foreach($response['properties'] as $res => $rec): ?>
                                                    <option <?= isset($commission['property_id']) && $commission['property_id']==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                                <?php endforeach; ?>
                                            <?php }else{?>
                                                     <option value="">-- No properties --</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                    
                                <div class="row-form clearfix">
                                    <div class="span4">Pay Owner in<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4">
                                        <select name="pay_owner_in" id="pay_owner_in">
                                            <option value="">Choose</option>
                                            <option value="GBP">GBP</option>
                                            <option value="dollars">dollars</option>
                                            <option value="rupees">rupees</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">Commision percentage<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input value="16" data-validation="required" data-validation-error-msg="Please enter Commision percentage" type="text" name="commision_percentage" id="commision_percentage"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">VAT% on commission<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input value="20" data-validation="required" data-validation-error-msg="Please enter VAT% on commission" type="text" name="vat_on_commission" id="vat_on_commission"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">Minimum commission<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input value="0" data-validation="required" data-validation-error-msg="Please enter Minimum commission" type="text" name="minimum_commision" id="minimum_commision"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">Minimum owner account value<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input value="0" data-validation="required" data-validation-error-msg="Please enter Minimum owner account value" type="text" name="minimum_owner_account_value" id="minimum_owner_account_value"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">Overseas tax<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input value="0" data-validation="required" data-validation-error-msg="Please enter Overseas tax" type="text" name="overseas_tax" id="overseas_tax"/></div>
                                </div>
                                <div class="row-form clearfix">
                                    <div class="span4">Calculate commission on net booking value<em style="color:#Ff0000;">*</em></div>
                                    <div class="span4"><input onclick="chkd();" class="checker" type="checkbox" name="cal_commission" id="cal_commission"/></div>
                                    
                                </div>
                                <div class="row-form clearfix"> 
                                    <div class="span3">Net booking value</div>
                                    <div class="span2"><input type="text" name="net_booking_value" id="net_booking_value"/></div>

                                     <div class="span3">Commission on net booking value</div>
                                    <div class="span2"><input type="text" name="commission_on_net_booking_val" id="commission_on_net_booking_val"/></div>
                                </div>

                                <div class="footer tar">
                                    <button class="btn btn-success" id="save_details">Save</button>
                                    <a class="btn" href="javascript:window.history.go(-1);">Cancel</a>
                                </div>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>