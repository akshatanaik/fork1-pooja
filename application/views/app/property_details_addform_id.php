<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

  
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

    

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 

    

</head>

<body>

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>    

       

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main Content -->

            <div class="workplace">

                 <div class="page-header">

                    <h1>Property Details Add Form</h1>

                </div>



                 <?php $id = $this->uri->segment(3); ?>

                 <div class="block-fluid tabs">

                    <ul>

                        <li><a href="#facilities">Facilities and Features</a></li>

                        <li><a href="#suitability">Suitability</a></li>

                        <li><a href="#website">Website</a></li>

                    </ul>   

                    <div class="alert alert-error" id="errormsg" style="visibility:hidden;"></div> 

                    <div class="alert alert-success" id="successmsg" style="visibility:hidden;"></div>    



                <form class="form-horizontal" action="<?= site_url('/property/savePropertyFeaturesforId/'.$id)?>" method="POST" onsubmit="return propertyfeatures(this);">

                    <div id="facilities">

                        <div class="row-fluid">

                            <div class="span12"> 

                                <div class="head clearfix">

                                    <div class="isw-favorite"></div>

                                    <h1>Facilities and Features</h1>

                                </div>

                                <div id="success_message"></div>

                                <div class="block-fluid">

                                    

                                    <div class="row-form clearfix">

                                        <div class="span3">Home size in Sq.m</div>

                                        <div class="span6"><input type="text" name="inputHome_size" id="inputHome_size"/> </div>

                                    </div>

                                     <div class="row-form clearfix">

                                        <div class="span4"><h4>Beds</h4></div>

                                        <div class="span4"><h4>Bathrooms</h4> </div>

                                        <div class="span4"><h4>Seating</h4> </div>

                                    </div>

                                     <div class="row-form clearfix">

                                        

                                        <div class="span2">Single beds</div>

                                        <div class="span2">        

                                            <select name="inputSingle_bed" id="inputSingle_bed">

                                                <option value="">Choose</option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                    

                                        <div class="span2">Family bathrooms</div>

                                        <div class="span2">        

                                            <select name="inputFamily_bathrooms" id="inputFamily_bathrooms">

                                                <option value="">Choose</option>

                                                <option value="1">1</option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div>

                                    

                                        <div class="span2">Dining seats</div>

                                        <div class="span2">        

                                            <select name="inputDining_seats" id="inputDining_seats">

                                                <option value="">Choose </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                    </div>

                                

                                    <div class="row-form clearfix">

                                        <div class="span2">Double beds</div>

                                        <div class="span2">        

                                            <select name="inputDouble_beds" id="inputDouble_beds">

                                                <option value="">Choose</option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                        <div class="span2">En suites </div>

                                        <div class="span2">        

                                            <select name="inputEn_suites" id="inputEn_suites">

                                                <option value="">Choose </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                        <div class="span2">Lounge seats </div>

                                        <div class="span2">        

                                            <select name="inputLounge_seats" id="inputLounge_seats">

                                                <option value="">Choose </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div>  

                                     </div> 

                                  

                                     <div class="row-form clearfix">

                                        <div class="span2">Sofa beds </div>

                                        <div class="span2">        

                                            <select name="inputSofa_beds" id="inputSofa_beds">

                                                <option value="">Choose   </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                        <div class="span2">Shower rooms </div>

                                        <div class="span2">        

                                            <select name="inputShower_rooms" id="inputShower_rooms">

                                                <option value="">Choose   </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 



                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span2"> Cots</div>

                                        <div class="span2">        

                                            <select name="inputCots" id="inputCots">

                                                <option value="">Choose   </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 

                                        <div class="span2">Total bathrooms </div>

                                        <div class="span2">        

                                            <select name="inputTotal_bathrooms" id="inputTotal_bathrooms">

                                                <option value="">Choose   </option>

                                                <option value="1">1 </option>

                                                <option value="2">2 </option>

                                                <option value="3">3 </option>

                                            </select>

                                        </div> 



                                    </div>        

                                

                                    <div class="row-form clearfix">

                                       <div class="span3">

                                           <h4>Indoors</h4> 

                                        </div>

                                    </div>

                                    

                                    <div class="row-form clearfix">

                                       

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCooker" id="inputCooker" value="1" />cooker 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputTv" id="inputTv" value="1"/>Tv

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputLog_fire" id="inputLog_fire" value="1"/>Log fire 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputFridge" id="inputFridge" value="1"/>fridge 

                                            </label>

                                        </div>

                                    </div>

                                    <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSatellite_tv" id="inputSatellite_tv" value="1"/>Satellite tv 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCentral_heating" id="inputCentral_heating" value="1"/>Central heating 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputFreezer" id="inputFreezer" value="1"/>freezer 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputVideo_player" id="inputVideo_player" value="1"/>Video player 

                                            </label>

                                        </div>



                                    </div>

                                    <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputAc" id="inputAc" value="1"/>Air conditioning 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputMicrowave" id="inputMicrowave" value="1"/>Microwave 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputDvd_player" id="inputDvd_player" value="1"/>DVD player 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputLinen" id="inputLinen" value="1"/>Linen provided 

                                            </label>

                                        </div>

                                    </div>

                                    <div class="row-form clearfix">

                                       <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputToaster" id="inputToaster" value="1"/>Toaster 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCd_player" id="inputCd_player" value="1"/>CD player 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputTowels" id="inputTowels" value="1"/>Towels provided 

                                            </label>

                                        </div> 



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputKettle" id="inputKettle" value="1"/>Kettle 

                                            </label>

                                        </div>

                                    </div>

                                    <div class="row-form clearfix">

                                         <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="InputInternet" id="InputInternet" value="1"/>Internet access 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSauna" id="inputSauna" value="1"/>Sauna 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputDishwasher" id="inputDishwasher" value="1"/>Dishwasher 

                                            </label>

                                        </div>

                                    

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputWifi" id="inputWifi" value="1"/>Wi-Fi avaliable 

                                            </label>

                                        </div>

                                    </div>

                                    <div class="row-form clearfix">

                                       <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputGym" id="inputGym" value="1"/>Gym 

                                            </label>

                                        </div> 



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputWashing_machine" id="inputWashing_machine" value="1"/>Washing machine 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputTelephone" id="inputTelephone" value="1"/>Telephone 

                                            </label>

                                        </div>

                                    

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputTable_tennis" id="inputTable_tennis" value="1"/>Table-tennis 

                                            </label>

                                        </div>

                                        

                                       

                                    </div>

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputClothes_dryer" id="inputClothes_dryer" value="1"/>Clothes dryer 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputFax_machine" id="inputFax_machine" value="1"/>Fax machine 

                                            </label>

                                        </div>

                                    

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPool_or_snooker_table" id="inputPool_or_snooker_table" value="1"/>Pool or Snooker table 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputIron" id="inputIron" value="1"/>Iron 

                                            </label>

                                        </div>

                                        

                                        

                                    </div>

                                    <div class="row-form clearfix">

                                         <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHair_dryer" id="inputHair_dryer" value="1"/>Hair dryer 

                                            </label>

                                        </div>

                                    

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputGames_room" id="inputGames_room" value="1"/>Games room 

                                            </label>

                                        </div>



                                         <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHigh_chair" id="inputHigh_chair" value="1"/>High chair 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSafe" id="inputSafe" value="1"/>Safe 

                                            </label>

                                        </div>

                                    </div>

                                    <div class="row-form clearfix">

                                         <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputStaffed_property" id="inputStaffed_property" value="1"/>Staffed property 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCleaning_services" id="inputCleaning_services" value="1"/>Cleaning services 

                                            </label>                                    </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputIroning_board" id="inputIroning_board" value="1"/>Ironing board

                                            </label>

                                        </div>

                                    

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputFan" id="inputFan" value="1"/>fan 

                                            </label>

                                        </div>

                                    </div>

                                



                                    <div class="row-form clearfix">

                                       <div class="span3">

                                           <h4>Outdoors</h4> 

                                        </div>

                                    </div>

                                

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShared_outdoor_pool_heated" id="inputShared_outdoor_pool_heated" value="1"/>Shared Outdoor pool(heated) 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShared_tennis_court" id="inputShared_tennis_court" value="1"/>Shared tennis court 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSolarium_or_roof_terrace" id="inputSolarium_or_roof_terrace" value="1"/>Solarium or roof terrace 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShared_outdoor_pool_unheated" id="inputShared_outdoor_pool_unheated" value="1"/>Shared Outdoor pool(unheated) 

                                            </label>

                                        </div>

                                    </div>

                                

                                    <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_tennis_court" id="inputPrivate_tennis_court" value="1"/>Private tennis court 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputBalancy_or_terrace" id="inputBalancy_or_terrace" value="1"/>Balcony or terrace 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_outdoor_pool_heated" id="inputPrivate_outdoor_pool_heated" value="1"/>Private outdoor pool(heated) 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShared_garden" id="inputShared_garden" value="1"/>Shared garden 

                                            </label>

                                        </div>

                                    </div>

                                

                                    <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSea_view" id="inputSea_view" value="1"/>Sea view 

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_outdoor_pool" id="inputPrivate_outdoor_pool" value="1"/>Private outdoor pool(unheated) 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_garden" id="inputPrivate_garden" value="1"/>Private garden 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_fishinglake_or_river" id="inputPrivate_fishinglake_or_river" value="1"/>Private fishinglake or river 

                                            </label>

                                        </div>

                                    </div>

                                

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputPrivate_indoor_pool" id="inputPrivate_indoor_pool" value="1"/>Private indoor pool 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputClimbing_frame" id="inputClimbing_frame" value="1"/>Climbing frame 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputBoat" id="inputBoat" value="1"/>Boat avaliable 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShared_indoor_pool" id="inputShared_indoor_pool" value="1"/>Shared indoor pool  

                                            </label>

                                        </div>

                                     </div>

                                 

                                     <div class="row-form clearfix">

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSwing_set" id="inputSwing_set" value="1"/>Swing set 

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputBicycle" id="inputBicycle" value="1"/>Bicycle avaliable

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputChildrens_pool" id="inputChildrens_pool" value="1"/> Children's pool

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputTrampoline" id="inputTrampoline" value="1"/> Trampoline

                                            </label>

                                        </div>

                                     </div>

                                 

                                     <div class="row-form clearfix">

                                       <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputParking" id="inputParking" value="1"/> Parking

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputJacuzzi_or_hot_tub" id="inputJacuzzi_or_hot_tub" value="1"/> Jacuzzi or hot tub

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputBbq" id="inputBbq" value="1"/> BBQ

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputSecure_parking" id="inputSecure_parking" value="1"/> Secure parking

                                            </label>

                                        </div>

                                        

                                     </div>

                                

                                     <div class="footer tar">

                                        <button class="btn" id="save_details">Save</button>

                                    </div>

                                </div>  

                            </div>

                        </div>

                    </div>

                </form>

                  



                <form class="form-horizontal" action="<?= site_url('/property/savePropertySuitablityforId/'.$id) ?>" method="POST" onsubmit="return propertysuitablity(this);">

                    <div id="suitability">

                        <div class="row-fluid">

                            <div class="span12"> 

                                <div class="head clearfix">

                                    <div class="isw-users"></div>

                                    <h1>Suitability</h1>

                                </div>

                                <div id="success_message1"></div>

                                <div class="block-fluid">

                                    <div class="row-form clearfix">

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputLong_term_lets" id="inputLong_term_lets" value="1"/>Long term lets(over 1-month)   

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputCorporate_bookings" id="inputCorporate_bookings" value="1"/> Corporate bookings

                                            </label>

                                        </div>

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHouse_swap" id="inputHouse_swap" value="1"/> house swap

                                            </label>

                                        </div>



                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputShort_breaks" id="inputShort_breaks" value="1"/>Short breaks(1-4 days) 

                                            </label>

                                        </div>

                                     </div>

                                     <div class="row-form clearfix">

                                        

                                        <div class="span3">

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="inputHen_or_stag_breaks" id="inputHen_or_stag_breaks" value="1"/> Hen or Stag breaks

                                            </label>

                                        </div>

                                     </div>

                                     <div class="row-form clearfix">

                                        <div class="span3">Childern</div>

                                        <div class="span6">        

                                            <select name="inputChildren" id="inputChildren" >

                                                <option value="">Choose </option>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Pets </div>

                                        <div class="span6">        

                                            <select name="inputPets" id="inputPets" >

                                                <option value="">Choose </option>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Smokers </div>

                                        <div class="span6">        

                                            <select name="inputSmokers" id="inputSmokers" >

                                                <option value="">Choose </option>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Restricted mobility </div>

                                        <div class="span6">        

                                            <select name="inputRestricted_mobility" id="inputRestricted_mobility">

                                               <option value="">Choose </option>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                    <div class="row-form clearfix">

                                        <div class="span3">Wheelchair users</div>

                                        <div class="span6">        

                                            <select name="inputWheelchair_users" id="inputWheelchair_users" >

                                                 <option value="">Choose </option>

                                                <option value="1">1</option>

                                                <option value="2">2</option>

                                            </select>

                                        </div>                            

                                    </div>  

                                   

                                </div>

                            </div>

                        </div>

                        </div>

                    



                       

                        <div id="website">

                        <div class="row-fluid">

                            <div class="span12"> 

                                <div class="head clearfix">

                                    <div class="isw-favorite"></div>

                                    <h1>Your website</h1>

                                </div>

                                <div class="block-fluid">

                                    <p>Link to your personal website from your listing. All we ask is that you link back to us in return. Please use this html code to set up the link.</p>

                                    <div class="row-form clearfix">

                                        <div class="span3">Your website http://</div>

                                        <div class="span6"><input value=""  type="text" name="inputWebsite" id="inputWebsite"/></div>

                                    </div>

                                    <div class="footer tar">

                                        <button class="btn">Save</button>

                                    </div>   

                                </div>

                                

                            </div>

                        </div>

                    </div>



                </form>

                <div id="cancel">

                    <a href="<?= site_url('/property/propertyList/'); ?>">&laquo; Cancel and go back</a>

                </div>

             </div>

             

         </div>   

    </div>

</body>

</html>

