<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.timepicker.min.js"></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/changepassword.js'></script> 
    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
    </style>
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>   
        
        <div class="menu">                
            <!-- include navigation -->
            <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

           
            <!-- Main content -->
            <div class="workplace">
                <div class="page-header">
                    <h1>Change Password</h1>
                </div> 
                <div class="row-fluid">
                <div class="span12"> 
                    <div class="head clearfix">
                        <div class="isw-favorite"></div>
                        <h1>Change Password</h1>
                    </div>
                    <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>
                    <form id="changePassword_form" class="form-horizontal" action="<?= site_url('/changepassword/chpassword/')?>" method="POST" onsubmit="return changepassword(this);">
                        <div class="block-fluid">
                            <div class="row-form clearfix">
                                <div class="span3">Current Password<em style="color:#Ff0000;">*</em></div>
                                <div class="span6"><input data-validation="required" data-validation-error-msg="Please enter Current Password." type="password" name="inputCurrent_Password" id="inputCurrent_Password"/></div>
                            </div>

                            <div class="row-form clearfix">
                                <div class="span3">New Password<em style="color:#Ff0000;">*</em></div>
                                <div class="span6"><input data-validation="required" data-validation-error-msg="Please enter New Password." type="password" name="inputNew_Password" id="inputNew_Password"/></div>
                            </div>

                            <div class="row-form clearfix">
                                <div class="span3">Confirm Password<em style="color:#Ff0000;">*</em></div>
                                <div class="span6"><input data-validation="required" data-validation-error-msg="Please enter Confirm Password." type="password" name="inputConfirm_Password" id="inputConfirm_Password"/></div>
                            </div>
                            <div class="footer tar">
                                <button class="btn btn-success" id="save_details">Save</button>
                                <a class="btn" href="javascript:window.history.go(-1);">Cancel</a>
                            </div>  
                        </div>
                    </form>
                    
                </div>
                </div>


            </div>
        </div>
    </div>
  
</body>
</html>