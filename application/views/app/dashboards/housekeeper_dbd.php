

                <div class="row-fluid">

                    <div class="span6">
                        <div class="head clearfix">
                            <div class="isw-archive"></div>
                            <h1>New Bookings</h1>
                            <ul class="buttons">
                                <li><a href="<?= site_url('/bookings/download_to_excel') ?>" class="isw-download"></a></li>

                                <li>
                                    <a href="#" class="isw-settings"></a>
                                    <ul class="dd-list">
                                        <li><a href="<?= site_url('/bookings/add_new_booking') ?>"><span class="isw-plus"></span> New Booking</a></li>

                                    </ul>
                                </li>
                            </ul>  
                        </div>
                        <div class="block-fluid ">


                           <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                <thead>
                                    <tr>
                                        
                                        <th width="15%">Customer</th>
                                        <th width="10%">Guests</th>
                                        <th width="10%">Arrival Date</th>
                                        <th width="10%">Departure Date</th>
                                        <th width="25%">Property</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php foreach($bookings as $rec): ?>
                                        <tr>
                                            

                                            <td><?= $rec['customer_name'] ?></td>
                                            <td>
                                                Adults:<?= $rec['adults'] ?>.
                                                Children:<?= $rec['children'] ?>.
                                                Infants:<?= $rec['infants'] ?>.
                                            </td>
                                            <td><?= $rec['arrival_date'] ?></td> 
                                            <td><?= $rec['departure_date'] ?></td>

                                            <td><?= $rec['property_name'] ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                </table>

                        </div>
                    </div>

                    <div class="span6">
                        <div class="head clearfix">
                            <div class="isw-calendar"></div>
                            <h1>Booking Calendar</h1>
                        </div>
                        <div class="block-fluid">
                            <div id="booking_calendar" class="fc"></div>
                        </div>
                    </div>

                </div>

                <div class="row-fluid">

                    <div class="span6">
                       <div class="head clearfix">
                            <div class="isw-calendar"></div>
                            <h1>Staff & Web Bookings</h1>
                        </div>
                        <div class="block-fluid">
                              <div id="chart-2" style="height: 300px; margin-top: 10px;">

                            </div>
                        </div>
                    </div>

                    <div class="span6">
                       <div class="head clearfix">
                            <div class="isw-calendar"></div>
                            <h1>Bookings Per Property</h1>
                        </div>
                        <div class="block-fluid">
                              <div id="chart-3" style="height: 300px;">

                              </div>
                        </div>
                    </div>

                </div>



<script>
dashboard.init_housekeeper();
</script>
