<html style="background-color:#3E3E3E;">
<head >
<!-- Site Title -->
     <title>Booking Brain</title>
     <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
     <style>
	 	body{
			width:670px;
			height:830px;
			margin:auto;
			background-color:white;
			border:1px solid #333;
			margin-top:50px;	
		}
		
		#container{
			width:600px;
			height:800px;
			margin:auto;
			//border:1px solid #333;
			background-color:white ;
			
		}
		
		#header{
			width:600px;
			height:150px;
			//border:1px solid #333;
			background-color:white ;
		}
		
		#header img{
			margin-top:25px;
			margin-left:0px;	
		}
		
		#content{
			width:600px;
			height:662px;
			//border:1px solid #333;
			background-color:white ;
			
		}
	 
	 
	 </style>
     
</head>

<body>
	<div id="container">
        <div id="header">
        	<!-- Header title -->
           
             <table cellspacing="0" cellpadding="0" border="0" width="600" align="center" >
			<tr>
				<td width="364" rowspan="5" align="left"><img align="middle" src="<?= INCLUDES ?>/app/img/b_logo.png" />		</td>
				<td width="261" align="right" ></td>
              
			</tr>
            
            <tr>
             <td style="color:#09F; text-align:right;"></td> 
            </tr>
			

		</table>


        </div><hr></hr>
        
        <div id="content">
        	<table cellspacing="0" cellpadding="2" border="0" width="600" align="center"  >
			
            <tr>
			
				<td width="92" align="left">Name:</td>
				<td width="150" align="right"></td>
				<td width="85">&nbsp;</td>
				<td width="92" align="left">Booking Date:</td>
				<td width="150" align="right"></td>
				<td width="0" align="right"></td>
			</tr>


			<tr>
				<td width="92" align="left">Address:</td>
				<td width="150" align="right"></td>
				<td width="85">&nbsp;</td>
				<td align="left" width="114" >Total(GBP):</td>
				<td width="150" align="right"></td>
				<td width="0" align="right"></td>
			</tr>           
           
		</table>
         <table>
         	<tr>
            	<td  width="92"></td>
                <td width="150"></td>
         		<td width="85">&nbsp;</td>
				<td valign="top" width="114" >Apply Online at:</td>
				<td width="150" valign="top"><a href="http://www.thebestofexmoor.co.uk/online-booking" >www.BookingBrain.com</a></td>
				<td width="0" align="right"></td>
            </tr>
         </table><br/>
        
        
        <table border="0" width="600" height="130" align="center" cellpadding="2" cellspacing="0" >
			<tr>
          		<td colspan="8" bgcolor="#09F" align="center" style="color:#FFF; height:25px;" >Booking Details</td>
 			</tr>
 			<tr>
      			<td colspan="8"></td>
 			</tr>
 
			<tr>
			
				<td width="92" align="left">Booking ID:</td>
				<td width="150" align="right" bgcolor="#DBF1FD">MSVG-201013</td>
				<td width="85">&nbsp;</td>
				<td width="92" align="left">Arival Date:</td>
				<td width="150" align="right" bgcolor="#DBF1FD"></td>
				<td width="0" align="right"></td>
			</tr>


			<tr>
				<!--<td align="left" width="17" ></td>-->
				<td width="92" align="left">No of Nights:</td>
				<td width="150" align="right" bgcolor="#DBF1FD"></td>
				<td width="85">&nbsp;</td>
				<td align="left" width="114" >Departure Date:</td>
				<td width="150" align="right" bgcolor="#DBF1FD"></td>
				<td width="0" align="right"></td>
			</tr>
		</table><br/>
        
        
        <table border="0" width="600" align="center" cellpadding="2" cellspacing="0" >
			<tr>
          		<td colspan="8" bgcolor="#09F" align="center" style="color:#FFF" >Payment Details</td>
 			</tr>
 			<tr>
      			<td colspan="8"></td>
 			</tr>
            
           <tr>
          
           
                <td width="286">In Words:</td>
                  <td class="spec" width= "1" background-color="#b4b4b4">&nbsp;
                     
                 </td>
                 <td width="238">
                 <div style="width: 50%; float: left; text-align: left;">Total:</div>
                 <div style="width: 50%; float: left; text-align: right;"></div><br/>
                 
                 <div style="width: 50%; float: left; text-align: left;">Amount Paid :</div>
                 <div style="width: 50%; float: left; text-align: right;">&dollar;00.00</div><br/><hr/>
                 
                 <div style="width: 50%; float: left; text-align: left;"> Balance Due(100%):</div>
                 <div style="width: 50%; float: left; text-align: right;"></div>
                 
               
                 </td>
              </tr>  

			
         </table><hr/>
         
         <p style="margin-top:80px;">
         	I agree to be held personally liable for payment of the total amount of this bill.
         
         </p>
         
         <p style="margin-top:65px;">
         	<p style="margin-bottom:0px; margin-top:-20px;">----------------------------------</p>
         	<div style="width:300px;">[ ]</div>
            <p style="margin-bottom:0px; margin-top:-40px; margin-left:400px;">---------------------------------</p>
            <div style="width:200px; margin-left:400px; margin-top:0px;">Received By</div>
         </p>
       </div>
    </div>




</body>
</html>     
     