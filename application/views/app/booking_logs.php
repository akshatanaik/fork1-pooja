<!-- bookings view -->
<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
     <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png"> 
    <title>Booking Brain-Bookings</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
            
            	<!-- display the list of bookings -->
            	 <div class="page-header">
                    <h1>Booking Logs</h1>                   
                </div>                  
                
                <div class="row-fluid">

                    <div class="span12">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>Logs</h1>    
                            <a class="btn" href="javascript:window.history.go(-1);" style="margin-left:810px;">Cancel</a>                 
                        </div>
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                <thead>
                                    <tr>                                        
                                        <th width="3%">Date</th>
                                        <th width="15%">Source</th>
                                        <th width="10%">Class</th>
                                        <th width="10%">Event</th>
                                        <th width="10%">Message</th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                                                                                              
                                        </tr>
                                    
                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>   
    </div>   
   

     <div style="display:none;" id="dialog-confirm" title="Delete?">
      <p>
        Booking is going to be deleted. Are you sure?    
     </p>
    </div> 

    <script> 
        $("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});
    </script>




</body>
</html>