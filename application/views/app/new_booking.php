<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    

    
	<link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">

    <title>Booking Brain</title>

    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>



    <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.timepicker.min.js"></script>



    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/booking.js'></script> 

    <style type="text/css">

        .form-error{ color:red; font-size: 10px;}

    </style>

</head>

<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>   

        

        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



           

            <!-- Main content -->

            <div class="workplace">

                <div class="page-header">

                    <h1>New/Edit Booking</h1>

                </div> 

                <div class="row-fluid">

                <div class="span12"> 

                    <div class="head clearfix">

                        <div class="isw-favorite"></div>

                        <h1>New/Edit Booking</h1>

                    </div>

                     <?php if($status=='error'){ ?>

                        <div id="error"><?= $response ?></div>

                     <?php }else{ ?>

                     <?= $this->load->view('app/booking_form_partial') ?>

                     <?php } ?>

                </div>

                </div>





            </div>

        </div>

    </div>

    <script>

       

    </script>

</body>

</html>