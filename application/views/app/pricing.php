<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    <link rel='stylesheet' type='text/css' href='<?= INCLUDES ?>/app/css/jquery.dop.BackendBookingCalendarPRO.css'  />
    <link rel='stylesheet' type='text/css' href='<?= INCLUDES ?>/app/css/fullcalendar.print.css' media='print' />
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />

     <script>
        $( document ).ready(function() {
            getMonth();
        });
        function getMonth(){
            for(i = 0; i <= 11; i++){
                for(j = 0; j < i; j++){
                    $('#booking_calendar' + i).fullCalendar('next');
                }
            }
        }
    </script>
     
     <style>
        [class*="span"] {
            float: left;
            margin-left: 0;
            min-height: 1px;
        }
        td.calendar{
            vertical-align: top;
        }
        .fc-other-month .fc-day-number {display:none;}
     </style>
    
</head>
<?php  $property = isset($_GET['property_id']) ? $_GET['property_id']:'';
       
?>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper">
            <!-- include header -->
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>
            <!-- display property filter as well -->
            <!--<div class="row-fluid"> 
                    <div class="row-form clearfix">
                        <div class="span2">Property</div>
                        <div class="span6">

                            <select >
                                    <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>
                                        <option value="">--Choose a property to set pricing --</option>
                                        <?php foreach($response['records'] as $res => $rec): ?>
                                            <option <?= isset($property) && $property==$rec['property_id'] ? 'selected="selected"':'' ?>  value="<?= $rec['property_id']?>"> <?= $rec['property_name']?></option>  
                                        <?php endforeach; ?>
                                    <?php } ?>
                                </select>
                        </div>
                    </div>
            </div>
            <div class="dr"><span></span></div>-->

            <div class="row-fluid"> 
                    <div class="row-form clearfix">
                        <div class="span1">Choose</div>
                        <div class="span6">
                            <select onchange="pricingCalendar.display_pricing_or_shortbreaks(this.value,<?php echo($property);  ?>)" >
                                    <option <?php if ($_GET['type'] == 'undefined') echo 'selected="selected"' ?> value="undefined">Choose</option>
                                    <option <?php if ($_GET['type'] == 'pricing') echo 'selected="selected"' ?> value="pricing">Pricing</option>
                                    <option <?php if ($_GET['type'] == 'shortbreaks') echo 'selected="selected"' ?> value="shortbreaks">Shortbreaks</option>          
                            </select>
                        </div>
                    </div>
            </div>
            <div class="dr"><span></span></div>
            
            <div class="workplace">
                
                  
                    
                
                        
                    <table>
                        <tbody>
                            <tr>
                                <td class="calendar">
                                    <div class="span4">
                                        <div class="head clearfix">
                                            <div class="isw-calendar"></div>
                                            <h1>Calendar</h1>
                                            <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                        </div>
                                       
                                            <div class="block-fluid">
                                                <div id="booking_calendar0" class="fc"></div>
                                            </div>
                                       
                                    </div>
                                </td>
                                <td class="calendar">
                                    <div class="span4" style="padding-top:0px;">
                                        <div class="head clearfix">
                                            <div class="isw-calendar"></div>
                                            <h1>Calendar</h1>
                                            <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                        </div>
                                       <div class="block-fluid">
                                       
                                                <div id="booking_calendar1" class="fc"></div>
                                            </div>
                                        
                                    </div>
                                </td>
                                <td class="calendar"> 
                                    <div class="span4">
                                        <div class="head clearfix">
                                            <div class="isw-calendar"></div>
                                            <h1>Calendar</h1>
                                            <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                        </div>
                                       
                                            <div class="block-fluid">
                                                <div id="booking_calendar2" class="fc"></div>
                                            </div>
                                     
                                    </div>
                                </td>
                            </tr>

                            <tr>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                  
                                        <div class="block-fluid">
                                            <div id="booking_calendar3" class="fc"></div>
                                        </div>
                                  
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                    
                                        <div class="block-fluid">
                                            <div id="booking_calendar4" class="fc"></div>
                                        </div>
                                    
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                            <li>
                                                <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                            </li>
                                        </ul>  
                                    </div>
                                  
                                        <div class="block-fluid">
                                            <div id="booking_calendar5" class="fc"></div>
                                        </div>
                                 
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                   
                                        <div class="block-fluid">
                                            <div id="booking_calendar6" class="fc"></div>
                                        </div>
                                   
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                   
                                        <div class="block-fluid">
                                            <div id="booking_calendar7" class="fc"></div>
                                        </div>
                                  
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                 
                                        <div class="block-fluid">
                                            <div id="booking_calendar8" class="fc"></div>
                                        </div>
                                 
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                  
                                        <div class="block-fluid">
                                            <div id="booking_calendar9" class="fc"></div>
                                        </div>
                                  
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                  
                                        <div class="block-fluid">
                                            <div id="booking_calendar10" class="fc"></div>
                                        </div>
                                  
                                </div>
                            </td>
                            <td class="calendar">
                                <div class="span4">
                                    <div class="head clearfix">
                                        <div class="isw-calendar"></div>
                                        <h1>Calendar</h1>
                                        <ul class="buttons">                                                                                   
                                                <li>
                                                    <a href="<?= site_url('/pricing/add_pricing') ?>?property_id=<?= $property ?>" class="isw-settings"></a>
                                                </li>
                                            </ul>  
                                    </div>
                                 
                                        <div class="block-fluid">
                                            <div id="booking_calendar11" class="fc"></div>
                                        </div>
                                  
                                </div>
                            </td>
                        </tr> 
                        </tbody>
                    </table>
                </div>
           


        </div>   
    </div>

    <!-- KEEP THE Pricing FORM HIDDEN-->
   <div id="fModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Pricing</h3>
        </div>
        <div class="row-fluid" style="height:230px; ">
            <div class="block-fluid">
             <?= $this->load->view('app/pricing_modal') ?>
            </div>
        </div>
        <div class="modal-footer">
            <button onclick="pricingCalendar.save();return false;" id="save" class="btn btn-success" >Save</button>
            <!--<button onclick="pricingCalendar.update();return false;" id="edit" class="btn btn-success" >Update</button>-->
            <button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>

        </div>
    </div>

     <!-- KEEP THE ShortBreaks FORM HIDDEN-->
    <div id="fModal_sb" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Short Breaks</h3>
        </div>
        <div class="row-fluid" style="height:100px; ">
            <div class="block-fluid">
             <?= $this->load->view('app/shortbreaks_modal') ?>
            </div>
        </div>
        <div class="modal-footer">
            <button onclick="pricingCalendar.saveShortBreaks();return false;" id="save" class="btn btn-success" >Save</button>
            <!--<button onclick="pricingCalendar.update();return false;" id="edit" class="btn btn-success" >Update</button>-->
            <button class="btn btn-error" data-dismiss="modal" aria-hidden="true">Cancel</button>

        </div>
    </div>

   <!-- <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.dop.BackendBookingCalendarPRO.js" ></script>

   <script>
        $('#pricing_calendar').DOPBackendBookingCalendarPRO({'ID': '1111','DataURL': "<?= site_url('/pricing/get_pricing')?>?property_id=<?= $property ?>",
                                                    'SaveURL': "<?= site_url('/pricing/save_pricing')?>?property_id=<?= $property ?>"
                                                  });
   </script> -->

   <!-- for calendar load the necessary js -->
    <script type="text/javascript" src="<?= INCLUDES ?>app/js/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <!-- <script type="text/javascript" src="<?= INCLUDES ?>app/js/bookingCalendar.js" ></script> -->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type="text/javascript" src="<?= INCLUDES ?>app/js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="<?= INCLUDES ?>app/js/pricing.js" ></script>
  
    <script>
         pricingCalendar.init('year');
    </script>

</body>
</html>
