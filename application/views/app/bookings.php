<!-- bookings view -->
<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    <title>Booking Brain-Bookings</title>
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>

    
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
            
            	<!-- display the list of bookings -->
            	 <div class="page-header">
                    <h1>Bookings</h1>
                </div>                  
                
                <div class="row-fluid">

                    <div class="span12">                    
                        <div class="head clearfix">
                            <div class="isw-grid"></div>
                            <h1>All Bookings</h1>      
                            <ul class="buttons">
                                <li><a href="<?= site_url('/bookings/download_to_excel') ?>" class="isw-download"></a></li>                                                        
                                
                                <li>
                                    <a href="#" class="isw-settings"></a>
                                    <ul class="dd-list">
                                        <li><a href="<?= site_url('/bookings/add_new_booking') ?>"><span class="isw-plus"></span> New Booking</a></li>
                                        
                                    </ul>
                                </li>
                            </ul>                        
                        </div>
                        <div class="block-fluid table-sorting clearfix">
                            <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th width="3%">ID</th>
                                        <th width="15%">Customer</th>
                                        <th width="10%">Guests</th>
                                        <th width="10%">Arrival Date</th>
                                        <th width="10%">Departure Date</th>
                                        <th width="10%">Arrival Time</th>
                                        <th width="10%">Departure Time</th>
                                        <th width="25%">Property</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php foreach($response['records'] as $rec): ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group" style="font-size: 10px;">
                                                    <button <?php if ($this->sessions->getsessiondata('role') == 'housekeeper') echo 'disabled="disabled"' ?> style="font-size: 10px;" class="btn dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                                                    <ul class="dropdown-menu" style="font-size: 10px;">
                                                        <li><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>">Invoice</a></li>
                                                        <li><a href="<?= site_url('/bookings/edit_booking') ?>?id=<?= $rec['id'] ?>">Edit</a></li>
                                                        <li><a href="javascript:void(0)" onclick="booking.delete('<?= $rec['id'] ?>')" id="delete">Delete</a></li>                                              
                                                    </ul>
                                                </div>
                                            </td>
                                            <td><?= $rec['id'] ?></td>
                                            <td><?= $rec['customer_name'] ?></td>
                                            <td>
                                                Adults:<?= $rec['adults'] ?>.
                                                Children:<?= $rec['children'] ?>.
                                                Infants:<?= $rec['infants'] ?>.
                                            </td>
                                            <td><?= $rec['arrival_date'] ?></td> 
                                            <td><?= $rec['departure_date'] ?></td>
                                            <td><?= $rec['arival_time'] ?></td>
                                            <td><?= $rec['departure_time'] ?></td> 
                                            <td><?= $rec['property_name'] ?></td>                                     
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>   
    </div>

    
    <script src="<?= INCLUDES ?>app/js/booking.js"></script>

     <div style="display:none;" id="dialog-confirm" title="Delete?">
      <p>
        Booking is going to be deleted. Are you sure?    
     </p>
    </div> 

    <script> 
        $("#tSortable").dataTable({"iDisplayLength": 5, "aLengthMenu": [5,10,25,50,100], "sPaginationType": "full_numbers", "aoColumns": [ { "bSortable": false }, null, null, null, null]});
    </script>




</body>
</html>