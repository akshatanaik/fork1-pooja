<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

      
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>   

   

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/helper.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 



    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>



    <script>

        function initialize() 

          {

            var mapOptions = {

              center: new google.maps.LatLng(-33.8688, 151.2195),

              zoom: 13

            };

            

            var map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);



            var input = /** @type {HTMLInputElement} */(

              document.getElementById('pac-input'));



              var types = document.getElementById('type-selector');

              map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

              map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);



              var autocomplete = new google.maps.places.Autocomplete(input);

              autocomplete.bindTo('bounds', map);



              var infowindow = new google.maps.InfoWindow();

              var marker = new google.maps.Marker({

                map: map

              });



              google.maps.event.addListener(autocomplete, 'place_changed', function() {

                infowindow.close();

                marker.setVisible(false);

                var place = autocomplete.getPlace();

                

                if (!place.geometry) {

                  return;

                }



                // If the place has a geometry, then present it on a map.

                if (place.geometry.viewport) {

                  map.fitBounds(place.geometry.viewport);

                } else {

                  map.setCenter(place.geometry.location);

                  map.setZoom(17);  // Why 17? Because it looks good.

                }

                

                marker.setIcon(/** @type {google.maps.Icon} */({

                url: place.icon,

                size: new google.maps.Size(71, 71),

                origin: new google.maps.Point(0, 0),

                anchor: new google.maps.Point(17, 34),

                scaledSize: new google.maps.Size(35, 35)

              })

            );

            marker.setPosition(place.geometry.location);

            marker.setVisible(true);



            var address = '';

            

            if (place.address_components) {

              address = [

                (place.address_components[0] && place.address_components[0].short_name || ''),

                (place.address_components[1] && place.address_components[1].short_name || ''),

                (place.address_components[2] && place.address_components[2].short_name || '')

              ].join(' ');

            }



            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);

            infowindow.open(map, marker);

          });



          // Sets a listener on a radio button to change the filter type on Places

          // Autocomplete.

          function setupClickListener(id, types) {

            var radioButton = document.getElementById(id);

            google.maps.event.addDomListener(radioButton, 'click', function() {

              autocomplete.setTypes(types);

            });

          }



          setupClickListener('changetype-all', []);

          setupClickListener('changetype-establishment', ['establishment']);

          setupClickListener('changetype-geocode', ['geocode']);

        }



        google.maps.event.addDomListener(window, 'load', initialize);



    </script>



    <style>

        html, body, #map-canvas {

            height: 300px;

            margin: 0px;

            padding: 0px

          }

          .controls {

            margin-top: 16px;

            border: 1px solid transparent;

            border-radius: 2px 0 0 2px;

            box-sizing: border-box;

            -moz-box-sizing: border-box;

            height: 32px;

            outline: none;

            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);

          }



          #pac-input {

            background-color: #fff;

            padding: 0 11px 0 13px;

            width: 400px;

            height: 32px;

            font-family: Roboto;

            font-size: 15px;

            font-weight: 300;

            text-overflow: ellipsis;

          }



          #pac-input:focus {

            border-color: #4d90fe;

            margin-left: -1px;

            padding-left: 14px;  /* Regular padding-left + 1. */

            width: 401px;

          }



          .pac-container {

            font-family: Roboto;

          }



          #type-selector {

            color: #fff;

            background-color: #4d90fe;

            padding: 5px 11px 0px 11px;

          }



          #type-selector label {

            font-family: Roboto;

            font-size: 13px;

            font-weight: 300;

          }



          input,label{

            display:inline-block;

          }

    </style>

    

</head>

<body>

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>  

        

        <div class="menu">                

              <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- main content -->

            <div class="workplace">

                 <div class="page-header">

                    <h1>Location Details Add Form</h1>

                </div>



                 <?php $id = $this->uri->segment(3); ?>

                <div class="block-fluid tabs">

                    <ul>

                        <li><a href="#property_location">Property location</a></li>

                        <li><a href="#localarea">Local area</a></li>

                        <li><a href="#how_to_get_there">How to get there</a></li>

                        <li><a href="#whats_nearby">What's_nearby</a></li>

                        <li><a href="#holiday_types">Holiday types</a></li>

                    </ul>

                

                    <div class="alert alert-error" id="errormsg" style="visibility:hidden;"></div> 

                    <div class="alert alert-success" id="successmsg" style="visibility:hidden;"></div>    





                    <form class="form-horizontal" action="<?= site_url('/property/savePropertyLocationforId/'.$id)?>" method="POST" onsubmit="return locationdetails(this);">

                  

                        <div id="property_location">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>Property Location</h1>

                                    </div>

                                    <div class="block-fluid">                        

                                        <div class="row-form clearfix">

                                            <div class="span3">Continent<em style="color:#F00;">*</em></div>

                                            <div class="span6">        

                                                <select name="inputContinent" id="inputContinent" >

                                                    <option value="">Choose</option>

                                                    <option value="europe">Europe</option>

                                                    <option value="asia">Asia</option>

                                                </select>

                                            </div>                            

                                        </div>                        



                                        <div class="row-form clearfix">

                                            <div class="span3">Country<em style="color:#F00;">*</em></div>

                                            <div class="span6">        

                                                <select name="inputCountry" id="inputCountry" >

                                                    <option value="">Choose</option>

                                                    <option value="england">England</option>

                                                    <option value="usa">USA</option>

                                                    <option value="uk">UK</option>

                                                </select>

                                            </div>                            

                                        </div>                                                                                    



                                        <div class="row-form clearfix">

                                            <div class="span3">Region</div>

                                            <div class="span6">        

                                                <select name="inputRegion" id="inputRegion" >

                                                    <option value="">Choose </option>

                                                    <option value="west_country">West country</option>

                                                </select>

                                            </div>                            

                                        </div>                                                                                 



                                        <div class="row-form clearfix">

                                            <div class="span3">Subregion</div>

                                            <div class="span6">        

                                                <select name="inputSubregion" id="inputSubregion" >

                                                    <option value="">Choose</option>

                                                    <option value="somerset">Somerset</option>

                                                </select>

                                            </div>                            

                                        </div>      



                                        <div class="row-form clearfix">

                                            <div class="span3">Town</div>

                                            <div class="span6">        

                                                <select name="inputTown" id="inputTown" >

                                                    <option value="">Choose</option>

                                                    <option value="minehead">Minehead</option>

                                                </select>

                                            </div>                            

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Suburb(optional)</div>

                                            <div class="span6">        

                                                <select name="inputSuburb" id="inputSuburb" > 

                                                    <option value="">Choose</option>

                                                    <option value="Studio">Studio</option>

                                                </select>

                                            </div>                            

                                        </div> 



                                        <div class="row-form clearfix">

                                            <div class="span3">Property address<em style="color:#F00;">*</em></div>

                                            <div class="span6"><input value=""  type="text" name="inputProperty_address" id="inputProperty_address"/></div>

                                        </div> 



                                        <div class="row-form clearfix">

                                            <div class="span3">Postcode<em style="color:#F00;">*</em></div>

                                            <div class="span6"><input value=""  type="text" name="inputPostcode" id="inputPostcode"/></div>

                                        </div> 



                                        <div class="row-fluid">

                                            <div class="span12">

                                                <div class="head clearfix">

                                                    <div class="isw-graph"></div>

                                                    <h1>Property Map</h1>

                                                </div>

                                                <div class="block">

                                                    <input id="pac-input" class="controls" type="text" placeholder="Enter a location">

                                                    <div id="type-selector" class="controls">

                                                        <input type="radio" name="type" id="changetype-all" checked="checked">

                                                        <label for="changetype-all">All</label>



                                                        <input type="radio" name="type" id="changetype-establishment">

                                                        <label for="changetype-establishment">Establishments</label>



                                                        <input type="radio" name="type" id="changetype-geocode">

                                                        <label for="changetype-geocode">Geocodes</label>

                                                    </div>

                                                    <div id="map-canvas"></div>

                                                </div> 

                                            </div> 

                                        </div>                    

                                    </div>

                                </div>

                            </div>

                        </div>

              

                        <div id="localarea">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>The local area</h1>

                                    </div>

                                    <div class="block-fluid">

                                        <div class="row-form clearfix">

                                            <div class="span3">The region</div>

                                            <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"></textarea> </div>

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">The area</div>

                                            <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"></textarea> </div>

                                        </div>

                                    </div>  

                                </div>

                            </div>

                        </div>

              

                        <div id="how_to_get_there">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-users"></div>

                                        <h1>How to get there</h1>

                                    </div>

                            

                                    <div class="block-fluid">                               

                                        <div class="row-form clearfix">

                                            <div class="span3">Airport</div>

                                            <div class="span6"><input  value="" type="text" name="inputAirport" id="inputAirport"/></div>  km

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Ferry port</div>

                                            <div class="span6"><input  value="" type="text" name="inputFerryport" id="inputFerryport"/></div>  km

                                        </div>

                                        <div class="row-form clearfix">

                                            <div class="span3">Train station </div>

                                            <div class="span6"><input  value="" type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km

                                        </div>

                                         <div class="row-form clearfix">

                                            <div class="span3">Car required</div>

                                            <div class="span6">        

                                                <select name="inputCar" id="inputCar" >

                                                     <option value="">Choose </option>

                                                    <option value="car_not_necessary">Car not necessary</option>

                                                    <option value="car_necessary">Car necessary</option>

                                                </select>

                                            </div>                            

                                        </div>

                                        <div class="row-form clearfix">



                                            <div class="span3">More tips of how to get there</div>

                                            <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"></textarea> </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                

                        <div id="whats_nearby">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>What's nearby</h1>

                                    </div>

                                    <div class="block-fluid">

                                        <div class="row-form clearfix">

                                            <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk

                                                </label>

                                            </div>



                                            <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive

                                                 </label>

                                            </div>



                                            <div class="span3">

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 

                                                </label>

                                            </div>



                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort

                                                 </label>                                       

                                            </div>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">                                        

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 

                                                </label>                                        

                                            </div>



                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby

                                                 </label>                                        

                                            </div>



                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 

                                                </label>                                       

                                            </div>



                                            <div class="span3">                                        

                                                <label class="checkbox inline">

                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property

                                                 </label>                                       

                                            </div>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">                                       

                                                 <label class="checkbox inline">

                                                    <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby

                                                </label>                                      

                                            </div>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest Beach</div>

                                            <div class="span6"><input  value="" type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">Nearest shops</div>

                                            <div class="span6"><input  value="" type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>



                        <div id="holiday_types">

                            <div class="row-fluid">

                                <div class="span12"> 

                                    <div class="head clearfix">

                                        <div class="isw-favorite"></div>

                                        <h1>Holiday types</h1>

                                    </div>

                                    <div class="block-fluid">

                                        <div class="row-form clearfix">

                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays

                                                </label>                                       

                                            </div>



                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays

                                                 </label>                                       

                                            </div>



                                            <div class="span3">

                                                <label class="checkbox inline">                                           

                                                    <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats

                                                </label>                                        

                                            </div>



                                            <div class="span3">                                        

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation

                                                 </label>                                         

                                            </div>

                                        </div>



                                        <div class="row-form clearfix">

                                            <div class="span3">                                        

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks

                                                </label>                                        

                                            </div>



                                            <div class="span3">                                        

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun

                                                 </label>                                        

                                            </div>



                                            <div class="span3">                                       

                                                <label class="checkbox inline">

                                                    <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife

                                                </label>                                       

                                            </div>

                                        </div>

                                    </div>   

                                </div>

                            </div>

                            <div class="footer tar">

                                <button class="btn" id="save_details">Save</button>

                            </div>

                        </div>

                    </form>

                     <div id="cancel">

                        <a href="<?= site_url('/property/propertyList/'); ?>">&laquo; Cancel and go back</a>

                    </div>

                </div>

            </div>   

        </div>

    </body>

</html>

