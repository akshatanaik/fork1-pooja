<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
	 
    <link rel='stylesheet' type='text/css' href='<?= INCLUDES ?>/app/css/fullcalendar.print.css' media='print' />
    <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/jquery.timepicker.css" />


    <script type="text/javascript" src="<?= INCLUDES ?>app/js/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script type="text/javascript" src="<?= INCLUDES ?>app/js/bookingCalendar.js" ></script>
    <script type="text/javascript" src="<?= INCLUDES ?>app/js/dashboard.js" ></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/charts/excanvas.min.js'></script>    
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/charts/jquery.flot.js'></script>    
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/charts/jquery.flot.stack.js'></script>    
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/charts/jquery.flot.pie.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/charts/jquery.flot.resize.js'></script>

     <script type="text/javascript">
     function func() {
                     if(!document.getElementById("find").value){
                                            alert("Enter search data");
                                         
                                            
                       }
                        if(document.getElementById("find").value){

                            
                            $('#search_results').show();
                       }
                    }
                        </script>
<style type="text/css">

    input::-moz-placeholder {
   font-size: 60%;
}
    </style>
    
</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!-- here let us build add the graphs & data  -->
            <div class="workplace">
                <div class="row-fluid">
                    <div class="span12">
                         <?php $php_self = $_SERVER['PHP_SELF']; 
                            $pathFragments = explode('/', $php_self);
                            $end = end($pathFragments); 
                            
                            ?>
                         <form style="margin-left:750px; margin-top:-15px;" name="search" action="<?= site_url('/dashboard/search/')?>" method="POST"> 
                            <input type="text" id="find" name="find" placeholder="First name/Last Name/Customer Name/Booking Id" /> 
                            <input type="hidden" name="searching" value="yes" /> 
                            <input type="submit" style="margin-top:-6px;" href="javascript:void(0)" onclick="func();" class="btn" name="search" value="Search" /> 
                            <!--<input type="submit" style="margin-top:-6px;" href="javascript:void(0)" onclick="func();" class="btn" name="search" value="Search" /> -->
                        </form>

                        <!-- search change poonam -->

                        <div id="search_results">
                            <div class="head clearfix">
                                <div class="isw-archive"></div>
                                <h1>Search results</h1>
                            </div>
                            <div class="block-fluid ">
                               <table cellpadding="0" cellspacing="0" width="100%" class="table" id="tSortable">
                                      

                                <?php 
                                    //$this->sessions->setsessiondata();
                                 
                                      
                                    
                                    if(count($response['search_data']) != 0 )

                                    {
                                     if($response['search_data'][0] == 'users' || 'booking' || 'property'){  ?>
                                 
                                    <thead>
                                        <tr>
                                            <th width="10%">Booking Id</th>
                                            <th width="15%">First Name</th>
                                            <th width="15%">Last Name</th>
                                            <th width="15%">Arrival Date</th>
                                            <th width="15%">Departure Date</th>
                                            <th width="15%">Customer Name</th>
                                            <th width="15%">Property Name</th>
                                           
                                                                        
                                        </tr>
                                    </thead>
                                    <?php } 
                                }else
                                {

                                   print_r("No records found");

                                }
                                ?>
                                    <tbody>
                                            <?php 
                                                //foreach($response as $rec):
                                                
                                                for($i = 0; $i< sizeof($response['search_data']); $i++) {
                                                    $rec = $response['search_data'][$i];

                                                $uk_arrivaldate = date("d/m/Y",strtotime($rec['arrival_date']));
                                                $uk_departuredate = date("d/m/Y",strtotime($rec['departure_date']));
                                                //var_dump($uk_date1);
                                             ?>
                                            <tr>
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $rec['id'] ?></a></td>
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $rec['firstname'] ?></a></td>
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $rec['lastname'] ?> </a></td>
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $uk_arrivaldate ?></a></td> 
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $uk_departuredate ?></a></td>  
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $rec['customer_name'] ?></a></td>
                                                <td><a href="<?= site_url('/bookings/invoice') ?>?id=<?= $rec['id'] ?>"><?= $rec['property_name'] ?></a></td>

                                                
                                                                                     
                                            </tr>
                                        <?php 
                                        } ?>
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                        <!-- end seach poonam -->

                        <div class="widgetButtons">                        
                            
                            <div class="bb">
                                <a href="#" class="tipb" title="Edit item"><span class="ibw-edit"></span></a>
                            </div>
                            <div class="bb gray">
                                <a href="#" class="tipb" title="Upload Pictures"><span class="ibb-folder"></span></a>

                            </div>
                           <div class="bb yellow">
                               <a href="<?= site_url('/property/addNewProperty'); ?>" class="tipb" title="Add new property"><span class="ibw-plus"></span></a>
                           </div>
                           <div class="bb red">
                               <a href="<?= site_url('/bookings/add_new_booking'); ?>" class="tipb" title="Add new booking"><span class="ibw-plus"></span></a>
                           </div>

                            <?php if($this->sessions->getsessiondata('role')=='admin') { ?>
                                <div class="bb blue">
                                    <a href="#" class="tipb" title="Settings"><span class="ibw-settings"></span></a>
                                </div>
                            <?php } ?>

                        </div>

                    </div>
                </div>

           
            <?php  if($this->sessions->getsessiondata('role')=='owner'){ ?>
                 <?= $this->load->view('app/dashboards/admin'); ?>
            <?php }elseif($this->sessions->getsessiondata('role')=='admin'){ ?>
                    <?= $this->load->view('app/dashboards/admin'); ?>
            <?php }else{ ?>
                    <?= $this->load->view('app/dashboards/housekeeper_dbd'); ?>
            <?php } 
                 $fLogin =  $this->sessions->getsessiondata('login_if_first'); 
                 if ($this->sessions->getsessiondata('login_if_first') == 'YES') {
                    $this->sessions->setsessiondata('login_if_first','NO'); 
                 }
            ?>

            </div>
            

        </div>   
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Account Created</h4>
        </div>
        <div class="modal-body" style="padding-left: 30px;">
             Welcome to Booking Brain,a way to manage and book the property.
             To quickly start make sure you watch the introduction video.

        </div>
        <div class="modal-footer">
        <button type="button" onclick="showVideo()" class="btn btn-default">Start</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
        </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 

    <div id="welcomeModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 class="modal-title" id="myModalLabel">SetupGuide</h3>
        </div>
        <div class="modal-body" >
            <iframe width="600" height="356"  src="http://www.youtube.com/embed/Nyn5KVISmdI?rel=0" frameborder="0" allowfullscreen></iframe>    
        </div>
      
    </div>

     <style type="text/css">
     #welcomeModal{width:600px; height:395px; left:50%; }
     .modal-footer{background-color:#fff; border-top: medium none;}
     .modal.fade.in{top:50%;}
     .modal-body{padding:0px;}
    
     
     </style>
   
    <script type="text/javascript">
        $(document).ready(function() {
            var end = '<?php echo $end; ?>';
            console.log(end);
            if(end != 'search') $('#search_results').hide();
                //else $('#search_results').show();
            var test = '<?php echo $fLogin; ?>';
            console.log(test);
            if(test == 'YES'){
                $('#myModal').modal({
                show: true,
                })
            }

            var unset = '';
        });

        function showVideo(){
            $('#myModal').hide();
            var showVideo = 'true';
             $('#welcomeModal').modal({
                show: true,
            })
        }
    </script>

<!-- for dahboard load the necessary js -->
<script>
//init the toolyip
$(".tipb").tooltip({placement: 'bottom', trigger: 'hover'});
</script>
</body>
</html>
