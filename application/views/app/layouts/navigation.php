<!-- navigation -->
<div class="breadLine">            
    <div class="arrow"></div>
    <div class="adminControl active">
        Welcome, <?= $this->sessions->getsessiondata('firstname') .' '. $this->sessions->getsessiondata('lastname') ?>
    </div>
</div>

<div class="admin">
    <div class="image">
        <?php 
            if($this->sessions->getsessiondata('image_url') == null)
                 $profile_image = INCLUDES."app/img/users/aqvatarius.jpg";
             else
                $profile_image = $this->sessions->getsessiondata('image_url');
        ?>
        <img src="<?=  $profile_image ?>" class="img-polaroid"/>                
    </div>
    <ul class="control">                      
        <li><span class="icon-cog"></span> <a href="<?= site_url('/website/help'); ?>">Help</a></li>
        
        <?php if($this->sessions->getsessiondata('login_provider')!='facebook' || $this->sessions->getsessiondata('login_provider')!='twitter'){ ?>
        <li><span class="icon-cog"></span> <a href="<?= site_url('/changepassword'); ?>">Change Password</a></li>
        <?php } ?>
        <li><span class="icon-share-alt"></span> <a href="<?= site_url('/logout'); ?>">Logout</a></li>
    </ul>
    <div class="info">
        <span>Welcome back!</span>
    </div>
</div>
    
<ul class="navigation">            
    <li class="<?= isset($navigation['tab']) && $navigation['tab']=='Dashboard'? 'active':'' ?>">
        <a href="<?= site_url('/dashboard'); ?>">
            <span class="isw-grid"></span><span class="text">Dashboard</span>
        </a>
    </li>
    <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Calendar'? 'active':'' ?>">
        <a href="<?= site_url('/calendar?type=month'); ?>">
            <span class="isw-calendar"></span><span class="text">Calendar</span>
        </a>
        <ul>
            <li>
                <a href="<?= site_url('/calendar?type=week'); ?>">
                    <span class="icon-th-large"></span><span class="text">Grid</span>
                </a>
            </li>
            <li>
                <a href="<?= site_url('/calendar?type=month'); ?>">
                    <span class="icon-th-large"></span><span class="text">Month</span>
                </a>
            </li>
            <li>
                <a href="<?= site_url('/calendar?type=year'); ?>">
                    <span class="icon-th-large"></span><span class="text">Year</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Booking Summary'? 'active':'' ?>">
        <a href="<?= site_url('/bookings'); ?>">
            <span class="isw-list"></span><span class="text">Booking</span>
        </a>
        <ul>
            <?php if($this->sessions->getsessiondata('role')!='housekeeper'){  ?>
            <li>
                <a href="<?= site_url('/bookings/add_new_booking'); ?>">
                    <span class="icon-align-justify"></span><span class="text">New Booking</span>
                </a>
            </li>
            <?php } ?>
            <li>
                <a href="<?= site_url('/bookings/bookings'); ?>">
                    <span class="icon-th-large"></span><span class="text">Summary</span>
                </a>
            </li>
        </ul>
    </li>
    <?php if($this->sessions->getsessiondata('role')!='housekeeper'){  ?>
    <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Properties'? 'active':'' ?>">
        <a href="<?= site_url('/property/propertyList'); ?>">
            <span class="isw-documents"></span><span class="text">Properties</span>
        </a>
        <ul>
            <li>
                <a href="<?= site_url('/property/propertyList'); ?>">
                    <span class="icon-align-justify"></span><span class="text">My properties</span>
                </a>
            </li>
            <li>
                <a href="<?= site_url('/property/addNewProperty'); ?>">
                    <span class="icon-align-justify"></span><span class="text">Add new property</span>
                </a>
            </li>

            
        </ul>
    </li>   

    <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Reports'? 'active':'' ?>">
        <a href="<?= site_url('/reports/'); ?>">
            <span class="isw-graph"></span><span class="text">Reports</span>
        </a>
        <ul>            
            <li>
                <a href="<?= site_url('/reports/booking_reports'); ?>">
                    <span class="icon-align-justify"></span><span class="text">Booking Report</span>
                </a>
            </li>
           
            <li>
                <a href="<?= site_url('/reports/pricing_report'); ?>">
                    <span class="icon-align-justify"></span><span class="text">Pricing Reports</span>
                </a>
            </li>

            <li>
                <a href="<?= site_url('/reports/commission_report'); ?>">
                    <span class="icon-align-justify"></span><span class="text">Commission Reports</span>
                </a>
            </li>
        </ul>
    </li>

    
        <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Housekeeper'? 'active':'' ?>">
            <a href="<?= site_url('/users'); ?>">
                <span class="isw-documents"></span><span class="text">Manage Housekeeper</span>
            </a>
            <ul>
                <li>
                    <a href="<?= site_url('/housekeeper'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Housekeeper</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('/housekeeper/add_new_housekeeper'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Add new </span>
                    </a>
                </li>

            </ul>
        </li> 
    <?php } ?>
    <?php if($this->sessions->getsessiondata('role')=='admin') { ?>
        <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Users'? 'active':'' ?>">
            <a href="<?= site_url('/users'); ?>">
                <span class="isw-documents"></span><span class="text">Manage Users</span>
            </a>
            <ul>
                <li>
                    <a href="<?= site_url('/users'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Users</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('/users/new_user'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Add new </span>
                    </a>
                </li>

            </ul>
        </li> 
        <li class="openable <?= isset($navigation['tab']) && $navigation['tab']=='Commissions'? 'active':'' ?>">
            <a href="<?= site_url('/commissions'); ?>">
                <span class="isw-documents"></span><span class="text">Manage Commissions</span>
            </a>
            <ul>
                <li>
                    <a href="<?= site_url('/commissions'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Commissions</span>
                    </a>
                </li>
                <li>
                    <a href="<?= site_url('/commissions/add_commissions'); ?>">
                        <span class="icon-align-justify"></span><span class="text">Add commission</span>
                    </a>
                </li>

            </ul>
        </li>
        

    <?php } ?> 
        <li class="<?= isset($navigation['tab']) && $navigation['tab']=='Account'? 'active':'' ?>">
            <a href="<?= site_url('/account/account_Profile') ?>">
                <span class="isw-graph"></span><span class="text">Account</span>
            </a>
        </li>   

    <?php $page_url=current_url(); ?>
    <?php $id=$_SERVER["QUERY_STRING"]; ?>
    <?php if($page_url ==  base_url().'index.php/bookings/invoice' ){ ?>
        <!-- here we will add a small calendar for showing the booking -->
        <div class="dr"><span></span></div>

        <li>
            <a href="<?= site_url('/commissions') ?>">
                <span class="isw-graph"></span><span class="text">Add Payment</span>
            </a>
        </li>
        <li>           
            <a href="$page_url/#sendmail" data-toggle="modal">
                <span class="isw-graph"></span><span class="text">Email</span>
            </a>
        </li>
        <li>
            <a href="<?= site_url('/bookings/pdfgeneration?'.$id) ?>">
                <span class="isw-graph"></span><span class="text">Print</span>
            </a>
        </li>        
    <?php } ?>

    <div class="dr"><span></span></div>
    <div class="widget-fluid">
        <div id="menuDatepicker"></div>
    </div>
</ul>




