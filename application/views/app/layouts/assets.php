
<link rel="icon" type="image/ico" href="<?= INCLUDES ?>app/favicon.ico"/>
<link href="<?= INCLUDES ?>app/css/stylesheets.css" rel="stylesheet" type="text/css" />  

<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-1.10.2.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-ui-1.10.1.custom.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-migrate-1.2.1.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery.mousewheel.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/cookie/jquery.cookies.2.2.0.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/bootstrap.min.js'></script>

<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/dataTables/jquery.dataTables.min.js'></script>  

<script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 
<script type='text/javascript' src='<?= INCLUDES ?>app/js/cookies.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/actions.js'></script>
<script type='text/javascript' src='<?= INCLUDES ?>app/js/settings.js'></script>
