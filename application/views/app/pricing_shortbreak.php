<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />


    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

    

     <style>

        [class*="span"] {

            float: left;

            margin-left: 0;

            min-height: 1px;

        }



        .row-fluid .span2 {

            width: 14.5299%;

            margin-right:-95px;

        }



     </style>

    

</head>



<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper">

            <!-- include header -->

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">

             <!-- include navigation -->

             <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            

            <div class="workplace">

               

                <?php if($method == 'save_shortbreak'){ ?>

               

                    <form class="form-horizontal" action="<?= site_url('/pricing/'.$method)?>?property_id=<?= $record ?>" method="POST" onsubmit="return pricing(this);">

                <?php }else{ ?>



                    <form class="form-horizontal" action="<?= site_url('/pricing/'.$method)?>?property_id=<?= $record ?>" method="POST" onsubmit="return pricing(this);">

                <?php  } ?>    

                    <div class="span12">

                    <div class="head clearfix">

                        <div class="isw-favorite"></div>

                        <h1>Short Breaks</h1>

                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">

                            <button class="btn btn-success">Save</button>

                            <a class="btn" href="<?= site_url('pricing/pricinglist');?>?property_id=<?= $record ?>">Cancel</a>

                        </div>

                    </div>

                    <div class="block-fluid">

                       <div id="pricing">

                            <div class="row-fluid">

                                <div class="span12"> 

                                                

                                    <div class="row-form clearfix">

                                        <div class="span3">Short break pricing model</div>

                                        <div class="span6">

                                            <select name="short_break_pricing_model" id="short_break_pricing_model">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?> 

                                                    <option value="<?= $rec['short_break_pricing_model'] ?>"><?= $rec['short_break_pricing_model'] ?></option> 

                                                <?php if($rec['short_break_pricing_model'] == "Midweek and weekend breaks only") { ?>

                                                    <option value="No short break">No short break</option> 

                                                <?php }else{ ?>

                                                    <option value="Midweek and weekend breaks only">Midweek and weekend breaks only(percentage shortbreak rates)</option> 

                                                <?php } ?>                                               

                                                <?php endforeach; ?>

                                                <?php }else{ ?> 

                                                    <option value="Midweek and weekend breaks only">Midweek and weekend breaks only(percentage shortbreak rates)</option>

                                                    <option value="No short break">No short break</option> 

                                                <?php } ?>                                             

                                            </select>

                                        </div>

                                    </div>                         



                                    <div class="row-form clearfix">

                                        <div class="span2">Midweek:</div>

                                        <div class="span2">Arrive:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="midweek_arrive" id="midweek_arrive">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?> 

                                                    <option value="<?= $rec['midweek_arrive'] ?>"><?= $rec['midweek_arrive'] ?></option> 

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option>  

                                                <?php endforeach; ?>

                                                <?php }else{ ?>  

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option>  



                                                <?php } ?>                                       

                                            </select>

                                        </div>

                                        <div class="span2">Depart:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="midweek_depart" id="midweek_depart">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <option value="<?= $rec['midweek_depart'] ?>"><?= $rec['midweek_depart'] ?></option>

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option> 

                                                <?php endforeach; ?>

                                                <?php }else{ ?> 

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option> 



                                                <?php } ?>                                         

                                            </select>

                                        </div>

                                        <div class="span2">

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>     

                                                <input type="text" name="midweek_percentage" id="midweek_percentage"  style="width:40%;" value="<?= $rec['midweek_percentage'] ?>"/> % 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <input type="text" name="midweek_percentage" id="midweek_percentage"  style="width:40%;"/> %

                                             <?php } ?>    

                                        </div>

                                        <div class="span2">Min of:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="midweek_min_nights" id="midweek_min_nights">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <option value="<?= $rec['midweek_min_nights'] ?>"><?= $rec['midweek_min_nights'] ?></option>

                                                    <option value="1">1</option>

                                                    <option value="2">2</option>

                                                    <option value="3">3</option>

                                                    <option value="4">4</option>

                                                    <option value="5">5</option>

                                                    <option value="6">6</option>

                                                    <option value="7">7</option> 

                                                <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <option value="1">1</option>

                                                    <option value="2">2</option>

                                                    <option value="3">3</option>

                                                    <option value="4">4</option>

                                                    <option value="5">5</option>

                                                    <option value="6">6</option>

                                                    <option value="7">7</option> 



                                                <?php } ?>                                         

                                            </select> nights

                                        </div> 

                                        <div class="span2"></div>      

                                        <div class="span2">                                               

                                            <label class="checkbox inline">

                                                <input type="checkbox" name="allow_full_week_booking" id="allow_full_week_booking" value="1"/> Allow full week bookings

                                            </label>

                                           

                                        </div>                            

                                    </div>                                                                                    



                                    <div class="row-form clearfix">

                                        <div class="span2">Weekend:</div>

                                        <div class="span2">Arrive:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="weekend_arrive" id="weekend_arrive">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <option value="<?= $rec['weekend_arrive'] ?>"><?= $rec['weekend_arrive'] ?></option>

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option>

                                                <?php endforeach; ?>

                                                <?php }else{ ?>  

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option>



                                                <?php } ?>                                        

                                            </select>

                                        </div>

                                        <div class="span2">Depart:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="weekend_depart" id="weekend_depart">

                                                 <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <option value="<?= $rec['weekend_depart'] ?>"><?= $rec['weekend_depart'] ?></option>

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option> 

                                                <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <option value="Mon">Mon</option>

                                                    <option value="Tue">Tue</option>

                                                    <option value="Wed">Wed</option>

                                                    <option value="Thur">Thur</option>

                                                    <option value="Fri">Fri</option>

                                                    <option value="Sat">Sat</option>

                                                    <option value="Sun">Sun</option> 



                                                <?php } ?>                                          

                                            </select>

                                        </div>

                                        <div class="span2" > 

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>     

                                                <input type="text" name="weekend_percentage" id="weekend_percentage"  style="width:40%;" value="<?= $rec['weekend_percentage'] ?>"/> % 

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <input type="text" name="weekend_percentage" id="weekend_percentage"  style="width:40%;"/> % 

                                             <?php } ?>    

                                           

                                        </div>

                                        <div class="span2">Min of:</div>

                                        <div class="span2">

                                            <select style="width:50%;" name="weekend_min_nights" id="weekend_min_nights">

                                                 <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <option value="<?= $rec['weekend_min_nights'] ?>"><?= $rec['weekend_min_nights'] ?></option>

                                                    <option value="1">1</option>

                                                    <option value="2">2</option>

                                                    <option value="3">3</option>

                                                    <option value="4">4</option>

                                                    <option value="5">5</option>

                                                    <option value="6">6</option>

                                                    <option value="7">7</option> 

                                                <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <option value="1">1</option>

                                                    <option value="2">2</option>

                                                    <option value="3">3</option>

                                                    <option value="4">4</option>

                                                    <option value="5">5</option>

                                                    <option value="6">6</option>

                                                    <option value="7">7</option> 



                                                <?php } ?>                                          

                                            </select> nights

                                        </div> 

                                        <div class="span2"></div>   

                                        <div class="span2">                                               

                                            <label class="checkbox inline">        

                                                    <input type="checkbox" name="allow_full_week_booking" id="allow_full_week_booking" value="1"/> Allow full week bookings

                                            </label>

                                           

                                        </div>                            

                                    </div>                                                                                 



                                    <div class="row-form clearfix">

                                        <div class="span3">Additional Charge</div>

                                        <div class="span2">                                               

                                            <label class="checkbox inline">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <input type="text" name="additional_charge" id="additional_charge" value="<?= $rec['additional_charge'] ?>"/>

                                                 <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <input type="text" name="additional_charge" id="additional_charge"/>

                                                <?php } ?> 

                                            </label>                                           

                                        </div>  

                                    </div>      



                                    <div class="row-form clearfix">

                                       <div class="span3">Minimum price</div>

                                        <div class="span2">                                               

                                            <label class="checkbox inline">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <input type="text" name="min_price" id="min_price" value="<?= $rec['min_price'] ?>"/>

                                                 <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <input type="text" name="min_price" id="min_price"/>

                                                <?php } ?> 

                                            </label>                                           

                                        </div> 

                                    </div> 



                                    <div class="row-form clearfix">

                                       <div class="span3">Days prior to arrival to always allow shortbreaks</div>

                                        <div class="span2">                                               

                                            <label class="checkbox inline">

                                                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <input type="text" name="allow_short_breaks" id="allow_short_breaks" value="<?= $rec['allow_short_breaks'] ?>"/>

                                                 <?php endforeach; ?>

                                                <?php }else{ ?>

                                                    <input type="text" name="allow_short_breaks" id="allow_short_breaks"/>

                                                <?php } ?> 

                                                

                                            </label>                                           

                                        </div> 

                                    </div>              

                                </div>

                             </div>

                        </div><!-- essentials -->        

                    </div>

                </form>

           

            </div>

        </div>   

    </div>



     <!-- for checkbox-->

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 

    <script type='text/javascript' src='<?= INCLUDES ?>app/js/pricing.js'></script> 



</body>

</html>

