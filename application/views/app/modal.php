<body data-baseurl="<?php echo base_url(); ?>">
    <div class="row-fluid">
        <div class="span12">
           
            <div class="row-fluid">
                <div class="span8">
                     <a href="<?php echo site_url('login/facebook') ?>" class="tip" title="facebook"><img src="<?= INCLUDES ?>app/img/sign_up_with_facebook.png" style="margin-left:50px; padding-top:25px;"/></a>                    
                </div>
                <div class="span1"></div>
            </div><br/>
            <div class="signup-or-separator">
                <img src="<?= INCLUDES ?>app/img/or.jpg" style=""/></a>
            </div>
            <div id="input_form">
                <form class="form-horizontal"  method="POST" id="user_registration">
                    <div class= "form-error" id="error-div"></div>
                     
                    <input type="hidden" name="id" id="id" value="<?= isset($user['id'])?$user['id']:'' ?> ">
                    <input type="hidden" name="login_provider" id="login_provider" value="<?= isset($user['login_provider'])?$user['login_provider']:'' ?> ">
                    <input type="hidden" name="login_provider_username" id="login_provider_username" value="<?= isset($user['login_provider_username'])?$user['login_provider_username']:'' ?> ">
                    <input type="hidden" name="facebook_user_id" id="facebook_user_id" value="<?= isset($user['facebook_user_id'])?$user['facebook_user_id']:'' ?> ">
                    <input type="hidden" name="profile_image_url" id="profile_image_url" value="<?= isset($user['profile_image_url'])?$user['profile_image_url']:'' ?> ">
                    <div class="control-group">
                        <div class="input-prepend">
                            <span class="add-on"><span class="icon-user"></span></span>
                            <input type="text" class="validate[required]" name="firstname" id="firstname" placeholder="First Name"/>
                        </div>                
                    </div>
                    <div class="dr"><span></span></div>
                    
                    <div class="control-group">
                        <div class="input-prepend">
                            <span class="add-on"><span class="icon-user"></span></span>
                            <input type="text" class="validate[required]"  name="lastname" id="lastname" placeholder="Last Name"/>
                        </div>                
                    </div>
                    <div class="dr"><span></span></div>
                    
                    <div class="control-group">
                        <div class="input-prepend">
                            <span class="add-on"><span class="icon-envelope"></span></span>
                            <input value="" class="validate[required,custom[email]]" type="text" name="email" id="email" onBlur="$('#username').val(this.value);"   placeholder="Email Address"/>
                        </div>                
                    </div>  
                    <div class="dr"><span></span></div>
                    
                    <input type="hidden" name="username" id="username" value="<?= isset($user['username'])?$user['username']:'' ?> ">
                                       
                    <div class="control-group">
                        <div class="input-prepend">
                            <span class="add-on"><span class="icon-lock"></span></span>
                             <input  data-validation-strength="2" data-validation="strength"  type="password" name="password_confirmation" id="password_confirmation" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="dr"><span></span></div>


                    <div class="control-group">
                        <div class="input-prepend">
                            <span class="add-on"><span class="icon-lock"></span></span>
                            <input data-validation="confirmation" type="password" name="password" id="password" placeholder="Confirm Password"/>
                        </div>
                    </div>
                    <div class="dr"><span></span></div>
                
                    <button class="signup" onClick="register.save();return false;"   id="buton"  type="button">Sign up with Email</button>
                </form>
              
            </div>
            

        </div>
    </div> 
    
    <style>
	.signup {
        display: block;
        height:30px;
        width: 65%;
        margin-left:10%;
        color: #FFF;
        text-align: center;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.25);
        background-color: #39B7CD;      
        border: 1px solid #bbbbbb;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
        margin-bottom:5%;
    }
    .dr,.dr span {background:none;}
    .modal .dr{margin: 0px 0px;}
    .dr{height:0px;}
     .form-error{ color:red; font-size: 12px; padding-left:30px;}
	</style> 
   
   <link rel="stylesheet" type="text/css" href="<?= INCLUDES ?>app/css/bootstrap.css" />
</body>