<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

 
    <title>Booking Brain</title>



    <!-- include all css and js file -->

    <?= $this->load->view('app/layouts/assets'); ?>

   

   <script type='text/javascript' src="<?= INCLUDES ?>app/js/jquery-1.6.2.min.js"></script>
   <script type='text/javascript' src="<?= INCLUDES ?>app/js/ui-lightness/jquery-ui-1.8.14.custom.min.js"></script>
   <script type='text/javascrip' src="<?= INCLUDES ?>app/js/core.js"></script>
    
    <style>

        [class*="block"] [class*="isw-"], [class*="block"] [class*="isb-"]

            {

                margin-left: 5px;

                margin-right: 1px;

                padding: 18px 0 0;

            }



            .block, .block-fluid 

            {

                margin-bottom: 15px;

            }
            ul li{
                 display: inline-block;
            }

    </style> 

    

</head>
<body data-baseurl="<?php echo base_url(); ?>">

    <div class="wrapper"> 

        <!-- include header -->   

        <?= $this->load->view('app/layouts/header'); ?>



        <div class="menu">                

            <!-- include navigation -->

            <?= $this->load->view('app/layouts/navigation'); ?>

        </div>



        <div class="content">

            <!-- include secondary navigation-->

            <?= $this->load->view('app/layouts/secondary_navigation'); ?>



            <!-- Main content -->

            <div class="workplace">

                 <div class="page-header">

                    <h1>Property Photos</h1>                   

                </div>



                 <?php $id = $this->uri->segment(3); //print_r($this->uri->segment(3));

                   //$this->sessions->setsessiondata('photo_id',$id);   ?> 
                <div class="row-fluid">

                    <div class="span12">                    

                        <div class="head clearfix">

                            <div class="isw-download"></div>

                            <h1>Upload Photos</h1>

                             <div class="footer tar" style="margin-right:5px;margin-top:2px;">                        

                                <a class="btn" href="<?= site_url('property/propertyList');?>">Cancel</a>

                            </div>      

                        </div>

                        <div class="block-fluid">

                            <p>Upload photos to really sell your property to potential guests. Click 'Add Photos',choose a picture from your files,then hit 'Upload'.

                               Want to change the order of your photos? Simply click on one and drag it up or down.</p>

                            <!--<div id="uploader_v4"></div>-->

                            <?php if(isset($error) &&  !empty($error)) {  ?>

                                <?php echo $error ?>

                            <?php } ?>

                            

                            <!--<div class="info">

                                <?php echo form_open_multipart('property/do_upload/'.$id) ?>

                                    <input type="file" name="userfile" class="btn" />

                                    <input type="submit" name="submit" class="btn" value="Start Upload" />

                            </div>-->

                            <div class="info">
                                        

                                <?php echo form_open_multipart('property/do_upload/'.$id) ?>
                    
                                    <input type="file" name="userfile[]" class="btn" multiple="" />

                                     <input type="hidden" value=<?= $id ?> id='prop_id'/>

                                 

                                    <input type="submit" name="submit" class="btn" value="Start Upload" />
                               
                            </div>



                        </div>

                    </div>                                                                  



                </div>   



               

                <div class="row-fluid">

                    <div class="span12"> 

                        <div class="head clearfix">

                            <div class="isw-favorite"></div>

                            <h1>Uploaded Photos </h1>

                        </div>

                        <div id="dragNdrop" class="block gallery clearfix">
                        <section id="wrapper">
                        <ul id="images">
                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                            <?php 
                            for($i=0;$i<sizeof($response['records']);$i++) {
                                 $listid=$response['records'][$i]['id'];
                                    
                            ?>
                                <li id="<?= $listid ?>" > <a class="fancybox" ><img  src="<?= $response['records'][$i]['image'] ?>" class="img-polaroid" style="width:150px; height:150px;" /></a>
                                    
                                </li>
                            
                            <?php }//endforeach; ?>

                            <?php }else {?>

                                <p>No Photos Available</p>

                            <?php } ?>                      
                        </ul>
                        </section>
                        </div>
                     
            

                        

                    </div>

                </div>

            </div>

        </div>   

    </div>

    <script>
    $( "#images" ).sortable({
        start : function(event, ui) {
            ui.item.addClass('active');
        },
   
        stop : function(event, ui) {
            var data_id = [];
            var Order = [];
            ui.item.removeClass('active').effect("highlight", { color: '#000' }, 1000, function() {
            
            $.each($('#images li'), function(index, event) {
                var thisId = $(this).attr('id');
                var thisOrder = parseInt(index, 10);
                $(this).children('span').html(thisOrder);
                jQuery.post('url_to_php_file.php', { id : thisId, order : thisOrder });
                
                data_id.push(thisId);
                Order.push(thisOrder);

            });
             console.log(data_id);
             console.log(Order);

            var id = document.getElementById('prop_id').value;
            console.log(id);
            window.location.href=$('body').data('baseurl')+'index.php/property/updatePropertyPhoto/'+id+'?data_id='+data_id+'&id='+id+'&order='+Order;  
        });   

    }
     
                     
    });


     
    </script>

</body>

</html>

