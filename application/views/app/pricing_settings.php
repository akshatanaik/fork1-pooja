<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    
     <style>
        [class*="span"] {
            float: left;
            margin-left: 0;
            min-height: 1px;
        }

        .row-fluid .span2 {
            width: 14.5299%;
            margin-right:-95px;
        }

        [class^="icon-"], [class*=" icon-"] {
            margin-left: 22px;
        }

     </style>

</head>

<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper">
            <!-- include header -->
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>
            
            <div class="workplace">
               
                <?php if(isset($record) &&  !empty($record)) {  ?>
                <form class="form-horizontal" action="<?= site_url('/pricing/save_pricing_settings') ?>?property_id=<?= $record ?>&type=undefined" method="POST" onsubmit="return pricing(this);">
                <div class="span12">
                    <div class="head clearfix">
                        <div class="isw-favorite"></div>
                        <h1>Start Day for full week bookings</h1>
                        <div class="footer tar" style="margin-right:5px;margin-top:2px;">
                            <button class="btn btn-success">Save</button>
                            <a class="btn" href="<?= site_url('pricing/index');?>?property_id=<?= $record ?>&type=undefined">Cancel</a>
                        </div>
                    </div>
                    <div class="block-fluid">
                        <div id="pricing">
                        <div class="row-fluid">
                            <div class="span12">                              
                           
                                <div class="row-form clearfix">
                                	<div class="span2"></div>

                                    <div class="span2">                                              
                                       <p class="actions">
                                            <a id="sun" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>
                                       

                                    <div class="span2">                                              
                                        <p class="actions">
                                            <a id="mon" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span2">                                               
                                        <p class="actions">
                                            <a id="tues" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span2">                                               
                                        <p class="actions">
                                            <a id="wed" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span2">                                               
                                        <p class="actions">
                                            <a id="thur" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span2">                                               
                                       <p class="actions">
                                            <a id="fri" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span2">                                               
                                       <p class="actions">
                                            <a id="sat" href="#"><span class="icon-chevron-down"></span></a>                                       
                                        </p>
                                    </div>

                                    <div class="span3"></div>
                                </div>                             

                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="jan" id="jan" value="January" />January</div>

                                    <div class="span2"> 
                                        <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['sun'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_sun" id="jan_sun" value="1" class="jan_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                            </label>
                                        <?php }else{ ?>                                             
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_sun" id="jan_sun" value="1" class="jan_sun"/> &nbsp;&nbsp; Sun
                                            </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_sun" id="jan_sun" value="1" class="jan_sun"/> &nbsp;&nbsp; Sun
                                            </label>
                                        <?php } ?>
                                    
                                    </div>
                                       

                                    <div class="span2">
                                        <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['mon'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_mon" id="jan_mon" value="1" class="jan_mon" checked="checked"/> &nbsp;&nbsp; Mon
                                            </label>
                                        <?php }else{ ?>                                                 
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="jan_mon" id="jan_mon" value="1" class="jan_mon"/>&nbsp;&nbsp;Mon
                                                </label>
                                            <?php } ?>
                                        <?php }else{ ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_mon" id="jan_mon" value="1" class="jan_mon"/> &nbsp;&nbsp;Mon
                                            </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2"> 
                                        <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['tues'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_tues" id="jan_tues" value="1" class="jan_tues" checked="checked"/> &nbsp;&nbsp;  Tues
                                            </label>
                                        <?php }else{ ?>                                                  
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_tues" id="jan_tues" value="1" class="jan_tues"/>&nbsp;&nbsp; Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_tues" id="jan_tues" value="1" class="jan_tues"/>&nbsp;&nbsp; Tues  
                                        </label>
                                        <?php } ?> 
                                       
                                    </div>

                                    <div class="span2">
                                        <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['wed'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_wed" id="jan_wed" value="1" class="jan_wed" checked="checked"/> &nbsp;&nbsp; Wed
                                            </label>
                                        <?php }else{ ?>                                                
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_wed" id="jan_wed" value="1" class="jan_wed"/>&nbsp;&nbsp; Wed 
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_wed" id="jan_wed" value="1" class="jan_wed"/>&nbsp;&nbsp; Wed 
                                        </label>
                                        <?php } ?> 
                                       
                                    </div>

                                    <div class="span2"> 
                                         <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['thur'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_thur" id="jan_thur" value="1" class="jan_thur" checked="checked"/>&nbsp;&nbsp;  Thur
                                            </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_thur" id="jan_thur" value="1" class="jan_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="jan_thur" id="jan_thur" value="1" class="jan_thur"/>&nbsp;&nbsp; Thur
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['fri'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_fri" id="jan_fri" value="1" class="jan_fri" checked="checked"/>&nbsp;&nbsp;  Fri
                                            </label>
                                        <?php }else{ ?>                                                
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_fri" id="jan_fri" value="1" class="jan_fri"/> &nbsp;&nbsp; Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="jan_fri" id="jan_fri" value="1" class="jan_fri"/> &nbsp;&nbsp; Fri
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][0]['month'] == 'January' && $response['records'][0]['sat'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="jan_sat" id="jan_sat" value="1" class="jan_sat" checked="checked"/>&nbsp;&nbsp;   Sat
                                            </label>
                                        <?php }else{ ?>                                                
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="jan_sat" id="jan_sat" value="1" class="jan_sat"/> &nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="jan_sat" id="jan_sat" value="1" class="jan_sat"/> &nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox"  value="1" class="selectall_jan"/> Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>                                  
                          

                     
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="feb" id="feb" value="February" />February</div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['sun'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_sun" id="feb_sun" value="1" class="feb_sun" checked="checked"/> &nbsp;&nbsp; Sun
                                            </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_sun" id="feb_sun" value="1" class="feb_sun"/> &nbsp;&nbsp; Sun
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_sun" id="feb_sun" value="1" class="feb_sun"/> &nbsp;&nbsp; Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['mon'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_mon" id="feb_mon" value="1" class="feb_mon" checked="checked"/> &nbsp;&nbsp; Mon
                                            </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_mon" id="feb_mon" value="1" class="feb_mon"/> &nbsp;&nbsp; Mon
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_mon" id="feb_mon" value="1" class="feb_mon"/> &nbsp;&nbsp; Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['tues'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_tues" id="feb_tues" value="1" class="feb_tues" checked="checked"/> &nbsp;&nbsp; Tues
                                            </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_tues" id="feb_tues" value="1" class="feb_tues"/> &nbsp;&nbsp; Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_tues" id="feb_tues" value="1" class="feb_tues"/> &nbsp;&nbsp; Tues  
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['wed'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_wed" id="feb_wed" value="1" class="feb_wed" checked="checked"/> &nbsp;&nbsp; Wed
                                            </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_wed" id="feb_wed" value="1" class="feb_wed"/> &nbsp;&nbsp; Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_wed" id="feb_wed" value="1" class="feb_wed"/> &nbsp;&nbsp; Wed 
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['thur'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_thur" id="feb_thur" value="1" class="feb_thur" checked="checked"/> &nbsp;&nbsp; Thur
                                            </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_thur" id="feb_thur" value="1" class="feb_thur"/> &nbsp;&nbsp; Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_thur" id="feb_thur" value="1" class="feb_thur"/> &nbsp;&nbsp; Thur
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['fri'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_fri" id="feb_fri" value="1" class="feb_fri" checked="checked"/> &nbsp;&nbsp; Fri
                                            </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_fri" id="feb_fri" value="1" class="feb_fri"/> &nbsp;&nbsp; Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_fri" id="feb_fri" value="1" class="feb_fri"/> &nbsp;&nbsp; Fri
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][1]['month'] == 'February' && $response['records'][1]['sat'] != 0) { ?>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="feb_sat" id="feb_sat" value="1" class="feb_sat" checked="checked"/> &nbsp;&nbsp; Sat
                                            </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="feb_sat" id="feb_sat" value="1" class="feb_sat"/> &nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="feb_sat" id="feb_sat" value="1" class="feb_sat"/> &nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" value="1" class="selectall_feb"/> Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                           


                      
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="march" id="march" value="March" />March</div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['sun'] != 0) { ?>
                                            <label class="checkbox inline">
                                            <input type="checkbox" name="march_sun" id="march_sun" value="1" class="march_sun" checked="checked"/> &nbsp;&nbsp; Sun
                                        </label>
                                        <?php }else{ ?>                                                    
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_sun" id="march_sun" value="1" class="march_sun"/> &nbsp;&nbsp;  Sun
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="march_sun" id="march_sun" value="1" class="march_sun"/> &nbsp;&nbsp; Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['mon'] != 0) { ?>
                                            <label class="checkbox inline">
                                            <input type="checkbox" name="march_mon" id="march_mon" value="1" class="march_mon" checked="checked"/> &nbsp;&nbsp; Mon
                                        </label>
                                        <?php }else{ ?>                                                  
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_mon" id="march_mon" value="1" class="march_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="march_mon" id="march_mon" value="1" class="march_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][1]['tues'] != 0) { ?>
                                            <label class="checkbox inline">
                                            <input type="checkbox" name="march_tues" id="march_tues" value="1" class="march_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                                    
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_tues" id="march_tues" value="1" class="march_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="march_tues" id="march_tues" value="1" class="march_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_wed" id="march_wed" value="1" class="march_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                                   
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_wed" id="march_wed" value="1" class="march_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                          <label class="checkbox inline">
                                            <input type="checkbox" name="march_wed" id="march_wed" value="1" class="march_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_thur" id="march_thur" value="1" class="march_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                                    
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_thur" id="march_thur" value="1" class="march_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_thur" id="march_thur" value="1" class="march_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['fri'] != 0) { ?>
                                            <label class="checkbox inline">
                                            <input type="checkbox" name="march_fri" id="march_fri" value="1" class="march_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                                   
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_fri" id="march_fri" value="1" class="march_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="march_fri" id="march_fri" value="1" class="march_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][2]['month'] == 'March' && $response['records'][2]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_sat" id="march_sat" value="1" class="march_sat" checked="checked"/>&nbsp;&nbsp; Sat
                                        </label>
                                        <?php }else{ ?>                                                    
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_sat" id="march_sat" value="1" class="march_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="march_sat" id="march_sat" value="1" class="march_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox"  class="selectall_march"/> Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                          
                      
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="april" id="april" value="April" />April</div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sun" id="april_sun" value="1" class="april_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sun" id="april_sun" value="1" class="april_sun"/>&nbsp;&nbsp; Sun
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sun" id="april_sun" value="1" class="april_sun"/>&nbsp;&nbsp; Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_mon" id="april_mon" value="1" class="april_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_mon" id="april_mon" value="1" class="april_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_mon" id="april_mon" value="1" class="april_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['tues'] != 0) { ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="april_tues" id="april_tues" value="1" class="april_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_tues" id="april_tues" value="1" class="april_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="april_tues" id="april_tues" value="1" class="april_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_wed" id="april_wed" value="1" class="april_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_wed" id="april_wed" value="1" class="april_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_wed" id="april_wed" value="1" class="april_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['thur'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="april_thur" id="april_thur" value="1" class="april_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_thur" id="april_thur" value="1" class="april_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_thur" id="april_thur" value="1" class="april_thur"/>&nbsp;&nbsp; Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_fri" id="april_fri" value="1" class="april_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_fri" id="april_fri" value="1" class="april_fri"/>&nbsp;&nbsp; Fri
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_fri" id="april_fri" value="1" class="april_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][3]['month'] == 'April' && $response['records'][3]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sat" id="april_sat" value="1" class="april_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sat" id="april_sat" value="1" class="april_sat"/>&nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="april_sat" id="april_sat" value="1" class="april_sat"/>&nbsp;&nbsp; Sat
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" value="1" class="selectall_april"/> Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                         

                    
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="may" id="may" value="May" />May</div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_sun" id="may_sun" value="1" class="may_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_sun" id="may_sun" value="1" class="may_sun"/>&nbsp;&nbsp; Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="may_sun" id="may_sun" value="1" class="may_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_mon" id="may_mon" value="1" class="may_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                                
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_mon" id="may_mon" value="1" class="may_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_mon" id="may_mon" value="1" class="may_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['tues'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_tues" id="may_tues" value="1" class="may_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_tues" id="may_tues" value="1" class="may_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_tues" id="may_tues" value="1" class="may_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_wed" id="may_wed" value="1" class="may_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_wed" id="may_wed" value="1" class="may_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_wed" id="may_wed" value="1" class="may_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_thur" id="may_thur" value="1" class="may_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_thur" id="may_thur" value="1" class="may_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_thur" id="may_thur" value="1" class="may_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_fri" id="may_fri" value="1" class="may_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_fri" id="may_fri" value="1" class="may_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_fri" id="may_fri" value="1" class="may_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][4]['month'] == 'May' && $response['records'][4]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_sat" id="may_sat" value="1" class="may_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_sat" id="may_sat" value="1" class="may_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="may_sat" id="may_sat" value="1" class="may_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" class="selectall_may"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                         
                     
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="june" id="june" value="June" />June</div>

                                    <div class="span2">
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sun" id="june_sun" value="1" class="june_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sun" id="june_sun" value="1" class="june_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sun" id="june_sun" value="1" class="june_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_mon" id="june_mon" value="1" class="june_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_mon" id="june_mon" value="1" class="june_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                      <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_mon" id="june_mon" value="1" class="june_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['tues'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_tues" id="june_tues" value="1" class="june_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_tues" id="june_tues" value="1" class="june_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_tues" id="june_tues" value="1" class="june_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_wed" id="june_wed" value="1" class="june_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_wed" id="june_wed" value="1" class="june_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_wed" id="june_wed" value="1" class="june_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_thur" id="june_thur" value="1" class="june_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_thur" id="june_thur" value="1" class="june_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_thur" id="june_thur" value="1" class="june_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_fri" id="june_fri" value="1" class="june_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_fri" id="june_fri" value="1" class="june_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_fri" id="june_fri" value="1" class="june_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][5]['month'] == 'June' && $response['records'][5]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sat" id="june_sat" value="1" class="june_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sat" id="june_sat" value="1" class="june_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="june_sat" id="june_sat" value="1" class="june_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="" id="" value="1" class="selectall_june"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                         
                    
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="july" id="july" value="July" />July</div>

                                    <div class="span2">  
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_sun" id="july_sun" value="1" class="july_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                                   
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_sun" id="july_sun" value="1" class="july_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_sun" id="july_sun" value="1" class="july_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_mon" id="july_mon" value="1" class="july_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_mon" id="july_mon" value="1" class="july_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_mon" id="july_mon" value="1" class="july_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['tues'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_tues" id="july_tues" value="1" class="july_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_tues" id="july_tues" value="1" class="july_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_tues" id="july_tues" value="1" class="july_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_wed" id="july_wed" value="1" class="july_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_wed" id="july_wed" value="1" class="july_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_wed" id="july_wed" value="1" class="july_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_thur" id="july_thur" value="1" class="july_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_thur" id="july_thur" value="1" class="july_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_thur" id="july_thur" value="1" class="july_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['fri'] != 0) { ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="july_fri" id="july_fri" value="1" class="july_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_fri" id="july_fri" value="1" class="july_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="july_fri" id="july_fri" value="1" class="july_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][6]['month'] == 'July' && $response['records'][6]['sat'] != 0) { ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="july_sat" id="july_sat" value="1" class="july_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="july_sat" id="july_sat" value="1" class="july_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="july_sat" id="july_sat" value="1" class="july_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox"  class="selectall_july"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                        
                   
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="aug" id="aug" value="August" />August</div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sun" id="aug_sun" value="1" class="aug_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sun" id="aug_sun" value="1" class="aug_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sun" id="aug_sun" value="1" class="aug_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_mon" id="aug_mon" value="1" class="aug_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_mon" id="aug_mon" value="1" class="aug_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_mon" id="aug_mon" value="1" class="aug_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['tues'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_tues" id="aug_tues" value="1" class="aug_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_tues" id="aug_tues" value="1" class="aug_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_tues" id="aug_tues" value="1" class="aug_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_wed" id="aug_wed" value="1" class="aug_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_wed" id="aug_wed" value="1" class="aug_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_wed" id="aug_wed" value="1" class="aug_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_thur" id="aug_thur" value="1" class="aug_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_thur" id="aug_thur" value="1" class="aug_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_thur" id="aug_thur" value="1" class="aug_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_fri" id="aug_fri" value="1" class="aug_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_fri" id="aug_fri" value="1" class="aug_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_fri" id="aug_fri" value="1" class="aug_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][7]['month'] == 'August' && $response['records'][7]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sat" id="aug_sat" value="1" class="aug_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sat" id="aug_sat" value="1" class="aug_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="aug_sat" id="aug_sat" value="1" class="aug_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox"  value="1" class="selectall_aug"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                        
                     
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="sep" id="sep" value="September" />September</div>

                                    <div class="span2"> 
                                        <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sun" id="sep_sun" value="1" class="sep_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sun" id="sep_sun" value="1" class="sep_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sun" id="sep_sun" value="1" class="sep_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['mon'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_mon" id="sep_mon" value="1"class="sep_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_mon" id="sep_mon" value="1"class="sep_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_mon" id="sep_mon" value="1"class="sep_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['tues'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_tues" id="sep_tues" value="1" class="sep_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_tues" id="sep_tues" value="1" class="sep_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="sep_tues" id="sep_tues" value="1" class="sep_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_wed" id="sep_wed" value="1" class="sep_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_wed" id="sep_wed" value="1" class="sep_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_wed" id="sep_wed" value="1" class="sep_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_thur" id="sep_thur" value="1" class="sep_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_thur" id="sep_thur" value="1" class="sep_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_thur" id="sep_thur" value="1" class="sep_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_fri" id="sep_fri" value="1" class="sep_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_fri" id="sep_fri" value="1" class="sep_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_fri" id="sep_fri" value="1" class="sep_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][8]['month'] == 'September' && $response['records'][8]['sat'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sat" id="sep_sat" value="1" class="sep_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sat" id="sep_sat" value="1" class="sep_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="sep_sat" id="sep_sat" value="1" class="sep_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" class="selectall_sep"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                      
                      
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="oct" id="oct" value="October" />October</div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['sun'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sun" id="oct_sun" value="1" class="oct_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sun" id="oct_sun" value="1" class="oct_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sun" id="oct_sun" value="1" class="oct_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['mon'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="oct_mon" id="oct_mon" value="1" class="oct_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_mon" id="oct_mon" value="1" class="oct_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_mon" id="oct_mon" value="1" class="oct_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['tues'] != 0) { ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="oct_tues" id="oct_tues" value="1" class="oct_tues" cheked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_tues" id="oct_tues" value="1" class="oct_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                         <label class="checkbox inline">
                                            <input type="checkbox" name="oct_tues" id="oct_tues" value="1" class="oct_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['wed'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_wed" id="oct_wed" value="1" class="oct_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_wed" id="oct_wed" value="1" class="oct_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_wed" id="oct_wed" value="1" class="oct_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['thur'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_thur" id="oct_thur" value="1" class="oct_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_thur" id="oct_thur" value="1" class="oct_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_thur" id="oct_thur" value="1" class="oct_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['fri'] != 0) { ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_fri" id="oct_fri" value="1" class="oct_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_fri" id="oct_fri" value="1" class="oct_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_fri" id="oct_fri" value="1" class="oct_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">       
                                     <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][9]['month'] == 'October' && $response['records'][9]['sat'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sat" id="oct_sat" value="1" class="oct_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sat" id="oct_sat" value="1" class="oct_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="oct_sat" id="oct_sat" value="1" class="oct_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" value="1" class="selectall_oct"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                      
                   
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="nov" id="nov" value="November" />November</div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['sun'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sun" id="nov_sun" value="1" class="nov_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sun" id="nov_sun" value="1" class="nov_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sun" id="nov_sun" value="1" class="nov_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['mon'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_mon" id="nov_mon" value="1" class="nov_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_mon" id="nov_mon" value="1" class="nov_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_mon" id="nov_mon" value="1" class="nov_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                      
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['tues'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_tues" id="nov_tues" value="1" class="nov_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_tues" id="nov_tues" value="1" class="nov_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_tues" id="nov_tues" value="1" class="nov_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['wed'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_wed" id="nov_wed" value="1" class="nov_wed" checked="checked "/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_wed" id="nov_wed" value="1" class="nov_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_wed" id="nov_wed" value="1" class="nov_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['thur'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_thur" id="nov_thur" value="1" class="nov_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_thur" id="nov_thur" value="1" class="nov_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_thur" id="nov_thur" value="1" class="nov_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['fri'] != 0) { ?>
                                      <label class="checkbox inline">
                                            <input type="checkbox" name="nov_fri" id="nov_fri" value="1" class="nov_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_fri" id="nov_fri" value="1" class="nov_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_fri" id="nov_fri" value="1" class="nov_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span2">  
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][10]['month'] == 'November' && $response['records'][10]['sat'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sat" id="nov_sat" value="1" class="nov_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                             
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sat" id="nov_sat" value="1" class="nov_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="nov_sat" id="nov_sat" value="1" class="nov_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                       
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" value="1" class="selectall_nov"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>

                                       
                                    </div>
                                </div>
                      
                      
                                <div class="row-form clearfix">
                                    <div class="span2"><input type="hidden" name="dec" id="dec" value="December" />December</div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['sun'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sun" id="dec_sun" value="1" class="dec_sun" checked="checked"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sun" id="dec_sun" value="1" class="dec_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                         <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sun" id="dec_sun" value="1" class="dec_sun"/> &nbsp;&nbsp;Sun
                                        </label>
                                        <?php } ?>
                                    </div>
                                       

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['mon'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_mon" id="dec_mon" value="1" class="dec_mon" checked="checked"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_mon" id="dec_mon" value="1" class="dec_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                       <?php } ?>
                                        <?php }else{ ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_mon" id="dec_mon" value="1" class="dec_mon"/>&nbsp;&nbsp;Mon
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['tues'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_tues" id="dec_tues" value="1" class="dec_tues" checked="checked"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_tues" id="dec_tues" value="1" class="dec_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_tues" id="dec_tues" value="1" class="dec_tues"/>&nbsp;&nbsp;Tues  
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['wed'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_wed" id="dec_wed" value="1" class="dec_wed" checked="checked"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_wed" id="dec_wed" value="1" class="dec_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_wed" id="dec_wed" value="1" class="dec_wed"/>&nbsp;&nbsp;Wed 
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['thur'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_thur" id="dec_thur" value="1" class="dec_thur" checked="checked"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_thur" id="dec_thur" value="1" class="dec_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_thur" id="dec_thur" value="1" class="dec_thur"/> &nbsp;&nbsp;Thur
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2"> 
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['fri'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_fri" id="dec_fri" value="1" class="dec_fri" checked="checked"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php }else{ ?>                                              
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_fri" id="dec_fri" value="1" class="dec_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_fri" id="dec_fri" value="1" class="dec_fri"/> &nbsp;&nbsp;Fri
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span2">
                                    <?php  if(!empty($response['records'])) { ?>
                                        <?php if($response['records'][11]['month'] == 'December' && $response['records'][11]['sat'] != 0) { ?>
                                       <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sat" id="dec_sat" value="1" class="dec_sat" checked="checked"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php }else{ ?>                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sat" id="dec_sat" value="1" class="dec_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                        <?php }else{ ?>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="dec_sat" id="dec_sat" value="1" class="dec_sat"/> &nbsp;&nbsp;Sat
                                        </label>
                                        <?php } ?>
                                    </div>

                                    <div class="span3">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" value="1" class="selectall_dec"/> &nbsp;&nbsp;Tick/Untick all( flexible arrival)
                                        </label>
                                       
                                    </div>
                                </div>
                        
                                <div class="row-form clearfix">
                                   <div class="span3">Highlight day on grid view</div>
                                    <div class="span6">
                                        <select >
                                            <option value="0">Sunday</option>
                                            <option value="1">Monday</option> 
                                            <option value="2">Tuesday</option> 
                                            <option value="3">Wednesday</option> 
                                            <option value="4">Thursday</option> 
                                            <option value="5">Friday</option> 
                                            <option value="6">Saturday</option> 
                                        </select>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        </div>                                                  
                 
                    </div>
                </div>
                 </form>
                <?php } ?>           
               
                <form class="form-horizontal" action="<?= site_url('/pricing/save_shortbreak') ?>?property_id=<?= $record ?>&type=undefined" method="POST" onsubmit="return pricing(this);">

                <div class="span12">
                <div class="head clearfix">
                    <div class="isw-favorite"></div>
                    <h1>Short Breaks</h1>
                    <div class="footer tar" style="margin-right:5px;margin-top:2px;">
                        <button class="btn btn-success">Save</button>
                        <a class="btn" href="<?= site_url('pricing/index');?>?property_id=<?= $record ?>&type=undefined">Cancel</a>
                    </div>
                </div>
                <div class="block-fluid">
                   <div id="pricing">
                        <div class="row-fluid">
                            <div class="span12">                                           
                                <div class="row-form clearfix">
                                    <div class="span3">Short break pricing model</div>
                                    <?php //print_r($response['shortbreak'][0]); ?>
                                    <div class="span6">
                                        <select name="short_break_pricing_model" id="short_break_pricing_model">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['short_break_pricing_model'] == 'Midweek and weekend breaks only') { ?>
                                                <option value="<?= $response['shortbreak'][0]['short_break_pricing_model'] ?>"><?= $response['shortbreak'][0]['short_break_pricing_model'] ?></option>
                                                <option value="No short break">No short break</option>
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['short_break_pricing_model'] ?>"><?= $response['shortbreak'][0]['short_break_pricing_model'] ?></option>
                                                <option value="Midweek and weekend breaks only">Midweek and weekend breaks only(percentage shortbreak rates)</option>
                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="Midweek and weekend breaks only">Midweek and weekend breaks only(percentage shortbreak rates)</option>
                                                <option value="No short break">No short break</option>

                                            <?php } ?>                                                                                     
                                        </select>
                                    </div>
                                </div>                         

                                <div class="row-form clearfix">
                                    <div class="span2">Midweek:</div>
                                    <div class="span2">Arrive:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="midweek_arrive" id="midweek_arrive">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['midweek_arrive'] == 'Mon') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_arrive'] ?>"><?= $response['shortbreak'][0]['midweek_arrive'] ?></option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>
                                                <option value="Thur">Thur</option>
                                            <?php }else if($response['shortbreak']['midweek_arrive'] == 'Tue') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_arrive'] ?>"><?= $response['shortbreak'][0]['midweek_arrive'] ?></option>
                                                <option value="Mon">Mon</option>                                                
                                                <option value="Wed">Wed</option>
                                                <option value="Thur">Thur</option>
                                            <?php }else if($response['shortbreak']['midweek_arrive'] == 'Wed') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_arrive'] ?>"><?= $response['shortbreak'][0]['midweek_arrive'] ?></option>                                                
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>                                                
                                                <option value="Thur">Thur</option>
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_arrive'] ?>"><?= $response['shortbreak'][0]['midweek_arrive'] ?></option>                        
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>                                               

                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>
                                                <option value="Thur">Thur</option>
                                            <?php } ?>                                              
                                
                                        </select>
                                    </div>
                                    <div class="span2">Depart:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="midweek_depart" id="midweek_depart">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['midweek_depart'] == 'Mon') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_depart'] ?>"><?= $response['shortbreak'][0]['midweek_depart'] ?></option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>
                                                <option value="Thur">Thur</option>
                                            <?php }else if($response['shortbreak']['midweek_depart'] == 'Tue') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_depart'] ?>"><?= $response['shortbreak'][0]['midweek_depart'] ?></option>
                                                <option value="Mon">Mon</option>                                                
                                                <option value="Wed">Wed</option>
                                                <option value="Thur">Thur</option>
                                            <?php }else if($response['shortbreak']['midweek_depart'] == 'Wed') { ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_depart'] ?>"><?= $response['shortbreak'][0]['midweek_depart'] ?></option>                                                
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>                                                
                                                <option value="Thur">Thur</option>
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_depart'] ?>"><?= $response['shortbreak'][0]['midweek_depart'] ?></option>                        
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>                                               

                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="Thur">Thur</option>
                                                <option value="Mon">Mon</option>
                                                <option value="Tue">Tue</option>
                                                <option value="Wed">Wed</option>
                                                
                                            <?php } ?>                                
                                        </select>
                                    </div>
                                    <div class="span2">
                                        <?php  if(!empty($response['shortbreak'])) { ?>                                        
                                            <input type="text" name="midweek_percentage" id="midweek_percentage"  value="<?= $response['shortbreak'][0]['midweek_percentage'] ?>" style="width:40%;"/> %
                                        <?php }else{ ?>
                                            <input type="text" name="midweek_percentage" id="midweek_percentage"  style="width:40%;"/> %
                                        <?php } ?>                                        
                                    </div>
                                    <div class="span2">Min of:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="midweek_min_nights" id="midweek_min_nights">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['midweek_min_nights'] == 3) { ?>                                               
                                                <option value="<?= $response['shortbreak'][0]['midweek_min_nights'] ?>"><?= $response['shortbreak'][0]['midweek_min_nights'] ?></option>
                                                <option value="4">4</option>                                               
                                                <option value="7">7</option> 
                                            <?php }else if($response['shortbreak']['midweek_min_nights'] == 4){ ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_min_nights'] ?>"><?= $response['shortbreak'][0]['midweek_min_nights'] ?></option>  
                                                <option value="3">3</option>                                                                                             
                                                <option value="7">7</option> 
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['midweek_min_nights'] ?>"><?= $response['shortbreak'][0]['midweek_min_nights'] ?></option> 
                                                <option value="3">3</option>
                                                <option value="4">4</option>                                               
                                                
                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="3">3</option>
                                                <option value="4">4</option>                                               
                                                <option value="7">7</option> 
                                            <?php } ?>                                                                            
                                        </select> nights
                                    </div> 
                                    <div class="span2"></div>      
                                    <div class="span2">                                               
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="allow_full_week_booking" id="allow_full_week_booking" value="1"/> &nbsp;&nbsp;Allow full week bookings
                                        </label>
                                       
                                    </div>                            
                                </div>                                                                                    

                                <div class="row-form clearfix">
                                    <div class="span2">Weekend:</div>
                                    <div class="span2">Arrive:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="weekend_arrive" id="weekend_arrive">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['weekend_arrive'] == 'Fri') { ?>                                                   
                                                <option value="<?= $response['shortbreak'][0]['weekend_arrive'] ?>"><?= $response['shortbreak'][0]['weekend_arrive'] ?></option>
                                                <option value="Sat">Sat</option>
                                                <option value="Sun">Sun</option>
                                            <?php }else if($response['shortbreak']['weekend_arrive'] == 'Sat') { ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_arrive'] ?>"><?= $response['shortbreak'][0]['weekend_arrive'] ?></option>
                                                <option value="Fri">Fri</option>
                                                <option value="Sun">Sun</option>
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_arrive'] ?>"><?= $response['shortbreak'][0]['weekend_arrive'] ?></option>
                                                <option value="Fri">Fri</option>
                                                <option value="Sat">Sat</option>
                                            <?php } ?>
                                            <?php }else{?>
                                                <option value="Fri">Fri</option>
                                                <option value="Sat">Sat</option>
                                                <option value="Sun">Sun</option>
                                            <?php } ?>                                                                            
                                        </select>
                                    </div>
                                    <div class="span2">Depart:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="weekend_depart" id="weekend_depart">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['weekend_depart'] == 'Fri') { ?>                                         
                                                
                                                <option value="<?= $response['shortbreak'][0]['weekend_depart'] ?>"><?= $response['shortbreak'][0]['weekend_depart'] ?></option>
                                                <option value="Sat">Sat</option>
                                                <option value="Sun">Sun</option> 
                                                <option value="Mon">Mon</option>
                                            <?php }else if($response['shortbreak']['weekend_depart'] == 'Sat'){ ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_depart'] ?>"><?= $response['shortbreak'][0]['weekend_depart'] ?></option>
                                                <option value="Fri">Fri</option>                                                
                                                <option value="Sun">Sun</option> 
                                                <option value="Mon">Mon</option>
                                            <?php }else if($response['shortbreak']['weekend_depart'] == 'Sun'){ ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_depart'] ?>"><?= $response['shortbreak'][0]['weekend_depart'] ?></option>
                                                <option value="Fri">Fri</option>
                                                <option value="Sat">Sat</option>
                                                <option value="Mon">Mon</option>
                                            <?php }else{ ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_depart'] ?>"><?= $response['shortbreak'][0]['weekend_arrive'] ?></option>
                                                <option value="Fri">Fri</option>
                                                <option value="Sat">Sat</option>
                                                <option value="Sun">Sun</option> 
                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="Sun">Sun</option> 
                                                <option value="Fri">Fri</option>
                                                <option value="Sat">Sat</option>
                                                
                                                <option value="Mon">Mon</option>
                                            <?php } ?>

                                                                                 
                                        </select>
                                    </div>
                                    <div class="span2" > 
                                        <?php  if(!empty($response['shortbreak'])) { ?>
                                            <input type="text" name="weekend_percentage" id="weekend_percentage"  value="<?= $response['shortbreak'][0]['weekend_percentage'] ?>" style="width:40%;"/> % 
                                        <?php }else{ ?>
                                            <input type="text" name="weekend_percentage" id="weekend_percentage"  style="width:40%;"/> % 
                                        <?php } ?>
                                       
                                    </div>
                                    <div class="span2">Min of:</div>
                                    <div class="span2">
                                        <select style="width:50%; margin-left:-50px;" name="weekend_min_nights" id="weekend_min_nights">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                            <?php if($response['shortbreak'][0]['weekend_min_nights'] == 2) { ?>      
                                                <option value="<?= $response['shortbreak'][0]['weekend_min_nights'] ?>"><?= $response['shortbreak'][0]['weekend_min_nights'] ?></option>
                                                <option value="3">3</option>                                               
                                                <option value="7">7</option> 
                                            <?php }else if($response['shortbreak']['weekend_min_nights'] == 3){ ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_min_nights'] ?>"><?= $response['shortbreak'][0]['weekend_min_nights'] ?></option>                                           

                                                <option value="2">2</option>
                                                <option value="7">7</option>
                                            <?php }else { ?>
                                                <option value="<?= $response['shortbreak'][0]['weekend_min_nights'] ?>"><?= $response['shortbreak'][0]['weekend_min_nights'] ?></option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>                                               
                                            <?php } ?>
                                            <?php }else{ ?>
                                                <option value="2">2</option>
                                                <option value="3">3</option>                                               
                                                <option value="7">7</option>
                                            <?php } ?>                                                                             
                                        </select> nights
                                    </div> 
                                    <div class="span2"></div>   
                                    <div class="span2">                                               
                                        <label class="checkbox inline">        
                                                <input type="checkbox" name="allow_full_week_booking" id="allow_full_week_booking" value="1"/> &nbsp;&nbsp;Allow full week bookings
                                        </label>
                                       
                                    </div>                            
                                </div>                                                                                 

                                <div class="row-form clearfix">
                                    <div class="span3">Additional Charge</div>
                                    <div class="span2">                                               
                                        <label class="checkbox inline">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                                <input type="text" name="additional_charge" id="additional_charge" value="<?= $response['shortbreak'][0]['additional_charge'] ?>"/>
                                            <?php }else{ ?>
                                                <input type="text" name="additional_charge" id="additional_charge"/>
                                            <?php } ?>
                                        </label>                                           
                                    </div>  
                                </div>      

                                <div class="row-form clearfix">
                                   <div class="span3">Minimum price</div>
                                    <div class="span2">                                               
                                        <label class="checkbox inline">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                                <input type="text" name="min_price" id="min_price" value="<?= $response['shortbreak'][0]['min_price'] ?>"/>
                                            <?php }else{ ?>
                                                <input type="text" name="min_price" id="min_price"/>
                                            <?php } ?>
                                        </label>                                           
                                    </div> 
                                </div> 

                                <div class="row-form clearfix">
                                   <div class="span3">Days prior to arrival to always allow shortbreaks</div>
                                    <div class="span2">                                               
                                        <label class="checkbox inline">
                                            <?php  if(!empty($response['shortbreak'])) { ?>
                                                <input type="text" name="allow_short_breaks" id="allow_short_breaks" value="<?= $response['shortbreak'][0]['allow_short_breaks'] ?>"/>
                                           <?php }else{ ?>
                                                <input type="text" name="allow_short_breaks" id="allow_short_breaks"/>
                                            <?php } ?>
                                            
                                        </label>                                           
                                    </div> 
                                </div>              
                            </div>
                         </div>
                    </div><!-- essentials -->        
                </div>
            </form>
            
            </div>          
           
            </div>
            </div>
        </div>   
    </div>

     <script>
    $(document).ready(function(){
        $("#sun").click(function(e){
            $('.jan_sun').attr('checked', true);
            $('.feb_sun').attr('checked', true);
            $('.march_sun').attr('checked', true);
            $('.april_sun').attr('checked', true);
            $('.may_sun').attr('checked', true);
            $('.june_sun').attr('checked', true);
            $('.july_sun').attr('checked', true);
            $('.aug_sun').attr('checked', true);
            $('.sep_sun').attr('checked', true);
            $('.oct_sun').attr('checked', true);
            $('.nov_sun').attr('checked', true);
            $('.dec_sun').attr('checked', true);
           
        });

        $("#sun").dblclick(function(e){
            $('.jan_sun').attr('checked', false);
            $('.feb_sun').attr('checked', false);
            $('.march_sun').attr('checked', false);
            $('.april_sun').attr('checked', false);
            $('.may_sun').attr('checked', false);
            $('.june_sun').attr('checked', false);
            $('.july_sun').attr('checked', false);
            $('.aug_sun').attr('checked', false);
            $('.sep_sun').attr('checked', false);
            $('.oct_sun').attr('checked', false);
            $('.nov_sun').attr('checked', false);
            $('.dec_sun').attr('checked', false);
           
        });

        $("#mon").click(function(e){
            $('.jan_mon').attr('checked', true);
            $('.feb_mon').attr('checked', true);
            $('.march_mon').attr('checked', true);
            $('.april_mon').attr('checked', true);
            $('.may_mon').attr('checked', true);
            $('.june_mon').attr('checked', true);
            $('.july_mon').attr('checked', true);
            $('.aug_mon').attr('checked', true);
            $('.sep_mon').attr('checked', true);
            $('.oct_mon').attr('checked', true);
            $('.nov_mon').attr('checked', true);
            $('.dec_mon').attr('checked', true);
           
        });

        $("#mon").dblclick(function(e){
            $('.jan_mon').attr('checked', false);
            $('.feb_mon').attr('checked', false);
            $('.march_mon').attr('checked', false);
            $('.april_mon').attr('checked', false);
            $('.may_mon').attr('checked', false);
            $('.june_mon').attr('checked', false);
            $('.july_mon').attr('checked', false);
            $('.aug_mon').attr('checked', false);
            $('.sep_mon').attr('checked', false);
            $('.oct_mon').attr('checked', false);
            $('.nov_mon').attr('checked', false);
            $('.dec_mon').attr('checked', false);
           
        });


        $("#tues").click(function(e){
            $('.jan_tues').attr('checked', true);
            $('.feb_tues').attr('checked', true);
            $('.march_tues').attr('checked', true);
            $('.april_tues').attr('checked', true);
            $('.may_tues').attr('checked', true);
            $('.june_tues').attr('checked', true);
            $('.july_tues').attr('checked', true);
            $('.aug_tues').attr('checked', true);
            $('.sep_tues').attr('checked', true);
            $('.oct_tues').attr('checked', true);
            $('.nov_tues').attr('checked', true);
            $('.dec_tues').attr('checked', true);
           
        });

        $("#tues").dblclick(function(e){
            $('.jan_tues').attr('checked', false);
            $('.feb_tues').attr('checked', false);
            $('.march_tues').attr('checked', false);
            $('.april_tues').attr('checked', false);
            $('.may_tues').attr('checked', false);
            $('.june_tues').attr('checked', false);
            $('.july_tues').attr('checked', false);
            $('.aug_tues').attr('checked', false);
            $('.sep_tues').attr('checked', false);
            $('.oct_tues').attr('checked', false);
            $('.nov_tues').attr('checked', false);
            $('.dec_tues').attr('checked', false);
           
        });

        $("#wed").click(function(e){
            $('.jan_wed').attr('checked', true);
            $('.feb_wed').attr('checked', true);
            $('.march_wed').attr('checked', true);
            $('.april_wed').attr('checked', true);
            $('.may_wed').attr('checked', true);
            $('.june_wed').attr('checked', true);
            $('.july_wed').attr('checked', true);
            $('.aug_wed').attr('checked', true);
            $('.sep_wed').attr('checked', true);
            $('.oct_wed').attr('checked', true);
            $('.nov_wed').attr('checked', true);
            $('.dec_wed').attr('checked', true);
           
        });

        $("#wed").dblclick(function(e){
            $('.jan_wed').attr('checked', false);
            $('.feb_wed').attr('checked', false);
            $('.march_wed').attr('checked', false);
            $('.april_wed').attr('checked', false);
            $('.may_wed').attr('checked', false);
            $('.june_wed').attr('checked', false);
            $('.july_wed').attr('checked', false);
            $('.aug_wed').attr('checked', false);
            $('.sep_wed').attr('checked', false);
            $('.oct_wed').attr('checked', false);
            $('.nov_wed').attr('checked', false);
            $('.dec_wed').attr('checked', false);
           
        });

        $("#thur").click(function(e){
            $('.jan_thur').attr('checked', true);
            $('.feb_thur').attr('checked', true);
            $('.march_thur').attr('checked', true);
            $('.april_thur').attr('checked', true);
            $('.may_thur').attr('checked', true);
            $('.june_thur').attr('checked', true);
            $('.july_thur').attr('checked', true);
            $('.aug_thur').attr('checked', true);
            $('.sep_thur').attr('checked', true);
            $('.oct_thur').attr('checked', true);
            $('.nov_thur').attr('checked', true);
            $('.dec_thur').attr('checked', true);
           
        });

        $("#thur").dblclick(function(e){
            $('.jan_thur').attr('checked', false);
            $('.feb_thur').attr('checked', false);
            $('.march_thur').attr('checked', false);
            $('.april_thur').attr('checked', false);
            $('.may_thur').attr('checked', false);
            $('.june_thur').attr('checked', false);
            $('.july_thur').attr('checked', false);
            $('.aug_thur').attr('checked', false);
            $('.sep_thur').attr('checked', false);
            $('.oct_thur').attr('checked', false);
            $('.nov_thur').attr('checked', false);
            $('.dec_thur').attr('checked', false);
           
        });

        $("#fri").click(function(e){
            $('.jan_fri').attr('checked', true);
            $('.feb_fri').attr('checked', true);
            $('.march_fri').attr('checked', true);
            $('.april_fri').attr('checked', true);
            $('.may_fri').attr('checked', true);
            $('.june_fri').attr('checked', true);
            $('.july_fri').attr('checked', true);
            $('.aug_fri').attr('checked', true);
            $('.sep_fri').attr('checked', true);
            $('.oct_fri').attr('checked', true);
            $('.nov_fri').attr('checked', true);
            $('.dec_fri').attr('checked', true);
        });

        $("#fri").dblclick(function(e){
            $('.jan_fri').attr('checked', false);
            $('.feb_fri').attr('checked', false);
            $('.march_fri').attr('checked', false);
            $('.april_fri').attr('checked', false);
            $('.may_fri').attr('checked', false);
            $('.june_fri').attr('checked', false);
            $('.july_fri').attr('checked', false);
            $('.aug_fri').attr('checked', false);
            $('.sep_fri').attr('checked', false);
            $('.oct_fri').attr('checked', false);
            $('.nov_fri').attr('checked', false);
            $('.dec_fri').attr('checked', false);
        });

        $("#sat").click(function(e){
            $('.jan_sat').attr('checked', true);
            $('.feb_sat').attr('checked', true);
            $('.march_sat').attr('checked', true);
            $('.april_sat').attr('checked', true);
            $('.may_sat').attr('checked', true);
            $('.june_sat').attr('checked', true);
            $('.july_sat').attr('checked', true);
            $('.aug_sat').attr('checked', true);
            $('.sep_sat').attr('checked', true);
            $('.oct_sat').attr('checked', true);
            $('.nov_sat').attr('checked', true);
            $('.dec_sat').attr('checked', true);
        });

        $("#sat").dblclick(function(e){
            $('.jan_sat').attr('checked', false);
            $('.feb_sat').attr('checked', false);
            $('.march_sat').attr('checked', false);
            $('.april_sat').attr('checked', false);
            $('.may_sat').attr('checked', false);
            $('.june_sat').attr('checked', false);
            $('.july_sat').attr('checked', false);
            $('.aug_sat').attr('checked', false);
            $('.sep_sat').attr('checked', false);
            $('.oct_sat').attr('checked', false);
            $('.nov_sat').attr('checked', false);
            $('.dec_sat').attr('checked', false);
        });
        
    });
    </script>
     
    <!-- for checkbox
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>-->
     <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/pricing.js'></script> 

    
     
    
</body>
</html>
