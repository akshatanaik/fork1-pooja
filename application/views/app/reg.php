<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Booking Brain</title>
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <!-- CSS files begin-->
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700|Open+Sans+Condensed:700,300,300italic|Open+Sans:400,300italic,400italic,600,600italic,700,700italic,800,800italic|PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="<?= INCLUDES ?>site/css/bootstrap.css" rel="stylesheet">
    <link href="<?= INCLUDES ?>site/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?= INCLUDES ?>site/css/responsiveslides.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= INCLUDES ?>site/css/prettyPhoto.css" type='text/css'>
    <link rel="stylesheet" type="text/css" media="screen" href="<?= INCLUDES ?>site/css/slide-in.css" />
    <!--[if lt IE 9]><link rel="stylesheet" type="text/css" media="screen" href="<?= INCLUDES ?>site/css/slide-in.ie.css" /><![endif]-->
    <link href="<?= INCLUDES ?>site/css/style.css" rel="stylesheet">
    <!-- Color Style Setting CSS file-->
    <link href="<?= INCLUDES ?>site/css/color-theme/color-fblue.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- fav and touch icons -->
    <link rel="shortcut icon" href="<?= INCLUDES ?>site/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= INCLUDES ?>site/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= INCLUDES ?>site/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= INCLUDES ?>site/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?= INCLUDES ?>site/ico/apple-touch-icon-57-precomposed.png">

     <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>



    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/stepywizard/jquery.stepy.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/register.js'></script>
   
</head>

<body data-baseurl="<?php echo base_url(); ?>" style="background:#fff;">

<!-- Head
================================================== -->
<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="span6">
                <!-- text widget begin here -->
                <ul class="info-text pull-left">
                    <li>
                        <a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Aenean commodo ligula eget.</a>
                    </li>
                    <li>
                        <a href="#">
                            This informational text widget 
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Lorem ipsum dolor sit
                        </a>
                        amet, consectetuer adipiscing elit. Aenean commodo ligula eget.
                    </li>
                </ul>
            </div>
            
        </div>
    </div>
</div>
<!-- Logo / Menu
================================================== -->

    <div class="container strip-line">
        <div class="row">
            <div class="span4">
                <a href="index.html" class="logo">
                    <img src="<?= INCLUDES ?>site/img/logo.png" alt="">
                </a>
            </div>
          
        </div>
    </div>



<div class="slider-home">

<div class="container">

<!-- Slogan
================================================== -->
 <div class="row">
 
 <div class="divider"></div>
 <div class="span12 promo-slogan">
   <h1>Hello and welcome to <span>Booking Brain</span> , a way to manage and book the property </h1>
 
 </div>
 <div class="divider"></div>



<!-- Slider
================================================== -->
  <div class="span12 cont-bs">
   <!-- Slideshow -->
    <ul class="rslides rslides1">
      <li><img src="<?= INCLUDES ?>site/img/slider-img03.jpg" alt="" /></li>
      <li><img src="<?= INCLUDES ?>site/img/slider-img02.jpg" alt="" /></li>
      <li><img src="<?= INCLUDES ?>site/img/slider-img03.jpg" alt="" /></li>
    </ul>
    
         <div class="content" style="margin-left:150px;margin-right: 150px; padding:0; margin-top:-335px;">

           
            <div class="workplace">
               
                <?php if(isset($status) && $status=='error'){ ?>
                        <div id="error"><?= $response ?></div>
                <?php }else{ ?>
                <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>
                <?= $this->load->view('app/mini_register') ?>
                <?php } ?>

            </div>
        </div>
    
  </div>

       


 </div>
</div>

</div>

<div class="container">

 
<!-- Home content
================================================== -->
<div class="divider"></div> 

<div class="row" style="width:1360px;">

 <div class="span3 block-info-h">
      <img src="<?= INCLUDES ?>site/img/create-icon.png" alt="">
      <h3><a href="#">Property Management</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
    <div class="span3 block-info-h">
      <img src="<?= INCLUDES ?>site/img/develop-icon.png" alt="">
      <h3><a href="#">Booking</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
    <div class="span3 block-info-h">
      <img src="<?= INCLUDES ?>site/img/usability-icon.png" alt="">
      <h3><a href="#">East to use</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
    <div class="span3 block-info-h">
      <img src="<?= INCLUDES ?>site/img/responsive.png" alt="">
      <h3><a href="#">Reports & Dashboards</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
 
</div>

 
  
 <div class="divider"></div>    
  
 <!-- Latest project
================================================== -->  
  
  <div class="row">
  <div class="span12 divider-strip"><h4>Latest projects</h4><span class="strip-block"></span></div>
  </div>

  <ul class="thumbnails">
    <li class="span3 item-block">
     <a href="<?= INCLUDES ?>site/img/bootstrap-mdo-sfmoma-01.jpg" class="zoom" rel="prettyPhoto" title="Image Title"></a>
     <a href="portfolio.html" class="link"></a>
      <a class="thumbnail" href="portfolio.html">
        <img src="<?= INCLUDES ?>site/img/example-sites/example1.jpg" alt="example-item">
      </a>
      <div class="desc">
      <a href="portfolio.html"> Images </a>
       <p> <em>Portfolio Item Images </em></p>
      </div>
    </li>
    <li class="span3 item-block">
     
       <iframe src="http://player.vimeo.com/video/28220269?title=0&amp;byline=0&amp;portrait=0" width="270" height="135" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
      <div class="desc">
      <a href="portfolio.html">Vimeo Video </a>
       <p> <em>Portfolio Item Images</em> </p>
      </div>
    </li>
   <li class="span3 item-block">
     <a href="<?= INCLUDES ?>site/img/bootstrap-mdo-sfmoma-01.jpg" class="zoom" rel="prettyPhoto" title="Image Title"></a>
     <a href="portfolio.html" class="link"></a>
      <a class="thumbnail" href="portfolio.html">
        <img src="<?= INCLUDES ?>site/img/example-sites/example1.jpg" alt="example-item">
      </a>
      <div class="desc">
      <a href="portfolio.html"> Images </a>
       <p> <em>Portfolio Item Images</em> </p>
      </div>
    </li>
    <li class="span3 item-block">
     <div class="row">
      <div class="span3">
     <video src="<?= INCLUDES ?>site/media/VH_videoAsset.flv" type="video/flv" controls="controls"></video>
     </div>
     </div>
      <div class="desc">
      <a href="portfolio.html"> Local Video </a>
       <p> <em>Portfolio Item Images</em> </p>
      </div>
    </li>
  </ul>
  
 <!-- Our Services
================================================== --> 
  <div class="row">
  <div class="span12 divider-strip"><h4>Our Services</h4><span class="strip-block"></span></div>
  </div>
  
  <div class="row">
   <div class="span4">
    <video src="<?= INCLUDES ?>site/media/VH_videoAsset.flv" type="video/flv" controls="controls"></video>
   </div>
   <div class="span8">
    <div class="row">
    
    <div class="span4 block-info pull-left">
      <img class="pull-left" src="<?= INCLUDES ?>site/img/create-icon.png" alt="">
      <h3><a href="#">web design</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
    </div>
    <div class="span4 block-info pull-left">
      <img class="pull-left" src="<?= INCLUDES ?>site/img/develop-icon.png" alt="">
      <h3><a href="#">development</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
    </div>
    <div class="span4 block-info pull-left">
      <img class="pull-left" src="<?= INCLUDES ?>site/img/usability-icon.png" alt="">
      <h3><a href="#">usability</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
    </div>
    <div class="span4 block-info pull-left">
      <img class="pull-left" src="<?= INCLUDES ?>site/img/responsive.png" alt="">
      <h3><a href="#">responsive</a></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.</p>
    </div>
    
    </div>
    </div>
    </div>
    <div class="divider"></div> 
  
  
  <!-- Latest blog
================================================== -->
  
    <div class="row">
  
    <div class="span6">
    <div class="divider-strip block-title"><h4>Latest blog</h4><span class="strip-block"></span></div>
      <div class="row">
    <div class="span3 blog-home">
     <img src="<?= INCLUDES ?>site/img/blog-examples/blog-home.jpg" alt="">
     <ul class="meta pull-left"><li class="data">21 may<br>2012</li><li class="post-format image"><span></span></li></ul>
     <h3><a href="#">Lorem ipsum dolor sit amet</a></h3> 
     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa...</p>
     <a href="blog.html" class="btn">Read more</a>
    </div>
    <div class="span3 blog-home">
    <iframe src="http://player.vimeo.com/video/28220269?title=0&amp;byline=0&amp;portrait=0" width="270" height="100" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
    <ul class="meta pull-left"><li class="data">21 may<br>2012</li><li class="post-format video"><span></span></li></ul>
     <h3><a href="#">Lorem ipsum dolor sit amet</a></h3> 
     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa...</p>
     <a href="blog.html" class="btn">Read more</a>
    </div>
    </div>
    </div>
    
    <div class="span6">
    
    <div class="divider-strip block-title"><h4>Testimonials</h4><span class="strip-block"></span></div>
      <div class="testimonialswrap" data-autorotate="3000">
        
        <ul class="testimonials-slider" id="testimonials">
            <li class="testimonials-slide"> 
                 <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
<small>John Doe</small></blockquote>
            </li>
            <li class="testimonials-slide"> 
                <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
<small>John Doe</small></blockquote>
            </li>
            <li class="testimonials-slide"> 
                <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
<small>John Doe</small></blockquote>
            </li>
            <li class="testimonials-slide"> 
                <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
<small>John Doe</small></blockquote>
            </li>
            <li class="testimonials-slide"> 
                <blockquote><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>
<small>John Doe</small></blockquote>
            </li>
        </ul>
        
        <div class="testimonials-controls">
            <a href="#testimonials" class="next-l">1</a>
            <a href="#testimonials" class="prev-l">2</a>
        </div>
        
    </div>
    </div>

    </div>

<div class="row">
 <div class="divider"></div>    
<!-- Promo
================================================== -->
 
 <div class="well no-padding-lr span12">
 <div class="row promo-buy">
  <div class="span9"><h1 class="center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </h1></div>
  <div class="span3"><a class="btn btn-large btn-success right margin-right" href="#"><i class="icon-shopping-cart icon-white"></i> Get Started »</a></div>
 </div>
 </div>
  
   
  </div>
  
  <div class="divider"></div>

    </div>


     <!-- Footer
      ================================================== -->
      <footer>
      <div class="container">
      <div class="row">
      
      <div class="span4">
      <h3>About</h3>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.</p>
      
                    
      </div>
      
      <!-- tweets begin here -->
      <div class="span4">
      <h3>Latest Tweets</h3>
       <div class="tweets">
                        <p>
                            Loading Tweets...
                        </p>
                        <ul id="tweet-list">
                        </ul>
                    </div>
      </div>
      
      <div class="span4">
      <!-- flickr begin here -->
      <h3>From Flickr</h3>
      
      <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;source=user&amp;user=52617155@N08&amp;layout=x&amp;display=random&amp;size=s"></script> 

      </div>
      
      <div class="span12 copy">
       &copy; 2012 NLINE. All Rights Reserved.
      </div>

      </div>
      </div>
      </footer>

   
    


  </body>
</html>