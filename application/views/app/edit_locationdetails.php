<?php

try {
    //$objDb = new PDO('mysql:host=localhost;dbname=booking_brain_db', 'root', '');
    $objDb = new PDO('mysql:host=internal-db.s75223.gridserver.com;dbname=db75223_booking_brain_db', 'db75223', 'Vbjq2euO');
    $objDb->exec('SET CHARACTER SET utf8');
    $sql = "SELECT continent
         FROM cont_country group by continent";
    $statement = $objDb->query($sql);
    $list = $statement->fetchAll(PDO::FETCH_ASSOC);
   
    
} catch(PDOException $e) {
    echo 'There was a problem';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>        
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Booking Brain</title>

    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>
    
      <style>
        [class*="span"] {
            float: left;
            margin-left: 0px;
            min-height: 1px;
        }
        .span12 {
            width: 1090px;
        }
        .span3 {
            width: 240px;
        }

        .block .footer, .block-fluid .footer {
            background-color: #F2F2F2;
            border-top: 1px solid #DAE1E8;
            margin-top: -1px;
            padding: 5px 5px 4px;
            text-align: right;
        }

           .form-error{ color:red; font-size: 10px;}
           
             select {
            width:100px; 
            overflow:hidden; 
            white-space:pre; 
            text-overflow:ellipsis;
            -webkit-appearance: none;

        }
        option {
                  border: solid 1px #DDDDDD; 
        }


    </style>

     <script type='text/javascript'>       

        function get_property_address(){

            var addrs =document.getElementById('inputProperty_address').value ;
            console.log('addrs--'+addrs);
            //var loc = document.getElementById("edit_iframe");
            $('#edit_iframe').hide();
            $('#iframe').show();

            document.getElementById("iframe").src = "https://www.google.com/maps/embed/v1/place?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q="+ addrs;
            console.log('reloads--');console.log(document.getElementById("iframe").src);
        }        
    
    </script>

    <script type='text/javascript'> 
    $(function () {
            $('#iframe').hide();
    });
    </script>   

</head>
<body data-baseurl="<?php echo base_url(); ?>">
    <div class="wrapper"> 
        <!-- include header -->   
        <?= $this->load->view('app/layouts/header'); ?>

        <div class="menu">                
             <!-- include navigation -->
             <?= $this->load->view('app/layouts/navigation'); ?>
        </div>

        <div class="content">
            <!-- include secondary navigation-->
            <?= $this->load->view('app/layouts/secondary_navigation'); ?>

            <!--   -->
            <div class="workplace">
                <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>
                <?php foreach($response['records'] as $rec): ?>
                <?php $id = $this->uri->segment(3); ?>
                                      
                 <form class="form-horizontal" action="<?= site_url('/property/updateLocationDetails/'.$id)?>" method="POST" onsubmit="return locationdetails(this);">
                     <div class="span12">
                        <div class="head clearfix">
                            <div class="isw-calendar"></div>
                            <h1>Edit Location Details</h1>

                            <div class="footer tar" style="margin-right:5px;margin-top:2px;">
                                <button class="btn btn-success">Update</button>
                                <a class="btn" href="<?= site_url('property/propertyList');?>">Cancel</a>
                            </div>
                        </div>
                        <!-- build  the form -->
                          <div class="block-fluid tabs">
                                 <ul >
                                    <li><a href="#property_location">Property location<em style="color:#F00;">*</em></a></li>
                                    <li><a href="#localarea">Local area</a></li>
                                    <li><a href="#how_to_get_there">How to get there</a></li>
                                    <li><a href="#whats_nearby">What's nearby</a></li>
                                    <li><a href="#holiday_types">Holiday types</a></li>
                                </ul> 

                                <div id="property_location">
                                    <div class="row-form clearfix">
                                            <div class="span3">Continent<em style="color:#F00;">*</em></div>
                                            <div class="span6">        
                                                <select name="inputContinent" id="inputContinent" onclick="func(); calljavascriptfunction('inputContinent','cont_country','continent','inputCountry','country');" data-validation="required" data-validation-error-msg="Continent field cannot be empty.">
                                                     <option value="<?= $rec['continent'] ?>"><?= $rec['continent'] ?></option>

                                                         <?php if (!empty($list)) { ?>
                                                               <?php foreach($list as $row) { ?>
                                                                    <option value="<?php echo $row['continent']; ?>">
                                                                        <?php echo $row['continent']; ?>
                                                                    </option>
                                                                <?php } ?>
                                                        <?php } ?>

                                                </select>
                                            </div>                            
                                        </div>                        

                                        <div class="row-form clearfix">
                                            <div class="span3">Country<em style="color:#F00;">*</em></div>
                                             
                                            <div class="span6">        
                                                <select name="inputCountry" id="inputCountry"  onclick="calljavascriptfunction(id,'country_region','country','inputRegion','region');" data-validation="required" data-validation-error-msg="Country field cannot be empty.">
                                                        <option value="<?= $rec['country'] ?>"><?= $rec['country'] ?></option>
                                                </select>
                                            </div>                            
                                        </div>                                                                                    

                                        <div class="row-form clearfix">
                                            <div class="span3">Region</div>
                                            <div class="span6">        
                                                <select name="inputRegion" id="inputRegion" onclick="calljavascriptfunction(id,'region_subregion','region','inputSubregion','subregion');">
                                                                                                     
                                                   
                                                          <option value="<?= $rec['region'] ?>"><?= $rec['region'] ?> </option>
                                                </select>
                                            </div>       
                                                                 
                                        </div>                                                                                 

                                        <div class="row-form clearfix">
                                            <div class="span3">Subregion</div>
                                            <div class="span6">        
                                                <select name="inputSubregion" id="inputSubregion" onclick="calljavascriptfunction(id,'subregion_town','subregion','inputTown','town');">
                                                
                                                   <option value="<?= $rec['subregion'] ?>"><?= $rec['subregion'] ?></option>
                                                  
                                                </select>
                                            </div>   
                                                                 
                                        </div>      

                                     <div class="row-form clearfix">
                                            <div class="span3">Town</div>
                                            <div class="span6">        
                                                <select name="inputTown" id="inputTown" >
                                                   
                                                        <option value="<?= $rec['town'] ?>"><?= $rec['town'] ?></option>
                                                  
                                                  
                                                </select>
                                            </div>
                                                                        
                                        </div> 

                                      <!-- <div class="row-form clearfix">
                                            <div class="span3">Suburb(optional)</div>
                                            <div class="span6">        -->
                                                <select name="inputSuburb" id="inputSuburb"  style="visibility:hidden;"> 
                                                    
                                                        <option value=" "?> </option>
                                                                                                    
                                                
                                                </select>
                                          <!--  </div>                            
                                        </div>  -->

                                        <!--<div class="row-form clearfix">
                                            <div class="span3">Property address</div>
                                            <div class="span6"><input value="<?= $rec['property_address'] ?>"  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty."/></div>
                                        </div> -->

                                        <div class="row-form clearfix">

                                            <div class="span3">Property address</div>

                                            <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                            <?php foreach($response['records'] as $rec): ?>

                                                <div class="span6"><input value="<?= $rec['property_address'] ?>"  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" onchange="get_property_address()" data-validation-error-msg="Property address field cannot be empty."/></div>

                                            <?php endforeach; ?>

                                            <?php }else{ ?>

                                                <div class="span6"><input  type="text" name="inputProperty_address" id="inputProperty_address" data-validation="required" data-validation-error-msg="Property address field cannot be empty." onchange="get_property_address()"/></div>

                                            <?php } ?>

                                        </div> 

                                        <div class="row-form clearfix">
                                            <div class="span3">Postcode</div>
                                            <div class="span6"><input value="<?= $rec['postcode'] ?>"  type="text" name="inputPostcode" id="inputPostcode" data-validation="required" data-validation-error-msg="Post code field cannot be empty."/></div>
                                        </div> 

                                        <!--<div class="row-fluid">
                                            <div class="span12">
                                                <div class="head clearfix">
                                                    <div class="isw-graph"></div>
                                                    <h1>Property Map</h1>
                                                </div>
                                               
                                                <iframe  width="1100" height="260" frameborder="0" style="border:0"
                                                  src="https://www.google.com/maps/embed/v1/search?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q=<?= $rec['property_address'] ?>">
                                                </iframe>
                                                
                                            </div> 
                                        </div> -->

                                        <div class="row-fluid">

                                            <div class="span12">

                                                <div class="head clearfix">

                                                    <div class="isw-graph"></div>

                                                    <h1>Property Map</h1>

                                                </div>

                                                 <?php if(isset($response['records']) &&  !empty($response['records'])) {  ?>

                                                <?php foreach($response['records'] as $rec): ?>

                                                    <iframe id="edit_iframe" width="1065" height="290" frameborder="0" style="border:0"

                                                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDX9ur1Wl-FKC6j6RUbt6t9WdtDAjM4pLQ&q=<?= $rec['property_address'] ?>

                                                        ">

                                                    </iframe>

                                                    <iframe id="iframe" width="1065" height="290" frameborder="0" style="border:0"

                                                        src="">

                                                    </iframe> 
                                                 <?php endforeach; ?>

                                                <?php }else{ ?> 
                                                    
                                                    <iframe id="iframe" width="1065" height="290" frameborder="0" style="border:0"

                                                        src="">

                                                    </iframe>
                                                
                                                <?php } ?>                                             

                                            </div>

                                        </div>            
                                </div>

                                <div id="localarea">
                                    <div class="row-form clearfix">
                                            <div class="span3">The region</div>
                                            <div class="span6"><textarea  type="text" name="inputRegion_description" id="inputRegion"> <?= $rec['region_description'] ?></textarea> </div>
                                    </div>
                                     <div class="row-form clearfix">
                                        <div class="span3">The area</div>
                                        <div class="span6"><textarea type="text" name="inputArea_description" id="inputArea"><?= $rec['area_description'] ?></textarea> </div>
                                    </div>
                                </div>

                                <div id="how_to_get_there">
                                    <div class="row-form clearfix">
                                        <div class="span3">Airport</div>
                                        <div class="span6"><input  value="<?= $rec['airport'] ?>" type="text" name="inputAirport" id="inputAirport"/></div>  km
                                    </div>
                                    <div class="row-form clearfix">
                                        <div class="span3">Ferry port</div>
                                        <div class="span6"><input  value="<?= $rec['ferry_port'] ?>" type="text" name="inputFerryport" id="inputFerryport"/></div>  km
                                    </div>
                                    <div class="row-form clearfix">
                                        <div class="span3">Train station </div>
                                        <div class="span6"><input  value="<?= $rec['train_station'] ?>" type="text" name="inputTrain_station" id="inputTrain_station"/></div>  km
                                    </div>
                                     <div class="row-form clearfix">
                                        <div class="span3">Car required</div>
                                        <div class="span6">        
                                            <select name="inputCar" id="inputCar" >
                                                <?php if($rec['car'] == null) { ?>
                                                    <option value="Choose">Choose</option>
                                                <?php } else { ?>
                                                    <option value="<?= $rec['car'] ?>"><?= $rec['car'] ?> </option>
                                                <?php } ?>   
                                                 
                                                <option value="car_not_necessary">Car not necessary</option>
                                                <option value="car_necessary">Car necessary</option>
                                            </select>
                                        </div>                            
                                    </div>
                                    <div class="row-form clearfix">

                                        <div class="span3">More tips of how to get there</div>
                                        <div class="span6"><textarea type="text" name="inputHow_to_get_there" id="inputHow_to_get_there"><?= $rec['how_to_get_there'] ?></textarea> </div>
                                    </div> 
                                </div>

                                <div id="whats_nearby">
                                     <div class="row-form clearfix">
                                            <div class="span3">
                                                <?php if($rec['golf_course_within_15km'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputGolf_course_15mins_walk"/>  Golf course within 15 mins walk
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputGolf_course_15mins_walk" id="inputGolf_course_15mins_walk" value="1" /> Golf course within 15 mins walk
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['golf_course_within_30km'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputGolf_course_30mins_drive"/> Golf course  within 30 mins drive
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputGolf_course_30mins_drive" id="inputGolf_course_30mins_drive" value="1"/>Golf course  within 30 mins drive
                                                 </label>
                                                 <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['tennis_court'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputTennis_court"/> Tennis courts nearby
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" value="1" name="inputTennis_court" id="inputTennis_court"  />Tennis courts nearby 
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['skiing'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputSkiing"/> Skiing:property is in a ski resort
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" value="1" name="inputSkiing" id="inputSkiing" />Skiing:property is in a ski resort
                                                 </label>
                                                 <?php } ?>
                                            </div>
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">
                                                <?php if($rec['water_sport'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputWater_sports"/>Water sports nearby
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" value="1" name="inputWater_sports" id="inputWater_sports"  />Water sports nearby 
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['water_park'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputWater_park"/> Water park nearby
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" value="1" name="inputWater_park" id="inputWater_park" />Water park nearby
                                                 </label>
                                                 <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['horse_riding'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputHorse_riding"/> Horse riding nearby 
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputHorse_riding" id="inputHorse_riding" value="1"/>Horse riding nearby 
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                 <?php if($rec['beach_front'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked" name="inputBeach_front"/> Beachfront property
                                                    </label>
                                                <?php }else{ ?>

                                                <label class="checkbox inline">
                                                    <input type="checkbox" value="1" name="inputBeach_front" id="inputBeach_front" />Beachfront property
                                                 </label>
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">
                                                <?php if($rec['fishing'] != 0) { ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" checked="checked" name="inputFishing"/> Fishing nearby
                                                </label>
                                                <?php }else{ ?>
                                                 <label class="checkbox inline">
                                                    <input type="checkbox" name="inputFishing" id="inputFishing" value="1" /> Fishing nearby
                                                </label>
                                                <?php } ?>
                                                
                                            </div>
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">Nearest Beach</div>
                                            <div class="span6"><input  value="<?= $rec['nearest_beach'] ?>" type="text" name="inputNearest_beach" id="inputNearest_beach"/></div>  km
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">Nearest shops</div>
                                            <div class="span6"><input  value="<?= $rec['nearest_shop'] ?>" type="text" name="inputNearest_shops" id="inputNearest_shops"/></div>  km
                                        </div>
                                </div>

                                <div id="holiday_types">
                                    <div class="row-form clearfix">
                                            <div class="span3">
                                                <?php if($rec['walking_holiday'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/> Walking holidays
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputWalking_holidays" id="inputWalking_holidays" value="1" /> Walking holidays
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['cycling_holiday'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/>Cycling holidays
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputCycling_holidays" id="inputCycling_holidays" value="1"/>Cycling holidays
                                                 </label>
                                                 <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <label class="checkbox inline">
                                                    <?php if($rec['rural_countryside_retreats'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/> Rural or countryside retreats
                                                    </label>
                                                <?php }else{ ?>
                                                    <input type="checkbox" name="inputRural_or_countryside_retreats" id="inputRural_or_countryside_retreats" value="1" /> Rural or countryside retreats
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['beach_lakeside_relaxation'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/> Beach or lakeside relaxation
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputBeach_or_lakeside_relaxation" id="inputBeach_or_lakeside_relaxation" value="1"/>Beach or lakeside relaxation
                                                 </label>
                                                 <?php } ?>
                                            </div>
                                        </div>

                                        <div class="row-form clearfix">
                                            <div class="span3">
                                                <?php if($rec['city_breaks'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/>City breaks
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputCity_breaks" id="inputCity_breaks" value="1" /> City breaks
                                                </label>
                                                <?php } ?>
                                            </div>

                                            <div class="span3">
                                                <?php if($rec['winter_sun'] != 0) { ?>
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/> Winter sun
                                                    </label>
                                                <?php }else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputWinter_sun" id="inputWinter_sun" value="1"/>Winter sun
                                                 </label>
                                                 <?php } ?>
                                            </div>

                                            <div class="span3">
                                               
                                                    <label class="checkbox inline">
                                                        <input type="checkbox" checked="checked"/> Nightlife
                                                    </label>
                                                <?php //}else{ ?>
                                                <label class="checkbox inline">
                                                    <input type="checkbox" name="inputNight_life" id="inputNight_life" value="1" /> Nightlife
                                                </label>
                                                <?php // } ?>
                                            </div>
                                </div>

                          </div>
                    </div>
                  </form>
                <?php endforeach; ?>
                <?php } ?>
            </div>
        </div>   
    </div>

     <!-- for validation -->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>

     <!-- for checkbox-->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src="<?= INCLUDES ?>app/js/ajax_form.js"></script> 
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/property.js'></script> 
    <script type="text/javascript">
    function func(){
        
         $('#inputRegion').empty();
         $('#inputSubregion').empty();
         $('#inputTown').empty();
        
    }
    
    function calljavascriptfunction(id,table,column,destination,dest_output){

     var baseurl=$('body').data('baseurl');

              $.ajax({
                 type : 'POST',
                  data : 'content1='+ $('#'+id).val() + '&table='+ table + '&column='+ column,
                  url : baseurl+'index.php/property/get_Location',
                  dataType: 'json',
                
                 success : function(data){
                       
                         document.getElementById(destination).disabled = false;
                         document.getElementById(destination).style.display = "block";
                          document.getElementById(destination).style.visibility = "visible";
                         $("#"+destination).empty();
                         var output = $.map(data, function(response){
                   
                         var records = $.map(response, function(records){
                         var record = $.map(records, function(record) {
                         for(i=0;i<record.length;i++){
                            $('#'+destination).append('<option value="' + record[i][dest_output]+ '">' + record[i][dest_output] + '</option>');
                        }
                    
                });
              });
                });
              
       
                }
             });
            }
    </script>


</body>
</html>
