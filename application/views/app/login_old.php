<!--<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    <link rel="icon" href="<?= INCLUDES ?>app/img/bb_favicon.png" type="image/png">
    <title>Login - Booking Brain</title>

    <link href="<?= INCLUDES ?>/app/css/login_styles.css" rel="stylesheet" type="text/css" />
   

    <style type="text/css">
        .form-error{ color:red; font-size: 10px;}
        .loginForm .change {margin-left: 222px;}
       

    </style>
</head>-->
<style type="text/css">
         .form-error{ color:red; font-size: 12px; padding-left:30px;}
        .loginForm .change {margin-left: 222px;}
       

    </style>
<body data-baseurl="<?php echo base_url(); ?>">
    <?php //echo('hiiii');var_dump($this->sessions->getsessiondata('access_token')); ?>
    <div class="loginBox">
        <div class="bLogo"></div>
        <div class="loginForm">
            

            <div class="row-fluid">
                <div class="span8">
                     <a href="<?php echo site_url('login/facebook') ?>" class="tip" title="facebook"><img src="<?= INCLUDES ?>app/img/loginwithfb-or.png" style="margin-left:25px; padding-top:25px; max-width:130%;"/></a>                    
                   
                </div>
                <div class="span1"></div>
            </div><br/>
            <div id="err-div" class="form-error"></div>
            <form id="login_form" class="form-horizontal" action="<?= site_url('/login/authenticate') ?>" method="POST">

                <!--<input type="hidden" name="returl" id="returl" value="<?= $returl ?>">-->
                
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-envelope"></span></span>
                        <input type="text" name="username" id="inputEmail" data-validation="required" data-validation-error-msg="Please enter Username"  placeholder="Username"/>
                    </div>
                </div>

                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><span class="icon-lock"></span></span>
                        <input type="password" name="password" id="inputPassword" data-validation="required" data-validation-error-msg="Please enter Password"placeholder="Password"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="control-group" style="margin-top: 5px;">
                            <label class="checkbox"><a href="#fModal" role="button"  data-toggle="modal">Forgot Password?</a></label>
                        </div>
                    </div>
                    <!--<div class="span4">
                        <button type="submit" class="btn btn-block">Sign in</button>
                    </div>-->
                </div>
                <button class="signup" onClick="login.authenticate();return false;"   id="buton"  type="button">Login</button>

            </form>
            <div class="row-fluid">
                <!--<div class="span8 white">
                    <p>Or use on of this services: <a href="<?php echo site_url('login/facebook') ?>" class="tip" title="facebook"><img src="<?= INCLUDES ?>app/img/facebook.png"/></a> 
     
                    <a href="<?php echo $authUrl ?>" class="tip" title="google"><img src="<?= INCLUDES ?>app/img/google.png"/></a> </p>
                </div>-->
                <div class="span1"></div>
                <div class="span3 change" style="margin-left:222px;">
                    <p>
                        <div class="btn-group" style="  color: #888888; font-size: 11px;line-height: 30px;margin: 0 10px;"><a style="color: #888888; font-size: 11px;line-height: 30px;margin: 0 10px;" href="javascript:void(0);" onClick="calModal()">Sign Up </a>
                                            <!--<a style="color: #888888; font-size: 11px;line-height: 30px;margin: 0 10px;" href="<?php echo site_url('registration') ?>" class=" dropdown-toggle" data-toggle="dropdown">Sign Up <span class="caret"></span></a>-->
                                            <!--<ul style="color: #888888; font-size: 11px;" class="dropdown-menu"> -->
                                                <!--<li><a href="<?php echo site_url('registration') ?>">Register</a></li>-->
                                                <!--<li><a href="<?php echo site_url('registration/facebook') ?>">Register with facebook</a></li>-->
                                                <!--<li><a href="<?php echo site_url('registration/google') ?>">Register with google</a></li>-->
                                               <!-- <li><a href="<?php echo site_url('registration/twitter') ?>">Register with twitter</a></li>-->
                                               
                                            <!--</ul> -->
                                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!-- define a modal for regsitration -->
        <div id="registermodal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <!-- here we will show options how to register -->
             <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Register</h3>
            </div>

            <div class="row-fluid">
                <div class="block-fluid">
                    <a class="btn btn-block btn-social btn-bitbucket">
                        <i class="fa fa-bitbucket"></i> Register 
                    </a>
                    <a class="btn btn-block btn-social btn-facebook">
                        <i class="fa fa-facebook"></i> Register with Facebook
                    </a>
                     <a class="btn btn-block btn-social btn-twitter">
                        <i class="fa fa-twitter"></i> Register with Twitter
                      </a>
                </div>
            </div>
        </div>

    <!-- define  a form for forgot password -->
    <!-- Bootrstrap modal form -->
        <div id="fModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <form id="forgot_password_form"  action="<?= site_url('/login/forgot_password') ?>" method="POST">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Forgot Password</h3>
            </div>
            <div class="row-fluid">
                <div class="block-fluid">
                    <div class="row-form clearfix">
                        <div class="span3">username/email:</div>
                        <div class="span9"><input type="text" id="username_or_email" name="username_or_email" data-validation="required" data-validation-error-msg="Please enter username/email" value=""/></div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <div class="row-fluid">
                    <div class="span9"></div>
                    <div class="span2">
                        <button type="submit" class="btn btn-block">Send</button>
                    </div>
                </div>
                <!--<button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Send</button>-->

            </div>
            </form>
        </div>

        <div id="regModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                
                <h3 id="myModalLabel">Sign Up</h3>
            </div>
            <div class="row-fluid" >
                <div class="block-fluid">
                    <?= $this->load->view('app/modal.php') ?>
                </div>
            </div>
        </div>

        <style type="text/css">
            #regModal{width:320px; left:60%;}
            .modal-footer{background-color:#fff; border-top: medium none;}
            .form-horizontal{padding-left: 30px;}
            .modal.fade.in{top:40%;}
        </style>




<!-- Include all the required jsvascripts-->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/jquery/jquery-1.10.2.min.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/bootstrap.min.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>

    <!-- for validation -->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/form-validator/jquery.form-validator.min.js' charset='utf-8'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/pnotify/jquery.pnotify.min.js'></script>

    <!-- for ajax form submit -->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/ajax_form.js' charset='utf-8'></script>

    <!-- including  js file that includes all that is needed for login -->
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/login.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/register.js'></script>

    <!-- check for flash messages -->
    <?php if(isset($flash_message) && $flash_message!=''){ ?>
        <script>
        $.pnotify({title: 'Oops', text: '<?= $flash_message ?>', opacity: .8, type: 'error'});
        </script>
    <?php } ?>
</body>
<!--</html>-->
