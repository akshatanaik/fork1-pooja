<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Booking Brain</title>
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">


    <!-- include all css and js file -->
    <?= $this->load->view('app/layouts/assets'); ?>



    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/uniform/uniform.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/plugins/stepywizard/jquery.stepy.js'></script>
    <script type='text/javascript' src='<?= INCLUDES ?>app/js/register.js'></script>

</head>
<body data-baseurl="<?php echo base_url(); ?>">
   

        <div class="content" style="margin-left:150px;margin-right: 150px; padding:0;">

            <!-- Main content -->
            <div class="workplace">
               
                <?php if(isset($status) && $status=='error'){ ?>
                        <div id="error"><?= $response ?></div>
                <?php }else{ ?>
                <div style="display:none;" class="alert alert-error" id="error-message-wrapper"></div>
                <?= $this->load->view('app/mini_register') ?>
                <?php } ?>

            </div>
        </div>
    </div>
   
</body>
</html>