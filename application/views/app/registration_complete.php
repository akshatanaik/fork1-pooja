<!DOCTYPE html>

<html lang="en">

<head>        

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <!--[if gt IE 8]>

        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <![endif]-->

    
    <link rel="icon" href="<?= INCLUDES ?>/app/img/bb_favicon.png" type="image/png">
    <title>Registration Complete</title>



    <?= $this->load->view('app/layouts/assets'); ?>

</head>

<body>

        

    <div class="errorPage">        

        <p class="name">Sucess</p>

        <p class="description">Account has been activated successfully</p>        

        <p><button class="btn btn-danger" onClick="document.location.href = '<?= ROOT ?>';">Back to website</button> <button class="btn btn-warning" onClick="document.location.href = '<?= site_url('/login') ?>';">Login</button></p>       

    </div>

    

</body>

</html>

